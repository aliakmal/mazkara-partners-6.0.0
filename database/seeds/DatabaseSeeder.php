<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;



class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		$this->call('ZonesTableSeeder');
		//$this->call('PopulateSettingsTableSeeder');
		//$this->call('reset_morphs');

		// $this->call('UserTableSeeder');
		//$this->call('CategoriesTableSeeder');
		//$this->call('IndiaZonesTableSeeder');
		//$this->call('AssetsFixerTableSeeder');
		//$this->call('BusinessesTableSeeder');
		//$this->call('PhotosTableSeeder');
		//$this->call('HighlightsTableSeeder');
		//$this->call('ServicesTableSeeder');
		//$this->call('TimingsTableSeeder');
		//$this->call('AccountsTableSeeder');
		//$this->call('RolesTableSeeder');

		//$this->call('CounterSeeder');
		//$this->call('Check_insTableSeeder');
		//$this->call('FavoritesTableSeeder');
		//$this->call('PromotionsTableSeeder');
		//$this->call('DealsTableSeeder');
		//$this->call('DiscountsTableSeeder');
		//$this->call('PromosTableSeeder');
		//$this->call('PackagesTableSeeder');
		//$this->call('FollowsTableSeeder');
		//$this->call('AdsTableSeeder');
		//$this->call('CampaignsTableSeeder');
		//$this->call('Ad_zonesTableSeeder');
		//$this->call('Ad_setsTableSeeder');
		//$this->call('GroupsTableSeeder');
		//$this->call('ActivitiesTableSeeder');
		//$this->call('CommentsTableSeeder');
		//$this->call('JobsTableSeeder');
		//$this->call('Special_packagesTableSeeder');
		//$this->call('MerchantsTableSeeder');
		//$this->call('Service_itemsTableSeeder');
		//$this->call('OffersTableSeeder');
		//$this->call('Business_zonesTableSeeder');
		//$this->call('Call_logsTableSeeder');
		//$this->call('KnumbersTableSeeder');
		//$this->call('Knumber_allocationsTableSeeder');
		//$this->call('Virtual_numbersTableSeeder');
		//$this->call('Virtual_number_allocationsTableSeeder');
		//$this->call('Combo_itemsTableSeeder');
		//$this->call('Menu_groupsTableSeeder');
		//$this->call('VouchersTableSeeder');

		//$this->call('PuneOfferTableSeeder');
		//$this->call('SlugsTableSeeder');
		//$this->call('InvoicesTableSeeder');
		//$this->call('PaymentsTableSeeder');
		//$this->call('Invoice_itemsTableSeeder');
		//$this->call('Service_zone_countersTableSeeder');
		//$this->call('Credit_notesTableSeeder');
		//$this->call('PostsTableSeeder');
		//$this->call('BrandsTableSeeder');
		//$this->call('SharesTableSeeder');
	}

}
