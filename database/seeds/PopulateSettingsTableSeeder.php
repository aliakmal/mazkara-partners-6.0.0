<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Models\Zone;
use App\Models\Setting;

class PopulateSettingsTableSeeder extends Seeder
{
    public function run()
    {
      $cities = Zone::cities()->get();
      $settings = array(
                        'call_log_sms_to_user'=>'',
                        'call_log_sms_to_client'=>''
                        );
      foreach($cities as $city){
        foreach($settings as $name=>$default){
          $s = Setting::firstOrCreate(['name'=>$name, 'zone_id'=>$city->id]);
          $s->value = isset($s->value) && !empty($s->value) ? $s->value:$default;
          $s->save();
        }
      }

        // TestDummy::times(20)->create('App\Post');
    }
}
