<?php

use Illuminate\Database\Seeder;

class reset_morphs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $map = ['Answer'=>'App\\Models\\Answer',
              'Ad'=>'App\\Models\\Ad',
              'Post'=>'App\\Models\\Post',
              'Brand'=>'App\\Models\\Brand',
              'Business'=>'App\\Models\\Business',
              'Category'=>'App\\Models\\Category',
              'Deal'=>'App\\Models\\Deal',
              'Discount'=>'App\\Models\\Discount',
              'Group'=>'App\\Models\\Group',
              'Package'=>'App\\Models\\Package',
              'Payment'=>'App\\Models\\Payment',
              'Promo'=>'App\\Models\\Promo',
              'Promotion'=>'App\\Models\\Promotion',
              'Review'=>'App\\Models\\Review',
              'Post'=>'App\\Models\\Post',
              'Selfie'=>'App\\Models\\Selfie',
              'Video'=>'App\\Models\\Video',
              'Photo'=>'App\\Models\\Photo',
              'Call_log'=>'App\\Models\\Call_log',
              'Question'=>'App\\Models\\Question',
              'Service'=>'App\\Models\\Service',
              'Special_package'=>'App\\Models\\Special_package',
              'User'=>'App\\Models\\User',
              'Zone'=>'App\\Models\\Zone'];

      foreach($map as $ii=>$vv){
        DB::table('photos')
              ->where('imageable_type', '=', $vv)
              ->update(array('imageable_type' => $ii));
      }


      foreach($map as $ii=>$vv){
        DB::table('comments')
              ->where('commentable_type', '=', $vv)
              ->update(array('commentable_type' => $ii));
      }

      foreach($map as $ii=>$vv){
        DB::table('favorites')
              ->where('favorable_type', '=', $vv)
              ->update(array('favorable_type' => $ii));
      }
      foreach($map as $ii=>$vv){
        DB::table('counters')
              ->where('countable_type', '=', $vv)
              ->update(array('countable_type' => $ii));
      }


      foreach($map as $ii=>$vv){
        DB::table('shares')
              ->where('sharable_type', '=', $vv)
              ->update(array('sharable_type' => $ii));
      }



    }
}
