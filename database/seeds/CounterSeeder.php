<?php

class CounterSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('timings')->truncate();

    $zones = Zone::all();
    foreach($zones as $zone){
      $zone->business_count = $zone->businesses()->count();
      $zone->save();
    }

    $services = Service::all();
    foreach($services as $s){
      $s->business_count = $s->businesses()->count();
      $s->save();
    }

    $highlights = Highlight::all();
    foreach($highlights as $s){
      $s->business_count = $s->businesses()->count();
      $s->save();
    }

    $categories = Highlight::all();
    foreach($categories as $s){
      $s->business_count = $s->businesses()->count();
      $s->save();
    }

		// Uncomment the below to run the seeder
		// DB::table('timings')->insert($timings);
	}

}
