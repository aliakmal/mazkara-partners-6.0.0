<?php
use Illuminate\Database\Seeder;
use App\Models\Zone;
use App\Models\Highlight;
use App\Models\Service;

class ZonesTableSeeder extends Seeder {

	public function run(){
		// Uncomment the below to wipe the table clean before populating



    $melb = Zone::firstOrCreate(['name'=>'Melbourne']);
    $syd = Zone::firstOrCreate(['name'=>'Sydney']);
    $categories = Category::lists('id', 'id')->all();
    $services = Service::lists('id', 'id')->all();
    $highlights = Highlight::lists('id', 'id')->all();

    foreach($highlights as $ii=>$highlight){
      $highlights[$ii] = ['state'=>'active'];
    }

    foreach($services as $ii=>$s){
      $services[$ii] = ['state'=>'active'];
    }

    foreach($categories as $ii=>$s){
      $categories[$ii] = ['state'=>'active'];
    }

    $melb->services()->sync($services);
    $melb->categories()->sync($categories);
    $melb->highlights()->sync($highlights);

    $syd->services()->sync($services);
    $syd->categories()->sync($categories);
    $syd->highlights()->sync($highlights);

    $melb_zones = [ 'Eastern Suburb' =>['Ashburton', 'Balwyn', 'Balwyn North', 'Bayswater', 'Bayswater North', 'Beaconsfield', 'Beaconsfield Upper', 'Berwick', 'Blackburn', 'Boronia', 'Box Hill', 'Bulleen', 'Burwood', 'Burwood East', 'Camberwell', 'Canterbury', 'Chirnside Park', 'Clayton', 'Clyde', 'Cranbourne', 'Croydon', 'Dandenong', 'Doncaster', 'Doncaster East', 'Donvale', 'Eltham', 'Endeavour Hills', 'Ferntree Gully', 'Forest Hill & Vermont', 'Glen Iris', 'Glen Waverley', 'Hampton Park', 'Hawthorn', 'Heathmont', 'Kangaroo Ground', 'Kew', 'Keysborough', 'Knoxfield', 'Lilydale', 'Lyndhurst', 'Lysterfield', 'Mitcham', 'Mooroolbark & Kilsyth', 'Mount Waverley', 'Mulgrave', 'Narre Warren', 'Noble Park', 'Nunawading', 'Ringwood', 'Rowville', 'Scoresby', 'Springvale', 'Surrey Hills', 'Templestowe', 'Upwey', 'Wantirna', 'Warrandyte', 'Wheelers Hill'], 
                    'Northern Suburb'=>['Airport West','Ascot Vale','Broadmeadows','Brunswick','Brunswick West','Bundoora','Campbellfield','Coburg','Coolaroo','Craigieburn','Diamond Creek','Eaglemont','Epping','Essendon','Fairfield','Fawkner','Flemington','Gladstone Park','Glenroy & Hadfield','Greensborough','Greenvale','Heidelberg','Heidelberg West','Hurstbridge','Ivanhoe','Keilor','Keilor East','Kensington','Kingsbury','Lalor','Macleod','Meadow Heights','Mernda & Doreen','Mill Park','Montmorency','Moonee Ponds','Niddrie','Northcote','Pascoe Vale','Preston','Reservoir','Rosanna','Roxburgh Park','Somerton','South Morang','Strathmore','Sunbury','Thomastown','Thornbury','Tullamarine','Viewbank','Watsonia','West Meadows','Wollert'],
                    'Southern Suburb'=>['Armadale','Aspendale & Chelsea','Beaumaris','Bentleigh','Black Rock','Brighton','Carnegie','Carrum Downs','Caulfield','Chadstone','Cheltenham','Clarinda','Dingley Village & Braeside','Elsternwick','Elwood','Frankston','Glen Huntly','Hampton','Hughesdale & Murrumbeena','Malvern','Malvern East','Mentone','Moorabbin & Highett','Mordialloc','Oakleigh','Ormond & McKinnon','Parkdale','Patterson Lakes & Carrum','Sandringham','Seaford'],
                    'CBD'=>["A'Beckett Street","ACDC Lane","Alfred Place","Artemis Lane","Bank Place","Bennetts Lane","Bligh Place","Block Place","Bond Street","Bourke Street","Brights Place","Carson Place","Celestial Avenue","Centre Place","Cohen Place","Collins Street","Coromandel Place","Corrs Lane","Croft Alley","Crossley Street","Degraves Street","Drewery Lane","Driver Lane","Duckboard","Elizabeth Street","Equitable Place","Exhibition Street","Exploration Lane","Federation Square","Flanagan Lane","Flinders Lane","Flinders Street","Francis Street","Franklin Street","Fulham Place","Gallagher Place","Globe Alley","Goldie Place","Goldsbrough Lane","Guests Lane","Guildford Lane","Hardware Lane","Hardware Street","Healeys Lane","Heffernan Lane","Highlander Lane","Hosier Lane","Howey Place","Katherine Place","King Street","La Trobe Street","Little Bourke Street","Little Collins Street","Little La Trobe Street","Little Lonsdale Street","Liverpool Street","Lonsdale Street","Madame Brussels Lane","Malthouse Lane","Manchester Lane","Market Lane","Market Street","McIlwraith Place","McKillop Street","Melbourne Place","Menzies Alley","Meyers Place","Mornane Place","Oliver Lane","Pelham Street","Presgrave Place","Queen Street","Rainbow Alley","Rankins Lane","Rebecca Walk","Ridgway Place","Rose Lane","Royal Arcade","Royal Lane","Russell Place","Russell Street","Somerset Place","Southern Cross Lane","Spencer Street","Spring Street","St Collins Lane","Stewart Street","Sutherland Street","Swanston Street","Tattersalls Lane","Therry Street","Victoria Street","Waratah Place","Warburton Lane","Whitehart Lane","William Street","Wills Street"],    
                    'Inner South'=>['Albert Park','Middle Park','Port Melbourne','Prahran','South Melbourne','South Yarra','St Kilda','St Kilda East','Toorak','Windsor'],
                    'Western Suburb'=>['Bacchus Marsh','Burnside Heights','Cairnlea','Caroline Springs','Deer Park','Derrimut','Eynesbury','Hillside','Hoppers Crossing','Kealba','Keilor Downs','Kings Park','Laverton North','Melton','Plumpton','Point Cook','Ravenhall','Seabrook','St Albans','Sunshine','Sunshine North','Sunshine West','Sydenham','Tarneit','Taylors Hill','Taylors Lakes','Werribee'],
                    'Inner West'=>['Aberfeldie','Altona','Altona Meadows','Altona North','Avondale Heights','Braybrook','Brooklyn','Footscray','Kingsville','Maidstone','Maribyrnong','Newport','Seddon','South Kingsville','Spotswood','Tottenham','West Footscray','West Melbourne','Williamstown','Yarraville'],
                    'Inner North'=>['Carlton','Carlton North','Clifton Hill','Fitzroy','Fitzroy North','North Melbourne','Parkville'],
                    'Inner East'=>['Abbotsford','Collingwood','East Melbourne','Richmond'],
                    'Yarra Valley and Dandenong Ranges'=>['Belgrave','Cockatoo','Emerald','Gembrook','Healesville','Kallista','Monbulk','Montrose','Mount Dandenong','Mount Evelyn','Officer','Olinda','Pakenham','Sassafras','Seville','Warburton','Yarra Glen'],
                    'City Centre'=>['Docklands','South Wharf','Southbank'],
                    'Miscelleneous'=>['Beveridge','Indented Head','Upper Plenty','Mount Eliza','Portarlington','Little River, Victoria','Wallan East','Wallan','St Leonards','Baxter, Victoria','Mornington Peninsula','Riddells Creek','Bellarine','Avalon, Victoria','Gisborne','Somerville, Victoria','Pearcedale','Mount Martha']];


    foreach($melb_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$melb->id]);
      $p->parent_id = $melb->id;//
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//append($c);
        $c->city_id = $melb->id;
        $c->country_code = 'AU';
        $c->state = 'inactive';
        $c->save();

      }
      $p->city_id = $melb->id;
      $p->state = 'inactive';
      $p->country_code = 'AU';
      $p->save();
    }
    $melb->country_code = 'AU';
    $melb->state = 'inactive';    
    $melb->save();




$syd_zones = ['City of Sydney'=>['Alexandria','Barangaroo','CBD','Chinatown','Chippendale','Circular Quay','Darling Harbour','Darlinghurst','Darlington','Glebe','Potts Point','Pyrmont','Redfern','Surry Hills','The Rocks','Ultimo','Waterloo','Woolloomooloo','Zetland'],
'Inner West'=>['Abbotsford','Annandale','Ashfield','Balmain','Breakfast Point','Burwood','Camperdown','Canada Bay','Canterbury','Concord','Croydon','Drummoyne','Dulwich Hill','Enmore','Erskineville','Five Dock','Haberfield','Homebush','Leichhardt','Marrickville','Newington','Newtown','North Strathfield','Petersham','Rhodes','Rozelle','St. Peters','Stanmore','Strathfield','Summer Hill','Sydney Olympic Park','Tempe'],
'South West'=>['Bankstown','Bonnyrigg','Cabramatta','Camden','Campbelltown','Canley Heights','Canley Vale','Casula','Chester Hill','Fairfield','Green Valley','Holsworthy','Ingleburn','Lakemba','Liverpool','Macquarie Fields','Minto','Moorebank','Narellan','Padstow','Panania','Punchbowl','Revesby','Seven Hills','Smithfield','Wetherill Park'],
'Western Suburbs'=>['Auburn','Berala','Chullora','Ermington','Erskine Park','Girraween','Granville','Greenacre','Guildford','Lidcombe','Merrylands','Mount Druitt','North Parramatta','North Rocks','Northmead','Oatlands','Old Toongabbie','Parramatta','Penrith','Quakers Hill','Riverstone','Rydalmere','Silverwater','St Marys','Woodpark'],
'Lower North Shore'=>['Artarmon','Cammeray','Chatswood','Cremorne','Crows Nest','Kirribilli','Lane Cove','McMahons Point','Milsons Point','Mosman','Naremburn','Neutral Bay','North Sydney','Northbridge','St Leonards','Willoughby','Wollstonecraft'],
'Eastern Suburbs'=>['Bellevue Hill','Bondi','Bondi Beach','Bondi Junction','Bronte','Clovelly','Coogee','Double Bay','Edgecliff','Maroubra','Moore Park','Paddington','Randwick','Rose Bay','Rushcutters Bay','Vaucluse','Watsons Bay','Waverley','Woollahra'],
'Southern Sydney'=>['Arncliffe','Belfield','Belmore','Beverley Park','Beverly Hills','Bexley','Blakehurst','Brighton-Le-Sands','Campsie','Canterbury','Earlwood','Hurlstone Park','Hurstville','Kingsgrove','Kogarah','Kyeemagh','Kyle Bay','Lugarno','Monterey','Oatley','Peakhurst','Penshurst','Ramsgate','Riverwood','Rockdale','Roselands','Sans Souci'],
'Northern Beaches'=>['Avalon','Balgowlah','Brookvale','Collaroy','Curl Curl','Dee Why','Frenchs Forest','Freshwater','Manly','Manly Vale','Mona Vale','Narrabeen','Newport','Palm Beach','Pittwater','Seaforth','Warriewood','Warringah'],
'Northern Suburbs'=>['Beecroft','Eastwood','Epping','Gladesville','Hunters Hill','Macquarie Park','Marsfield','Meadowbank','North Ryde','Pennant Hills','Ryde','West Ryde'],
'Sutherland Shire'=>['Bangor','Caringbah','Como','Cronulla','Engadine','Gymea','Heathcote','Illawong','Jannali','Kareela','Kirrawee','Kurnell','Menai','Miranda','Oyster Bay','Sutherland','Sylvania','Taren Point','Wooloware','Woronora','Yarrawarrah'],
'South East Sydney'=>['Botany Bay','Eastgardens','Eastlakes','Kensington','Kingsford','La Perouse','Little Bay','Malabar','Mascot','Matraville','Rosebery','Sydney Airport'],
'Upper North Shore'=>['Berowra','Brooklyn','Gordon','Hornsby','Killara','Lindfield','Pymble','Roseville','St Ives','Terrey Hills','Turramurra','Wahroonga'],
'The Hills'=>['Baulkham Hills','Bella Vista','Castle Hill','Dural','Kellyville','North Rocks','Rouse Hill'],
'North West Sydney'=>['Blacktown','Richmond','The Hills','Windsor'],

'Blue Mountains'=>['Bilpin','Blaxland','Glenbrook','Jenolan','Katoomba','Leura','Lithgow','Little Hartley','Medlow Bath','Mount Victoria','Oberon','Springwood'],
'Miscelleneous'=>['Horsfield Bay','Pearl Beach','Umina Beach','Koolewong','Woy Woy','Somersby','Blackwall','Ettalong Beach','West Gosford','Hardys Bay','Killcare','Davistown','Gosford','Empire Bay','East Gosford','North Gosford','Box Head','Green Point','Peats Ridge','Wyoming, New South Wales','Kincumber','Erina','Erina Heights','St Albans, New South Wales','Ourimbah','Avoca Beach','Terrigal','Wamberal','Shelly Beach','Forresters Beach','Tumbi Umbi','Berkeley Vale','Yarramalong','Bateau Bay','Killarney Vale','Tuggerah','Long Jetty','Cedar Brush Creek','Toowoon Bay','Wyong','Blue Bay','The Entrance','Kurmond','The Entrance North','Kurrajong','Magenta','Kurrajong Heights','Lake Haven','Charmhaven','Toukley','East Toukley']];



    foreach($syd_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$syd->id]);
      $p->parent_id = $syd->id;//
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//append($c);
        $c->city_id = $syd->id;
        $c->country_code = 'AU';
        $c->state = 'inactive';        
        $c->save();

      }
      $p->city_id = $syd->id;
      $p->country_code = 'AU';
      $p->state = 'inactive';
      $p->save();
    }
    $syd->country_code = 'AU';
    $syd->state = 'inactive';
    $syd->save();


return;








    $adb = Zone::firstOrCreate(['name'=>'Abu Dhabi']);
    $shj = Zone::firstOrCreate(['name'=>'Sharjah']);
    $categories = Category::lists('id', 'id')->all();
    $services = Service::lists('id', 'id')->all();
    $highlights = Highlight::lists('id', 'id')->all();

    foreach($highlights as $ii=>$highlight){
      $highlights[$ii] = ['state'=>'active'];
    }

    foreach($services as $ii=>$s){
      $services[$ii] = ['state'=>'active'];
    }

    foreach($categories as $ii=>$s){
      $categories[$ii] = ['state'=>'active'];
    }

    $adb->services()->sync($services);
    $adb->categories()->sync($categories);
    $adb->highlights()->sync($highlights);

    $shj->services()->sync($services);
    $shj->categories()->sync($categories);
    $shj->highlights()->sync($highlights);


$bangalore_zones = ['South Bangalore'=>['Koramangala','BTM','Jayanagar','Electronic City','JP Nagar',
                    'Bannerghatta Road','Banashankari','HSR','Basavanagudi','Sarjapur Road',
                    'Bellandur','Kumaraswamy Layout','Kanakapura Road','Uttarahalli'],

'East Bangalore'=>['Whitefield','Indiranagar','Marathahalli','Old Airport Road','Brookefield',
                  'Domlur','KR Puram','Kaggadasapura','Thippasandra','Old Madras Road','CV Raman Nagar',
                  'Jeevan Bhima Nagar','Varthur Main Road, Whitefield'],

'North Bangalore'=>['Yelahanka','RT Nagar','Banaswadi','Frazer Town','New BEL Road','Kalyan Nagar',
                    'Kammanahalli','Nagawara','Jalahalli','Rammurthy Nagar','Sahakara Nagar','Hennur',
                    'Hebbal','Sanjay Nagar','Jakkur','International Airport','HBR Layout'],

'West Bangalore'=>['Malleshwaram','Rajajinagar','Majestic','Yeshwantpur','Rajarajeshwari Nagar',
                  'Vijay Nagar','Seshadripuram','Basaveshwara Nagar','Mysore Road','Race Course Road',
                  'Sadashiv Nagar','Nagarbhavi','Kengeri','City Market','Magadi Road','Sankey Road',
                  'Peenya'],

'Central Bangalore'=>['Brigade Road','Ulsoor','MG Road','Residency Road','Shivajinagar','Cunningham Road',
                      'Lavelle Road','Richmond Road','Church Street','Vasanth Nagar','Commercial Street',
                      'Shanti Nagar','Wilson Garden','St. Marks Road','Richmond Town','Infantry Road'],
];

    $bng = Zone::firstOrCreate(['name'=>'Bangalore', 'parent_id'=>null]);

    foreach($bangalore_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$bng->id]);
      $p->parent_id = $bng->id;//

      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//append($c);
        $c->city_id = $bng->id;
        $c->state = 'inactive';
        $c->country_code = 'IN';
        $c->save();

      }
      $p->state = 'inactive';
      $p->city_id = $bng->id;
      $p->country_code = 'IN';
      $p->save();
    }
    $bng->country_code = 'IN';
    $bng->save();
    $bng->services()->sync($services);
    $bng->categories()->sync($categories);
    $bng->highlights()->sync($highlights);


    return;




    $sharjah_zones = ['Buhaira Corniche'=>['Al Taawun','Al Khan','Al Majaz'],
                      'Suburbs'=>['Wasit Suburb','Mughaidir Suburb','Al Riqa Suburb','Halwan Suburb'],
                      'Al Heera Suburb'=>['Sharqan','Al Qadisia','Al Rifaah','Al Mirgab'],
                      'Al Sharq'=>['Bu Tina','Al Nasseriya','Maysaloon','Al Qulayaa'],
                      'Al Qasimia'=>['Abu Shagara','Al Mahatah','Al Nud','Al Soor'],
                      'Nahda and Around'=>['Al Nahda','Industrial Area'],
                      'Rolla'=>['Al Mujarrah','Al Nabaah','Al Shuwaihean','Al Mareija','Al Ghuwair'],
                      'Outer Sharjah'=>['University City','Saif Zone','Muwailih Commercial']];

    $shj = Zone::firstOrCreate(['name'=>'Sharjah']);

    foreach($sharjah_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$shj->id]);
      $p->parent_id = $shj->id;//
      //$shj->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//append($c);
        $c->city_id = $shj->id;
        $c->country_code = 'AE';
        $c->save();

      }
      $p->city_id = $shj->id;
      $p->country_code = 'AE';
      $p->save();
    }
    $shj->country_code = 'AE';
    $shj->save();

    $abdh_zones = ['Abu Dhabi City'=>['Al Mushrif','Eastern Mangrove',
                                      'Al Madina Al Riyadiya','Khalifa Park Area',
                                      'Muroor/Muroor Road','Al Maqta','Khalifa City',
                                      'Masdar City','Yas Island','Al Raha','Al Shahama',
                                      'Al Dhafrah','Al Nahyan','Al Karamah','Al Wahda',
                                      'Marina Village','Al Khubeirah','Al Khalidiya',
                                      'Al Bateen','Baniyas','Al Mafraq','Around Abu Dhabi',
                                      'Tourist Club Area','Al Markaziya','Najda','Madinat Zayed',
                                      'Istiqlal','Al Mina','Al Maryah Island','Al Reem Island',
                                      'Mussafah','Saadiyat Island','Mohammed Bin Zayed City',
                                      'Al Ras Al Akhdar','Embassies District']];

    $adb = Zone::firstOrCreate(['name'=>'Abu Dhabi']);

    foreach($abdh_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$adb->id]);
      $p->parent_id = $adb->id;//
      //$adb->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//$p->append($c);
        $c->city_id = $adb->id;
        $c->country_code = 'AE';
        $c->save();

      }
      $p->city_id = $adb->id;
      $p->country_code = 'AE';
      $p->save();
    }
    $adb->country_code = 'AE';
    $adb->save();


    $displayable_cities = [1, 88];
    \DB::statement('UPDATE zones set state = "inactive"');

    \DB::statement('UPDATE zones set state = "active" where city_id in ('.join(',', $displayable_cities).')');
    \DB::statement('UPDATE zones set state = "active" where id in ('.join(',', $displayable_cities).')');


    return;

    $zn = Zone::firstOrCreate(['name'=>'Dubai']);

    DB::table('businesses')
            ->where('zone_id', 0)
            ->update(array('city_id' => $zn->id));

            return;
            //exit;
$dubai_zones = ['Barsha' => ['Al Quoz', 'Arabian Ranches', 'Barsha 1', 'Barsha 2', 'Barsha 3', 'Barsha South', 'Dubai Motor City', 'Mall of the Emirates (MOE)'],
'Bur Dubai' => ['Al Karama', 'Burjuman', 'Khalid Bin Waleed', 'Mankhool', 'Meena Bazaar', 'Oud Metha', 'Satwa'],
'Deira' => ['Al Garhoud', 'Al Muteena', 'Al Ras', 'Al Rigga', 'Baniyas', 'Deira City Centre (DCC)', 'Festival City', 'Naif', 'Rashidiya'],
'Downtown' => ['Business Bay', 'DIFC', 'Dubai Mall Area', 'Emaar Boulevard', 'World Trade Centre Area (WTC)'],
'Jebel Ali' => ['Dubai Investment Park (DIP)', 'Green Community Village', 'JAFZA', 'Jebel Ali Industrial Area'],
'Jumeirah' => ['Al Safa', 'Jumeirah 1', 'Jumeirah 2', 'Jumeirah 3', 'Jumeirah Beach Road', 'Madinat Jumeirah', 'Umm Suqeim'],
'New Dubai' => ['Discovery Gardens', 'Dubai Marina', 'Emirates Hills', 'Ibn Batuta', 'Jumeirah Beach Residence (JBR)', 'Jumeirah Islands', 'Jumeirah Lakes Towers (JLT)', 'Palm Jumeirah', 'The Gardens', 'The Greens'],
'Outer Dubai' => ['Al Nahda', 'Al Qusais', 'Dubai Academic CIty', 'Dubai Land', 'Dubai Silicon Oasis', 'International City', 'Meydan', 'Mirdiff', 'Mirdiff City Centre', 'Nad Al Sheba', 'Ras Al Khor'],
'TECOM' => ['Dubai Internet City', 'Dubai Media City', 'Knowledge Village']];

    $dxb = Zone::firstOrCreate(['name'=>'Dubai']);

    foreach($dubai_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$dxb->id]);
      $dxb->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $p->append($c);
        $c->country_code = 'AE';
        $c->save();

      }
      $p->country_code = 'AE';
      $p->save();
    }
    $dxb->country_code = 'AE';
    $dxb->save();

//    $in_zones = ['Pune',
//                 'Mumbai', 
//                 'Delhi'];
//
//    foreach($in_zones as $name){
//      $ind = Zone::firstOrCreate(['name'=>$name]);
//      $ind->state = 'inactive';
//      $ind->country_code = 'IN';
//      $ind->save();
//    }

    $zones = Zone::all();
    foreach($zones as $zone){
      if(!$zone->isCity()){
        $zone->city_id = $zone->ancestors()->first()->id;
        $zone->save();
      }
    }

		// Uncomment the below to run the seeder
		// DB::table('zones')->insert($zones);
	}

}
