<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu_groups', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('business_id')->unsigned()->index();
			$table->integer('sort');
			$table->timestamps();

			$table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu_groups');
	}

}
