<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgainTheForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function($table){
            $table->integer('chain_id')->nullable()->unsigned()->change();
            $table->integer('zone_id')->nullable()->unsigned()->change();
            $table->integer('city_id')->nullable()->unsigned()->change();
        });

        DB::statement('update businesses set chain_id = NULL    where chain_id = 0');
        DB::statement('update businesses set zone_id = NULL     where zone_id = 0');
        DB::statement('update businesses set city_id = NULL     where city_id = 0');
        DB::statement('update businesses set chain_id = null where chain_id not in (select id from groups)');
        
        Schema::table('businesses', function($table){
            $table->foreign('chain_id')->references('id')->on('groups')->onDelete('set null');
            //$table->foreign('zone_id')->references('id')->on('zones')->onDelete('set null');
            //$table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
