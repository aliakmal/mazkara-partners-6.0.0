<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComboItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('combo_items', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('business_id')->index();
			$table->string('name');
			$table->text('description');
			$table->float('cost');
			$table->integer('duration');
			$table->string('cost_type');
			$table->string('duration_type');
			$table->string('state');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('combo_items');
	}

}
