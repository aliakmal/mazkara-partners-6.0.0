<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotQuestionServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('question_service', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('question_id')->unsigned()->index();
			$table->integer('service_id')->unsigned()->index();
			$table->foreign('question_id')->references('id')->on('posts')->onDelete('cascade');
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('question_service');
	}

}
