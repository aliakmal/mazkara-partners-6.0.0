<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreIndexesToBusinesses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('businesses', function($table)
		{
			$table->index('name');
			$table->index('geolocation_longitude');
			$table->index('geolocation_latitude');
			$table->index('zone_cache');
			$table->index('active_offers_count');
			$table->index('type');
			$table->index('total_ratings_count');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
