<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('location');
            $table->string('nationality');
            $table->integer('uploaded_by')->unsigned()->index();
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('cascade');
            $table->integer('city_id');
            $table->timestamps();
        });

        Schema::create('business_resume', function (Blueprint $table) {
            $table->integer('business_id')->unsigned()->index();
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
            $table->integer('resume_id')->unsigned()->index();
            $table->foreign('resume_id')->references('id')->on('resumes')->onDelete('cascade');
            $table->primary(['business_id', 'resume_id']);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
