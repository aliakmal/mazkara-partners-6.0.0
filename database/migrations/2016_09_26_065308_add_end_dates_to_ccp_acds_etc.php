<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndDatesToCcpAcdsEtc extends Migration
{
    public function up()
    {
        Schema::table('virtual_number_allocations', function($table)
        {
            $table->date('valid_until');
        });
        
        Schema::table('groups', function($table)
        {
            $table->date('valid_until');
        });

        Schema::table('businesses', function($table)
        {
            $table->date('facebook_like_box_valid_until');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
