<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDealsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deals', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->text('caption');
			$table->text('description');
			$table->text('fine_print');
			$table->integer('offer_amount');
			$table->integer('original_amount');
			$table->date('starts');
			$table->date('ends');
			$table->string('type');
			$table->integer('business_id');
			$table->string('status');

			$table->timestamps();
		});

		Schema::dropIfExists('promotions');
		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deals');
	}

}
