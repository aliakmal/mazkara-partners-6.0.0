<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToAssociationsBusiness extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('business_category', function(Blueprint $table) {
			$table->index(['category_id', 'business_id']);
		});
		Schema::table('business_highlight', function(Blueprint $table) {
			$table->index(['highlight_id', 'business_id']);
		});
		Schema::table('business_service', function(Blueprint $table) {
			$table->index(['service_id', 'business_id']);
		});
		Schema::table('favorites', function(Blueprint $table) {
			$table->index(['favorable_id', 'favorable_type']);
		});

		Schema::table('check_ins', function(Blueprint $table) {
			$table->index(['business_id', 'user_id']);
		});
		Schema::table('reviews', function(Blueprint $table) {
			$table->index(['business_id', 'user_id']);
		});

		Schema::table('photos', function(Blueprint $table) {
			$table->index(['imageable_type', 'imageable_id']);
			$table->index('imageable_type');
			$table->index('type');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
