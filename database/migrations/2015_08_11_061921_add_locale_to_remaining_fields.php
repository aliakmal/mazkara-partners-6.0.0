<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocaleToRemainingFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns', function($table)
		{
    	$table->integer('city_id');
		});

		Schema::table('ads', function($table)
		{
    	$table->integer('city_id');
		});
		
		Schema::table('ad_sets', function($table)
		{
    	$table->integer('city_id');
		});
		
		Schema::table('business_zones', function($table)
		{
    	$table->integer('city_id');
		});
		
		Schema::table('merchants', function($table)
		{
    	$table->integer('city_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
