<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeFieldsNullableInCallLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		DB::statement('ALTER TABLE `call_logs` MODIFY `agent_list` VARCHAR(255) NULL;');	
		DB::statement('ALTER TABLE `call_logs` MODIFY `call_connected_to` VARCHAR(255) NULL;');	
		DB::statement('ALTER TABLE `call_logs` MODIFY `call_transfer_status` VARCHAR(255) NULL;');	
		DB::statement('ALTER TABLE `call_logs` MODIFY `call_transfer_duration` VARCHAR(255) NULL;');	
		DB::statement('ALTER TABLE `call_logs` MODIFY `caller_circle` VARCHAR(255) NULL;');	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
