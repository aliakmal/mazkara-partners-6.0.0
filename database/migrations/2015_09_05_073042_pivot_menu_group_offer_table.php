<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotMenuGroupOfferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu_group_offer', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('menu_group_id')->unsigned()->index();
			$table->integer('offer_id')->unsigned()->index();
			$table->foreign('menu_group_id')->references('id')->on('menu_groups')->onDelete('cascade');
			$table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu_group_offer');
	}

}
