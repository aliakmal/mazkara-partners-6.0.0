<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFixableCoordiantesToBusinesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->float('legacy_geolocation_longitude', 10,6)->default(0);
            $table->float('legacy_geolocation_latitude', 10,6)->default(0);
        });

        $sql = "update businesses set legacy_geolocation_latitude = geolocation_latitude, legacy_geolocation_longitude = geolocation_longitude";
        DB::statement($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
