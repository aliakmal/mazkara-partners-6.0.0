<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceZoneCountersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_zone_counters', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('zone_id')->unsigned()->index();
			$table->integer('service_id')->unsigned()->index();
			$table->integer('business_active_count');
			$table->integer('business_count');

			$table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_zone_counters');
	}

}
