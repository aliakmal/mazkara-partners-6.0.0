<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDjangoApiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::dropIfExists('django_admin_log');

      Schema::dropIfExists('auth_user_user_permissions');

      Schema::dropIfExists('auth_user_groups');

      Schema::dropIfExists('auth_group_permissions');

      Schema::dropIfExists('auth_permission');

      Schema::dropIfExists('django_session');

      Schema::dropIfExists('auth_group');

      Schema::dropIfExists('django_content_type');

      Schema::dropIfExists('api_business_service');

      Schema::dropIfExists('api_zones');

      Schema::dropIfExists('auth_user');

      Schema::create('auth_user', function(Blueprint $table) {
        $table->increments('id');
        $table->string('password');
        $table->datetime('last_login');
        $table->tinyInteger('is_superuser')->unique();
        $table->string('username');
        $table->string('first_name');
        $table->string('last_name');
        $table->string('email');
        $table->tinyInteger('is_staff');
        $table->tinyInteger('is_active');
        $table->datetime('date_joined');
      });

      Schema::create('api_zones', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
      });

      Schema::create('api_business_service', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('business_id');
        $table->integer('service_id');
        $table->integer('starting_price');
      });

      Schema::create('django_content_type', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('app_label');
        $table->string('model');
      });

      Schema::create('auth_group', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name')->unique();
      });

      Schema::create('django_session', function(Blueprint $table) {
        $table->string('session_key')->primary();
        $table->text('session_data');
        $table->datetime('expire_date');
      });
      Schema::create('auth_permission', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->integer('content_type_id')->unsigned()->index();
        $table->string('codename');
        $table->foreign('content_type_id')->references('id')->on('django_content_type')->onDelete('cascade');
      });

      Schema::create('auth_group_permissions', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('group_id')->unsigned()->index();
        $table->integer('permission_id')->unsigned()->index();
        $table->foreign('group_id')->references('id')->on('auth_group')->onDelete('cascade');
        $table->foreign('permission_id')->references('id')->on('auth_permission')->onDelete('cascade');

      });

      Schema::create('auth_user_groups', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('group_id')->unsigned()->index();
        $table->integer('user_id')->unsigned()->index();
        $table->foreign('group_id')->references('id')->on('auth_group')->onDelete('cascade');
        $table->foreign('user_id')->references('id')->on('auth_user')->onDelete('cascade');
      });


      Schema::create('auth_user_user_permissions', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('permission_id')->unsigned()->index();
        $table->integer('user_id')->unsigned()->index();
        $table->foreign('permission_id')->references('id')->on('auth_permission')->onDelete('cascade');
        $table->foreign('user_id')->references('id')->on('auth_user')->onDelete('cascade');
      });

      Schema::create('django_admin_log', function(Blueprint $table) {
        $table->increments('id');
        $table->datetime('action_time');
        $table->longText('object_id');
        $table->string('object_repr');
        $table->integer('action_flag');
        $table->longText('change_message');

        $table->integer('user_id')->unsigned()->index();
        $table->integer('content_type_id')->unsigned()->index();
        $table->foreign('user_id')->references('id')->on('auth_user')->onDelete('cascade');

        $table->foreign('content_type_id')->references('id')->on('django_content_type')->onDelete('cascade');

      });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
