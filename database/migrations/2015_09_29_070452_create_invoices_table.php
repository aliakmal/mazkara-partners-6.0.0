<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->integer('merchant_id')->unsigned()->index();
			$table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
			$table->datetime('dated');
			$table->string('type')->default('performa')->index();
			$table->string('currency')->default('AED');
			$table->float('amount');
			$table->string('state')->default('pending')->index();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
