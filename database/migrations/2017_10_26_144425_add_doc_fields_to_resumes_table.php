<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDocFieldsToResumesTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resumes', function(Blueprint $table) {

            $table->string('doc_file_name')->nullable();
            $table->integer('doc_file_size')->nullable()->after('doc_file_name');
            $table->string('doc_content_type')->nullable()->after('doc_file_size');
            $table->timestamp('doc_updated_at')->nullable()->after('doc_content_type');

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resumes', function(Blueprint $table) {

            $table->dropColumn('doc_file_name');
            $table->dropColumn('doc_file_size');
            $table->dropColumn('doc_content_type');
            $table->dropColumn('doc_updated_at');

        });
    }

}