<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeLikesFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function($table){
            $table->integer('number_of_likes')->default(0)->nullable()->change();
            $table->integer('number_of_comments')->default(0)->nullable()->change();
            $table->integer('number_of_shares')->default(0)->nullable()->change();
        });

        Schema::table('reviews', function($table){
            $table->integer('number_of_likes')->default(0)->nullable()->change();
            $table->integer('number_of_comments')->default(0)->nullable()->change();
            $table->integer('number_of_shares')->default(0)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
