<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotAnswerQuestionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('answer_question', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('answer_id')->unsigned()->index();
			$table->integer('question_id')->unsigned()->index();
			$table->foreign('answer_id')->references('id')->on('posts')->onDelete('cascade');
			$table->foreign('question_id')->references('id')->on('posts')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('answer_question');
	}

}
