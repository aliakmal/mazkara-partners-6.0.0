<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotComboItemServiceItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('combo_item_service_item', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('combo_item_id')->unsigned()->index();
			$table->integer('service_item_id')->unsigned()->index();
			$table->foreign('combo_item_id')->references('id')->on('combo_items')->onDelete('cascade');
			$table->foreign('service_item_id')->references('id')->on('service_items')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('combo_item_service_item');
	}

}
