<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPplFlagToAllocationsAndCallLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('call_logs', function($table){
			$table->integer('is_ppl')->default(0);
		});

		Schema::table('virtual_number_allocations', function($table){
			$table->integer('is_ppl')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
