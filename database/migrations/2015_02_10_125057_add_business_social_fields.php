<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessSocialFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('businesses', function($table)
		{
    	$table->string('facebook')->nullable();
    	$table->string('twitter')->nullable();
    	$table->string('instagram')->nullable();
    	$table->string('google')->nullable();
    	$table->integer('chain_id')->unsigned()->default(0);
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
