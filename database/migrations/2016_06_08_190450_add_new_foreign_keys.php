<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

/*        
        Schema::table('accounts', function($table){
            $table->integer('user_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['user_id']);
        });
        
        Schema::table('accounts', function($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::table('activities', function($table){
            $table->integer('user_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['user_id']);
        });

        Schema::table('activities', function($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });


        Schema::table('ad_sets', function($table){
            $table->integer('business_zone_id')->nullable()->unsigned()->change();
            $table->integer('category_id')->nullable()->unsigned()->change();
            $table->integer('city_id')->nullable()->unsigned()->change();

//            $table->dropForeign(['business_zone_id']);
//            $table->dropForeign(['category_id']);
//            $table->dropForeign(['city_id']);
        });

        Schema::table('ad_sets', function($table){
            $table->foreign('business_zone_id')->references('id')->on('business_zones')->onDelete('set null');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });
*/
        Schema::table('business_zones', function($table){
            $table->integer('city_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['city_id']);
        });

        Schema::table('business_zones', function($table){
            $table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });

/*
        Schema::table('ads', function($table){
            //$table->integer('ad_set_id')->unsigned()->change();
            $table->integer('city_id')->nullable()->unsigned()->change();
//            //$table->dropForeign(['city_id']);
        });

        Schema::table('ads', function($table){
            //$table->foreign('ad_set_id')->references('id')->on('ad_sets')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });

        Schema::table('businesses', function($table){
            $table->integer('chain_id')->nullable()->unsigned()->change();
            $table->integer('zone_id')->nullable()->unsigned()->change();
            $table->integer('city_id')->nullable()->unsigned()->change();
        });

        DB::statement('update businesses set chain_id = NULL    where chain_id = 0');
        DB::statement('update businesses set zone_id = NULL     where zone_id = 0');
        DB::statement('update businesses set city_id = NULL     where city_id = 0');

        Schema::table('businesses', function($table){
            //$table->foreign('chain_id')->references('id')->on('groups')->onDelete('set null');
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });
*/

        Schema::table('campaigns', function($table){
            $table->integer('merchant_id')->nullable()->unsigned()->change();
            $table->integer('city_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['city_id']);
//            $table->dropForeign(['merchant_id']);
        });

        Schema::table('campaigns', function($table){
            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });

        Schema::table('comments', function($table){
            $table->integer('user_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['user_id']);
        });

        Schema::table('comments', function($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::table('favorites', function($table){
            $table->integer('user_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['user_id']);
        });
        Schema::table('favorites', function($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });


        Schema::table('offers', function($table){
            $table->integer('business_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['business_id']);
        });

        Schema::table('offers', function($table){
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('set null');
        });

        Schema::table('posts', function($table){
            $table->integer('city_id')->nullable()->unsigned()->change();
//            $table->dropForeign(['city_id']);
        });

        Schema::table('posts', function($table){
            $table->foreign('city_id')->references('id')->on('zones')->onDelete('set null');
        });

/*        Schema::table('reviews', function($table){
            $table->integer('business_id')->nullable()->unsigned()->change();
        });
        Schema::table('reviews', function($table){
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('set null');
        });
        */


        Schema::table('zones', function($table){
            $table->integer('parent_id')->nullable()->unsigned()->change();
        });
        Schema::table('zones', function($table){
            $table->foreign('parent_id')->references('id')->on('zones')->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
