<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotServiceZoneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_zone', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('service_id')->unsigned()->index();
			$table->integer('zone_id')->unsigned()->index();
			$table->string('state');
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
			$table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_zone');
	}

}
