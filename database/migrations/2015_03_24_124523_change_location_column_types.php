<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLocationColumnTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
Schema::table('businesses', function($table)
{
    $table->dropColumn('geolocation_longitude');
    $table->dropColumn('geolocation_latitude');
});		

Schema::table('businesses', function($table)
{
    $table->float('geolocation_longitude', 10,8)->default(0);
    $table->float('geolocation_latitude', 10,8)->default(0);
});		

    //Schema::query('ALTER TABLE businesses MODIFY COLUMN geolocation_longitude DOUBLE(9,6)');		
    //Schema::query('ALTER TABLE businesses MODIFY COLUMN geolocation_latitude DOUBLE(9,6)');		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
