<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdSetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_sets', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('campaign_id');
			$table->integer('ad_zone_id');
			$table->integer('slot');
			$table->integer('ad_id');
			$table->string('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_sets');
	}

}
