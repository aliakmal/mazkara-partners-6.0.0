<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncrementalStoredProcedures extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    DB::unprepared('CREATE PROCEDURE mzkIncrementCounter(IN count_type VARCHAR(255), IN count_id INT, IN _type VARCHAR(255)) 
  									BEGIN
  										update `counters` set `views` = `views` + 1
  										where `counters`.`countable_type` = count_type 
												and `counters`.`countable_id` = count_id
												and `counters`.`type` = _type 
												and DATE(`counters`.`dated`) = CURDATE();
    									
  									END');	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
