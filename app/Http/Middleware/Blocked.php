<?php namespace App\Http\Middleware;

use Closure, Auth;
use Illuminate\Contracts\Auth\Guard;

class Blocked {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;
	protected $except = [
	 'blocked'
	];
	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
    if ( Auth::check() && Auth::user()->isBlocked() )
    {

      // Do what you need here
      // the User is blocked!
      return response()->view('blocked');
		}

		return $next($request);
	}

}
