<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Validator, Lang;
use MazkaraHelper;
use App\Models\Check_in;

class Check_insController extends Controller {

	/**
	 * Check_in Repository
	 *
	 * @var Check_in
	 */
	protected $check_in;

	public function __construct(Check_in $check_in)
	{
		$this->check_in = $check_in;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 *
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('check_ins.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Check_in::$rules);

		if ($validation->passes())
		{
			$this->check_in->create($input);

			return Redirect::route('check_ins.index');
		}

		return Redirect::route('check_ins.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$check_in = $this->check_in->findOrFail($id);

		return View::make('check_ins.show', compact('check_in'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$check_in = $this->check_in->find($id);

		if (is_null($check_in))
		{
			return Redirect::route('check_ins.index');
		}

		return View::make('check_ins.edit', compact('check_in'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Check_in::$rules);

		if ($validation->passes())
		{
			$check_in = $this->check_in->find($id);
			$check_in->update($input);

			return Redirect::route('check_ins.show', $id);
		}

		return Redirect::route('check_ins.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->check_in->find($id)->delete();

		return Redirect::route('check_ins.index');
	}

}
