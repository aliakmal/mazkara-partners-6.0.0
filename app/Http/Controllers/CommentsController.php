<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Validator, Lang;
use MazkaraHelper;

use App\Models\Comment;

use App\Models\Business;
use App\Models\Post;
use App\Models\User;
use App\Models\Category;
	use App\Models\Photo;
use App\Models\Review;

class CommentsController extends Controller {

	/**
	 * Comment Repository
	 *
	 * @var Comment
	 */
	protected $comment;

	public function __construct(Comment $comment)
	{
		$this->comment = $comment;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.comments.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Comment::$rules);
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		if ($validation->passes())
		{
			$this->comment->create($input);

			return Redirect::back();//('admin.comments.index');
		}

		return Redirect::back()//route('admin.comments.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$comment = $this->comment->findOrFail($id);

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.comments.show', compact('comment'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$comment = $this->comment->find($id);

		if (is_null($comment))
		{
			return Redirect::back();//route('admin.comments.index');
		}

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.comments.edit', compact('comment'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Comment::$rules);
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		if ($validation->passes())
		{
			$comment = $this->comment->find($id);
			$comment->update($input);

			return Redirect::back();////route('admin.comments.show', $id);
		}

		return Redirect::back()//route('admin.comments.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->comment->find($id)->delete();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		return Redirect::back();//route('admin.comments.index');
	}

}
