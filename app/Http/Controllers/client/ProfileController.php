<?php
namespace App\Http\Controllers\client;

use Confide, BaseController, View, Config, Validator, Redirect, Input;
use User, App, Auth, Mail,  Ad_set, Ad_slot, Photo, Image, Business, Campaign, Response;
use App\Http\Controllers\Controller;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	private $business;
	public function __construct(Business $business){

		$this->business = $business;
	}

	public function dashboard()
	{
		$user = User::find(Auth::user()->id);
		$businesses = $user->businesses();
		return  View::make('client.home.dashboard', compact('user', 'businesses'));
	}

	public function setCurrentBusiness($id){
		mzk_client_set_default_business($id);
    return Redirect::back();
	}


	public function getPhotos()
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		return View::make('client.businesses.photos', compact('business'));
	}

	public function getPhoto($photoId)
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
		$photo = Photo::find($photoId);
		$image = Image::make($photo->image->url());


		return View::make('client.businesses.photo', compact('business', 'photo', 'image'));
	}


	public function postPhoto($photoId)
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$photo = Photo::find($photoId);

		$img = Image::make($photo->image->url());
		$img->crop(round($input['w']), round($input['h']), round($input['x']), round($input['y']));
		$filename = storage_path().'/media/'.md5(time()).'.jpg';
		$img->save($filename);

		//$img->save($photo->image->url());//exit;
		$photo->image = $filename ;
		$photo->save();
		
		return Redirect::route('client.businesses.show.photo', array('photoId'=>$photoId));
	}


	public function rotatePhoto($photoId, $degrees)
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
		$photo = Photo::find($photoId);

		$img = Image::make($photo->image->url());
		$img->rotate($degrees);//crop(round($input['w']), round($input['h']), round($input['x']), round($input['y']));
		$filename = storage_path().'/media/'.md5(time()).'.jpg';
		$img->save($filename);

		$photo->image = $filename ;
		$photo->save();
		
		return Redirect::back();
	}


	public function postPhotos()
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

		$images = Input::only('images');
		$business->saveImages($images['images']);

		$deletablePhotos = Input::only('deletablePhotos');
		$business->removeAllImages($deletablePhotos['deletablePhotos'] ? $deletablePhotos['deletablePhotos']:[]);

    $orderables = Input::only('orderables');

    if(isset($orderables['orderables'])):
      $orderables = Input::only('orderables');
      foreach($orderables['orderables'] as $p_id=>$p_order){
        if(in_array($p_id, ($deletablePhotos['deletablePhotos'] ? $deletablePhotos['deletablePhotos']:[]))){
          continue;
        }
        $p = Photo::find($p_id);
        $p->sort = $p_order;
        $p->save();
      }
    endif;

    $cover = Input::only('is_cover');
    $business->updateImageForCover($cover['is_cover']);

		return Redirect::route('client.businesses.show.photos');
	}








	public function getRateCards()
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		return View::make('client.businesses.rate-cards', compact('business'));
	}

	public function postRateCards()
	{
		$current_business_id = mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

		$images = Input::only('images');
		$rate_cards = Input::only('rate_card');
		$business->saveRateCards($rate_cards['rate_card']);
		$deletablePhotos = Input::only('deletablePhotos');

		$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

    $orderables = Input::only('orderables');
    if(isset($orderables['orderables'])):
      foreach($orderables['orderables'] as $p_id=>$p_order){
        $p = Photo::find($p_id);
        $p->sort = $p_order;
        $p->save();
      }
    endif;

		return Redirect::route('client.businesses.show.rate_cards');//('BusinessesController@getRateCards', $id);
	}


  public function getChangeRequest($bid){
    return View::make('client.pages.change-request');
  }

  public function postChangeRequest($bid){
    $rules = array('message'=>'required');
    $input = Input::all();
    $validation = Validator::make($input, $rules);

    if ($validation->passes()){

      $input['business_id'] = $bid;//mzk_client_get_default_business();
      $input['business'] = Business::find($input['business_id']);

      Mail::send('emails.client-change-request', ['data'=>$input, 'server'=>$_SERVER], function($message)  use ($input) {
        $message->to('moderation@fabogo.com')
                ->subject('Mazkara: Change Request for '.$input['business']->name.' ['.time(). ']');
      });     
      return Redirect::back()->with('notice', 'We have received your change request and will get in touch with you shortly');
    }

    return Redirect::back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');

    return Redirect::back();
  }







}
