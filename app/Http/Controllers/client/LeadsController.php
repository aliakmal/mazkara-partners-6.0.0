<?php
namespace App\Http\Controllers\client;

use Confide, View, Config, Validator, Redirect, Input;
use  App, Auth, Response;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Lead;
use App\Models\Call_log;
use App\Models\Review;
use App\Models\Ad_slot;
use App\Models\Activity;
use App\Models\Business ;
use App\Models\Comment;


class LeadsController extends Controller {

	/**
	 * Review Repository
	 *
	 * @var Review
	 */
	protected $lead, $business, $feed_manager;

	public function __construct(Lead $lead, Business $business, Activity $activity){

		$this->lead = $lead;
		$this->business = $business;
    $this->feed_manager = $activity;
	}

	public function index($bid = false)
	{


		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
		$show_leads = false;
    if($business->canAccessLeadsReport()){
      $show_leads = true;
    }else{
      $show_leads = false;
    }

	  if($show_leads == false){
	  	return Redirect::to('/partner/'.$bid);
	  }

    $params = Input::all();

		$leads = $this->lead->select();

    if(isset($params['interested']) && !empty($params['interested'])){
      $leads = $leads->where('interested_in', 'like', '%'.$params['interested'].'%');
    }

    if(isset($params['search']) && !empty($params['search'])){
      $leads = $leads->where('name', 'like', '%'.$params['search'].'%')->whereOr('email', 'like', '%'.$params['search'].'%');
    }

    if(isset($params['start']) && !empty($params['start'])){
      $params['end'] = isset($params['end'])?$params['end']:\Carbon\Carbon::now()->toDateString();
      $leads = $leads->betweenDates($params['start'], $params['end']);
    }


		$leads->byBusiness($current_business_id)->orderBy('id', 'DESC');
    $leads = $leads->paginate(20);
    $results = array();
    $current_date = false;

    foreach($leads as $lead){
      //dump($lead->businesses()->where('business_id','=',$current_business_id)->first());die();
//      $leadable = $lead->businesses()->where('business_id','=',$current_business_id)->first();
      //if(\Carbon\Carbon::parse($leadable->pivot->allocated_at)->toFormattedDateString() != $current_date){
      if(\Carbon\Carbon::parse($lead->created_at)->toFormattedDateString() != $current_date){
        //$current_date = \Carbon\Carbon::parse($leadable->pivot->allocated_at)->toFormattedDateString();
    		$current_date = \Carbon\Carbon::parse($lead->created_at)->toFormattedDateString();
    		$results[$current_date] = [];
    	}

    	$results[$current_date][] = $lead;
    }



		return View::make('client.leads.index', 
    																			compact('leads', 'results', 'business', 'params'));
	}

	public function getRatingsData($bid = false){
		$input = Input::all();
		// get start and end date
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		$start_date = isset($input['start']) ? $input['start'] : $business->created_at;
    $end_date = isset($input['end']) ? $input['end'] : date('Y-m-d');

		$weeks = mzk_get_weeks_between_range_array($start_date, $end_date);

    $line_chart = [];
    
    for($i=0; $i<5; $i++){
	    $line_chart[$i.' to '.($i+1)] = [];
	    foreach($weeks as $vv){
		 		$line_chart[$i.' to '.($i+1)][] = [	strtotime($vv[0]), 
		 																				$this->review->query()->betweenDates($vv[0], $vv[1])
		 																										->byBusiness($current_business_id)
		 																										->betweenRatings($i, ($i+1))->count()]; 
	    }
    }

		return Response::json($line_chart);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	* public function index()
	* {
	* 	$reviews = $this->review->all();
*
* 	* 	return View::make('reviews.index', compact('reviews'));
	* }
	 */

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reviews.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = Auth::user()->id;
		$validation = Validator::make($input, Review::$rules);

		MazkaraHelper::clearCurrentPageCacheName(URL::previous());
		if ($validation->passes())
		{
			$review = $this->review->create($input);
      $data = array('user_id' =>  $input['user_id'], 
                    'verb'  =>  (trim($input['body'])!=""?'reviewed':'rated'), 
                    'itemable_type' =>  'Review', 
                    'itemable_id' =>  $review->id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $input['user_id'];
      $feed->verb = 'reviewed'; 
      $feed->itemable_type = 'Review';
      $feed->itemable_id = $review->id;
      $feed->save();

			return Redirect::back();//route('businesses.show', array('id'=>$input['business_id']));
		}

		return Redirect::back()//route('businesses.show', array('id'=>$input['business_id']))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($bid = false, $id)
	{
		$review = $this->review->findOrFail($id);
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);


		return View::make('client.reviews.show', compact('review', 'business'));

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$review = $this->review->find($id);

		if (is_null($review))
		{
			return Redirect::route('reviews.index');
		}

		return View::make('reviews.edit', compact('review'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$input['user_id'] = Auth::user()->id;
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		$validation = Validator::make($input, Review::$rules);

		if ($validation->passes())
		{
			$review = $this->review->find($id);
			$review->update($input);

			return Redirect::back();//('reviews.show', $id);
		}
		return Redirect::back();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($bid, $id){

		$review = $this->review->find($id);
    $business = $review->business;
    $review->delete();
    $business->updateRatingAndReviewsCount();
		MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		return Redirect::back();//('reviews.index');
	}

}
