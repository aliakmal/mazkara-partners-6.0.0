<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Validator, Lang;
use MazkaraHelper;
use App\Http\Controllers\Controller;

use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Review;
use App\Models\User;
use App\Models\Activity;
use App\Models\Zone;
use App\Models\Favorite;

class PostsController extends Controller {

	/**
	 * Post Repository
	 *
	 * @var Post
	 */
	protected $post;
	protected $params;

	protected $filters;
  protected $meta;

	public function __construct(Post $post){
		$this->post = $post;
		$this->params = [];
		$this->filters = [];
    $this->meta = new \RyanNielson\Meta\Meta();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($service_slug = false)
	{
		$this->post = $this->post->with('cover', 'likes', 'comments')->onlyPosts()->isViewable();
		$this->filterAllPosts($service_slug);
		$posts = $this->post->orderBy('published_on', 'DESC')->paginate(10);

    $suggested_posts = Post::with('cover')->onlyPosts()->isViewable()->take(5)->get();
    foreach($posts as $post){
      $post_ids[] = $post->id;
    }

    $favorited_posts = [];

    if(Auth::check()){
      $user = Auth::user();
      $favorited_posts = $user->hasFavouritedMultiple('Post', $post_ids);
    }

    foreach($posts as $post){
      $post->has_cover = $post->hasCover();
      $post->image_medium_url = $post->has_cover?$post->cover->image->url('medium'):'';
      $post->image_small_url = $post->has_cover?$post->cover->image->url('small'):'';
      $post->isFavorite = in_array($post->id, $favorited_posts)?true:false;
    }


    $service_ids = DB::table('post_service')->distinct('service_id')->lists('service_id');
		$service_tags = Service::whereIn('id', $service_ids)->get();
		$params = $this->params;
    $filters = $this->filters;

    $service = false;
    $businesses_with_prices = [];
    if($service_slug){
      $service = Service::where('slug', '=', $service_slug)->first();

      $this->post->ofServices([$service->id]);
      $this->setParams('service', $service->id);
      $businesses_with_prices = Business::withServices([$service->id], true)->isDisplayable()->get()->take(5);
    }

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
    //die(json_encode(['posts'=>$posts->toArray(), 'suggested_posts'=>$suggested_posts, 'businesses_with_prices'=> $businesses_with_prices]));
  	return View::make('site.posts.index', compact(  'posts', 'service_tags',
                                                    'service', 'businesses_with_prices',
                                                    'filters', 'params', 'suggested_posts'))
                                ->with('canonical_url', $canonical_url);
	}

	public function filterAllPosts($service_slug = false){
		$this->filterByAuthor();
    $this->filterBySearch();
		$this->filterByServiceTag($service_slug);
	}

  public function filterByAuthor(){
    if(Input::has('author') && (Input::get('author')!='')){
      $author = Input::get('author');
      $this->post->ofAuthors($author);
      $this->setParams('author', $author);
      $author = User::find($author);
      $this->setFilters('second-heading', 'Stories by '.$author->full_name);
    }
  }

	public function filterBySearch(){
    if(Input::has('search') && (Input::get('search')!='')){
    	$search = Input::get('search');
    	$this->post->ofSearch($search);
	    $this->setParams('search', $search);
    }
	}

	public function filterByServiceTag($service_slug = false){
    if($service_slug){
      $service = Service::where('slug', '=', $service_slug)->first();
      $services = [];
      if($service->parent_id == 0){
        $services = $service->children()->lists('id', 'id')->all();

      }else{
        $services = $service->parent->children()->lists('id', 'id')->all();

      }

      if(count($services)>1){
        array_pop($services);
      }
      if(count($services)>1){
        array_pop($services);
      }
      $services[] = $service->id;

      $this->post->ofServices($services);
    }

    if(Input::has('st') && (Input::get('st')!='')){
    	$st = Input::get('st');
    	$this->post->ofServices([$st]);
	    $this->setParams('st', $st);
    }
	}
  protected function setFilters($key, $val){
    $this->filters[$key] = $val;
  }

  protected function getFilters($key){
    return isset($this->filters[$key]) ? $this->filters[$key] : '';
  }

	protected function setParams($key, $val){
		$this->params[$key] = $val;
	}

  protected function getParams($key){
    return isset($this->params[$key]) ? $this->params[$key] : '';
  }

  public function follow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = mzk_get_class($post);
    if(!$user->hasFavourited($class_name, $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>$class_name, 'favorable_id'=>$id]);
    }
  }

  public function unfollow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = mzk_get_class($post);
    if($user->hasFavourited($class_name, $id)){
      $user->favorites()->where('favorable_id', '=', $id)
                        ->where('favorable_type', '=', $class_name)->delete();
    }
  }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
    $user = User::find(Auth::user()->id);//Auth::user()->id);

    
    $activities = [];//Activity::select()->byUserIds([$user->id])->orderby('id', 'desc')->paginate(10);

    return View::make('site.users.posts.create', compact('user'));

		//return View::make('posts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$input = Input::all();
		$validation = Validator::make($input, Post::$post_rules);
		if ($validation->passes())
		{
      $data = Input::only(Post::$fields);
      $data['type'] = 'post';
			$post = $this->post->create($data);
			
			$post->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$post->services()->sync($input['services']);
			}

      return Redirect::to($post->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug){
    $post = $this->post->findBySlug($slug);

//    if($post->slug!=$slug){
//      // return Redirect::to(route('posts.show.by.slug', ['id'=>$post->id, 'slug'=>$post->slug]));
//    }
    $post->view();

		switch($post->type){
      case POST::ARTICLE:
        return $this->showPost($post);
      break;
      case POST::VIDEO:
        return $this->showVideo($post);
      break;
      case POST::SELFIE:
        return $this->showPhoto($post);
      break;
    }
	}

  public function showMobile($id){
    $post = $this->post->find($id);
    $post->view();

    return View::make('site.posts.show-mobile', compact('post'));

  }

  public function showPost($post){
    $suggested_posts = Post::with('cover')->isViewable()->onlyPosts()->take(5)->get();

    $service = false;
    $businesses_with_prices = [];

    if(count($post->services)>0){
      $service = $post->services()->first();
    }else{
      $service = Service::first();
    }

    $this->post->ofServices([$service->id]);
    $this->setParams('service', $service->id);

    $businesses_with_prices = Business::withServices([$service->id], true)
                                            ->isDisplayable()
                                            ->byLocale(MazkaraHelper::getLocaleID())
                                            ->get()->take(5);


    $this->meta->set(
        array('title' =>  $post->getPageSEOTitle().' - '.mzk_seo_name(), //$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $post->getPageSEOMetaDesc(),
              'og'=>[
                'title' => $post->getPageSEOTitle().' - '.mzk_seo_name(),
                'image'=> $post->getCoverUrl('medium'),
                'description' => $post->getPageSEOMetaDesc(),
              ]
      )
    );


    $ads = $this->setupAdsForDisplay($post->services()->lists('service_id','service_id')->all());

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;


    return View::make('site.posts.show', 
                            compact('post', 'businesses_with_prices', 
                                    'service', 'suggested_posts', 'ads'))
                      ->with('canonical_url', $canonical_url)
                      ->with('meta', $this->meta);;

  }

  public function showPhoto($post){
    $suggested_posts = Post::with('cover')->isViewable()->onlySelfies()->take(5)->get();
    $service = false;
    $businesses_with_prices = [];


    if(count($post->services)>0){
      $service = $post->services()->first();
    }else{
      $service = Service::first();
    }

    $this->post->ofServices([$service->id]);
    $this->setParams('service', $service->id);
    $businesses_with_prices = Business::withServices([$service->id], true)
                                        ->isDisplayable()
                                        ->byLocale(MazkaraHelper::getLocaleID())
                                        ->get()->take(5);


    $this->meta->set(
        array('title' =>  $post->title.' - '.mzk_seo_name(), //$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $post->caption,
              'og'=>[
                'title' => $post->title.' - '.mzk_seo_name(),
                'image'=> $post->getCoverUrl('medium'),
                'description' => $post->caption,
              ]
      )
    );

    $ads = $this->setupAdsForDisplay($post->services()->lists('service_id','service_id'));

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

    return View::make('site.users.selfies.show', 
                            compact('post', 'businesses_with_prices', 
                                    'service', 'suggested_posts', 'ads'))
                      ->with('canonical_url', $canonical_url)
                      ->with('meta', $this->meta);;
  }

  public function showVideo($post){
    $suggested_posts = Post::with('cover')->isViewable()->onlyVideos()->take(5)->get();

    $service = false;
    $businesses_with_prices = [];
    if(count($post->services)>0){
      $service = $post->services()->first();
    }else{
      $service = Service::first();
    }

    $this->post->ofServices([$service->id]);
    $this->setParams('service', $service->id);
    $businesses_with_prices = Business::withServices([$service->id], true)
                                          ->isDisplayable()
                                          ->byLocale(MazkaraHelper::getLocaleID())
                                          ->get()->take(5);

    $this->meta->set(
        array('title' =>  $post->title.' - '.mzk_seo_name(), //$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $post->caption,
              'og'=>[
                'title' => $post->title.' - '.mzk_seo_name(),
                'image'=> $post->getCoverUrl('medium'),
                'description' => $post->caption,
              ]
      )
    );

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;


    $ads = $this->setupAdsForDisplay($post->services()->lists('service_id','service_id'));

    return View::make('site.users.videos.show', 
                            compact('post', 'businesses_with_prices', 
                                    'service', 'suggested_posts', 'ads'))
                      ->with('canonical_url', $canonical_url)
                      ->with('meta', $this->meta);;


  }

  public function showCanonicalUrl(){
    $input = Input::all();
    $vars = ['page', 'sort', 'highlights', 'zones', 'fabtrack'];
    foreach($input as $ii=>$vv){
      if(in_array($ii, $vars)){
        
        return true;
      }
    }

    return false;
  }



  protected function setupAdsForDisplay($services = false, $zone = false){
    //$category = $this->getParams('category')?$this->getParams('category'):[];

    $categories = mzk_categories_from_services($services);
    $zone = $zone ? $zone : mzk_get_localeID();

    $zones = $zone ? array_merge([$zone], array_keys(Zone::where('city_id','=',$zone)->lists('id', 'id')->all())):[];

    //$business_zones = Business_zone::select()->byZones($zones)->lists('id', 'id');
    $business_zones = Zone::whereIn('id', $zones)->lists('business_zone_id','business_zone_id')->all();
    $ads = mzk_get_ad_lists_to_show_today(['categories'=>$categories, 
                                                 'business_zones'=>$business_zones]);
    return $ads;
  }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = $this->post->find($id);

    if (is_null($post) || !($post->isEditableBy(Auth::user()->id)))
    {
      return Redirect::back();
    }

    $user = User::find($post->author_id);//Auth::user()->id);

    $activities = [];//Activity::select()->byUserIds([$user->id])->orderby('id', 'desc')->paginate(10);

    return View::make('site.users.posts.edit', compact('post','user', 'activities'));
	}

public function reviewToDraft($id){
  $post = $this->post->find($id);
  $post->setStateDraft();
  $post->save();
  return Redirect::back();
}

public function draftToReview($id){
  $post = $this->post->find($id);
  $post->setStateReview();
  $post->save();
  return Redirect::back();
}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Post::$post_rules);

		if ($validation->passes())
		{
			$post = $this->post->find($id);
			$post->update(Input::only(Post::$fields));
      $post->setStateDraft();
			$post->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$post->services()->sync($input['services']);
			}

      return Redirect::to($post->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = $this->post->find($id);
    $user = $post->author;
    $post->delete();

		return Redirect::to(route('users.profile.show', [$user->id]));
	}

}
