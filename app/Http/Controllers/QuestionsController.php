<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Validator, Lang;
use MazkaraHelper;
use App\Http\Controllers\Controller;

use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Review;
use App\Models\Activity;
use App\Models\Favorite;

class QuestionsController extends Controller {

	/**
	 * Post Repository
	 *
	 * @var Post
	 */
	protected $question;
  protected $service;
  protected $answer;
	protected $params;

	protected $filters;

	public function __construct(Question $question, Answer $answer, Service $service){
		$this->question = $question;
    $this->answer = $answer;
    $this->service = $service;

    $this->params = [];
		$this->filters = [];
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
    $services = Service::query()->showParents()->get();
    $input = Input::all();
    if(isset($input['search'])){
      $this->search();
      return;
    }
    $data = [];
    foreach($services as $service){
      $questions = Question::ofServices($service->children()->lists('id','id'))->onlyQuestions()->get()->take(3);
      if($questions->count()>0){
        $service->questions = $questions;
        $data[] = $service;
      }
    }

    $popular_questions = Question::query()->onlyQuestions()->orderBy('views', 'desc')->get()->take(8);

    $services = Service::query()->showOnly()->get()->take(10);	

    $businesses_with_prices = Business::query()
                                          ->isDisplayable()
                                          ->byLocale(MazkaraHelper::getLocaleID())
                                          ->paginate(5);

  	return View::make('site.questions.index', compact('data', 'businesses_with_prices', 'popular_questions', 'services'));
	}


  public function search(){
    $params = Input::only('search');
    $questions = Question::ofSearch($params['search'])->onlyQuestions()->paginate(10);
    $services = Service::query()->showOnly()->get()->take(10);  

    $businesses_with_prices = Business::withServices([Service::query()->showOnly()->get()->take(10)->lists('id', 'id')], true)
                                          ->isDisplayable()
                                          ->byLocale(MazkaraHelper::getLocaleID())
                                          ->get()->take(5);

    return View::make('site.questions.search', compact('params', 'businesses_with_prices', 'questions', 'services'));
  }

  public function service($service){
    $service = $this->service->getBySlug($service);

    $questions = Question::ofServices(array_merge([$service->id], $service->children()->lists('id','id')))->onlyQuestions()->paginate(10);

    $services = Service::query()->showOnly()->get()->take(10);  

    $businesses_with_prices = Business::withServices([$service->id], true)
                                          ->isDisplayable()
                                          ->byLocale(MazkaraHelper::getLocaleID())
                                          ->get()->take(5);

    return View::make('site.questions.service', compact('service', 'businesses_with_prices', 'questions', 'services'));
  }


  protected function setFilters($key, $val){
    $this->filters[$key] = $val;
  }

  protected function getFilters($key){
    return isset($this->filters[$key]) ? $this->filters[$key] : '';
  }

	protected function setParams($key, $val){
		$this->params[$key] = $val;
	}

  protected function getParams($key){
    return isset($this->params[$key]) ? $this->params[$key] : '';
  }

  public function follow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = get_class($post);

    if(!$user->hasFavourited($class_name, $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>$class_name, 'favorable_id'=>$id]);
    }
  }

  public function unfollow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = get_class($post);
    
    if($user->hasFavourited($class_name, $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', $class_name)->delete();
    }
  }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
    $user = User::find(Auth::user()->id);

    return View::make('site.questions.create', compact('user'));

		//return View::make('posts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(){
		$input = Input::all();
		$validation = Validator::make($input, Question::$rules);
		if ($validation->passes())
		{
      $data = Input::only(Question::$fields);
      $data['type'] = Post::QUESTION;
			$question = $this->question->create($data);
      $question->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$question->services()->sync($input['services']);
			}

      return Redirect::to($question->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}




	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, $slug)
	{
		$question = $this->question->find($id);
    $question->view();

    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;
    $breadcrumbs->addCrumb('Talk', '/talk');
    $breadcrumbs->addCssClasses('breadcrumb');
    $breadcrumbs->setDivider('›');

    if($question->services()->count() > 0):
      $service = $question->services()->first();
      $breadcrumbs->addCrumb($service->name, route('questions.service', [$service->slug]));
    endif;

    $breadcrumbs->addCrumb($question->title);
    $services = Service::query()->showOnly()->get()->take(10);  
    $businesses_with_prices = Business::withServices([$service->id], true)
                                          ->isDisplayable()
                                          ->byLocale(MazkaraHelper::getLocaleID())
                                          ->get()->take(5);

    return View::make('site.questions.show', compact('question', 'service', 'services', 'businesses_with_prices', 'breadcrumbs'));
	}

  protected function setupAdsForDisplay($services = false, $zone = false){
    //$category = $this->getParams('category')?$this->getParams('category'):[];

    $categories = mzk_categories_from_services($services);
    $zone = $zone ? $zone : mzk_get_localeID();

    $zones = $zone ? array_merge([$zone], array_keys(Zone::where('city_id','=',$zone)->lists('id', 'id'))):[];

    //$business_zones = Business_zone::select()->byZones($zones)->lists('id', 'id');
    $business_zones = Zone::whereIn('id', $zones)->lists('business_zone_id','business_zone_id');
    $ads = mzk_get_ad_lists_to_show_today(['categories'=>$categories, 
                                                 'business_zones'=>$business_zones]);
    return $ads;
  }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$question = $this->question->find($id);

    if (is_null($question) || !($question->isEditableBy(Auth::user()->id)))
		{
			return Redirect::back();
		}
    $user = User::find($question->author_id);//Auth::user()->id);

    $activities = [];//Activity::select()->byUserIds([$user->id])->orderby('id', 'desc')->paginate(10);

    return View::make('site.questions.edit', compact('question','user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Question::$rules);

		if ($validation->passes())
		{
			$question = $this->question->find($id);
			$question->update(Input::only(Question::$fields));
      $question->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$question->services()->sync($input['services']);
			}

      return Redirect::to($question->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->post->find($id)->delete();

		return Redirect::back();
	}

}
