<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;


class Call_logsController extends Controller {

	/**
	 * Call_log Repository
	 *
	 * @var Call_log
	 */
	protected $call_log;

	public function __construct(Call_log $call_log)
	{
		$this->call_log = $call_log;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$call_logs = $this->call_log->all();

		return View::make('call_logs.index', compact('call_logs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('call_logs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$this->call_log->create($input);

			return Redirect::route('call_logs.index');
		}

		return Redirect::route('call_logs.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$call_log = $this->call_log->findOrFail($id);

		return View::make('call_logs.show', compact('call_log'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$call_log = $this->call_log->find($id);

		if (is_null($call_log))
		{
			return Redirect::route('call_logs.index');
		}

		return View::make('call_logs.edit', compact('call_log'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$call_log = $this->call_log->find($id);
			$call_log->update($input);

			return Redirect::route('call_logs.show', $id);
		}

		return Redirect::route('call_logs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->call_log->find($id)->delete();

		return Redirect::route('call_logs.index');
	}

}
