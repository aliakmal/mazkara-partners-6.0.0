<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, MazkaraHelper;
//use Vinelab\Http\Client as HttpClient;

use GuzzleHttp\Client as HttpClient;

use App\Models\Category;
use App\Models\Virtual_number;
use App\Models\Virtual_number_allocation;
use App\Models\Business;

use App\Http\Controllers\Controller;

class Virtual_number_allocationsController extends Controller {

	/**
	 * Virtual_number_allocation Repository
	 *
	 * @var Virtual_number_allocation
	 */
	protected $virtual_number_allocation, $business, $virtual_number;

	public function __construct(Business $business, Virtual_number $virtual_number, Virtual_number_allocation $virtual_number_allocation)
	{
		$this->business = $business;
		$this->virtual_number = $virtual_number;

		$this->virtual_number_allocation = $virtual_number_allocation;
    $this->layout = 'layouts.admin-crm';    
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$virtual_number_allocations = $this->virtual_number_allocation->all();
		$businesses = $this->business->query()->byLocale();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $businesses->search($input['search']);
      $params['name'] = $input['search'];
      $params['search'] = $input['search'];
    }

    if(Input::has('byID') && ($input['byID']!="")){
      $businesses->where('id', 'LIKE', $input['byID']);
      $params['byID'] = $input['byID'];
    }

    if(Input::has('bySlipID') && ($input['bySlipID']!="")){
      $businesses->where('ref', 'LIKE', $input['bySlipID']);
      $params['bySlipID'] = $input['bySlipID'];
    }

    if(Input::has('byLotID') && ($input['byLotID']!="")){
      $businesses->where('lot_id', 'LIKE', $input['byLotID']);
      $params['byLotID'] = $input['byLotID'];
    }

    if(Input::has('sort') && ($input['sort']!='')){
      $params['sort'] = $input['sort'];
    	switch($params['sort']){
        case 'idAsc':
          $businesses->orderBy('id', 'ASC');
        break; 
        case 'idDesc':
          $businesses->orderBy('id', 'DESC');
        break; 
    		case 'nameAsc':
	    		$businesses->orderBy('name', 'ASC');
    		break; 
				case 'nameDesc':
	    		$businesses->orderBy('name', 'DESC');
    		break; 
				case 'lastUpdateAsc':
	    		$businesses->orderBy('updated_at', 'ASC');
    		break; 
				case 'lastUpdateDesc':
	    		$businesses->orderBy('updated_at', 'DESC');
    		break; 
    	}
    }else{
  		$businesses->orderBy('businesses.id', 'DESC');
    }


    $businesses = $businesses->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.virtual_number_allocations.index', compact('params', 'businesses', 'virtual_number_allocations'));
	}



	public function getAllocateForBusiness($id){
		$business = $this->business->findOrFail($id);
		$virtual_numbers = $this->virtual_number->onlyAllocatable();
		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.virtual_number_allocations.allocate', compact('virtual_numbers', 'business'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create(){
		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.virtual_number_allocations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Virtual_number_allocation::$rules);

		if ($validation->passes()){
			$input['starts'] = mzk_current_date_time();
      $virtual_number = Virtual_number::find($input['virtual_number_id']);
			$this->virtual_number_allocation->create($input);
			$client = new HttpClient();
      $url = 'http://etsintl.kapps.in/webapi/mazkara/api/mazkara_agent_mapping.py?auth_key=51c41934-94b1-4be7-82c2-6531251a76ab';
      $url .='&knowlarity_number='.$virtual_number->body.'&mapped_number='.
                                join(',', array_filter([ trim($input['phone_1']),
                                                          trim($input['phone_2']),
                                                          trim($input['phone_3'])])); 

      $response = $client->get($url);
      //$response = mzk_xml2array($response->xml());



			return Redirect::route('admin.businesses.virtual_numbers.show', $input['business_id'])
                          ->with('message', 'Success');//$response);
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$virtual_number_allocation = $this->virtual_number_allocation->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.virtual_number_allocations.show', compact('virtual_number_allocation'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$virtual_number_allocation = $this->virtual_number_allocation->find($id);

		if (is_null($virtual_number_allocation))
		{
			return Redirect::route('virtual_number_allocations.index');
		}

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.virtual_number_allocations.edit', compact('virtual_number_allocation'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deallocate($id)
	{
		$input = array_except(Input::all(), '_method');
		$virtual_number_allocation = $this->virtual_number_allocation->find($id);
		$virtual_number_allocation->deallocate();

    $virtual_number = Virtual_number::find($virtual_number_allocation->virtual_number_id);

    $client = new HttpClient();
    $url = 'http://etsintl.kapps.in/webapi/mazkara/api/mazkara_agent_mapping.py?auth_key=51c41934-94b1-4be7-82c2-6531251a76ab';
    $url .='&knowlarity_number='.$virtual_number->body.
                      '&mapped_number=None'; 

    $response = $client->get($url);
    
    //$response = mzk_xml2array($response->xml());

		return Redirect::back()->with('message', 'Ok');
	}




  public function updateVirtualNumberAllocation(){
    $input = Input::all();

    $data = ['id' => $input['pk']];

    $virtual_number_allocation = Virtual_number_allocation::find($data['id']);

    $virtual_number_allocation->$input['name'] = $input['value'];
    $virtual_number_allocation->save();
    return Redirect::back();

  }



	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Virtual_number_allocation::$rules);

		if ($validation->passes())
		{
			$virtual_number_allocation = $this->virtual_number_allocation->find($id);
			$virtual_number_allocation->update($input);

			return Redirect::route('admin.virtual_number_allocations.show', $id);
		}

		return Redirect::route('admin.virtual_number_allocations.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->virtual_number_allocation->find($id)->delete();

		return Redirect::route('admin.virtual_number_allocations.index');
	}

}
