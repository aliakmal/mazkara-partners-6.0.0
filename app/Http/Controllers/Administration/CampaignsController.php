<?php
namespace App\Http\Controllers\Administration;


use View, Config, Validator, Redirect, Response, Input;

use App\Models\User;
use App\Models\Campaign;
use App\Models\Ad_slot;
use App\Models\Ad_set;
use App\Models\Business_zone;

use App\Http\Controllers\Controller;

class CampaignsController extends Controller {

	/**
	 * Campaign Repository
	 *
	 * @var Campaign
	 */
	protected $campaign;

	public function __construct(Campaign $campaign)
	{
		$this->campaign = $campaign;
    $this->layout = 'layouts.admin-crm';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$campaigns = $this->campaign->query();
		$input = Input::all();
    if(Input::has('search') && ($input['search']!='')){
      $campaigns->bySearch($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('merchant_id') && ($input['merchant_id']!='')){
      $campaigns->byMerchants([$input['merchant_id']]);
      $params['merchant_id'] = $input['merchant_id'];
    }

    if(Input::has('active') && ($input['active']!='')){
      $campaigns->current();
      $params['active'] = $input['active'];
    }

		$campaigns->orderBy('id', 'DESC');
    $campaigns = $campaigns->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.campaigns.index', compact('campaigns'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.campaigns.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Campaign::$rules);

		if ($validation->passes())
		{
			$campaign = $this->campaign->create($input);
			$campaign->setCity();
			$campaign->save();
			
			return Redirect::route('admin.campaigns.index');
		}

		return Redirect::route('admin.campaigns.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$campaign = $this->campaign->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.campaigns.show', compact('campaign'));
	}

	public function getAvailableSlots($id)
	{
		$campaign = $this->campaign->findOrFail($id);
		$input = Input::all();
		$slots = Ad_set::getAvailableSlots($input['business_zone_id'], $input['category_id'], $campaign);
		return Response::Json($slots);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$campaign = $this->campaign->find($id);

		if (is_null($campaign))
		{
			return Redirect::route('admin.campaigns.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.campaigns.edit', compact('campaign'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Campaign::$rules);

		if ($validation->passes())
		{
			$campaign = $this->campaign->find($id);
			$campaign->update($input);

			return Redirect::route('admin.campaigns.show', $id);
		}

		return Redirect::route('admin.campaigns.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->campaign->find($id)->delete();

		return Redirect::route('admin.campaigns.index');
	}

}
