<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, Response;

use App\Models\Service_item;
use App\Models\User;

use App\Models\Menu_group;

use App\Http\Controllers\Controller;

class Menu_groupsController extends Controller {

	/**
	 * Menu_group Repository
	 *
	 * @var Menu_group
	 */
	protected $menu_group;

	public function __construct(Menu_group $menu_group)
	{
		$this->menu_group = $menu_group;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$menu_groups = $this->menu_group->all();

		return View::make('menu_groups.index', compact('menu_groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('menu_groups.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Menu_group::$rules);

		if ($validation->passes()){
			$menu_group = $this->menu_group->create($input);
			$menu_group->toArray();
			$menu_group['menu_groups'] = Menu_group::optionables(['business_id' => $menu_group->business_id]);
		}

			return Response::json($menu_group);//::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$menu_group = $this->menu_group->findOrFail($id);

		return View::make('menu_groups.show', compact('menu_group'));
	}

	public function bulk_allocate(){
		$input = Input::all();

		$items = isset($input['items']) ? $input['items'] : [];

		foreach($items as $item_id){
			$item = Service_item::find($item_id);
			$item->menu_group_id = $input['allocate_menu_group_id'];
			$item->save();
		}

		$mg = Menu_group::find($input['allocate_menu_group_id']);

		if($mg){
			return Redirect::to('/admin/businesses/'.$mg->business_id.'#items');
		}else{
			return Redirect::back();
		}

	}

	public function updatable()
	{
		$input = Input::all();

		$data = ['id' => $input['pk']];
		$data[$input['name']] = $input['value'];
		{
			$menu_group = $this->menu_group->find($data['id']);
			$menu_group->update($data);

			return Redirect::back();
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$menu_group = $this->menu_group->find($id);

		if (is_null($menu_group))
		{
			return Redirect::route('menu_groups.index');
		}

		return View::make('menu_groups.edit', compact('menu_group'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Menu_group::$rules);

		if ($validation->passes())
		{
			$menu_group = $this->menu_group->find($id);
			$menu_group->update($input);

			return Redirect::route('menu_groups.show', $id);
		}

		return Redirect::route('menu_groups.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->menu_group->find($id)->delete();

		return Redirect::route('menu_groups.index');
	}

}
