<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, MazkaraHelper;
//use Vinelab\Http\Client as HttpClient;


use App\Models\Setting;

use App\Http\Controllers\Controller;

class SettingsController extends Controller {

	/**
	 * Virtual_number_allocation Repository
	 *
	 * @var Virtual_number_allocation
	 */
	protected $setting;

	public function __construct(Setting $setting)
	{
		$this->setting = $setting;
    $this->layout = 'layouts.admin-content';    
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  public function getSettings()
  {
    //$this->layout = View::make('layouts.admin');
    $settings = Setting::byLocale()->byType('acd')->orderby('type')->get();
    dump($settings);
    return  View::make('administration.settings.get', compact('settings'));
  }

  public function postSettings()
  {
    $input = Input::all();
    $settings = [];
    foreach($input['settings'] as $id=>$setting){
      $s = ['id'=>$id];
      foreach($setting as $name=>$value){
        $s['name'] = $name;
        $s['value'] = $value;
      }
      $settings[] = $s;
    }
    foreach($settings as $s){
      $setting = Setting::find($s['id']);
      $setting->name = $s['name'];
      $setting->value = $s['value'];
      $setting->save();
    }
    return  Redirect::back()->with('notice', 'Settings have been saved');
  }



}
