<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, MazkaraHelper;
//use Vinelab\Http\Client as HttpClient;

use GuzzleHttp\Client as HttpClient;

use App\Models\Category;
use App\Models\Business;

use App\Http\Controllers\Controller;

class RichController extends Controller {

	/**
	 * Virtual_number_allocation Repository
	 *
	 * @var Virtual_number_allocation
	 */
	protected $business;

	public function __construct(Business $business)
	{
		$this->business = $business;
    $this->layout = 'layouts.admin-crm';    
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){

		$businesses = $this->business->query()->byLocale();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $businesses->search($input['search']);
      $params['name'] = $input['search'];
      $params['search'] = $input['search'];
    }

    if(Input::has('byID') && ($input['byID']!="")){
      $businesses->where('id', 'LIKE', $input['byID']);
      $params['byID'] = $input['byID'];
    }

    if(Input::has('bySlipID') && ($input['bySlipID']!="")){
      $businesses->where('ref', 'LIKE', $input['bySlipID']);
      $params['bySlipID'] = $input['bySlipID'];
    }

    

   if(Input::has('rich') && (!in_array($input['rich'], ['', 'all']))){
      if($input['rich'] == 'rich'){
        $businesses->isRich();
      }else{
        $businesses->isNotRich();
      }
      $params['rich'] = $input['rich'];
    }


    if(Input::has('byLotID') && ($input['byLotID']!="")){
      $businesses->where('lot_id', 'LIKE', $input['byLotID']);
      $params['byLotID'] = $input['byLotID'];
    }

    if(Input::has('sort') && ($input['sort']!='')){
      $params['sort'] = $input['sort'];
    	switch($params['sort']){
        case 'idAsc':
          $businesses->orderBy('id', 'ASC');
        break; 
        case 'idDesc':
          $businesses->orderBy('id', 'DESC');
        break; 
    		case 'nameAsc':
	    		$businesses->orderBy('name', 'ASC');
    		break; 
				case 'nameDesc':
	    		$businesses->orderBy('name', 'DESC');
    		break; 
				case 'lastUpdateAsc':
	    		$businesses->orderBy('updated_at', 'ASC');
    		break; 
				case 'lastUpdateDesc':
	    		$businesses->orderBy('updated_at', 'DESC');
    		break; 
    	}
    }else{
  		$businesses->orderBy('businesses.id', 'DESC');
    }

    $businesses = $businesses->paginate(20);

    return View::make('administration.rich-listings.index', compact('params', 'businesses'));
	}



	public function setRich($id){
		$business = $this->business->findOrFail($id);
    $business->setRich();
    $business->save();
    return Redirect::back()
      ->withInput()
      ->with('notice', $business->name.' marked as rich listings');
	}

  public function setUnrich($id){
    $business = $this->business->findOrFail($id);
    $business->setUnrich();
    $business->save();
    return Redirect::back()
      ->withInput()
      ->with('notice', $business->name.' marked as not rich listings');
  }



}
