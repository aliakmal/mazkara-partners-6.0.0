<?php
namespace App\Http\Controllers\Administration;

use URL, View, Config, Validator, Redirect, Input, Auth, Response;

use App\Models\Business;
use App\Models\Payment;
use App\Models\Invoice;
use App\Models\Activity;


use App\Http\Controllers\Controller;


class PaymentsController extends Controller {

	/**
	 * Payment Repository
	 *
	 * @var Payment
	 */

	protected $payment, $feed_manager;

	public function __construct(Activity $activity, Payment $payment){
    $this->feed_manager = $activity;

		$this->payment = $payment;
	  $this->layout = 'layouts.admin-finance';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(){
		$payments = $this->payment->query();//->byLocale();

    $params = [];
    $input = Input::all();

    if(Input::has('search-invoice') && ($input['search-invoice']!="")){
      $payments->searchByInvoice($input['search-invoice']);
      $params['search-invoice'] = $input['search-invoice'];
    }

    if(Input::has('state') && ($input['state']!="")){
      $payments->byStates($input['state']);
      $params['state'] = $input['state'];
    }

    if(Input::has('merchant_id') && ($input['merchant_id']!="")){
      $payments->byMerchants($input['merchant_id']);
      $params['merchant_id'] = $input['merchant_id'];
    }

		$payments = $payments->orderby('id', 'desc')->paginate(20);
    $all_merchants = \App\Models\Merchant::orderby('name', 'asc')->lists('name', 'id')->all();		
		return  View::make('administration.payments.index', compact('payments', 'all_merchants'));
	}

  public function getPayment(){
    $input = Input::all();
    $payment = Payment::where('id', '=', $input['payment_id'])->get()->first();
    $result = false;
    if($payment){
      $result = ['payment'=>$payment->toArray()];
    }
    return Response::json($result);//::back();
  }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return  View::make('administration.payments.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('photo');
		$validation = Validator::make($input, Payment::$rules);

		if ($validation->passes()){
			$payment = $this->payment->create($input);

      $images = Input::only('photo');
      $payment->saveImage($images['photo']);

			return Redirect::route('admin.payments.index');
		}

		return Redirect::route('admin.payments.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$payment = $this->payment->findOrFail($id);
    $activities = Activity::where('itemable_type', '=', 'Payment')->where('itemable_id', '=', $payment->id)->get();
		return View::make('administration.payments.show', compact('payment', 'activities'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$payment = $this->payment->find($id);

		if (is_null($payment))
		{
			return Redirect::route('admin.payments.index');
		}

		return  View::make('administration.payments.edit', compact('payment'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), 'photo', 'deletablePhotos', '_method');
		$validation = Validator::make($input, Payment::$rules);

		if ($validation->passes())
		{
			$payment = $this->payment->find($id);
			$payment->update($input);
      $deletablePhotos = Input::only('deletablePhotos');
      if($deletablePhotos){
        $payment->removeImage();
      }

      $images = Input::only('photo');
      $payment->saveImage($images['photo']);

			return Redirect::route('admin.payments.show', $id);
		}

		return Redirect::route('admin.payments.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function allocate($id){
		$input = Input::all();
		$payment = $this->payment->find($id);

		//invoices()->attach($input['invoice_id']);
		$invoice = Invoice::find($input['invoice_id']);
    $payment->allocateToInvoice($invoice);

    $data = array('user_id' =>  Auth::user()->id, 
                  'verb'    =>  'allocated', 
                  'itemable_type' =>  'Payment',
                  'itemable_id'   =>  $payment->id);

    $feed = $this->feed_manager->create($data);
    $feed->user_id = Auth::user()->id;
    $feed->verb = 'allocated';
    $feed->itemable_type = 'Payment';
    $feed->itemable_id = $payment->id;
    $feed->meta = ['invoice'=>['id'=>$invoice->id,'title'=>$invoice->title]];
    $feed->save();

		return Redirect::back();
	}

  public function deallocate($id){
    $input = Input::all();
    $payment = $this->payment->find($id);

    //invoices()->attach($input['invoice_id']);
    $invoice = Invoice::find($input['invoice_id']);
    $payment->deallocateFromInvoice($invoice);

    $data = array('user_id' =>  Auth::user()->id, 
                  'verb'    =>  'deallocated', 
                  'itemable_type' =>  'Payment',
                  'itemable_id'   =>  $payment->id);

    $feed = $this->feed_manager->create($data);
    $feed->user_id = Auth::user()->id;
    $feed->verb = 'deallocated';
    $feed->itemable_type = 'Payment';
    $feed->itemable_id = $payment->id;
    $feed->meta = ['invoice'=>['id'=>$invoice->id,'title'=>$invoice->title]];
    $feed->save();

    return Redirect::back();
  }

	public function changeStatusRecievedToDeposited($id){
		$payment = $this->payment->find($id);
		$payment->stateRecievedToDeposited();
		$payment->save();

    $data = array('user_id' =>  Auth::user()->id, 
                  'verb'  	=>  'deposited', 
                  'itemable_type' =>  'Payment',
                  'itemable_id' 	=>  $payment->id);

    $feed = $this->feed_manager->create($data);
    $feed->user_id = Auth::user()->id;
    $feed->verb = 'deposited';
    $feed->itemable_type = 'Payment';
    $feed->itemable_id = $payment->id;
    $feed->save();
    


		return Redirect::back();
	}

	public function changeStatusDepositedToReturned($id){
		$payment = $this->payment->find($id);
		$input = Input::only('reason');
		$payment->stateDepositedToReturned($input['reason']);
		$payment->save();

    $data = array('user_id' =>  Auth::user()->id, 
                  'verb'  	=>  'returned', 
                  'itemable_type' =>  'Payment',
                  'itemable_id' 	=>  $payment->id);

    $feed = $this->feed_manager->create($data);
    $feed->user_id = Auth::user()->id;
    $feed->verb = 'returned';
    $feed->meta = $input;
    $feed->itemable_type = 'Payment';
    $feed->itemable_id = $payment->id;
    $feed->save();
    

		return Redirect::back();
	}

	public function changeStatusReturnedToDeposited($id){
		$payment = $this->payment->find($id);
		$payment->stateReturnedToDeposited();
		$payment->save();

    $data = array('user_id' =>  Auth::user()->id, 
                  'verb'  	=>  'deposited', 
                  'itemable_type' =>  'Payment',
                  'itemable_id' 	=>  $payment->id);

    $feed = $this->feed_manager->create($data);
    $feed->user_id = Auth::user()->id;
    $feed->verb = 'deposited';
    $feed->itemable_type = 'Payment';
    $feed->itemable_id = $payment->id;
    $feed->save();

		return Redirect::back();
	}

	public function changeStatusDepositedToCleared($id){
		$payment = $this->payment->find($id);
		$payment->stateDepositedToCleared();
		$payment->save();


    $data = array('user_id' =>  Auth::user()->id, 
                  'verb'  	=>  'cleared', 
                  'itemable_type' =>  'Payment',
                  'itemable_id' 	=>  $payment->id);

    $feed = $this->feed_manager->create($data);
    $feed->user_id = Auth::user()->id;
    $feed->verb = 'cleared';
    $feed->itemable_type = 'Payment';
    $feed->itemable_id = $payment->id;
    $feed->save();

		return Redirect::back();
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$payment = $this->payment->find($id);
    $invoices = $payment->invoices;

    foreach($invoices as $invoice){
      $payment->deallocateFromInvoice($invoice);
    }

    $payment->delete();

		return Redirect::route('admin.payments.index');
	}

}
