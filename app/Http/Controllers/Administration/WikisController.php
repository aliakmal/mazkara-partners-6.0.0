<?php
namespace App\Http\Controllers\Administration;


use Confide, BaseController, View, Config, Response, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Api\Wiki;
use App\Models\Api\Service;
use App\Models\Api\Merchant as Api_merchant;
use App\Models\Api\Business as Api_business;

use App\Http\Controllers\Controller;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

class WikisController extends Controller {

	/**
	 * Brandadvert Repository
	 *
	 * @var Brandadvert
	 */
	protected $wiki;

	public function __construct(Wiki $wiki)
	{
		$this->wiki = $wiki;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$wikis = $this->wiki->query();
		$services = Service::all();
		$params = Input::all();

		if(isset($params['search']) && !empty($params['search'])){
			$wikis = $wikis->where('description', 'like', '%'.$params['search'].'%')
										->orWhere('title', 'like', '%'.$params['search'].'%')
										->orWhere('service_details', 'like', '%'.$params['search'].'%');
		}

		if(isset($params['service_id']) && !empty($params['service_id'])){
			$wikis = $wikis->where('service_id', '=', $params['service_id']);
		}

		$wikis = $wikis->orderby('id', 'desc')->paginate(20);
		return View::make('administration.wikis.index', compact('wikis', 'services', 'params'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$excludables = \App\Models\Api\Wiki::select()->get()->lists('service_id', 'service_id')->all();
		$services = \App\Models\Api\Service::select()->whereNotIn('id', $excludables)->where('parent_id', '>', 0)->get()->lists('name', 'id')->all();

		return View::make('administration.wikis.create', compact('services'));
	}


	public function store()
	{
		$input = Input::all();
		//dump($input);die();
		$validation = Validator::make($input, Wiki::$rules);

		if ($validation->passes()){
			$data = Input::all();
			$wiki = new \App\Models\Api\Wiki();

			$wiki->description = $data['description'];
			$wiki->title = $data['title'];
			$wiki->service_details = $data['service_details'];
			$display_json = array();

			if(isset($data['display_json']) && isset($data['display_json']['data_array'])){
				foreach($data['display_json']['data_array'] as $js){
					$display_json[] = $js;
				}
			}

			$data['display_json']['data_array'] = $display_json;

			$wiki->display_json = json_encode($data['display_json']);
			$wiki->active = $data['active'];
			$wiki->service_id = $data['service_id'];
			$wiki->cover_image_id = $data['cover_image_id'];
			$wiki->save();

			return Redirect::route('admin.wikis.index');
		}
		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$wiki = $this->wiki->findOrFail($id);
		return View::make('administration.wikis.show', compact('wiki'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$wiki = $this->wiki->find($id);
		$excludables = \App\Models\Api\Wiki::select()->get()->lists('service_id', 'service_id')->all();
		unset($excludables[$wiki->service_id]);
		$services = \App\Models\Api\Service::select()->whereNotIn('id', $excludables)->where('parent_id', '>', 0)->get()->lists('name', 'id')->all();

		if (is_null($wiki)){
			return Redirect::route('admin.wikis.index');
		}

		return View::make('administration.wikis.edit', compact('wiki', 'services'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Wiki::$rules);

		if ($validation->passes()){
			$data = Input::all();
			$wiki = \App\Models\Api\Wiki::find($id);

			$wiki->description = $data['description'];
			$wiki->title = $data['title'];
			$wiki->service_details = $data['service_details'];
			$display_json = array();
			if(isset($data['display_json']) && isset($data['display_json']['data_array'])){
				foreach($data['display_json']['data_array'] as $js){
					$display_json[] = $js;
				}
			}
			$data['display_json']['data_array'] = $display_json;
			$wiki->display_json = json_encode($data['display_json']);
			$wiki->active = $data['active'];
			$wiki->service_id = $data['service_id'];
			$wiki->cover_image_id = $data['cover_image_id'];
			$wiki->save();

			return Redirect::route('admin.wikis.index');
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
			$wiki = $this->wiki->find($id);

			if(!$wiki){
		    return Redirect::back()->with('warning', 'The wiki has been deleted');
			}

			$wiki->delete();

		return Redirect::back();
	}

}
