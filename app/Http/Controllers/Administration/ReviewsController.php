<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, DB, Redirect, Response, Input;

use App\Models\Review;

use App\Http\Controllers\Controller;

class ReviewsController extends Controller {

	/**
	 * Review Repository
	 *
	 * @var Review
	 */
	protected $review;

	public function __construct(Review $review)
	{
		$this->review = $review;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$params = Input::all();
		$reviews = $this->review->select('reviews.*');


		if(isset($params['type']) && ($params['type']!='all')){
			if($params['type']=='reviews'){
				$reviews->isReview();
			}elseif($params['type']=='ratings'){
				$reviews->isRating();
			}
		}

		if(isset($params['users'])&& ($params['users']!=0)){
			$reviews->byUser($params['users']);
		}

    if(isset($params['flags'])&& ($params['flags']!='')){
      $reviews->byFlags($params['flags']);
    }

    if(isset($params['start'])&& ($params['start']!='')){
      $reviews->betweenDates($params['start'], $params['end']);
    }

		if(isset($params['business']) && ($params['business']!=0)){
			$reviews->byBusiness($params['business']);
		}

		$reviews->isNotCheatRate()->byLocale()->orderBy('id', 'DESC');
    $reviews = $reviews->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.reviews.index', compact('reviews', 'params'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.reviews.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = Auth::user()->id;
		$validation = Validator::make($input, Review::$rules);
		if ($validation->passes())
		{
			$this->review->create($input);

			return Redirect::route('admin.businesses.show', array('id'=>$input['business_id']));
		}

		return Redirect::route('admin.businesses.show', array('id'=>$input['business_id']))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$review = $this->review->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.reviews.show', compact('review'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$review = $this->review->find($id);
    $business = $review->business;

    $services = (mzk_get_active_services()) ;
		if (is_null($review))
		{
			return Redirect::route('admin.reviews.index');
		}

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.reviews.edit', compact('review', 'business', 'services'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function hideReview($id){
    $review = $this->review->find($id);
    $review->status = 'inactive';
    $review->save();

    return Redirect::back();
  }

  public function unhideReview($id){
    $review = $this->review->find($id);
    $review->status = 'active';
    $review->save();

    return Redirect::back();
  }

	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		//$validation = Validator::make($input, Review::$rulesForEdit);
		$data = $input;
		unset($data['services']);

		//if ($validation->passes())
		{
			$review = $this->review->find($id);
			$review->update($data);
			$review->services()->sync(isset($input['services']) && is_array($input['services'])?$input['services']:[]);
      $review->flagClear();

			return Redirect::route('admin.reviews.show', $id);
		}

		//return Redirect::route('admin.reviews.edit', $id)
			//->withInput()
			//->withErrors($validation)
			//->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$review = $this->review->find($id);
    $business = $review->business;
		$review->delete();

    $business->updateRatingAndReviewsCount();
		
		return Redirect::back();//('admin.reviews.index');
	}

	public function reports(){
    return View::make('administration.reviews.reports');
	}

	public function data(){
    $input = Input::all();

    $start_date = isset($input['start_date'])?$input['start_date']:\Carbon\Carbon::now()->subDays(10);
    $end_date  = isset($input['end_date'])?$input['end_date']:\Carbon\Carbon::now()->addDays(2);

    $sql = ' SELECT COUNT( reviews.id ) as counts, DATE( reviews.created_at ) as dates ';
    $sql .='FROM reviews inner join businesses on businesses.id = reviews.business_id ';
		$sql .=" WHERE reviews.created_at >= :start_date AND reviews.created_at <= :end_date AND reviews.body <> ' ' ";
    $sql .=" AND reviews.is_cheat <> 1 AND businesses.city_id = :city_id ";
    $sql .=' GROUP BY DATE( reviews.created_at ) ORDER BY reviews.created_at ASC ';
    $d = DB::select(DB::raw($sql) , ['start_date'=>$start_date, 'city_id'=>mzk_get_localeID(), 'end_date'=>$end_date]);
    $result = 	[	'data'=>[],
						    	'total_count'=>0,
						    	'range_count'=>0 ];

    $sql2 = ' SELECT COUNT( reviews.id ) as counts, DATE( reviews.created_at ) as dates ';
    $sql2 .='FROM reviews inner join businesses on businesses.id = reviews.business_id ';
    $sql2 .=" WHERE reviews.created_at >= :start_date AND reviews.created_at <= :end_date AND reviews.body = ' ' ";
    $sql2 .=" AND reviews.is_cheat <> 1 AND businesses.city_id = :city_id ";
    $sql2 .=' GROUP BY DATE( reviews.created_at ) ORDER BY reviews.created_at ASC ';
    $d2 = DB::select(DB::raw($sql2) , ['start_date'=>$start_date, 'city_id'=>mzk_get_localeID(), 'end_date'=>$end_date]);


    foreach($d as $o){
      $dt = strtotime($o->dates)*1000;
    	$result['data'][$dt] = ['dates'=>$dt, 'reviews'=>$o->counts, 'ratings'=>0];
    	$result['range_count']+=$o->counts;
    }

    foreach($d2 as $o){
      $dt = strtotime($o->dates)*1000;
      if(isset($result['data'][$dt])){
        $result['data'][$dt]['ratings'] = $o->counts;
      }else{
        $result['data'][$dt] = ['dates'=>$dt, 'reviews'=>0, 'ratings'=>$o->counts];
      }
      $result['range_count']+=$o->counts;
    }

    if($input['interval']=='week'){
      $c = 7;
      $dd = [];
      $current = 0;
      foreach($result['data'] as $day){
        $c++;
        if($c>=7){
          $c = 0;
          $current = $day['dates'];
          $dd[$current] = $day;
          $dd[$current]['interval'] = 'week';
        }else{
          $dd[$current]['reviews']+= $day['reviews'];
          $dd[$current]['ratings']+= $day['ratings'];
        }
      }
	    $result['data'] = array_values($dd);
    }

    if($input['interval']=='day'){
      $result['data'] = array_values($result['data']);
    }




    $result['avg_count'] = count($result['data']) == 0 ? 0 : ceil($result['range_count']/count($result['data']));

    $result['total_count'] = DB::select(DB::raw('SELECT COUNT(id) as total_reviews FROM reviews'));
		$result['total_count'] = $result['total_count'][0]->total_reviews;

    return Response::Json($result);
	}

}
