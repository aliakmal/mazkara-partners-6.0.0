<?php
namespace App\Http\Controllers\Administration;

use Confide, BaseController, View, Config, Validator, Redirect, Input;
use Highlight, Discount;
use App\Http\Controllers\Controller;

class DiscountsController extends Controller {

	/**
	 * Discount Repository
	 *
	 * @var Discount
	 */
	protected $discount;

	public function __construct(Discount $discount)
	{
		$this->discount = $discount;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$discounts = $this->discount->all();

		//$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('discounts.index', compact('discounts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    $this->layout->content =  View::make('discounts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Discount::$rules);

		if ($validation->passes())
		{
			$discount = $this->discount->create($input);
			$images = Input::only('photos');

			$discount->saveImages($images['photos']);

			return Redirect::route('discounts.index');
		}

		return Redirect::route('discounts.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$discount = $this->discount->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('discounts.show', compact('discount'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$discount = $this->discount->find($id);

		if (is_null($discount))
		{
			return Redirect::route('discounts.index');
		}

		//$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('discounts.edit', compact('discount'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Discount::$rules);

		if ($validation->passes()){
			$discount = $this->discount->find($id);
			$discount->update($input);
			$images = Input::only('photos');
			$discount->saveImages($images['photos']);
			$deletablePhotos = Input::only('deletablePhotos');
			$discount->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

			return Redirect::route('discounts.show', $id);
		}

		return Redirect::route('discounts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->discount->find($id)->delete();

		return Redirect::route('discounts.index');
	}

}
