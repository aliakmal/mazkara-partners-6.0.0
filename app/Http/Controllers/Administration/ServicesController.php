<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, MazkaraHelper ;

use App\Models\Service;
use App\Models\Category;


use App\Http\Controllers\Controller;


class ServicesController extends Controller {

	/**
	 * Service Repository
	 *
	 * @var Service
	 */
	protected $service;

	public function __construct(Service $service)
	{
		$this->service = $service;
    $this->layout = 'layouts.admin-content';

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$services = Service::defaultOrder()->get()->linkNodes();

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.services.index', compact('services'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
		$categories = Category::all();
    return   View::make('administration.services.create', compact('categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Service::$rules);
		$data = Input::only(Service::$fields);

		if ($validation->passes())
		{
			
			$images = Input::only('images');
			$service = $this->service->create($data);
			$service->saveImages($images['images']);
			$cat = Input::only('categories');
			if(count($cat['categories'])>0){
			$service->categories()->attach($cat['categories']?$cat['categories']:[]);
			$service->categories()->sync($cat['categories']?$cat['categories']:[]);
			}

			$service->attachCities();
			$service->zones()->updateExistingPivot(MazkaraHelper::getAdminDefaultLocaleID(), ['state'=>$input['state']]);
			
			mzk_reset_all_service_cache();
			
			return Redirect::route('admin.services.index');
		}

		return Redirect::route('admin.services.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$service = $this->service->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.services.show', compact('service'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$service = $this->service->find($id);

		if (is_null($service))
		{
			return Redirect::route('admin.services.index');
		}
		$current_categories = Category::all();

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.services.edit', compact('service', 'current_categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method', 'deletablePhotos');
		$validation = Validator::make($input, Service::$rules);
		$data = Input::only(Service::$editable_fields);
		if ($validation->passes()){
			$service = $this->service->find($id);
			
			$service->update($data);
			$images = Input::only('images');
			$service->saveImages($images['images']);
			$deletablePhotos = Input::only('deletablePhotos');
			$service->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);
			$cat = Input::only('categories');
			if(count($cat['categories'])>0){
				$service->categories()->attach($cat['categories']?$cat['categories']:[]);
				$service->categories()->sync($cat['categories']?$cat['categories']:[]);
			}

			$service->zones()->updateExistingPivot(MazkaraHelper::getAdminDefaultLocaleID(), ['state'=>$input['state']]);
			$service->zones()->sync([MazkaraHelper::getAdminDefaultLocaleID() => ['state'=>$input['state']]], false);
			
			mzk_reset_all_service_cache();
			
			return Redirect::route('admin.services.show', $id);
		}

		return Redirect::route('admin.services.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this->service->find($id)->delete();

		return Redirect::route('admin.services.index');
	}

}
