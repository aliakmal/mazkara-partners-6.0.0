<?php
namespace App\Http\Controllers\Administration;


use Confide, BaseController, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Advert_campaign;
use App\Models\Advert;
use App\Models\Api\Merchant as Api_Merchant;
use App\Models\Api\Business as Api_Business;

use App\Http\Controllers\Controller;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

class Advert_campaignsController extends Controller {

	/**
	 * Brandadvert_campaign Repository
	 *
	 * @var Brandadvert_campaign
	 */
	protected $advert_campaign;

	public function __construct(Advert_campaign $advert_campaign)
	{
		$this->advert_campaign = $advert_campaign;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$advert_campaigns = $this->advert_campaign->query();

		$params = Input::all();

		if(isset($params['search']) && !empty($params['search'])){
			$advert_campaigns = $advert_campaigns->where('description', 'like', '%'.$params['search'].'%');
		}

		if(isset($params['merchant_id']) && !empty($params['merchant_id'])){
			$advert_campaigns = $advert_campaigns->where('merchant_id', '=', $params['merchant_id']);
		}


		// $merchant_ids = $advert_campaigns->lists('merchant_id','merchant_id')->all();

		$merchants = Api_Merchant::select()->byLocale()->get()->lists('name', 'id')->all();
		$merchant_ids = Api_Merchant::select()->byLocale()->get()->lists('id', 'id')->all();

		$advert_campaigns = $advert_campaigns->whereIn('merchant_id', $merchant_ids)->orderby('id', 'desc')->paginate(20);

		//->whereIn('id', $merchant_ids)->get()->lists('name', 'id')->all();



		return View::make('administration.advert_campaigns.index', compact('advert_campaigns', 'merchants', 'params'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$merchants = Api_Merchant::select()->byLocale()->orderby('name', 'asc')->get()->lists('displayable', 'id')->all();
		return View::make('administration.advert_campaigns.create', compact('merchants'));
	}




	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Advert_campaign::$rules);

		if ($validation->passes())
		{
			$data = Input::only(Advert_campaign::$fields);
			$advert_campaign = new Advert_campaign();
			foreach($data as $i=>$v){
				$advert_campaign->$i = $v;
			}

			//$advert_campaign = $this->advert_campaign->create($data);
			$advert_campaign->save();
			return Redirect::route('admin.advert_campaigns.index');
		}

		return Redirect::route('admin.advert_campaigns.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$advert_campaign = $this->advert_campaign->findOrFail($id);
		$adverts = \App\Models\Advert::select()->where('campaign_id', '=', $id)->get();
		$adverts = \App\Models\Advert::select(\DB::raw('*, group_concat(slot) as slots, group_concat(id) as ids, group_concat(service_id) as service_ids, group_concat(zone_id) as zone_ids'))->where('campaign_id', '=', $id)
										->groupBy('start_date', 'end_date', 'slot')->get();


		$advertz = \App\Models\Advert::select()->where('campaign_id', '=', $id)->get();

		$venue_ids = $advertz->lists('business_id','business_id')->all();
		$zone_ids = $advertz->lists('zone_id','zone_id')->all();
		$service_ids = $advertz->lists('service_id','service_id')->all();

		$zones = \App\Models\Api\Zone::select()->whereIn('id', $zone_ids)->lists('name', 'id')->all();
		$services = \App\Models\Api\Service::select()->whereIn('id', $service_ids)->lists('name', 'id')->all();
		$venues = \App\Models\Api\Business::select()->whereIn('id', $venue_ids)->lists('name', 'id')->all();

		return View::make('administration.advert_campaigns.show', compact('advert_campaign', 'adverts','zones','services','venues'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$advert_campaign = $this->advert_campaign->find($id);
		$merchants = Api_Merchant::select()->byLocale()->orderby('name', 'asc')->get()->lists('displayable', 'id')->all();

		$last_date_advert = \App\Models\Advert::where('campaign_id', '=', $id)->orderby('end_date', 'desc')->first();
		$first_date_advert = \App\Models\Advert::where('campaign_id', '=', $id)->orderby('start_date', 'asc')->first();

		$start_at = $first_date_advert->start_date;
		$end_at = $first_date_advert->end_date;
		if (is_null($advert_campaign)){
			return Redirect::route('admin.advert_campaigns.index');
		}

		return View::make('administration.advert_campaigns.edit', compact('merchants', 'start_at', 'end_at', 'advert_campaign'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Advert_campaign::$rules);

		if ($validation->passes())
		{
			$advert_campaign = $this->advert_campaign->find($id);

			$data = Input::only(Advert_campaign::$fields);

			$advert_campaign->update($data);

			return Redirect::route('admin.advert_campaigns.show', $id);
		}

		return Redirect::route('admin.advert_campaigns.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$advert_campaign = $this->advert_campaign->find($id);
		if(!$advert_campaign){
	    return Redirect::back()->with('warning', 'The advert_campaign has already been deleted');
		}

		Advert::where('campaign_id', '=', $id)->delete();

		$advert_campaign->delete();

		return Redirect::route('admin.advert_campaigns.index');
	}

}
