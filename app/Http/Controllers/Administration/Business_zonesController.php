<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\User;
use App\Models\Zone;
use App\Models\AdSet;
use App\Models\Ad;
use App\Models\Business_zone;

use App\Http\Controllers\Controller;

class Business_zonesController extends Controller {

	/**
	 * Business_zone Repository
	 *
	 * @var Business_zone
	 */
	protected $business_zone, $zone;

	public function __construct(Business_zone $business_zone, Zone $zone)
	{
		$this->business_zone = $business_zone;
		$this->zone = $zone;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$business_zones = $this->business_zone->query();//->byLocale();
		$business_zones = $business_zones->orderby('id', 'desc')->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.business_zones.index', compact('business_zones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$zones = $this->zone->defaultOrder()->hasNoBusinessZone()->get()->linkNodes();
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.business_zones.create', compact('zones'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Business_zone::$rules);

		if ($validation->passes())
		{
			$data = Input::except('zones');
			$business_zone = $this->business_zone->create($data);
			$zones = Input::only('zones');
			$business_zone->setCity();
			$business_zone->save();

			$business_zone->updateAssociatedZones($zones['zones']?$zones['zones']:[]);


			return Redirect::route('admin.business_zones.index');
		}

		return Redirect::route('admin.business_zones.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$business_zone = $this->business_zone->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.business_zones.show', compact('business_zone'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$business_zone = $this->business_zone->find($id);
		$zones = $this->zone->defaultOrder()->hasNoBusinessZone($id)->get()->linkNodes();
		if (is_null($business_zone))
		{
			return Redirect::route('admin.business_zones.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.business_zones.edit', compact('business_zone','zones'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Business_zone::$rules);

		if ($validation->passes())
		{
			$data = Input::except('zones');
			$zones = Input::only('zones');

			$business_zone = $this->business_zone->find($id);
			$business_zone->update($data);

			$business_zone->updateAssociatedZones($zones['zones']?$zones['zones']:[]);

			return Redirect::route('admin.business_zones.show', $id);
		}

		return Redirect::route('admin.business_zones.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->business_zone->find($id)->delete();

		return Redirect::route('admin.business_zones.index');
	}

}
