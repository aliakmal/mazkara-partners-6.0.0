<?php
namespace App\Http\Controllers\Administration;

use Confide, BaseController, View, Response, Config, Validator;
use Combo_item, Service_item, Business, Comment, Redirect, Input;
use App\Http\Controllers\Controller;

class Combo_itemsController extends Controller {

	/**
	 * Combo_item Repository
	 *
	 * @var Combo_item
	 */
	protected $combo_item;

	public function __construct(Combo_item $combo_item)
	{
		$this->combo_item = $combo_item;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$combo_items = $this->combo_item->all();

		return View::make('combo_items.index', compact('combo_items'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('combo_items.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Combo_item::$rules);

		if ($validation->passes())
		{
			$combo = $this->combo_item->create(Input::except('item_ids'));
			$items = Input::only('item_ids');
			$items = count($items['item_ids'])>0 ? $items['item_ids']:[];
			$combo->items()->sync($items);
			$items = $combo->items;
			$combo->toArray();
			$combo['items'] = $items;

			$combo['all_groups'] = (['0'=>'Enter New Group'] + Service_item::query()
                                                                        ->byBusiness($combo['business_id'])
                                                                        ->groupby('grouping')->get()
                                                                        ->lists('grouping','grouping')
                                                            + Combo_item::query()
                                                                ->byBusiness($combo['business_id'])
                                                                ->groupby('grouping')->get()
                                                                ->lists('grouping','grouping')

                              ); 

			return Response::json($combo);//::back();

		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$combo_item = $this->combo_item->findOrFail($id);

		return View::make('combo_items.show', compact('combo_item'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$combo_item = $this->combo_item->find($id);

		if (is_null($combo_item))
		{
			return Redirect::route('combo_items.index');
		}

		return View::make('combo_items.edit', compact('combo_item'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Combo_item::$rules);

		if ($validation->passes())
		{
			$combo_item = $this->combo_item->find($id);
			$combo_item->update($input);

			return Redirect::route('combo_items.show', $id);
		}

		return Redirect::route('combo_items.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}
	public function updatable()
	{
		$input = Input::all();

		$data = ['id' => $input['pk']];
		$data[$input['name']] = $input['value'];
		{

			$combo = $this->combo_item->find($data['id']);
			if(isset($data['items'])){
			$items = count($data['items'])>0 ? $data['items']:[];
			$combo->items()->sync($items);
			}else{
				$combo->update($data);

			}

			return Redirect::back();
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->combo_item->find($id)->delete();

		return Redirect::route('combo_items.index');
	}

}
