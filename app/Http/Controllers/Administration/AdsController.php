<?php
namespace App\Http\Controllers\Administration;


use View, Config, Validator, Redirect, Input;

use App\Models\Ad;	
use App\Models\AdSet;	
use App\Models\User;	

use App\Http\Controllers\Controller;


class AdsController extends Controller {

	/**
	 * Ad Repository
	 *
	 * @var Ad
	 */
	protected $ad;

	public function __construct(Ad $ad)
	{
		$this->ad = $ad;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ads = $this->ad->orderby('ad_set_id', 'desc')->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ads.index', compact('ads'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.ads.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Ad::$rules);

		if ($validation->passes())
		{
			$data = Input::except('photo');
			$ad = $this->ad->create($data);
			$ad->saveImage($input['photo']);

			$ad->setCity();
			$ad->save();

			return Redirect::route('admin.ads.index');
		}

		return Redirect::route('admin.ads.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ad = $this->ad->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.ads.show', compact('ad'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ad = $this->ad->find($id);

		if (is_null($ad))
		{
			return Redirect::route('admin.ads.index');
		}

		return View::make('administration.ads.edit', compact('ad'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Ad::$rules);

		if ($validation->passes())
		{
			$data = Input::except('photo', '_method', 'deletablePhotos');
			$ad = $this->ad->find($id);
			$ad->update($data);
			$deletable = Input::get('deletablePhotos');
			if(count($deletable)>0){
				$ad->removeImage();
			}
			$ad->saveImage($input['photo']);


			return Redirect::route('admin.ads.show', $id);
		}

		return Redirect::route('admin.ads.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ad->find($id)->delete();

		return Redirect::route('admin.ads.index');
	}

}
