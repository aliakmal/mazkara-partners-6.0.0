<?php
namespace App\Http\Controllers\Administration;

use BaseController, View, Config, Validator, Redirect, Input;
use App\Models\Photo, MazkaraHelper, Response;
use App\Http\Controllers\Controller;

class PhotosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    return View::make('photos.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('photos.create');
	}

	public function upload()
	{
    $input = Input::all();
		$photo = new Photo(['type'=>Photo::ORIGINAL]);
  	$photo->image = $input['file'];
  	$photo->type = Photo::IMAGE;
  	$photo->save();
    $data = ['url'=>$photo->image->url()];
    return Response::json($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('photos.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('photos.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	public function changeType(){
		$input = Input::all();
		$photo = Photo::find($input['id']);
		$photo->type = $input['type'];
		$photo->save();
		return Response::Json([]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
