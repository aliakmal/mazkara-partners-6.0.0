<?php
namespace App\Http\Controllers\Administration;

use Confide, BaseController, View, Config, Validator, Redirect, Input;
use Service, Special_package;
use App\Http\Controllers\Controller;

class Special_packagesController extends Controller {

	/**
	 * Special_package Repository
	 *
	 * @var Special_package
	 */
	protected $special_package;

	public function __construct(Special_package $special_package)
	{
		$this->special_package = $special_package;
    $this->layout = 'layouts.admin-content';

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$special_packages = $this->special_package->all();

		//$this->layout = View::make('layouts.admin');
    $this->layout->content =  View::make('administration.special_packages.index', compact('special_packages'));
	}

	public function setOrder(){
    $orderables = Input::only('orderables');
    foreach($orderables['orderables'] as $p_id=>$p_order){
      $p = Special_package::find($p_id);
      $p->sort = $p_order;
      $p->save();
    }

		return Redirect::back();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    $this->layout->content =  View::make('administration.special_packages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('photo', 'services');
		$validation = Validator::make($input, Special_package::$rules);

		if ($validation->passes())
		{
			$special_package = $this->special_package->create($input);
      $images = Input::only('photo');
      $special_package->saveImage($images['photo']);

			$services = Input::only('services');
			$special_package->services()->attach($services['services']);
			$special_package->services()->sync($services['services']?$services['services']:[]);
			$special_package->business->updatePackagesCount();
			
			return Redirect::back();//('admin.special_packages.index');
		}

		return Redirect::back()//route('admin.special_packages.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function bulk(){

		$input = Input::only('packages');
			$data = Input::except('packages');

		foreach($input['packages'] as $image){

			$special_package = $this->special_package->create($data);
			$special_package->saveImage($image);
		}
			$special_package->business->updatePackagesCount();

		return Redirect::back();//('admin.special_packages.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$special_package = $this->special_package->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    $this->layout->content =  View::make('administration.special_packages.show', compact('special_package'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$special_package = $this->special_package->find($id);

		if (is_null($special_package))
		{
			return Redirect::route('admin.special_packages.index');
		}

		$this->layout = View::make('admin.layouts.admin');
    $this->layout->content =  View::make('administration.special_packages.edit', compact('special_package'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Special_package::$rules);

		if ($validation->passes())
		{
			$special_package = $this->special_package->find($id);
			$special_package->update($input);

			return Redirect::route('admin.special_packages.show', $id);
		}

		return Redirect::route('admin.special_packages.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$special_package = $this->special_package->find($id);
		$special_package->services()->detach();
		$business = $special_package->business;
		$special_package->delete();
		$business->updatePackagesCount();

		return Redirect::back();//('admin.special_packages.index');
	}

}
