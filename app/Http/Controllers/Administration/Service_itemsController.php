<?php
namespace App\Http\Controllers\Administration;

use View, Config, Response, Validator, Redirect, Input;

use App\Models\Service_item;
use App\Models\Combo_item;


use App\Http\Controllers\Controller;

class Service_itemsController extends Controller {

	/**
	 * Service_item Repository
	 *
	 * @var Service_item
	 */
	protected $service_item;

	public function __construct(Service_item $service_item)
	{
		$this->service_item = $service_item;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$service_items = $this->service_item->all();

		return View::make('service_items.index', compact('service_items'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('service_items.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Service_item::$rules);
		$data = Input::except('services');
		$service_item = $this->service_item->create($data);
		$services = Input::only('services');//$data['']
		$services = count($services['services'])>0 ? $services['services']:[];
		//dump($services);
		$service_item->services()->sync($services);
		$services = $service_item->services()->get()->lists('name', 'name');
		$service_item->toArray();
		$service_item['services'] = join(',', $services);
		return Response::json($service_item);//::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$service_item = $this->service_item->findOrFail($id);

		return View::make('service_items.show', compact('service_item'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$service_item = $this->service_item->find($id);

		if (is_null($service_item))
		{
			return Redirect::route('service_items.index');
		}

		return View::make('service_items.edit', compact('service_item'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Service_item::$rules);

		if ($validation->passes())
		{
			$service_item = $this->service_item->find($id);
			$service_item->update($input);

			return Redirect::back();
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function updatable()
	{
		$input = Input::all();

		$data = ['id' => $input['pk']];
		$data[$input['name']] = $input['value'];
		{
			$service_item = $this->service_item->find($data['id']);
			$service_item->update($data);

			return Redirect::back();
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->service_item->find($id)->delete();

		//return Redirect::back();
	}


	public function listTags()
	{
		$tag = Input::get('term');
		$tags = \Conner\Tagging\Tagged::where('tag_name', "like", "%".$tag."%")
										->join('service_items', 'tagging_tagged.taggable_id', '=', 'service_items.id')
										->get();
		$resp = [];
		foreach ($tags as $tag) {
			$tmp['id'] = $tag->id;
			$tmp['value']= $tag->tag_name;
			$tmp['service_id'] = $tag->service_id;
			$resp[] = $tmp;
		}
		return Response::json($resp);
		
	}
	
}
