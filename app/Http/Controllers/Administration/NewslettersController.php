<?php

namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;

use App\Http\Controllers\Controller;

class NewslettersController extends Controller {

	/**
	 * Comment Repository
	 *
	 * @var Comment
	 */


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->layout = View::make('layouts.admin-crm');
    return View::make('administration.newsletters.index');
	}


	public function weeklyPosts()
	{
    $user = User::select()->first();//User::where('email', '=', 'ali@fabogo.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()->orderBy('published_on', 'DESC')->take(5)->get();

		$user->name = "{{NAME}}";

    return View::make('emails.weekly.stories', compact('user', 'posts'));
	}

	public function customPosts()
	{
		$input = Input::all();
    $user = User::select()->first();//User::where('email', '=', 'ali@fabogo.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()
    														->orderBy('published_on', 'DESC')
    														->whereIn('id', explode(',', $input['ids']))->get();

		$user->name = "{{NAME}}";

    return View::make('emails.weekly.stories', compact('user', 'posts'));
	}



}
