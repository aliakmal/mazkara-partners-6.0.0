<?php
namespace App\Http\Controllers\Administration;


use Confide, BaseController, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Brand;
use App\Models\AdSet;
use App\Models\Ad;

use App\Http\Controllers\Controller;


class BrandsController extends Controller {

	/**
	 * Brand Repository
	 *
	 * @var Brand
	 */
	protected $brand;

	public function __construct(Brand $brand)
	{
		$this->brand = $brand;
    $this->layout = 'layouts.admin-content';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$brands = $this->brand->all();

		return View::make('administration.brands.index', compact('brands'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('administration.brands.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Brand::$rules);

		if ($validation->passes())
		{
			$brand = $this->brand->create(Input::only(Brand::$fields));
			
			$brand->saveLogo(isset($input['logo'])?$input['logo']:null);

			return Redirect::route('admin.brands.index');
		}

		return Redirect::route('admin.brands.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$brand = $this->brand->findOrFail($id);
		return View::make('administration.brands.show', compact('brand'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$brand = $this->brand->find($id);

		if (is_null($brand)){
			return Redirect::route('admin.brands.index');
		}

		return View::make('administration.brands.edit', compact('brand'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Brand::$rules);

		if ($validation->passes())
		{
			$brand = $this->brand->find($id);
			$brand->update(Input::only(Brand::$fields));
			
			$brand->saveLogo(isset($input['logo'])?$input['logo']:null);

			return Redirect::route('admin.brands.show', $id);
		}

		return Redirect::route('admin.brands.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->brand->find($id)->delete();

		return Redirect::route('admin.brands.index');
	}

}
