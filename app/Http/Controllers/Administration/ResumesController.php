<?php
namespace App\Http\Controllers\Administration;


use Confide, BaseController, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Resume;

use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;

class ResumesController extends Controller {

	/**
	 * resume Repository
	 *
	 * @var resume
	 */
	protected $resume;

	public function __construct(Resume $resume)
	{
		$this->resume = $resume;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$nationality = mzk_get_countries_list();

		$available_nationalities = $this->resume->get()->lists('nationality', 'nationality')->all();

		$ns = array_intersect(array_keys($nationality), array_values($available_nationalities));
		
		$nationalities = [];
		foreach($ns as $vv){
			$nationalities[$vv] = isset($nationality[$vv])?$nationality[$vv]:$vv;
		}


		$resumes = $this->resume->query()->byLocale();

		$params = Input::all();
		$bs = \App\Models\Business::select()->resumesAccessible()->byLocale()->get();


		$specializations = $this->resume->getSpecializations();
		$experiences = $this->resume->getExperiences();

		$businesses = array();
		foreach($bs as $business){
			$businesses[$business->id] = $business->id.' - '.$business->name.'('.$business->zone_cache.')';
		}



		if(isset($params['search']) && !empty($params['search'])){
			$resumes = $resumes->where('name', 'like', '%'.$params['search'].'%');
		}
		if(isset($params['location']) && !empty($params['location'])){
			$resumes = $resumes->where('location', 'like', $params['location'].'%');
		}
		if(isset($params['experience']) && !empty($params['experience'])){
			$resumes = $resumes->where('experience', '=', $params['experience']);
		}

		if(isset($params['specialization']) && !empty($params['specialization'])){
			$resumes = $resumes->bySpecializations([$params['specialization']]);
		}
		if(isset($params['nationality']) && !empty($params['nationality'])){
			$resumes = $resumes->where('nationality', 'like', $params['nationality'].'%');
		}

		$resumes = $resumes->orderby('created_at', 'desc')->paginate(20);;
		return View::make('administration.resumes.index', compact('resumes', 'nationalities', 'specializations', 'experiences', 'params', 'businesses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		$specializations = $this->resume->getSpecializations();
		$experiences = $this->resume->getExperiences();
		$nationalities = mzk_get_countries_list();
		// dump($experiences);die();
		return View::make('administration.resumes.create', compact('specializations', 'nationalities','experiences'));
	}

	public function attachResumes()
	{
    $input = Input::all();
    if(isset($input['resume_ids']) && (strlen($input['resume_ids'])>0)){
			$business = \App\Models\Business::find($input['business']);
			$business->resumes()->detach(explode(',', $input['resume_ids']));

			$business->resumes()->attach(explode(',', $input['resume_ids']), ['allocated_at'=>\Carbon\Carbon::now()]);

	    return Redirect::back()->with('notice', 'Success - '.count($input['resume_ids']).' resumes allocated to '.$business->name );
    }

    return Redirect::back()->with('warning', 'Select resumes to import');
	}


	public function getImport(){
		$bs = \App\Models\Business::select()->resumesAccessible()->byLocale()->get();

		$businesses = array();
		foreach($bs as $business){
			$businesses[$business->id] = $business->id.' - '.$business->name.'('.$business->zone_cache.')';
		}

		return View::make('administration.resumes.import', compact('businesses'));
	}

	public function postImport(Request $request){
    $path_name = storage_path().'/media/'.'csv/';
    $file_name = 'csv-'.time().'.csv';
    $request->file('csv')->move($path_name, $file_name);
    dump($path_name);
    $csv = Excel::load($path_name.$file_name, function($reader) {
      })->get();
    $user_id = \Auth::user()->id;

    $resume_ids = array();

    foreach($csv as $row){
    	$data = array('name'=>$row->name, 
    								'phone'=>$row->phone, 
    								'interested_in'=>$row->interested_in, 
    								'email'=>$row->email, 
    								'uploaded_by'=>$user_id);
    	$resume = \App\Models\resume::create($data);
			$resume->setCity();
			$resume->save();

    	$resume_ids[] = $resume->id;
    }
    $input = Input::all();
    if(count($input['businesses'])>0){
  		foreach($input['businesses'] as $business_id){
  			$business = \App\Models\Business::find($business_id);
  			$business->resumes()->attach($resume_ids, ['allocated_at'=>\Carbon\Carbon::now()]);
  		}
    }

    return Redirect::back()->with('message', 'You have imported '.count($resume_ids).' resumes.'.(count($input['businesses'])>0?' Into '.count($input['businesses']).' venues':'' ));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules = Resume::$rules;
		$rules['doc'] = 'required|mimes:doc,pdf,docx,odt';
		$validation = Validator::make($input, $rules);
		//dump($input);die();
		if ($validation->passes())
		{
			$data =Input::only(Resume::$fields);
			foreach(Resume::$fields as $f){
				$data[$f] = isset($input[$f])?$input[$f]:'';
			}
			$data['uploaded_by'] = \Auth::user()->id;

			$resume = $this->resume->create($data);
			$resume->setCity();
			$resume->doc = $input['doc'];
			$specializations = $input['specializations'];
			$s_data = [];
			foreach($specializations as $specialization){
				$s_data[] = ['resume_id'=>$resume->id, 'specialization'=>$specialization];
			}
			$resume->save();

			\App\Models\Specialization::insert($s_data);


			return Redirect::route('admin.resumes.index');
		}

		return Redirect::route('admin.resumes.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$resume = $this->resume->findOrFail($id);
		return View::make('administration.resumes.show', compact('resume'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$resume = $this->resume->find($id);
		$nationalities = mzk_get_countries_list();

		if (is_null($resume)){
			return Redirect::route('admin.resumes.index');
		}
		$specializations = $this->resume->getSpecializations();
		$experiences = $this->resume->getExperiences();

		return View::make('administration.resumes.edit', compact('resume', 'nationalities', 'specializations', 'experiences'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$rules = Resume::$rules;
		$rules['doc'] = 'mimes:doc,pdf,docx,odt';
		$validation = Validator::make($input, $rules);


		if ($validation->passes())
		{
			$resume = $this->resume->find($id);

			$data =Input::only(Resume::$fields);
			unset($data['city_id']);
			$data['uploaded_by'] = \Auth::user()->id;

			$resume->update($data);

			$resume->specializations()->delete();

			$specializations = isset($input['specializations'])?$input['specializations']:[];
			$s_data = [];
			foreach($specializations as $s){
				$s_data[] = ['resume_id'=>$resume->id, 'specialization'=>$s];

			}

			\App\Models\Specialization::insert($s_data);

			$resume->save();

			return Redirect::route('admin.resumes.show', $id);
		}

		return Redirect::route('admin.resumes.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->resume->find($id)->deleteBasic();

		return Redirect::route('admin.resumes.index');
	}

}
