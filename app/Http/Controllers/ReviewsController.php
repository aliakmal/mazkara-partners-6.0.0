<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, Mail, URL, View, Redirect, Response;
use Location, Request, Validator, Lang;
use MazkaraHelper;
use App\Http\Controllers\Controller;

use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Review;
use App\Models\Activity;
use App\Models\User;
use App\Models\Favorite;

class ReviewsController extends Controller {

	/**
	 * Review Repository
	 *
	 * @var Review
	 */
	protected $review;

	public function __construct(Review $review, Activity $activity)
	{
		$this->review = $review;
    $this->feed_manager = $activity;
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	* public function index()
	* {
	* 	$reviews = $this->review->all();
*
* 	* 	return View::make('reviews.index', compact('reviews'));
	* }
	 */

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reviews.create');
	}

	public function getForBusiness(){
		$input = Input::all();
		$count = isset($input['count'])?$input['count']:3;

		$reviews = Review::isCompleteReview()->byBusiness($input['business_id'])->orderby('id', 'desc')->paginate($count);
		$result = [];
		$view = View::make('site.reviews.subset', compact('reviews'));
		$result['html'] = $view->render();

		return Response::Json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = Auth::user()->id;
		$validation = Validator::make($input, Review::$rules);
		MazkaraHelper::clearCurrentPageCacheName(URL::previous());
		if ($validation->passes()){
			$data = $input;
			unset($data['services']);
			// double check has the review been added before by accident
			if($this->review->byBusiness($input['business_id'])->byUser($input['user_id'])->count()>0){
				$review = $this->review->byBusiness($input['business_id'])->byUser($input['user_id'])->get()->first();
				$review->update($data);
			}else{
				$review = $this->review->create($data);
			}

			$review->flagPending();
			$review->save();
			$review->services()->sync(isset($input['services']) && is_array($input['services'])?$input['services']:[]);

      $data = array('user_id' =>  $input['user_id'], 
                    'verb'  =>  (trim($input['body'])!=""?'reviewed':'rated'), 
                    'itemable_type' =>  'Review', 
                    'itemable_id' =>  $review->id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $input['user_id'];
      $feed->verb = 'reviewed'; 
      $feed->itemable_type = 'Review';
      $feed->itemable_id = $review->id;
      $feed->save();
      $user = User::find(Auth::user()->id);
      $d = ['user'=>$user->toArray(), 
      			'review'=>$review->toArray(), 
      			'business'=>Business::find($review->business_id)];

//			Mail::send('emails.review-new', ['data'=>$d, 'server'=>$_SERVER], function($message) {
//				$message->to('reviews@mazkara.com', 'Mazkara')
//                ->subject('Mazkara['.MazkaraHelper::getLocaleLabel().']: New Review Added ['.time().']');
//			});

			if(Request::ajax()){
				$view = View::make('site.reviews.success');
				$result = array();
				$result['html'] = $view->render();
				return Response::Json($result);
			}else{
				return Redirect::back();//route('businesses.show', array('id'=>$input['business_id']));
			}
		}

		return Redirect::back()//route('businesses.show', array('id'=>$input['business_id']))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$review = $this->review->findOrFail($id);

		return View::make('reviews.show', compact('review'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$review = $this->review->find($id);

		if (is_null($review))
		{
			return Redirect::route('reviews.index');
		}

		return View::make('reviews.edit', compact('review'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), 'services', '_method');
		$input['user_id'] = Auth::user()->id;
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		$validation = Validator::make($input, Review::$rules);

		if ($validation->passes()){
			$review = $this->review->find($id);
			$review->update($input);
			$review->flagPending();

			$input = Input::only('services');
			$review->services()->sync(isset($input['services']) && is_array($input['services'])?$input['services']:[]);
			if(Request::ajax()){
				$view = View::make('site.reviews.success');
				$result = array();
				$result['html'] = $view->render();
				return Response::Json($result);

			}else{
				return Redirect::back();//('reviews.show', $id);
			}
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$review = $this->review->find($id);
    $business = Business::find($review->business_id);
		$review->delete();
    $business->updateRatingAndReviewsCount();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		return Redirect::back();//('reviews.index');
	}

}
