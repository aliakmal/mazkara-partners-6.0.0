<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Review, Timing, Highlight, Photo;
use User,  Response, Zone, Comment;

class CommentsController extends \BaseController {
  use ControllerTrait;

	/**
	 * Comment Repository
	 *
	 * @var Comment
	 */
	protected $comment;

	public function __construct(Comment $comment)
	{
		$this->comment = $comment;
	}

	public function index(){
		$input = Input::all();
		$count = isset($input['count'])?$input['count']:10;
		$commentable = isset($input['commentable'])?$input['commentable']:false;
		$commentable_id = isset($input['commentable_id'])?$input['commentable_id']:false;

		$this->comment = $this->comment->with(array('user'=>function($query){
      $query->select("id", "username","email","name","phone","designation","designated_at_text","designated_at_url","contact_email_address","location");
    }));

		if($commentable){
			$this->comment->where('commentable_type', '=', $commentable);
		}

		if($commentable_id){
			$this->comment->where('commentable_id', '=', $commentable_id);
		}

		$comments = $this->comment->orderBy('id', 'DESC')->paginate($count);
    foreach($comments as &$comment){
      $comment->user->photo = '';//['user']['photo'] =  '';
      $user = $comment->user;
      if(count($user->avatar)>0){
        $comment->user->photo = $user->avatar->image->url('small');
      }
      unset($comment->user->avatar);
    }
    //foreach($posts as $ii=>$post){
    //  $posts[$ii]->cover_url = $post->cover_url;//?$post->cover->image->url():$post->cover;
    //}

    $data = $comments->toArray();
    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Comment::$rules);

		if ($validation->passes())
		{
			$input['user_id'] = $this->getUserIdFromApi();
      unset($input['action']);
      unset($input['key']);
			$comment = $this->comment->create($input);
      $comment = $this->comment->with(array('user'=>function($query){
                    $query->select("id", "username","email","name","phone","designation","designated_at_text","designated_at_url","contact_email_address","location");
                  }))->where('id', '=', $comment->id)->first();

      $comment->user->photo = '';//['user']['photo'] =  '';
      $user = $comment->user;
      if(count($user->avatar)>0){
        $comment->user->photo = $user->avatar->image->url('small');
      }
      unset($comment->user->avatar);

      $result = mzk_api_response($comment->toArray(), 200, true, 'Success - You added a Comment');
    }else{
      $result = mzk_api_response([], 200, false, $validation->errors());
    }

    return $this->response->array($result);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		$comment = $this->comment->find($id);

		if (is_null($comment))
		{
			return Redirect::back();//route('admin.comments.index');
		}

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.comments.edit', compact('comment'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = array_except(Input::all(), '_method');
    $id = $input['id'];
		$validation = Validator::make($input, Comment::$rules);

		if ($validation->passes())
		{
			$comment = $this->comment->find($id);
			$comment->update($input);
      $comment = $this->comment->with(array('user'=>function($query){
                    $query->select("id", "username","email","name","phone","designation","designated_at_text","designated_at_url","contact_email_address","location");
                  }))->where('id', '=', $comment->id)->first();

      $comment->user->photo = '';//['user']['photo'] =  '';
      $user = $comment->user;
      if(count($user->avatar)>0){
        $comment->user->photo = $user->avatar->image->url('small');
      }
      unset($comment->user->avatar);

      $result = mzk_api_response($comment->toArray(), 200, true, 'Success - You added a Post');
    }else{
      $result = mzk_api_response([], 200, false, $validation->errors());
    }

    return $this->response->array($result);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function destroy()
  {
    $input = Input::all();
    $comment = $this->comment->find($input['id']);
    if($this->getUserIdFromApi() == $comment->user_id){
      $result = mzk_api_response([], 200, true, 'Permission denied - Comment not deleted');
    }else{
      $comment->delete();
      $result = mzk_api_response([], 200, true, 'Success - You deleted your comment');
    }
    return $this->response->array($result);
  }

  protected function getUserIdFromApi($key = false){
    if(!$key){
      $input = Input::all();
      $key = $input['key'];
    }

    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }
  }

}
