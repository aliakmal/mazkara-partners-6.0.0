<?php

namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Service, Timing, Highlight, Photo;
use User, Response, Zone, Offer;

class OffersController extends \BaseController {
  use ControllerTrait;

  public function getIndex(){

    $input = Input::all();
    $offers = Offer::select()->with('business')->onlyActive();

    $offers = $offers->paginate(20);
    $result = [];

    $result = mzk_api_response(['offers'=>$offers], 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
  }


  public function getShow(){

    $input = Input::all();

    $offer  = Offer::find($input['id']);
   
    $result = mzk_api_response(compact('offer'), 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
  }


}