<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Review, Timing, Highlight, Photo;
use User,  Response, Zone;

class ReviewsController extends \ReviewsController {
  use ControllerTrait;

	public function getReviewsIndex(){

    $input = Input::all();
    $reviews = $this->review->select()->with('business')->isReview();

    if(isset($input['business_id'])){
      $reviews->byBusiness($input['business_id']);
    }

    if(isset($input['user_id'])){
      $reviews->byUser($input['user_id']);
    }

    if(isset($input['my'])){
      $key = Input::get('key');
      $apiKey = ApiKey::where('key', '=', $key);
      $user_id = $apiKey->first()->user_id;

      $reviews->byUser($user_id);
    }

    $reviews = $reviews->paginate(20);
    $result = [];
    foreach($reviews as $ii=>$review){
      if($review->user_id > 0){
        $user = $review->user;
        $business = $review->business;

        $f = ["description","phone","email","website","geolocated","zone_id","geolocation_city","geolocation_state","geolocation_country","geolocation_address","created_at","updated_at",
"slug","active","facebook","twitter","instagram","google","chain_id","landmark","created_by_id","updated_by_id","deleted_by_id","geolocation_longitude",
"geolocation_latitude","rate_card_count","image_count","reviews_count","favorites_count","checkins_count","services_count","categories_count","highlights_count","timings_count","zone_cache","rating_average","active_deals_count",
"total_deals_count","total_packages_count","active_packages_count","total_ratings_count","cost_estimate","active_offers_count","city_id","ref","lot_id","has_sample_menu","has_stock_cover_image","popularity"];

        foreach($f as $vv){
          unset($review->business->$vv);
        }

        unset($review->business->meta);

        $review->business->photo = $review->business->hasThumbnail() ? $business->thumbnail('largeCropped') : '';
        
        $review->user->photo = '';//['user']['photo'] =  '';
        if(count($user->avatar)>0){
          $review->user->photo = $user->avatar->image->url('small');
        }

        $fs = ['username','email','password','confirmation_code','remember_token','confirmed',
                'created_at','avatar', 'updated_at','twitter','instagram','slug','check_ins_count'];
        foreach($fs as $vv){
          unset($review->user->$vv);
        }

      }else{
        //$review = $review->toArray();
      }

      //$result[] = $review;
    }




    $result = mzk_api_response(['reviews'=>$reviews], 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
	}


  public function getShow(){

    $input = Input::all();

    $review  = $this->review->find($input['id']);
   
    $result = mzk_api_response(compact('review'), 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
  }


  public function getRatingsIndex(){

    $input = Input::all();

    $reviews = $this->review->select()->with('user')->isRating();

    if(isset($input['business_id'])){
      $reviews = $reviews->byBusiness($input['business_id']);
    }

    if(isset($input['user_id'])){
      $reviews->byUser($input['user_id']);
    }

    $reviews = $reviews->paginate(20)->toArray();
   
    $result = mzk_api_response(compact('reviews'), 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
  }


  protected function getUserIdFromApi($key){
    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }

  }


  public function addReview(){
    $input = Input::all();
    $data = Input::except('key', 'action');
    $data['user_id'] = $this->getUserIdFromApi($input['key']);
    $validation = Validator::make($data, Review::$rules);
    $business = Business::find($input['business_id']);

    if ($validation->passes()){
      if($business->isReviewedBy($data['user_id'])==false){
        $review = $this->review->create($data);
        $review->services()->sync(isset($input['services'])?$input['services']:[]);

      }else{
        $review = $business->isReviewedBy($data['user_id']);
        $review->update($data);
        $review->services()->sync(isset($input['services'])?$input['services']:[]);

      }

      $result = mzk_api_response(compact('review'), 200, true, 'Success');

      return $this->response->withArray($result)->setStatusCode(200);
    }else{
      $result = mzk_api_response([], 401, false, $validation->errors());
      return $this->response->withArray($result)->setStatusCode(401);
    }
  }

  public function addRating(){
    $input = Input::all();
    $data = Input::except('key', 'action');
    $data['user_id'] = $this->getUserIdFromApi($input['key']);
    $data['body'] = ' ';
    $validation = Validator::make($data, Review::$rules);

    $business = Business::find($input['business_id']);

    if ($validation->passes()){
      if($business->isReviewedBy($data['user_id'])==false){
       $review = $this->review->create($data);
      }else{
        $review = $business->isReviewedBy($data['user_id']);
        $review->update($data);
      }

      $result = mzk_api_response(compact('review'), 200, true, 'Success');

      return $this->response->withArray($result)->setStatusCode(200);
    }else{
      $result = mzk_api_response([], 401, false, $validation->errors());
      return $this->response->withArray($result)->setStatusCode(401);
    }
  }


  public function updateReview(){
    $input = Input::all();
    $data = Input::except('key', 'action');
    $data['user_id'] = $this->getUserIdFromApi($input['key']);
    $validation = Validator::make($data, Review::$update_rules);

    if ($validation->passes()){
      $review = $this->review->find($input['id']);
      $review->update($data);
      $review->services()->sync($input['services']?$input['services']:[]);
      
      $result = mzk_api_response(compact('review'), 200, true, 'Success');

      return $this->response->withArray($result)->setStatusCode(200);
    }else{
      $result = mzk_api_response([], 401, false, $validation->errors());
      return $this->response->withArray($result)->setStatusCode(401);
    }
  }


  public function destroyReview()
  {

    $input = Input::all();
    $id = $input['id'];
    $review = $this->review->find($id);
    $input = Input::all();
    $data = Input::except('key', 'action');

    $user_id = $this->getUserIdFromApi($input['key']);

    if(!$review){
      $result = mzk_api_response([], 200, true, 'Success');
      return $this->response->withArray($result)->setStatusCode(200);
    }

    if(!$user_id){
      $result = mzk_api_response([], 401, false, 'Authentication failure');
      return $this->response->withArray($result)->setStatusCode(401);

    }else{

      if($review->user_id != $user_id){
        $result = mzk_api_response([], 401, false, 'Authentication failure');
        return $this->response->withArray($result)->setStatusCode(401);

      }else{
        $review->delete();
        $result = mzk_api_response([], 200, true, 'Success');
        return $this->response->withArray($result)->setStatusCode(200);
      }
    }

  }






}
