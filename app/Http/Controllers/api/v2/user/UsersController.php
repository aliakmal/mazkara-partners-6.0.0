<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;

use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;

use Confide;
use MazkaraHelper;
use User, Input, App, Role, Config, Mail, Lang, Zone, Post;
use Auth, Review, Validator, Business, Response, Account, Debugbar, Activity;

class UsersController extends \BaseController {
  use ControllerTrait;
  protected $user, $account;

  public function __construct(User $user, Account $account){
    $this->user = $user;
    $this->account = $account;
  }

  public function updateAccount(){
    $user_id = $this->getUserIdFromApi(Input::get('key'));
    $user = User::find($user_id);
    $data = Input::all();
    $fields = ['name', 'phone'];

    if(isset($data['email']) && (strlen(trim($data['email']))>0) ){
      $repo = App::make('UserRepository');

      $oldUser = clone $user;
      $user->email = $data['email'];

      if (!$repo->save($user)) {
        $error = $user->errors()->all(':message');
        $result = mzk_api_response([], 200, false, join(', ', $error));
      }
    }

    $user = User::find($user_id);

    foreach($fields as $ff){
      if( isset($data[$ff]) && (strlen(trim($data[$ff]))>0) ){
        $user->$ff = $data[$ff];
      }
    }

    $user->save();
    if(isset($data['photo'])){
      $user->saveImage($data['photo']);
      $user->save();
    }

    $user = User::where('id', $user_id)->first();
    $photo = '';
    $blur_photo = '';

    if(count($user->avatar)>0){
      $photo =  $user->avatar->image->url('small');
      $blur_photo =  $user->avatar->image->url('blurred');
    }

    $key = $this->getApiKeyFromUser($user->id);
      $user->toArray();
    $user['photo'] = $photo;
    $user['blur_photo'] = $blur_photo;


    $result = mzk_api_response(['user'=>$user, 'key'=>$key], 200, true, 'Saved successfully!');
    return $this->response->array($result)->setStatusCode(200);
  }

  public function register(){

    $data = Input::only(User::$profile_fields);
    $input = Input::all();
    if(!isset($data['name'])){
      $data['name'] = mzk_get_name_from_email($input['email']);
    }
    $validation = Validator::make($data, User::$profile_rules);


    if(isset($input['provider'])&&isset($input['provider_id'])){
      $user = $this->getUserFromProvider($provider, $provider_id);
      return $this->response->array($user->toArray())->setStatusCode(200);
    }


    if($validation->passes()):

      $repo = App::make('UserRepository');
      $input['password_confirmation'] = $input['password'];
      $user = $repo->signup($input);

      if ($user->id) {

        $role = Role::where('name', '=', 'user')->first();
        $user->attachRole($role);
        $user->confirmed = 1;
        $user->save();
        $user->update($data);

        if(isset($input['provider'])){
          Account::create([ 'user_id'=>$user->id, 
                            'provider'=>$input['provider'], 
                            'provider_id'=>$input['provider_id']]);
        }

        if (Config::get('confide::signup_email')) {
          Mail::queue(
            Config::get('confide::email_account_confirmation'),
            compact('user'),
            function ($message) use ($user) {
              $message->to($user->email, $user->username)
                      ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
            }
          );
        }
        $result = [
                    'key'=>$this->getApiKeyFromUser($user->id),
                    'user'=>$user->toArray(),
                    ];
        $result = mzk_api_response($result, 200, true, 'Success');
        return $this->response->array($result)->setStatusCode(200);

      } else {
        $result = mzk_api_response($input, 200, false, join(',', $user->errors()->all(':message')));

        return $this->response->array($result);
      }
    else:
      $result = mzk_api_response($input, 200, false, join(',', $validation->messages()->toArray()));
      return $this->response->array($result);

    endif;
  }

  public function getApiKeyFromUserId($user_id){
    // We have validated this user
    // Assign an API key for this session
    $apiKey = ApiKey::where('user_id', '=', $user_id)->first();
    if (!isset($apiKey)) {
      $apiKey                = new ApiKey;
      $apiKey->user_id       = $user_id;
      $apiKey->key           = $apiKey->generateKey();
      $apiKey->level         = 5;
      $apiKey->ignore_limits = 0;
    } else {
      $apiKey->generateKey();
    }

    if (!$apiKey->save()) {
      return $this->response->errorInternalError("Failed to create an API key. Please try again.");
    }

    // We have an API key.. i guess we only need to return that.
    return $this->response->withItem($apiKey, new ApiKeyTransformer)->statusCode(200);;
    // We have an API key.. i guess we only need to return that.
    // return $this->response->array($user->toArray());  
  }

  public function getApiKeyFromUser($user_id){
    // We have validated this user
    // Assign an API key for this session
    $apiKey = ApiKey::where('user_id', '=', $user_id)->first();
    if (!isset($apiKey)) {
      $apiKey                = new ApiKey;
      $apiKey->user_id       = $user_id;
      $apiKey->key           = $apiKey->generateKey();
      $apiKey->level         = 5;
      $apiKey->ignore_limits = 0;
    } else {
      $apiKey->generateKey();
    }

    if (!$apiKey->save()) {
      return $this->response->errorInternalError("Failed to create an API key. Please try again.");
    }

    // We have an API key.. i guess we only need to return that.
    return $apiKey->key;//, new ApiKeyTransformer)->statusCode(200);;
    // We have an API key.. i guess we only need to return that.
    // return $this->response->array($user->toArray());  
  }


  public function authProvider($provider, $provider_id){

    $validator = Validator::make([
        'provider' => $provider,
        'provider_id' => $provider_id
      ],
      [
        'provider' => 'required|max:255',
        'provider_id' => 'required|max:255'
      ]
    );

    if ($validator->fails()) {
      return $this->response->errorBadRequest($validator->messages()->toArray());
    }

    $user = $this->getUserFromProvider($provider, $provider_id);

    if(!$user){
      return $this->response->errorUnauthorized("Authorization failed");
    }

    return $this->getApiKeyFromUserId($user->id);
  }
  
  public function getUserFromProvider($provider, $provider_id){
    return User::findByProvider($provider, $provider_id);
  }



  public function authUsernamePassword($username, $password){

    $credentials['username'] = $username;
    $credentials['password'] = $password;
    
    $validator = Validator::make([
        'username' => $credentials['username'],
        'password' => $credentials['password']
      ],
      [
        'username' => 'required|max:255',
        'password' => 'required|max:255'
      ]
    );

    if ($validator->fails()) {
      return $this->response->errorBadRequest($validator->messages()->toArray());
    }


    if (Confide::logAttempt($credentials) == false) {
      return $this->response->errorUnauthorized("Your username or password is incorrect");
    }else{
      $user = Confide::getUserByEmailOrUsername($credentials['username']);
    }

    $key = $this->getApiKeyFromUser($user->id);
    $user = $user->toArray();
    $result = mzk_api_response(['user'=>$user, 'key'=>$key], 200, true, 'Success');

    return $this->response->array($result)->setStatusCode(200);

    //return $this->getApiKeyFromUserId($user->id);
  }


  public function authenticate() {
    $input = Input::all();
    if(isset($input['username']) && isset($input['password'])){
      return $this->authUsernamePassword($input['username'], $input['password']);
    }elseif(isset($input['provider']) && isset($input['provider_id'])){
      return $this->authProvider($input['provider'], $input['provider_id']);
    }

    return $this->response->errorBadRequest('Required Parameters not passed');
  }

  public function authWithFacebook() {
    $input = Input::all();

    $account = new Account(['provider'=>Account::FACEBOOK, 
                            'provider_id'=>$input['id'], 
                            'meta'=>$input]);

    // do we have a user with this facebook id?
    $user = $this->user->findByFacebookId($input['id']);
    if($user){
      Auth::loginUsingId($user->id);
      MazkaraHelper::fbToken($input['token']);
    }elseif(!isset($input['email'])){
      $data = $input;
      $repo = App::make('UserRepository');
      $data['email'] = $input['id'].'@facebook.com';
      $user = $repo->signup(User::getRepoDefaults($data['email']));
      $user->quickSetRegistered(['name'=>$data['name']]);
      $user->accounts()->save($account);
      Auth::loginUsingId($user->id);
    }elseif($user = $this->user->findByEmail($input['email'])){
      $user = $user->accounts()->save($account);
      Auth::loginUsingId($user->id);
      MazkaraHelper::fbToken($input['token']);
    }else{
      $repo = App::make('UserRepository');
      $user = $repo->signup(User::getRepoDefaults($input['email']));
      $user->quickSetRegistered(['name'=>isset($input['name'])?$input['name']:mzk_get_name_from_email($input['email'])]);
      $user->accounts()->save($account);
      Auth::loginUsingId($user->id);
      MazkaraHelper::fbToken($input['token']);
    }
    $key = $this->getApiKeyFromUser($user->id);
    $user = $user->toArray();
    $result = mzk_api_response(['user'=>$user, 'key'=>$key], 200, true, 'Success');

    return $this->response->array($result)->setStatusCode(200);

    return $this->getApiKeyFromUserId($user->id);
  }




  public function authWithGoogle() {
    $input = Input::all();


      $account = new Account(['provider'=>Account::GOOGLE, 
                                            'provider_id'=>$input['id'], 
                                            'meta'=>$input]);

      // do we have a user with this facebook id?
      $user = $this->user->findByGoogleId($input['id']);
      
      if($user){
        Auth::loginUsingId($user->id);
      }elseif(isset($input['email']) && ($user = $this->user->findByEmail($input['email']))){
        $user->accounts()->save($account);
        Auth::loginUsingId($user->id);
      }else{
        $repo = App::make('UserRepository');
        $user = $repo->signup(User::getRepoDefaults($input['email']));
        $user->quickSetRegistered(['name'=>isset($input['name'])?$input['name']:mzk_get_name_from_email($input['email'])]);

        $user->accounts()->save($account);

        Auth::loginUsingId($user->id);
        Mail::queue('emails.user-signup', ['data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
          $message->to('users@fabogo.com', 'Mazkara')->subject('Mazkara: New User Signup via Google ['.time().']');
        });
      }
    $key = $this->getApiKeyFromUser($user->id);
    $user = $user->toArray();
    $result = mzk_api_response(['user'=>$user, 'key'=>$key], 200, true, 'Success');

    return $this->response->array($result)->setStatusCode(200);


  }


  public function guest() {

    $user = User::first();
    return $this->getApiKeyFromUserId($user->id);
  }


    public function getUserDetails() {
        $key = Input::get('key');
        $validator = Validator::make([
                'key' => $key,
            ],
            [
                'key' => 'required'
            ]
        );

        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->messages()->toArray());
        }

        $apiKey = ApiKey::where('key', '=', $key);

        if(!$apiKey){
            return $this->response->errorNotFound();
        }
        $user_id = $apiKey->first()->user_id;
        $user = User::where('id', $user_id)->first();
        $photo = '';
        $blur_photo = '';

        if(count($user->avatar)>0){
          $photo =  $user->avatar->image->url('small');
          $blur_photo =  $user->avatar->image->url('blurred');

        }

        $user->toArray();
        $user['photo'] = $photo;
        $user['blur_photo'] = $blur_photo;



        $reviews = Review::select()->with('user', 'business')->isReview()->byUser($user_id)->get()->take(2);




    foreach($reviews as $ii=>$review){
      if($review->user_id > 0){
        $usr = $review->user;
        $business = $review->business;

        $f = ["description","phone","email","website","geolocated","zone_id","geolocation_city","geolocation_state","geolocation_country","geolocation_address","created_at","updated_at",
"slug","active","facebook","twitter","instagram","google","chain_id","landmark","created_by_id","updated_by_id","deleted_by_id","geolocation_longitude",
"geolocation_latitude","rate_card_count","image_count","reviews_count","favorites_count","checkins_count","services_count","categories_count","highlights_count","timings_count","zone_cache","rating_average","active_deals_count",
"total_deals_count","total_packages_count","active_packages_count","total_ratings_count","cost_estimate","active_offers_count","city_id","ref","lot_id","has_sample_menu","has_stock_cover_image","popularity"];

        foreach($f as $vv){
          unset($review->business->$vv);
        }

        unset($review->business->meta);

        $review->business->photo = $review->business->hasThumbnail() ? $business->thumbnail('largeCropped') : '';
        $review->user->photo = '';//['user']['photo'] =  '';
        if(count($usr->avatar)>0){
          $review->user->photo = $usr->avatar->image->url('small');
        }

        $fs = ['username','email','password','confirmation_code','remember_token','confirmed',
                'created_at','avatar', 'updated_at','twitter','instagram','slug','check_ins_count'];
        foreach($fs as $vv){
          unset($review->user->$vv);
        }

      }else{
        //$review = $review->toArray();
      }

      //$result[] = $review;
    }





        $reviews = $reviews->toArray();

        if(isset($user)){
          $result = mzk_api_response(compact('user','reviews'), 200, true, 'Success');
          return $this->response->array($result);
        }else{
          return $this->response->errorNotFound();
        }

    }

    public function deauthenticate() {
        $key = Input::get('key');
        $validator = Validator::make([
                'key' => $key,
            ],
            [
                'key' => 'required'
            ]
        );


        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->messages()->toArray());
        }

        $apiKey = ApiKey::where('key', '=', $key);

        if(!$apiKey){
          return $this->response->errorUnauthorized("There is no such user to deauthenticate.");
        }

        $apiKey->delete();

        return $this->response->withArray([
            'ok' => [
                'code'      => 'SUCCESSFUL',
                'http_code' => 200,
                'message'   => 'User was successfuly deauthenticated'
            ]
        ]);
    }

    public function error404(){
      return $this->response->errorBadRequest("Invalid Action.");
    }

  protected function getUserIdFromApi($key){
    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }
  }


  public function follows(){
    $type = Input::get('follow_type');
    $id = Input::get('follow_id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    //$type = strstr($type, 'App\Models')? $type : 'App\Models\\'.$type;
    $user_ids = [];
    switch($type):
      case 'Post':
      case 'Video':
      case 'Selfie':
      case 'Question':
      case 'Answer':
        $user_ids = \DB::table('favorites')->whereIn('favorable_type', ['Post','Video','Selfie','Question','Answer'])
                                ->where('favorable_id', '=', $id)->lists('user_id','user_id');
      default:
        $user_ids = \DB::table('favorites')->whereIn('favorable_type', [$type])
                                ->where('favorable_id', '=', $id)->lists('user_id','user_id');
      break;
    endswitch;
    
    $accounts = User::whereIn('id', $user_ids)->paginate(20);

    foreach($accounts as $ii=>$vv){
      $accounts[$ii]->photo = $vv->gravatar;
      $accounts[$ii]->is_followed = $vv->isFollowed($user_id);
    }

    $data = ['users'=>$accounts->toArray()];
    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);

  }

  public function follow(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    if(!$user->hasFavourited('User', $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>'User', 'favorable_id'=>$id]);

      $data = array('user_id' =>  $user->id, 
                    'verb'  => 'followed', 
                    'itemable_type' =>  'User', 
                    'itemable_id' =>  $id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $user->id;
      $feed->verb = 'followed'; 
      $feed->itemable_type = 'User';
      $feed->itemable_id = $id;
      $feed->save();

      $result = mzk_api_response([], 200, true, 'Success - User has followed');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already followed');
    }
    return $this->response->array($result);
  }

  public function unfollow(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    if($user->hasFavourited('User', $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'User')->delete();
      $result = mzk_api_response([], 200, true, 'Success - User has followed');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already followed');
    }
    return $this->response->array($result);
  }

  protected function displayableFields(){
    $f = ["id", "username", "email", "confirmed","created_at","name","about","gender",
      "twitter","instagram","favorites_count","ratings_count",
      "reviews_count","check_ins_count","followers_count",
      "follows_count"];    

    return $f;
  }


  public function getIndex(){

    $f = $this->displayableFields();
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    $accounts = User::query()->select($f);

    $search = Input::get('search');

    if(trim($search)!=''){
      $accounts->byName($search);
    }

    $accounts = $accounts->paginate(20);

    foreach($accounts as $ii=>$vv){
      $accounts[$ii]->photo = $vv->gravatar;
      $accounts[$ii]->is_followed = $vv->isFollowed($user_id);
    }

    $data = ['users'=>$accounts->toArray()];
    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function getFollowers(){

    $f = $this->displayableFields();

    $user_id = Input::get('user_id');
    $key = Input::get('key');
    $user = User::find($user_id);
    $my_id = $this->getUserIdFromApi($key);
    $me = User::find($my_id);
    $follower_ids = $me->followers()->lists('user_id', 'user_id');
    $accounts = User::select($f)->whereIn('id', $follower_ids)->get();

    foreach($accounts as $ii=>$vv){
      $accounts[$ii]->photo = $vv->gravatar;
      $accounts[$ii]->is_followed = $vv->isFollowed($my_id);
    }

    $data = ['users'=>$accounts->toArray()];
    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function getMyFavorites(){
    $key = Input::get('key');

    $my_id = $this->getUserIdFromApi($key);
    $user = User::find($my_id);
    $result = [];
    
    foreach($user->followsBusinesses()->get() as $favourite):
      $business = $favourite->favorable;
      $business->user_has_checked_in = $user->hasCheckedIn($business->id);
      $business->user_has_favourited = $user->hasFavourited('Business', $business->id);
      $data = [
        'id'=>$business->id,
        'name'=>$business->name,
        'description'=>$business->description,
        'website'=>$business->website,
        'zone_id'=>$business->zone_id,
        'geolocated'=>$business->geolocated,
        'geolocation_city'=>$business->geolocation_city,
        'geolocation_state'=>$business->geolocation_state,
        'geolocation_country'=>$business->geolocation_country,
        'geolocation_address'=>$business->geolocation_address,
        'created_at'=>(string)$business->created_at,
        'updated_at'=>(string)$business->updated_at,
        'active'=>$business->active,
        'chain_id'=>$business->chain_id,
        'landmark'=>$business->landmark,
        'geolocation_longitude'=>$business->geolocation_longitude,
        'geolocation_latitude'=>$business->geolocation_latitude,
        'rate_card_count'=>$business->rate_card_count,
        'image_count'=>$business->image_count,
        'reviews_count'=>$business->reviews_count,
        'favorites_count'=>$business->favorites_count,
        'checkins_count'=>$business->checkins_count,
        'services_count'=>$business->services_count,
        'categories_count'=>$business->categories_count,
        'highlights_count'=>$business->highlights_count,
        'zone_cache'=>$business->zone_cache,
        'rating_average'=>$business->rating_average,
        'photo'=>$business->hasThumbnail() ? $business->thumbnail('largeCropped') : '',
        'user_has_checked_in' => $user->hasCheckedIn($business->id),
        'user_has_favourited' => $user->hasFavourited('Business', $business->id),
        'phone'=>$business->displayablePhone(),
        'is_open'  => $business->isOpenNow(),
        'is_open_until' => $business->openUntilNow(),
        'will_open' => $business->willOpen(),   
        'has_offers' => $business->active_offers_count > 0 ? true : false,
             
      ];

      $result[] = $data;//();
    endforeach;
  
    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function getFavoritesCount(){
    $key = Input::get('key');

    $my_id = $this->getUserIdFromApi($key);
    $user = User::find($my_id);
    $result = ['favorites_count'=>$user->followsBusinesses()->get()->count()];

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function getFavorites(){
    $user_id = Input::get('user_id');
    $user = User::find($user_id);
    $result = [];
    if(($user->followsBusinesses()->count())>0):
    foreach($user->followsBusinesses()->get() as $favourite):
      $business = $favourite->favorable;
      $business->user_has_checked_in = $user->hasCheckedIn($business->id);
      $business->user_has_favourited = $user->hasFavourited('Business', $business->id);

      $business->is_open  = $business->isOpenNow();
      $business->is_open_until = $business->openUntilNow();

      $result[] = $business->toArray();
    endforeach;
    endif;

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function getFollows(){

    $f = $this->displayableFields();

    $user_id = Input::get('user_id');
    $key = Input::get('key');
    $user = User::find($user_id);
    $my_id = $this->getUserIdFromApi($key);
    $me = User::find($my_id);
    $follower_ids = $user->follows()->lists('favorable_id', 'favorable_id');
    $accounts = User::select($f)->whereIn('id', $follower_ids)->get();

    foreach($accounts as $ii=>$vv){
      $accounts[$ii]->photo = $vv->gravatar;
      $accounts[$ii]->is_followed = $vv->isFollowed($my_id);
    }

    $data = ['users'=>$accounts->toArray()];
    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function forgotPassword()
  {
    if (Confide::forgotPassword(Input::get('email'))) {
      $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
      $result = mzk_api_response([], 200, true, $notice_msg);
    } else {
      $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
      $result = mzk_api_response([], 200, false, $error_msg);
    }

    return $this->response->array($result);
  }

  public function show(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    $user->photo = $user->gravatar;
    $user->is_followed = $user->isFollowed($user_id);

    $f = $this->displayableFields();
    
    $followers = $user->followers()->get()->lists('user_id','user_id');
    $followers = count($followers)>0 ? $followers : [];
    $followers = User::select($f)->whereIn('id', $followers)->get();
    foreach($followers as $ii=>$vv){
      $followers[$ii]->photo = $vv->gravatar;
      $followers[$ii]->is_followed = $vv->isFollowed($user_id);
    }

    $follows = $user->followsUsers()->get()->lists('favorable_id','favorable_id');
    $follows = count($follows)>0 ? $follows : [];
    $follows = User::select($f)->whereIn('id', $follows)->get();
    foreach($follows as $ii=>$vv){
      $follows[$ii]->photo = $vv->gravatar;
      $follows[$ii]->is_followed = $vv->isFollowed($user_id);
    }

    $activities = Activity::select()->byUserIds([$user_id])->orderby('id', 'desc')->paginate(10);
    $data = [ 'activities'  =>  $activities, 
              'user'        =>  $user->toArray(), 
              'followers'   =>  $followers->toArray(),
              'iFollow'     =>  $follows->toArray() ];

    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function setCurrentUsersZone(){

    $zone_id = Input::get('zone_id');
    $zone = [];

    if(!($zone_id >0)){
      $lat = Input::has('latitude') ? Input::get('latitude') : false;
      $lon = Input::has('longitude') ? Input::get('longitude') : false;
      if(($lat!= false) && ($lon!=false)){
        // get a closeby business
        $b = Business::query()->nearLatLng($lat, $lon, 100)->orderby('distance', 'asc')->get()->take(1)->first();
        if($b->zone_id >0){
          $zone_id = $b->zone_id;
        }
      }
    }


    if($zone_id >0){
      $key = Input::get('key');
      $apiKey = ApiKey::where('key', '=', $key)->get()->first();
      $zone = Zone::find($zone_id);

      if($apiKey){
        $apiKey->current_zone = $zone_id;
        $apiKey->save();
      }

      if($zone->isCity()){
        $current_city = $zone->toArray();
        $zone = $zone->toArray();
      }else{
        $current_city = Zone::where('id', '=', $zone->city_id)->get()->first()->toArray();
        $zone = $zone->toArray();
        $zone['name']=join(', ',[$zone['name'],$current_city['name']]);
      }


    }
    $result = mzk_api_response(['current_city'=>$current_city, 'current_zone'=>$zone] , 200, true, 'Success');
    return $this->response->array($result);

  }


}