<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Service, Timing, Service_zone_counter, Highlight, Photo;
use User, Post, Question, Auth, Activity, Response, Zone;

class PageController extends \HomeController {
  use ControllerTrait;

  protected $current_city, $current_zone;

  public function search(){
    $q = Input::get('search');
    $key = Input::get('key');

    $apiKey = ApiKey::where('key', '=', $key)->get()->first();
    $z = false;
    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $z = $z->isCity()?$z->id:$z->city_id;
      }
    }

    $service_lists = Service::query()->showOnly()->byLocaleActive($z)->showOnly()->search($q)->take(10)->get();
    $num_outlets = 20;//count($service_lists) < 10 ?(5 - count($service_lists))+2:2;
    $outlets = Business::query()->select('name', 'zone_cache', 'id', 'slug')
                                ->byLocale($z)->search($q)->onlyActive()
                                ->take($num_outlets)->get();
    //$categories = Category::query()->search($q)->byLocaleActive()->take(3)->get()->toArray();
    //$chains = Group::search($q)->byLocale()->take(3)->get();
    $result = [];
    $zone = Input::get('zone');
    if(empty($zone)){
      $zone = false;
    }else{
      $zone = Zone::find($zone);
    }
    $services = [];
    foreach($service_lists as $v){
      
      $p = [
            'service' =>[
                          $v->id
                        ]
            ];
      $service_name = '';
      if($v->parent_id > 0){
        $service_name = $v->parent->name.', ';
      }

      $service_name.= $v->name;

      $result[] = [ 'name'=>$service_name, 'id'=>$v->id, 'type'=>'SERVICE' ];

    }

    foreach($outlets as $v){
      $result[] = [ 'id'=>$v->id,
                    'name'=>$v->name, 
                    'zone'=>$v->zone_cache,
                    'type'=>'VENUE' ];
    }

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function searchVenues(){
    $q = Input::get('search');
    $key = Input::get('key');


    $apiKey = ApiKey::where('key', '=', $key)->get()->first();
    $z = false;
    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $z = $z->isCity()?$z->id:$z->city_id;
      }
    }


    $num_outlets = 20;//count($service_lists) < 10 ?(5 - count($service_lists))+2:2;
    $outlets = Business::query()->select('name', 'zone_cache', 'id', 'slug')
                                ->byLocale($z)->search($q)->onlyActive()
                                ->take($num_outlets)->get();
    //$categories = Category::query()->search($q)->byLocaleActive()->take(3)->get()->toArray();
    //$chains = Group::search($q)->byLocale()->take(3)->get();
    $result = $outlets->toArray();


    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }


  public function megaSearch(){
    $q = Input::get('search');
    $key = Input::get('key');

    $input = Input::all();
    $only = isset($input['only']) ? $input['only'] : ['services', 'venues', 'stories', 'selfies','videos','questions','users'];

    $apiKey = ApiKey::where('key', '=', $key)->get()->first();
    $z = false;
    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $z = $z->isCity()?$z->id:$z->city_id;
      }
    }
    $user_id = $this->getUserIdFromApi();


    if(in_array('services', $only)){
      $services = Service::query()->showOnly()->byLocaleActive($z)->showOnly()->search($q)->paginate(10);
    }else{
      $services = new \stdClass();
    }

    if(in_array('venues', $only)){
      $venues = Business::query()->select('name', 'zone_cache', 'id', 'slug')
                                  ->byLocale($z)->search($q)->onlyActive()
                                  ->paginate(10);
    }else{
      $venues = new \stdClass();
    }

    if(in_array('stories', $only)){
      $stories = Post::query()->ofSearch($q)->onlyPosts()->isViewable()->orderBy('id', 'DESC')->paginate(10);
      foreach($stories as $ii=>$post){
        $stories[$ii]->cover_url = $post->cover_url;//?$post->cover->image->url():$post->cover;
        $stories[$ii]->hasLiked = $post->liked($user_id);
        $stories[$ii]->likeCount = $post->num_likes();
        $stories[$ii]->commentCount = $post->num_comments();
        $stories[$ii]->cover = null;
        $stories[$ii]->bookmarked = $post->isBookmarked($user_id)?true:false;

      }

    }else{
      $stories = new \stdClass();
    }

    if(in_array('selfies', $only)){
      $selfies = Post::query()->ofSearch($q)->onlySelfies()->isViewable()->orderBy('id', 'DESC')->paginate(10);
      foreach($selfies as $ii=>$post){
        $selfies[$ii]->cover_url = $post->cover_url;//?$post->cover->image->url():$post->cover;
        $selfies[$ii]->hasLiked = $post->liked($user_id);
        $selfies[$ii]->likeCount = $post->num_likes();
        $selfies[$ii]->commentCount = $post->num_comments();
        $selfies[$ii]->cover = null;
        $selfies[$ii]->bookmarked = $post->isBookmarked($user_id)?true:false;

      }
    }else{
      $selfies = new \stdClass();
    }

    if(in_array('videos', $only)){
      $videos = Post::query()->ofSearch($q)->onlyVideos()->isViewable()->orderBy('id', 'DESC')->paginate(10);
      foreach($videos as $ii=>$post){
        $videos[$ii]->cover_url = $post->cover_url;//?$post->cover->image->url():$post->cover;
        $videos[$ii]->hasLiked = $post->liked($user_id);
        $videos[$ii]->likeCount = $post->num_likes();
        $videos[$ii]->commentCount = $post->num_comments();
        $videos[$ii]->cover = null;
        $videos[$ii]->bookmarked = $post->isBookmarked($user_id)?true:false;

      }
    }else{
      $videos = new \stdClass();
    }

    if(in_array('questions', $only)){
      $questions = Question::ofSearch($q)->onlyQuestions()->paginate(10);
    }else{
      $questions = new \stdClass();
    }

    if(in_array('users', $only)){
      $users = User::query()->search($q)->orderBy('id', 'DESC')->paginate(10);
    }else{
      $users = new \stdClass();
    }

    $result = [];
    
    $result['venues']   = $venues;
    $result['stories']  = $stories;
    $result['selfies']  = $selfies;
    $result['videos']   = $videos;
    $result['questions']= $questions;
    $result['users']    = $users;
    $result['services'] = $services;

//    foreach($services as $v){
//      $service_name = '';
//      if($v->parent_id > 0){
//        $service_name = $v->parent->name.', ';
//      }
//
//      $service_name.= $v->name;
//
//      $result['services'][] = [ 'name'=>$service_name, 'id'=>$v->id, 'type'=>'SERVICE' ];
//    }
//
//    $result['venues'] = [];
//    foreach($venues as $v){
//      $result['venues'][] = [ 'id'=>$v->id,
//                              'name'=>$v->name, 
//                              'zone'=>$v->zone_cache,
//                              'type'=>'VENUE' ];
//    }
//
//
//    $result['stories'] = [];
//    foreach($stories as $v){
//
//      $author = [ 'id'=>$v->author_id, 
//                  'name'=>$v->authors_full_name,
//                  'designation'=>$v->authors_designation,
//                  'image'=> count($v->author->avatar)>0 ? $v->author->avatar->image->url('small'):'',
//                ];
//
//      $result['stories'][] = [ 'id'=>$v->id,
//                                'name'=>$v->title, 
//                                'hasLiked' => $v->liked($user_id),
//                                'likeCount' => $v->num_likes(),
//                                'commentCount' => $v->num_comments(),
//                                'image'=>$v->cover_url,
//                                'type'=>POST::ARTICLE ];
//    }
//
//    $result['selfies'] = [];
//    foreach($selfies as $v){
//      $author = [ 'id'=>$v->author_id, 
//                  'name'=>$v->authors_full_name,
//                  'designation'=>$v->authors_designation,
//                  'image'=> count($v->author->avatar)>0 ? $v->author->avatar->image->url('small'):'',
//                ];
//
//      $result['selfies'][] = [ 'id'=>$v->id,
//                              'name'=>$v->title, 
//                              'hasLiked' => $v->liked($user_id),
//                              'likeCount' => $v->num_likes(),
//                              'commentCount' => $v->num_comments(),
//                              'image'=>$v->cover_url,
//                              'author'=>$author,
//                              'type'=> POST::SELFIE ];
//    }
//
//    $result['videos'] = [];
//    foreach($videos as $v){
//      $author = [ 'id'=>$v->author_id, 
//                  'name'=>$v->authors_full_name,
//                  'designation'=>$v->authors_designation,
//                  'image'=> count($v->author->avatar)>0 ? $v->author->avatar->image->url('small'):'',
//                ];
//
//      $result['videos'][] = [ 'id'=>$v->id,
//                              'name'=>$v->title, 
//                              'hasLiked' => $v->liked($user_id),
//                              'likeCount' => $v->num_likes(),
//                              'commentCount' => $v->num_comments(),
//                              'image'=>$v->cover_url,
//                              'author'=>$author,
//                              'type'=>POST::VIDEO ];
//    }
//
//    $result['questions'] = [];
//    foreach($questions as $v){
//      $author = [ 'id'=>$v->author_id, 
//                  'name'=>$v->authors_full_name,
//                  'designation'=>$v->authors_designation,
//                  'image'=> count($v->author->avatar)>0 ? $v->author->avatar->image->url('small'):'',
//                ];
//
//      $result['questions'][] = [ 'id'=>$v->id,
//                                  'name'=>$v->title, 
//                                  'author'=>$author,
//                                  'answersCount'=>count($v->answers),
//                                  'type'=>POST::QUESTION ];
//    }
//
//    $result['users'] = [];
//    foreach($users as $v){
//      $result['users'][] = [  'id'=>$v->id,
//                              'name'=>$v->full_name, 
//                              'image'=> count($v->avatar)>0 ? $v->avatar->image->url('small'):'',
//                              'type'=>'USER' ];
//    }

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }


  protected function getUserIdFromApi($key = false){
    if(!$key){
      $input = Input::all();
      $key = $input['key'];
    }

    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }
  }


  public function searchServices(){
    $q = Input::get('search');
    $key = Input::get('key');

    $apiKey = ApiKey::where('key', '=', $key)->get()->first();
    $z = false;
    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $z = $z->isCity()?$z->id:$z->city_id;
      }
    }


    $service_lists = Service::query()->showOnly()->byLocaleActive($z)->showOnly()->search($q)->take(10)->get();
    $result = [];
    $zone = Input::get('zone');
    if(empty($zone)){
      $zone = false;
    }else{
      $zone = Zone::find($zone);
    }
    $services = [];
    foreach($service_lists as $v){
      $service_name = '';
      if($v->parent_id > 0){
        $service_name = $v->parent->name.', ';
      }

      $service_name.= $v->name;

      $result[] = [ 'name'=>$service_name, 'id'=>$v->id];
    }

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }



  protected function getCategories(){
    $cs = Category::select()->byLocaleActive()->get();//('name', 'id');
    $categories = array();

    foreach($cs as $ii=>$cat){
      $categories[] = ['id'=>$cat->id, 'name'=>$cat->name, 
                          'business_count' => $cat->business_count
      ];
    }

    return $categories;
  }

  protected function getHighlights($locale = false){
    $hs = Highlight::select()->byLocale($locale)->get();
    $highlights = array();
    $genders = [];
    foreach($hs as $ii=>$hi){
      if($hi->isActive($locale)){
        if(!in_array($hi->id, [4,5,6])){
          $highlights[] = ['id'=>$hi->id, 'name'=>$hi->name, 
                        'icon'=>'https://s3.amazonaws.com/mazkaracdn/mobile/icons/'.$hi->slug.'.png'];
        }
      }
    }

    return $highlights;
  }


  protected function getGenders(){
    $hs = Highlight::select()->showActive()->get();
    $highlights = array();
    $genders = [];

    foreach($hs as $ii=>$hi){
      if($hi->isActive()){
        if(in_array($hi->id, [4,5,6])){
          $genders[] = ['id'=>$hi->id, 'name'=>$hi->name, 
                        'icon'=>'https://s3.amazonaws.com/mazkaracdn/mobile/icons/'.$hi->slug.'.png'];
        }
      }
    }

    return $genders;
  }

  protected function setCurrentCityZone($apiKey, $default){
    $current_city = $default; // work on this
    $current_zone = $current_city;


    if($apiKey){
      // if user has gps enabled get the current city
      $cz = $this->setLocation();

      if($cz != false){
        // set the users current zone
        $apiKey->current_zone = $cz;
        $apiKey->save();
      }

      // change the current city
      if($apiKey->current_zone>0){
        $zn = Zone::where('id', '=', $apiKey->current_zone)->get()->first();
        $current_zone = $zn;

        if($zn->isCity()){
          $current_city = $zn->toArray();
          $current_zone = $current_zone->toArray();
        }else{
          $current_city = Zone::where('id', '=', $zn->city_id)->get()->first()->toArray();
          $current_zone = $current_zone->toArray();
          $current_zone['name']=join(', ',[$current_zone['name'],$current_city['name']]);
      }
      }else{
        $apiKey->current_zone = $current_city['id'];
        $apiKey->save();
      }
    }

    $this->current_city = $current_city;
    $this->current_zone = $current_zone;


  }


	public function home(){


    $home_screen = 'https://s3.amazonaws.com/mazkaracdn/mobile/home/homeimage4.jpg'; 

    $cities = Zone::query()->cities()->showActive()->get()->toArray();

    $key = Input::get('key');
    $apiKey = ApiKey::where('key', '=', $key)->get()->first();
    $this->setCurrentCityZone($apiKey, $cities[0]);
    
    $current_city = $this->current_city;
    $current_zone = $this->current_zone;

    $categories = $this->getCategories();

    $highlights = $this->getHighlights($current_city['id']);
    $genders = $this->getGenders();

    $services = array();
    $parents_services = array();
    $popular_services = [];

    // set up the service hierarchy list here
    $srs = Service::whereNull('parent_id')->get();


    $zone_to_count_against = $current_city;
    // count is dependant upon if the current zone is a city or a subzone
    $cz = Zone::find($current_zone['id']);

    if($current_city['id'] == $current_zone['id']){ // if this is a city then count must be of the city alone
      $zone_to_count_against = $current_city;
    }elseif ($cz->isSubZone()) {
      # code...
      $zone_to_count_against = $cz->parent()->first()->id;
    }elseif($cz->isZone()){
      $zone_to_count_against = $current_zone['id'];
    }

    //$service_business_count = Business::select()->ofServices($des_services)->byLocale($current_city['id'])->count();
    $service_counters = Service_zone_counter::select()->ofZones([$zone_to_count_against])->lists('business_count', 'service_id');

    foreach($srs as $ix=>$s1){
      if(!$s1->isActive($current_city['id'])){
        continue;
      }

      $des_services = array_values($s1->getDescendants()->lists('id','id'));
      $des_services[] = $s1->id;
      
      $service = array(
                        'id'=>$s1->id,
                        'name'=>$s1->name,
                        'parent_id'=>$s1->parent_id,
                        'children' =>array(),
                        'business_count' => isset($service_counters[$s1->id])?$service_counters[$s1->id]:$s1->getBusinessCountByZone($zone_to_count_against),//$s2->getBusinessCount($current_city['id'])//business_count
                        'photo'=>mzk_assets('assets/mobile/home/'.$s1->slug.'.jpg')
                      );


      $parents_services[$ix] = $service;
      
      $srs2 = Service::where('parent_id', '=', $s1->id)->get();
      foreach($srs2 as $iz=>$s2){

        if(!$s2->isActive($current_city['id'])){
          continue;
        }

        $service['children'][] = array( 'id' => $s2->id,
                                        'name' => $s2->name,
                                        'parent_id' => $s2->parent_id,
                                        //'business_count' => $service_counters[$s2->id]
                                        'business_count' => isset($service_counters[$s2->id])?$service_counters[$s2->id]:$s2->getBusinessCountByZone($zone_to_count_against),//$s2->getBusinessCount($current_city['id'])//business_count

                                        //$s2->getBusinessCountByZone($zone_to_count_against)//$s2->getBusinessCountByZone(Zone::find($current_zone['id'])->getChildIds()),//$s2->getBusinessCount($current_city['id'])//business_count
                                        );
      }

      $services[] = $service;
    }

    $popular_services = Service::whereNotNull('parent_id')->whereRaw('CHARACTER_LENGTH(name) < 10')->get()->take(8)->toArray();//all(array('id', 'name', 'parent_id'));//->get();


    $zs = Zone::defaultOrder()->byLocale($current_city['id'])->showActive()->get()->linkNodes();
    $zones = array();
    foreach($zs as $zone){
      $zones[] = ['id'=>$zone->id, 'name'=>$zone->stringPath(), 'parent_id'=>$zone->parent_id];
    }


    $result = mzk_api_response(compact('categories', 'cities', 'current_city', 'current_zone', 'home_screen', 'popular_services', 'genders', 
                                       'cities', 'parents_services', 'zones', 'services', 'highlights'),
                                 200, true, 'Success');
    return $this->response->withArray($result)->setStatusCode(200);
	}

  protected function setLocation(){
    $lat = Input::has('latitude') ? Input::get('latitude') : false;
    $lon = Input::has('longitude') ? Input::get('longitude') : false;
    if(($lat!= false) && ($lon!=false)){
      // get a closeby business
      $b = Business::query()->nearLatLng($lat, $lon)->orderby('distance', 'asc')->get()->take(1)->first();
      if($b->zone_id >0){
        return $b->zone_id;
      }
    }

    return false;
  }

  public function searchZones(){
    $cities = Zone::query()->cities()->showActive()->get()->lists('id', 'id');
    $result = [];
    
    if(Input::has('search')):
      $search = Input::get('search');
      $zones = Zone::query()->showActive()->search($search)->whereIn('city_id', $cities+[null,0])->get()->take(30);
      foreach($zones as $zone){
        $result[] = [ 'id'=>$zone->id, 'name'=>$zone->name, 
                      'displayable_name'=>$zone->isCity()?$zone->name:join(' ', [$zone->name, '('.$zone->parent->name.')']), 
                      'type'=>$zone->isCity() ? 'city' : 'zone'];
      }
    else:
      if(Input::has('latitude')):
        $lat = Input::get('latitude');
        $lon = Input::get('longitude');
        $b = Business::query()->nearLatLng($lat, $lon)->orderby('distance', 'asc')->get()->take(1)->first();
        if($b){
          $result[] = Zone::find($b->zone_id)->toArray();
        }

      endif;

    endif;

    $result = mzk_api_response($result,
                                 200, true, 'Success');
    return $this->response->withArray($result)->setStatusCode(200);
  }

  public function getOffersScreen(){
    $data =['image'=>mzk_assets('assets/mobile/screens/coming-soon-offers.png'), 'link'=>''];
    $result = mzk_api_response($data,
                                 200, true, 'Success');
    return $this->response->withArray($result)->setStatusCode(200);    
  }



  public function getShow(){

  }


  public function feed(){
    //if (App::environment('production')){
    //  return View::make('hello');
    //}
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    //$followed_users = array_keys($user->followsUsers()->lists('favorable_id','favorable_id'));
    $activities = Activity::select()->orderby('id', 'desc')->paginate(10);
    $result = mzk_api_response(['feed'=>$activities],
                                 200, true, 'Success');
    return $this->response->withArray($result)->setStatusCode(200);    
  }






}
