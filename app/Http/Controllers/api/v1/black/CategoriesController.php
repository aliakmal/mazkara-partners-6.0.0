<?php
namespace App\Http\Controllers\api\v1\black;
use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Category;
use App\Http\Controllers\BaseController;

class CategoriesController extends \BaseController {

	/**
	 * Category Repository
	 *
	 * @var Category
	 */
  use ControllerTrait;

	protected $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = $this->category->all();
    return $this->response->array($categories->toArray());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('administration.categories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Category::$rules);
		$data = Input::only(Category::$fields);

		if ($validation->passes())
		{
			$category = $this->category->create($data);
			$images = Input::only('images');
			$category->saveImages($images['images']);

			return Redirect::route('admin.categories.index');
		}

		return Redirect::route('admin.categories.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = $this->category->findOrFail($id);

		$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('administration.categories.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = $this->category->find($id);

		if (is_null($category))
		{
			return Redirect::route('admin.categories.index');
		}

		$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('administration.categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Category::$rules);
		$data = Input::only(Category::$fields);

		if ($validation->passes())
		{
			$category = $this->category->find($id);
			$category->update($input);
			$images = Input::only('images');
			$category->saveImages($images['images']);

			return Redirect::route('admin.categories.show', $id);
		}

		return Redirect::route('admin.categories.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->category->find($id)->delete();

		return Redirect::route('admin.categories.index');
	}

}
