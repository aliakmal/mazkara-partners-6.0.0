<?php

namespace App\Http\Controllers\api\v1\black;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide;
use User, Input, Validator, Response, Role, App;
use App\Http\Controllers\BaseController;

class UsersController extends \BaseController {
  use ControllerTrait;

  public function authenticate() {
    $credentials['username'] = Input::get('username');
    $credentials['password'] = Input::get('password');

    $validator = Validator::make([
        'username' => $credentials['username'],
        'password' => $credentials['password']
      ],
      [
        'username' => 'required|max:255',
        'password' => 'required|max:255'
      ]
    );

    if ($validator->fails()) {
      return $this->response->errorBadRequest($validator->messages()->toArray());
    }
    $repo = App::make('UserRepository');

    if (!$repo->login($credentials)) {
      return $this->response->errorUnauthorized("Your username or password is incorrect");      
    }else{
      $user = User::find(Auth::user()->id);
    }

    if(!$user->hasRole('client')){
      return $this->response->errorUnauthorized("Your username or password is incorrect");      
    }

    //try {
    //  $user = User::whereUsername($credentials['username'])->first();
    //  $credentials['email'] = $user->email;
    //} catch (\ErrorException $e) {
    //  return $this->response->errorUnauthorized("Your username or password is incorrect");
    //}

    //if (Confide::logAttempt($credentials) == false) {
    //    return $this->response->errorUnauthorized("Your username or password is incorrect");
    //}



    // We have validated this user
    // Assign an API key for this session
    $apiKey = ApiKey::where('user_id', '=', $user->id)->first();
    if (!isset($apiKey)) {
      $apiKey                = new ApiKey;
      $apiKey->user_id       = $user->id;
      $apiKey->key           = $apiKey->generateKey();
      $apiKey->level         = 5;
      $apiKey->ignore_limits = 0;
    } else {
      $apiKey->generateKey();
    }

    if (!$apiKey->save()) {
      return $this->response->errorInternalError("Failed to create an API key. Please try again.");
    }

    // We have an API key.. i guess we only need to return that.
    return $this->response->withItem($apiKey, new ApiKeyTransformer);
    // We have an API key.. i guess we only need to return that.
    // return $this->response->array($user->toArray());
  }

    public function getUserDetails() {
        $key = Input::get('key');
        $validator = Validator::make([
                'key' => $key,
            ],
            [
                'key' => 'required'
            ]
        );

        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->messages()->toArray());
        }

        $apiKey = ApiKey::where('key', '=', $key);

        if(!$apiKey){
            return $this->response->errorNotFound();
        }

        $user = User::where('id', $apiKey->first()->user_id)->first();

        return isset($user) ? $this->response->array($user->toArray()) : $this->response->errorNotFound();
    }

    public function deauthenticate() {
        $key = Input::get('key');
        $validator = Validator::make([
                'key' => $key,
            ],
            [
                'key' => 'required'
            ]
        );


        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->messages()->toArray());
        }

        $apiKey = ApiKey::where('key', '=', $key);

        if(!$apiKey){
            return $this->response->errorUnauthorized("There is no such user to deauthenticate.");
        }

        $apiKey->delete();

        return $this->response->withArray([
            'ok' => [
                'code'      => 'SUCCESSFUL',
                'http_code' => 200,
                'message'   => 'User was successfuly deauthenticated'
            ]
        ]);
    }

    public function error404(){
        return $this->response->errorBadRequest("Invalid Action.");
    }

}
