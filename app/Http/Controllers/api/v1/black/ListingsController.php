<?php
namespace App\Http\Controllers\api\v1\black;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Service, Timing, Highlight, Photo;
use User,  Response, Zone;
use App\Http\Controllers\BaseController;

class ListingsController extends \BaseController {
  use ControllerTrait;
	/**
	 * Business Repository
	 *
	 * @var Business
	 */
	protected $business;

	public function __construct(Business $business){
		$this->business = $business;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
    $count = Input::get('per_page') ? Input::get('per_page') : 10;

		$businesses = $this->business->query();
		$params = [];

    if(Input::has('search') && (Input::get('search')!='')){
      $businesses->where('name', 'like', Input::get('search').'%');
      $params['name'] = Input::get('name');
    }

    if(Input::has('zone') && (Input::get('zone')!='')){
      //$businesses->where('zone_id', Input::get('zone'));

      $znes = Input::get('zone');
      $zz = Input::get('zone');
      foreach($zz as $z):
        $znes = array_merge($znes, array_keys(Zone::descendantsOf($z)->lists('name', 'id')));
      endforeach;

      $businesses->ofZones($znes);

      $params['zone'] = Input::get('zone');
    }

    if(Input::has('service') && (Input::get('service')!='')){
      $businesses->join('business_service', function($join){
        $join->on('businesses.id', '=', 'business_service.business_id')
                 ->where('business_service.service_id', '=', Input::get('service'));
      });
      $params['service'] = Input::get('service');
    }

    if(Input::has('category') && (Input::get('category')!='')){

      $businesses->join('business_category', function($join){
        $join->on('businesses.id', '=', 'business_category.business_id')
                 ->where('business_category.category_id', '=', Input::get('category'));
      });

      $params['category'] = Input::get('category');
    }

    if(Input::has('active') && (Input::get('active')!='')){
      $businesses->where('active', Input::get('active'));
      $params['active'] = Input::get('active');
    }

    if(Input::has('sort') && (Input::get('sort')!='')){
      $params['sort'] = Input::get('sort');
    	switch($params['sort']){
    		case 'nameAsc':
	    		$businesses->orderBy('name', 'ASC');
    		break; 
				case 'nameDesc':
	    		$businesses->orderBy('name', 'DESC');
    		break; 
				case 'lastUpdateAsc':
	    		$businesses->orderBy('updated_at', 'ASC');
    		break; 
				case 'lastUpdateDesc':
	    		$businesses->orderBy('updated_at', 'DESC');
    		break; 
    	}
    }else{
      $businesses->orderBy('updated_at', 'DESC');
    }

    $businesses = $businesses->paginate($count);

    return $this->response->array($businesses->toArray());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function form()
	{
		$cs = Category::lists('name', 'id');
    $categories = array();

    foreach($cs as $ii=>$cat){
      $categories[] = ['id'=>$ii, 'name'=>$cat];
    }

    $zs = Zone::defaultOrder()->get()->linkNodes();
    $zones = array();
    foreach($zs as $zone){
      $zones[] = ['id'=>$zone->id, 'name'=>$zone->stringPath()];
    }

    $services = array();
    $srs = Service::whereNull('parent_id')->get();//all(array('id', 'name', 'parent_id'));//->get();
    foreach($srs as $s1){
      $service = array(
                        'id'=>$s1->id,
                        'name'=>$s1->name,
                        'parent_id'=>$s1->parent_id,
                        'children' =>array()
                      );
      $srs2 = Service::where('parent_id', '=', $s1->id)->get();
      foreach($srs2 as $s2){
        $service['children'][] = array( 'id' => $s2->id,
                                        'name' => $s2->name,
                                        'parent_id' => $s2->parent_id);
      }

      $services[] = $service;
    }


		$highlights = Highlight::select('id', 'name')->get()->toArray();
    $chains = Business::select('id', 'name')->where('chain_id', '=', '0')->get()->toArray();//->toArray();
    $states = array();
    $s = Business::activeStates();
    foreach($s as $i=>$v){
      $states[] = array('id'=>$i, 'name'=>$v);
    }
    $form = [
              'active'=>$states,
              'categories'=>$categories,
              'zones'=>$zones,
              'services'=>$services,
              'highlights'=>$highlights,
              'chains'=>$chains
              ];
    return $this->response->array($form);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'key', 'services'), '_method');

		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'action', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'key', 'services');

		if ($validation->passes())
		{
			$business = $this->business->create($data);

			$cat = Input::only('categories');
			//$business->categories()->attach($cat['categories']);
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);
			$business = Business::find($business->id);

      return $this->response->array($business->toArray());
		}

    return $this->response->errorUnauthorized($validation->errors());
	}




	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
    $id = Input::get('id');
		$business = $this->business->findOrFail($id);

		$data = $business->toArray();
		$data['zone'] = count($business->zone) ? $business->zone->toArray():(object)[];
		$data['categories'] = count($business->categories) ? $business->categories->toArray():[];

		$data['timings'] = count($business->timings) ? $business->timings->toArray():[];
		$data['services'] = count($business->services) ? $business->services->toArray():[];
		$data['highlights'] = count($business->highlights) ? $business->highlights->toArray():[];
		//$data['photos'] = $business->photos() ? $business->photos->toArray():[];
		$data['photos'] = array();
		foreach($business->photos as $photo){
			$data['photos'][] = ['id'=>$photo->id, 'thumb'=>$photo->image->url('thumbnail'), 'full'=>$photo->image->url()];
		}
		$data['rateCards'] = array();//$business->rateCards() ? $business->rateCards->toArray():[];

    foreach($business->rateCards as $photo){
      $data['rateCards'][] = ['id'=>$photo->id, 'thumb'=>$photo->image->url('thumbnail'), 'full'=>$photo->image->url()];
    }

    return $this->response->array($data);
	}

  public function reload(){
    $ids = Input::get('ids');
    //if(count($ids)>0):
    //  // temp
    //  $ids = (array_chunk($ids, count($ids)/2));
    //  $ids = array_pop($ids);
    //  // <-temp
    //endif;

    $key = Input::get('key');
    $apiKey = ApiKey::where('key', '=', $key)->first();


    $businesses = $this->business->whereIn('id', $ids)//->get()->toArray();
                      ->where('updated_at', '>', $apiKey->updated_at)->get()->toArray();


    $apiKey->touch();

    $result = array('data'=>$businesses);

    return $this->response->array($result);

  }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){

		$business = $this->business->find($id);

		if (is_null($business)){
			return Redirect::route('admin.businesses.index');
		}

		$selected_categories = $business->categories()->lists('category_id');

		$categories = Category::where('parent_id', null)->get();
		$services = Service::where('parent_id', null)->get();
		$highlights = Highlight::all();

		$selected_services = $business->services()->lists('service_id');
		$selected_highlights = $business->highlights()->lists('highlight_id');

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.edit', compact('business', 'selected_categories', 'categories', 'services', 'selected_services', 'highlights', 'selected_highlights'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'key', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'action',  'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'key', 'services');
    $id = Input::get('id');

		if ($validation->passes())
		{
			$business = $this->business->find($id);
			$business->update($data);


			$cat = Input::only('categories');
			if($cat['categories'])
        $business->categories()->sync($cat['categories']?$cat['categories']:[]);

      return $this->response->array($business->toArray());
		}

    return $this->response->errorUnauthorized("Something went wrong.");

	}

  public function deleteImages()
  {
    $id = Input::get('id');
    $business = $this->business->find($id);
    $deletablePhotos = Input::only('deletablePhotos');

    if($deletablePhotos['deletablePhotos'])
    {
      $business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);
    }


    return $this->response->array($business->toArray());
  }

  public function updateRateCards()
  {
    $id = Input::get('id');
    $business = $this->business->find($id);

    $rate_card = Input::file('rate_card');
    if($rate_card)
    {
      $business->saveRateCards([$rate_card]);
    }

    //$rate_cards = Input::only('rate_card');

    //if($rate_cards['rate_card'])
    //{
    //  $business->saveRateCards($rate_cards['rate_card']);
    //}


    return $this->response->array($business->toArray());
  }


  public function updatePhotos(){
    $id = Input::get('id');
    $business = $this->business->find($id);
    //$images = Input::only('images');
    $image = Input::file('image');

    if($image){
      $business->saveImages([$image]);
    }

    //if($images['images'])
    //{
    //  $business->saveImages($images['images']);
    //}
    return $this->response->array($business->toArray());
  }

  public function updateServices()
  {
    $id = Input::get('id');
    $business = $this->business->find($id);
    $cat = Input::only('services');

    if($cat['services'])
    {
      $business->services()->sync($cat['services']?$cat['services']:[]);
    }


    return $this->response->array($business->toArray());
  }



  public function updateHighlights()
  {
    $id = Input::get('id');
    $business = $this->business->find($id);
    $hi = Input::only('highlights');
    if($hi['highlights']){
      $business->highlights()->sync($hi['highlights']?$hi['highlights']:[]);
    }

    return $this->response->array($business->toArray());
  }


  public function updateTimings(){
    $id = Input::get('id');
    $business = $this->business->find($id);

    $timings = Input::only('timings');
    if($timings['timings']){
      $business->saveTimings($timings['timings']);
    }

    return $this->response->array($business->toArray());
  }


  public function updateLocation(){

    $data = Input::only('landmark','geolocation_latitude','geolocation_longitude',
                        'geolocation_address', 'geolocation_city','geolocation_state',
                        'geolocation_country');
    $id = Input::get('id');

    $business = $this->business->find($id);
    $business->update($data);

    return $this->response->array($business->toArray());
  }




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->business->find($id)->delete();

		return Redirect::route('admin.businesses.index');
	}

}
