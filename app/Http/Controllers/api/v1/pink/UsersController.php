<?php

namespace App\Http\Controllers\api\v1\pink;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide;
use User, Input, Validator, Response, Role, App;

use App\Http\Controllers\BaseController;

class UsersController extends Controller {
  use ControllerTrait;

  public function register(){

    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    if($validation->passes()):

      $repo = App::make('UserRepository');
      $user = $repo->signup(Input::all());

      if ($user->id) {

        $role = Role::where('name', '=', 'user')->first();
        $user->attachRole($role);
        $user->confirmed = 1;
        $user->save();
        $user->update($data);
        if (Config::get('confide::signup_email')) {
          Mail::queue(
            Config::get('confide::email_account_confirmation'),
            compact('user'),
            function ($message) use ($user) {
              $message->to($user->email, $user->username)
                      ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
            }
          );
        }

        return $this->response->array($user->toArray());

      } else {

        return $this->response->errorBadRequest($user->errors()->all(':message'));
      }
    else:
      return $this->response->errorBadRequest($validation->messages()->toArray());

    endif;
  }





  public function authenticate() {
    $credentials['username'] = Input::get('username');
    $credentials['password'] = Input::get('password');

    $validator = Validator::make([
        'username' => $credentials['username'],
        'password' => $credentials['password']
      ],
      [
        'username' => 'required|max:255',
        'password' => 'required|max:255'
      ]
    );

    if ($validator->fails()) {
      return $this->response->errorBadRequest($validator->messages()->toArray());
    }

    try {
      $user = User::whereUsername($credentials['username'])->first();
      $credentials['email'] = $user->email;
    } catch (\ErrorException $e) {
      return $this->response->errorUnauthorized("Your username or password is incorrect");
    }

    if (Confide::logAttempt($credentials) == false) {
        return $this->response->errorUnauthorized("Your username or password is incorrect");
    }



    // We have validated this user
    // Assign an API key for this session
    $apiKey = ApiKey::where('user_id', '=', $user->id)->first();
    if (!isset($apiKey)) {
      $apiKey                = new ApiKey;
      $apiKey->user_id       = $user->id;
      $apiKey->key           = $apiKey->generateKey();
      $apiKey->level         = 5;
      $apiKey->ignore_limits = 0;
    } else {
      $apiKey->generateKey();
    }

    if (!$apiKey->save()) {
      return $this->response->errorInternalError("Failed to create an API key. Please try again.");
    }

    // We have an API key.. i guess we only need to return that.
    return $this->response->withItem($apiKey, new ApiKeyTransformer);
    // We have an API key.. i guess we only need to return that.
    // return $this->response->array($user->toArray());
  }

    public function getUserDetails() {
        $key = Input::get('key');
        $validator = Validator::make([
                'key' => $key,
            ],
            [
                'key' => 'required'
            ]
        );

        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->messages()->toArray());
        }

        $apiKey = ApiKey::where('key', '=', $key);

        if(!$apiKey){
            return $this->response->errorNotFound();
        }

        $user = User::where('id', $apiKey->first()->user_id)->first();

        return isset($user) ? $this->response->array($user->toArray()) : $this->response->errorNotFound();
    }

    public function deauthenticate() {
        $key = Input::get('key');
        $validator = Validator::make([
                'key' => $key,
            ],
            [
                'key' => 'required'
            ]
        );


        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->messages()->toArray());
        }

        $apiKey = ApiKey::where('key', '=', $key);

        if(!$apiKey){
            return $this->response->errorUnauthorized("There is no such user to deauthenticate.");
        }

        $apiKey->delete();

        return $this->response->withArray([
            'ok' => [
                'code'      => 'SUCCESSFUL',
                'http_code' => 200,
                'message'   => 'User was successfuly deauthenticated'
            ]
        ]);
    }

    public function error404(){
        return $this->response->errorBadRequest("Invalid Action.");
    }

}
