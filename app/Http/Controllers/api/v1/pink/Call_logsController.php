<?php
namespace App\Http\Controllers\api\v1\pink;

use App\Http\Controllers\BaseController;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use App\Models\Category;
use App\Models\Virtual_number;
use App\Models\Call_log;

 use Response, Mail, Input, Validator;

class Call_logsController extends Controller {

	/**
	 * Call_log Repository
	 *
	 * @var Call_log
	 */
	protected $call_log, $virtual_number;


	public function __construct(Call_log $call_log, Virtual_number $virtual_number)
	{
		$this->call_log = $call_log;
		$this->virtual_number = $virtual_number;
	}

	public function create(){

		$input = Input::only(Call_log::$fields);
		$validation = Validator::make($input, Call_log::$rules);
		if ($validation->passes()){
			$vn = Virtual_number::query()->byBody($input['called_number']);//'+97145508313');//
			if($vn->count() > 0){
				$current_business = $vn->get()->first()->current_business();//->get()->first();

				if($current_business){
					$input['business_id'] = $current_business->id;
					$cvn = $vn->get()->first()->current_virtual_number_allocations();
					$input['is_ppl'] = $cvn->is_ppl;
					$call_log = $this->call_log->create($input);
					
					$call_log = $call_log->toArray();
			    $result = mzk_api_response(compact('call_log'), 200, true, 'Success');
		      $call_log['business_name'] = $current_business->name;
		      $merchant = $current_business->merchants()->first();
		      $email = $merchant->email;
		      $poc_email = $merchant->pocs()->first()->email; 

		      Mail::send('emails.notify-call', ['data'=>$call_log, 'server'=>$_SERVER], function($message) use ($call_log, $poc_email, $email) {
		        $message->to($email, 'Fabogo')->from('no-reply@fabogo.com')
			        			->bcc($poc_email, 'Sales POC')
		                ->subject('You just recieved a call from Fabogo');
		      });

			    //return $this->response->withArray($result)->setStatusCode(200);
					$result = mzk_api_success_response($result);
					return Response::Json($result);

				}else{
					$result = mzk_api_success_response('Called number is not allocated to an existing merchant');
					return Response::Json($result);

			    //return $this->response->errorUnauthorized('Called number is not allocated to an existing merchant');
				}
			}else{
				$result = mzk_api_success_response('Called number does not exist.');
				return Response::Json($result);

			}
		}
		$result = mzk_api_success_response($validation->errors());
		return Response::Json($result);
	}

}
