<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Validator, Lang;
use MazkaraHelper;
use App\Http\Controllers\Controller;

use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Favorite;
use App\Models\Review;
use App\Models\User;
use App\Models\Activity;
use App\Models\Selfie;

class SelfiesController extends Controller {

	/**
	 * Post Repository
	 *
	 * @var Post
	 */
	protected $post;
	protected $params;

	protected $filters;

	public function __construct(Selfie $post){
		$this->post = $post;
		$this->params = [];
		$this->filters = [];
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->post = $this->post->with('cover', 'likes', 'comments')->isViewable();
		$this->filterAllPosts();
		$posts = $this->post->orderBy('id', 'DESC')->paginate(10);

    $suggested_posts = Post::with('cover')->isViewable()->take(5)->get();

    $service_ids = DB::table('post_service')->distinct('service_id')->take(5)->lists('service_id');
		$service_tags = Service::whereIn('id', $service_ids)->get();
		$params = $this->params;
    $filters = $this->filters;
	
  	View::make('site.posts.index', compact('posts', 'service_tags', 'filters', 'params', 'suggested_posts'));
	}

	public function filterAllPosts(){
		$this->filterByAuthor();
    $this->filterBySearch();
		$this->filterByServiceTag();
	}

  public function filterByAuthor(){
    if(Input::has('author') && (Input::get('author')!='')){
      $author = Input::get('author');
      $this->post->ofAuthors($author);
      $this->setParams('author', $author);
      $author = User::find($author);
      $this->setFilters('second-heading', 'Stories by '.$author->full_name);
    }
  }

	public function filterBySearch(){
    if(Input::has('search') && (Input::get('search')!='')){
    	$search = Input::get('search');
    	$this->post->ofSearch($search);
	    $this->setParams('search', $search);
    }
	}

	public function filterByServiceTag(){
    if(Input::has('st') && (Input::get('st')!='')){
    	$st = Input::get('st');
    	$this->post->ofServices([$st]);
	    $this->setParams('st', $st);
    }
	}
  protected function setFilters($key, $val){
    $this->filters[$key] = $val;
  }

  protected function getFilters($key){
    return isset($this->filters[$key]) ? $this->filters[$key] : '';
  }

	protected function setParams($key, $val){
		$this->params[$key] = $val;
	}

  protected function getParams($key){
    return isset($this->params[$key]) ? $this->params[$key] : '';
  }

  public function follow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = mzk_get_class($post);

    if(!$user->hasFavourited($class_name, $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>$class_name, 'favorable_id'=>$id]);
    }
  }

  public function unfollow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = mzk_get_class($post);
    
    if($user->hasFavourited($class_name, $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', $class_name)->delete();
    }
  }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
    $user = User::find(Auth::user()->id);//Auth::user()->id);

    $activities = [];//Activity::select()->byUserIds([$user->id])->orderby('id', 'desc')->paginate(10);

    return View::make('site.users.selfies.create', compact('user', 'activities'));

		//return View::make('posts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$input = Input::all();

		$validation = Validator::make($input, Post::$selfie_rules);
		if ($validation->passes())
		{
      $data = Input::only(Post::$fields);
      $data['type'] = 'photo';
			$post = $this->post->create($data);

			
			$post->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$post->services()->sync($input['services']);
			}

      return Redirect::to($post->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$post = $this->post->with('cover')->find($slug);
    $suggested_posts = Post::with('cover')->isViewable()->take(5)->get();
    $suggested_venues = Business::ofServices($post->services()->lists('service_id','service_id'))->take(5)->get();

    $this->layout = View::make('layouts.parallax');
    $ads = $this->setupAdsForDisplay($post->services()->lists('service_id','service_id'));

    $this->layout->content =  View::make('site.posts.show', compact('post', 'suggested_venues', 'suggested_posts', 'ads'));
	}

  protected function setupAdsForDisplay($services = false, $zone = false){
    //$category = $this->getParams('category')?$this->getParams('category'):[];

    $categories = mzk_categories_from_services($services);
    $zone = $zone ? $zone : mzk_get_localeID();

    $zones = $zone ? array_merge([$zone], array_keys(Zone::where('city_id','=',$zone)->lists('id', 'id'))):[];

    //$business_zones = Business_zone::select()->byZones($zones)->lists('id', 'id');
    $business_zones = Zone::whereIn('id', $zones)->lists('business_zone_id','business_zone_id');
    $ads = mzk_get_ad_lists_to_show_today(['categories'=>$categories, 
                                                 'business_zones'=>$business_zones]);
    return $ads;
  }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = $this->post->find($id);

    if (is_null($post) || !($post->isEditableBy(Auth::user()->id)))
		{
			return Redirect::back();
		}
    $user = User::find($post->author_id);//Auth::user()->id);

    $activities = [];//Activity::select()->byUserIds([$user->id])->orderby('id', 'desc')->paginate(10);

    $this->layout = View::make('layouts.parallax');

    $this->layout->content = View::make('site.users.selfies.edit', compact('post','user', 'activities'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Post::$selfie_rules);

		if ($validation->passes())
		{
			$post = $this->post->find($id);
			$post->update(Input::only(Post::$fields));
			$post->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$post->services()->sync($input['services']);
			}

      return Redirect::to($post->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->post->find($id)->delete();

		return Redirect::back();
	}

}
