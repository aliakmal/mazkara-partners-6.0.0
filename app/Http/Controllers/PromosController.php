<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;
use App\Http\Controllers\Controller;

use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;


class PromosController extends Controller {

	/**
	 * Promo Repository
	 *
	 * @var Promo
	 */
	protected $promo;

	public function __construct(Promo $promo)
	{
		$this->promo = $promo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$promos = $this->promo->all();

		return View::make('promos.index', compact('promos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('promos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Promo::$rules);

		if ($validation->passes())
		{
			$this->promo->create($input);

			return Redirect::route('promos.index');
		}

		return Redirect::route('promos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$promo = $this->promo->findOrFail($id);

		return View::make('promos.show', compact('promo'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$promo = $this->promo->find($id);

		if (is_null($promo))
		{
			return Redirect::route('promos.index');
		}

		return View::make('promos.edit', compact('promo'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Promo::$rules);

		if ($validation->passes())
		{
			$promo = $this->promo->find($id);
			$promo->update($input);

			return Redirect::route('promos.show', $id);
		}

		return Redirect::route('promos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->promo->find($id)->delete();

		return Redirect::route('promos.index');
	}

}
