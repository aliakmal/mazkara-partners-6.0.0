<?php
use App\Models\Zone;
use App\Models\Category;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');
// 
// Route::get('home', 'HomeController@index');
// 
// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

Route::group(['before' => 'shieldsquare'], function(){



Route::any('api/white', 'HomeController@apiRedirectHack');
Route::any('api/call_logs/create', array('uses'=>'Administration\Call_logsController@logCall'));

Route::any('api/call_logs/test', array('uses'=>'Administration\Call_logsController@testSMS'));




Route::filter('manage_highlights', function()
{
  if (! Entrust::can('manage_highlights') ) // Checks the current user
  {
    return Redirect::to('/admin');
  }
});
Route::filter('manage_zones', function()
{
  if (! Entrust::can('manage_zones') ) // Checks the current user
  {
    return Redirect::to('/admin');
  }
});

Route::filter('manage_users', function()
{
  if (! Entrust::can('manage_users') ) // Checks the current user
  {
    return Redirect::to('/admin');
  }
});
Route::filter('manage_categories', function()
{
  if (! Entrust::can('manage_categories') ) // Checks the current user
  {
    return Redirect::to('/admin');
  }
});

Route::when('admin/zones*', 'manage_zones');
Route::when('admin/highlights*', 'manage_highlights');
Route::when('admin/accounts*', 'manage_users');
Route::when('admin/categories*', 'manage_categories');

Route::get('jobs', ['before' => 'mazkara.cache', 'after' => 'mazkara.cache', 'uses'=>'HomeController@jobs']);
Route::get('jobs/{id}', ['before' => 'mazkara.cache', 'after' => 'mazkara.cache', 'uses'=>'HomeController@showJob']);
Route::get('about', ['before' => 'mazkara.cache', 'after' => 'mazkara.cache', 'uses'=>'HomeController@about']);
Route::get('contact', ['before' => 'mazkara.cache', 'after' => 'mazkara.cache', 'uses'=>'HomeController@contact']);
Route::post('contact', ['before' => 'mazkara.cache', 'after' => 'mazkara.cache', 'uses'=>'HomeController@postContact']);
Route::get('culture', ['uses'=>'HomeController@culture']);

$locale = Request::segment(1);

if (!Cache::has(mzk_cache_name('zones.cities.slug.list'))){
  $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);
  Cache::add(mzk_cache_name('zones.cities.slug.list'), Zone::cities()->get()->lists('slug', 'id')->all(), $expiresAt);
}

if (!Cache::has(mzk_cache_name('zones.subzones.slug.list'))){
  $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);
  Cache::add(mzk_cache_name('zones.subzones.slug.list'), Zone::notCities()->get()->lists('slug', 'id')->all(), $expiresAt);
}

Route::pattern('city', '[a-z0-9-]+');
Route::pattern('subzone', '[a-z0-9-]+');

Route::bind('city', function($value, $route) {  
  $locales = Cache::get(mzk_cache_name('zones.cities.slug.list'));

  if($zid = array_search($value, $locales)){
    return $zid;
  }
  App::abort(404);
});

Route::bind('subzone', function($value, $route) {  
  $locales = Cache::get(mzk_cache_name('zones.subzones.slug.list'));

  if($zid = array_search($value, $locales)){
    return $zid;
  }
  App::abort(404);
});


//Cache::get('zones.cities.list');
//if(in_array($locale, MazkaraHelper::getCitiesSlugList())):
  //Route::get('{city}', 'BusinessesController@listByCity');
  //Route::get('{city}/beauty-and-wellness-centers', 'BusinessesController@listByCity');
  //Route::get('{city}/{subzone}-beauty-and-wellness-centers', 'BusinessesController@listByCityAndZone');
  //Route::get('{city}/{slug}', ['as'=>'businesses.view.by.slug', 'uses'=> 'BusinessesController@slug']);
  //Route::get('{city}/{slug}', ['as'=>'businesses.view', 'uses'=> 'BusinessesController@slug']);
  //Route::get('find/{category-slug}', ['as'=>'businesses.search.by.category', 'uses'=> 'BusinessesController@category']);
//endif;

Route::group(['before' => ''], function(){


Route::group(array('before' => 'mazkara.cache', 'after' => 'mazkara.cache'), function(){
  //remember(1800, 'city.route.listings')
  $cities = mzk_cities_as_associative_from_json('id', 'slug');

  foreach($cities as $city_id => $city):
  //  Route::get($city, [ 'as'=>'businesses.list.by.city.for.'.$city, 
  //                      'uses'=>'BusinessesController@listByCity']);



    Route::get($city, [ 'as'=>'businesses.list.by.city.for.'.$city, 
                        'uses'=>'HomeController@index']);
    Route::get($city.'/beauty-and-wellness-centers/{services?}',                      
                      function() use($city){
                        return Redirect::to(route('businesses.list.by.city.append.for.'.$city));
                      });

    Route::get($city.'/salons-spas-and-fitness/{services?}', 
                      function() use($city){
                        return Redirect::to(route('businesses.list.by.city.append.for.'.$city));
                      });

    Route::get($city.'/salons-and-spas/{services?}', 
                      ['as'=>'businesses.list.by.city.append.for.'.$city, 
                        'uses'=>'BusinessesController@listByCity']);

    Route::get($city.'/{subzone}-beauty-and-wellness-centers/{services?}', 
                      function() use($city){
                        return Redirect::to(route('businesses.list.by.city.and.zone.for.'.$city));
                      });

    Route::get($city.'/{subzone}-salons-spas-and-fitness/{services?}', 
                      function() use($city){
                        return Redirect::to(route('businesses.list.by.city.and.zone.for.'.$city));
                      });
    Route::get($city.'/{subzone}-salons-and-spas/{services?}', 
                      ['as'=>'businesses.list.by.city.and.zone.for.'.$city, 
                        'uses'=>'BusinessesController@listByCityAndZone']);
    //remember(1800, 'category.route.listings')
    $categories = mzk_categories_as_associative_from_json('id', 'slug');
    $categories = array_reverse($categories);
    foreach($categories as $category_id=>$category_slug):
      
      $name = 'businesses.list.category.'.$category_slug.'.by.city.append.for.'.$city;
      Route::get($city.'/'.(str_plural($category_slug)), 
                  ['as'=>$name, 'uses'=>'BusinessesController@listByCity']);

      Route::get($city.'/'.(str_plural($category_slug)).'/{services?}', 
                  ['as'=>$name, /*'uses'=>'BusinessesController@listByCity']*/
                      function($services) use($city){
                        return Redirect::to($city.'/salons-and-spas/'.$services);
                      }
                  ]);
      
      $name = 'businesses.list.category.'.$category_slug.'.by.city.and.zone.for.'.$city;
      Route::get($city.'/{subzone}-'.(str_plural($category_slug)), 
                  ['as'=>$name, 'uses'=>'BusinessesController@listByCityAndZone']);

      Route::get($city.'/{subzone}-'.(str_plural($category_slug)).'/{services?}', 
                  ['as'=>$name,
                     function($subzone, $services) use($city){
                        if(is_numeric($subzone)){
                          $subzone = Zone::find($subzone)->slug; 
                        }
                        return Redirect::to($city.'/'.$subzone.'-salons-and-spas/'.$services);
                      }
                  ]);


    endforeach;

    Route::get($city.'/beauty-and-wellness-centers-chain/{chain}', 
                      ['as'=>'businesses.chains.by.city.legacy.append.for.'.$city, 
                        'uses'=>'BusinessesController@listChainsByCity']);

    Route::get($city.'/salons-spas-and-fitness-chain/{chain}', 
                      ['as'=>'businesses.chains.by.city.append.for.'.$city, 
                        'uses'=>'BusinessesController@listChainsByCity']);
    //Route::get($city.'/{slug}', ['as'=>'businesses.view', 'uses'=> 'BusinessesController@slug']);
    Route::get('find/{category-slug}', 
                  ['as'=>'businesses.search.by.category', 
                  'uses'=> 'BusinessesController@category']);
  endforeach;

  Route::resource('businesses', 'BusinessesController');
});

//remember(1800, 'city.route.listings')
$cities = mzk_cities_as_associative_from_json('id', 'slug');

foreach($cities as $city_id => $city):
//foreach(Zone::query()->cities()->get()->lists('slug', 'id')->all() as $city):

  Route::get($city.'/directory', [ 'as'=>'sitemap.for.'.$city, 
                                'uses'=> 'HomeController@sitemapForCity']);

  Route::get($city.'/directory/venues', [ 'as'=>'sitemap.venues.for.'.$city, 
                                'uses'=> 'HomeController@sitemapVenuesForCity']);

  Route::get($city.'/directory/locations', [ 'as'=>'sitemap.locations.for.'.$city, 
                                'uses'=> 'HomeController@sitemapLocationsForCity']);

  Route::get($city.'/{slug}', [ 'as'=>'businesses.view.by.slug.for.'.$city, 
                                'uses'=> 'BusinessesController@slug']);

  Route::get($city.'/{slug}/rate-card', [ 'as'=>'businesses.rates.by.slug.for.'.$city, 
                                          'uses'=> 'BusinessesController@slugRateCard']);

  Route::get($city.'/{slug}/packages', [ 'as'=>'businesses.packages.by.slug.for.'.$city, 
                                          'uses'=> 'BusinessesController@slugPackages']);

  Route::get($city.'/{slug}/photos', ['as'=>'businesses.photos.by.slug.for.'.$city, 
                                      'uses'=> 'BusinessesController@slugPhotos']);

  Route::get($city.'/{slug}/packages', ['as'=>'businesses.packages.by.slug.for.'.$city, 
                                        'uses'=> 'BusinessesController@slugPackages']);

  Route::get($city.'/{slug}/reviews', ['as'=>'businesses.reviews.by.slug.for.'.$city, 
                                        'uses'=> 'BusinessesController@slugReviews']);

  Route::get($city.'/{slug}/location', ['as'=>'businesses.location.by.slug.for.'.$city, 
                                        'uses'=> 'BusinessesController@slugLocation']);

  // Route::get($city.'/promo/{slug}/{id}', ['as'=>'deals.promo.by.slug.for.'.$city, 'uses'=> 'DealsController@slug']);
  // Route::get($city.'/package/{slug}/{id}', ['as'=>'deals.package.by.slug.for.'.$city, 'uses'=> 'DealsController@slug']);
  // Route::get($city.'/discount/{slug}/{id}', ['as'=>'deals.discount.by.slug.for.'.$city, 'uses'=> 'DealsController@slug']);

  Route::get($city.'/{chain}/outlets', 
                    ['as'=>'businesses.groups.by.city.append.for.'.$city, 
                      'uses'=>'BusinessesController@listGroupsByCity']);

  Route::get($city.'/claim/{id}', ['as'=>'claim.businesses.get.for.'.$city, 'uses'=> 'BusinessesController@getClaim']);
  Route::post($city.'/claim/{id}', ['as'=>'claim.businesses.post.for.'.$city, 'uses'=> 'BusinessesController@postClaim']);

endforeach;

Route::group(array('prefix'=>'admin' ), function(){

  //  Confide routes
  Route::post('/tag', 'Administration\\HomeController@tag');

  //  Route::get('users', 'Administration\\UsersController@index');
  //  Route::get('users/create', 'Administration\\UsersController@create');
  //  Route::post('users', 'Administration\\UsersController@store');
  Route::get('users/login', 'Administration\\UsersController@login');
  //  Route::post('users/login', 'Administration\\UsersController@doLogin');
  Route::get('users/confirm/{code}', 'Administration\\UsersController@confirm');
  Route::get('users/forgot_password', 'Administration\\UsersController@forgotPassword');
  Route::post('users/forgot_password', 'Administration\\UsersController@doForgotPassword');
  Route::get('users/reset_password/{token}', 'Administration\\UsersController@resetPassword');
  Route::post('users/reset_password', 'Administration\\UsersController@doResetPassword');
  Route::get('users/logout', 'Administration\\UsersController@logout');

  
});

Route::get('businesses/increment/views/count', 'BusinessesController@incrementPageView');
Route::get('businesses/increment/calls/count', 'BusinessesController@incrementCallView');
Route::get('ads/increment/ctr', 'HomeController@incrementCTR');

});



Route::group(array('before' => 'admin.auth', 'prefix'=>'admin' ), function(){
  Route::get('/', ['as'=>'admin.launchpad', 'uses'=>'Administration\\HomeController@launchpad']);
  Route::get('/set-locale/{slug}', ['as'=>'admin.set.locale', 'uses'=>'Administration\\HomeController@setCurrentLocale']);
  Route::get('dashboard', ['as'=>'admin.live.statistics', 'uses'=>'Administration\\HomeController@liveStatistics']);
  Route::get('get-live-statistics', ['as'=>'admin.get.live.statistics', 'uses'=>'Administration\\HomeController@getLiveStatistics']);



  Route::get('businesses/{id}', array( function($id)
  {
        return Redirect::to('/content/businesses/'.$id);
  }));

});
Route::group(array('before' => 'admin.auth', 'prefix'=>'sms' ), function(){

  Route::get('/', ['as'=>'admin.sms.index', 'uses'=>'Administration\\SmsController@index']);
  Route::get('send', ['as'=>'admin.sms.get', 'uses'=>'Administration\\SmsController@getSend']);
  Route::post('send', ['as'=>'admin.sms.post', 'uses'=>'Administration\\SmsController@postSend']);
  Route::get('settings', ['as'=>'admin.sms.get.settings', 'uses'=>'Administration\\SmsController@getSettings']);
  Route::post('settings', ['as'=>'admin.sms.post.settings', 'uses'=>'Administration\\SmsController@postSettings']);
});

Route::group(array('before' => 'admin.auth', 'prefix'=>'restricted' ), function(){
  Route::get('/', 'Administration\\AccountsController@index');
  Route::get('accounts/export', ['as'=>'admin.accounts.export', 'uses'=>'Administration\\AccountsController@export']);
  Route::get('reports', ['as'=>'admin.accounts.reports', 'uses'=>'Administration\\AccountsController@reports']);

  Route::get('users/data', ['as'=>'admin.accounts.data', 'uses'=>'Administration\\AccountsController@data']);
  Route::get('reviews/data', ['as'=>'admin.reviews.data', 'uses'=>'Administration\\ReviewsController@data']);
  Route::get('reviews/reports', ['as'=>'admin.reviews.reports', 'uses'=>'Administration\\ReviewsController@reports']);
  Route::get('reviews/hide/{id}', ['as'=>'admin.reviews.hide', 'uses'=>'Administration\\ReviewsController@hideReview']);
  Route::get('reviews/unhide/{id}', ['as'=>'admin.reviews.unhide', 'uses'=>'Administration\\ReviewsController@unhideReview']);

  Route::get('settings', ['as'=>'admin.auth.get.settings', 'uses'=>'Administration\\SettingsController@getSettings']);
  Route::post('settings', ['as'=>'admin.auth.post.settings', 'uses'=>'Administration\\SettingsController@postSettings']);

  Route::get('users/{id}/ban', ['as'=>'admin.auth.users.ban', 'uses'=>'Administration\\AccountsController@getBan']);
  Route::get('users/{id}/unban', ['as'=>'admin.auth.users.unban', 'uses'=>'Administration\\AccountsController@getUnban']);

  Route::resource('reviews', 'Administration\\ReviewsController', array('names' => mzk_route_helper('reviews', 'admin')));
  Route::resource('accounts', 'Administration\\AccountsController', array('names' => mzk_route_helper('accounts', 'admin')));
});

Route::group(array('before' => 'admin.auth', 'prefix'=>'content' ), function(){

  Route::get('/', ['as'=>'admin.dashboard', 'uses'=>'Administration\\HomeController@adminDashboard']);
  Route::get('accounts/{id}', array( function($id)
  {
    return Redirect::to('/restricted/accounts/'.$id);
  }));

  Route::get('sms/send', ['as'=>'admin.sms.get', 'uses'=>'Administration\\SmsController@getSend']);
  Route::post('sms/send', ['as'=>'admin.sms.post', 'uses'=>'Administration\\SmsController@postSend']);

  Route::any('businesses/bulk', ['as'=>'admin.businesses.bulk', 'uses'=>'Administration\\BusinessesController@bulk']);
  Route::get('businesses/{id}/basic', ['as'=>'admin.businesses.get.basic', 'uses'=>'Administration\\BusinessesController@getBasic']);
  Route::post('businesses/{id}/basic', ['as'=>'admin.businesses.post.basic', 'uses'=>'Administration\\BusinessesController@postBasic']);

  Route::get('businesses/home-create', ['as'=>'admin.businesses.get.home.create', 'uses'=>'Administration\\BusinessesController@getCreateHomeBasic']);
  Route::post('businesses/home-create', ['as'=>'admin.businesses.post.home.create', 'uses'=>'Administration\\BusinessesController@postCreateHomeBasic']);


  Route::get('businesses/{id}/home-basic', ['as'=>'admin.businesses.get.home.basic', 'uses'=>'Administration\\BusinessesController@getHomeBasic']);
  Route::post('businesses/{id}/home-basic', ['as'=>'admin.businesses.post.home.basic', 'uses'=>'Administration\\BusinessesController@postHomeBasic']);

  Route::get('businesses/{id}/meta-update', ['as'=>'admin.businesses.meta.update', 'uses'=>'Administration\\BusinessesController@metaUpdate']);

  Route::any('businesses/{id}/bulk/cheat/rate', ['as'=>'admin.business.bulk.cheat.rate', 'uses'=>'Administration\\BusinessesController@postCheatRates']);

  Route::get('businesses/export', ['as'=>'admin.businesses.export', 'uses'=>'Administration\\BusinessesController@export']);

  Route::get('businesses/{id}/services', ['as'=>'admin.businesses.get.services', 'uses'=>'Administration\\BusinessesController@getServices']);
  Route::post('businesses/{id}/services', ['as'=>'admin.businesses.post.services', 'uses'=>'Administration\\BusinessesController@postServices']);
  Route::post('businesses/{id}/ajax/services', ['as'=>'admin.businesses.ajax.services', 'uses'=>'Administration\\BusinessesController@postAjaxServices']);

  Route::get('businesses/{id}/highlights', ['as'=>'admin.businesses.get.highlights', 'uses'=>'Administration\\BusinessesController@getHighlights']);
  Route::post('businesses/{id}/highlights', ['as'=>'admin.businesses.post.highlights', 'uses'=>'Administration\\BusinessesController@postHighlights']);

  Route::get('businesses/{id}/timings', ['as'=>'admin.businesses.get.timings', 'uses'=>'Administration\\BusinessesController@getTimings']);
  Route::post('businesses/{id}/timings', ['as'=>'admin.businesses.post.location', 'uses'=>'Administration\\BusinessesController@postTimings']);

  Route::get('businesses/{id}/location', ['as'=>'admin.businesses.get.location', 'uses'=>'Administration\\BusinessesController@getLocation']);
  Route::post('businesses/{id}/location', ['as'=>'admin.businesses.post.location', 'uses'=>'Administration\\BusinessesController@postLocation']);

  Route::get('businesses/{id}/photos', ['as'=>'admin.businesses.show.photos', 'uses'=>'Administration\\BusinessesController@getPhotos']);
  Route::post('businesses/{id}/photos', ['as'=>'admin.businesses.save.photos', 'uses'=>'Administration\\BusinessesController@postPhotos']);
  Route::post('businesses/{id}/bulk/photos', ['as'=>'admin.businesses.save.bulk.photos', 'uses'=>'Administration\\BusinessesController@postBulkPhotos']);



  Route::post('businesses/{id}/stock/photos', ['as'=>'admin.businesses.save.stock.photo', 'uses'=>'Administration\\BusinessesController@postStockPhoto']);

  Route::get('businesses/{id}/rate-cards', ['as'=>'admin.businesses.show.rate_cards', 'uses'=>'Administration\\BusinessesController@getRateCards']);
  Route::post('businesses/{id}/rate-cards', ['as'=>'admin.businesses.save.rate_cards', 'uses'=>'Administration\\BusinessesController@postRateCards']);
  Route::post('businesses/{id}/fix-photos', ['as'=>'admin.businesses.fix.photos', 'uses'=>'Administration\\BusinessesController@fixPhotos']);
  Route::post('businesses/{id}/fix-rate-cards', ['as'=>'admin.businesses.fix.rate_cards', 'uses'=>'Administration\\BusinessesController@fixRateCards']);
  Route::post('deals/{id}/fix-photos', ['as'=>'admin.deals.fix.photos', 'uses'=>'Administration\\BusinessesController@fixDealPhotos']);

  Route::get('businesses/{id}/photo/{photoId}', ['as'=>'admin.businesses.show.photo','uses'=>'Administration\\BusinessesController@getPhoto']);
  Route::post('businesses/{id}/photo/{photoId}', ['as'=>'admin.businesses.edit.photo', 'uses'=>'Administration\\BusinessesController@postPhoto'] );

  Route::get('businesses/{id}/photo/crop/{photoId}', ['as'=>'admin.businesses.getCrop.photo','uses'=>'Administration\\BusinessesController@getCrop']);
  Route::post('businesses/{id}/photo/crop/{photoId}', ['as'=>'admin.businesses.postCrop.photo', 'uses'=>'Administration\\BusinessesController@postCrop'] );


  Route::get('businesses/{id}/photo/{photoId}/rotate/{degrees}', ['as'=>'admin.businesses.rotate.photo','uses'=>'Administration\\BusinessesController@rotatePhoto']);
  Route::get('businesses/{id}/resluggify', ['as'=>'admin.businesses.resluggify', 'uses'=>'Administration\\BusinessesController@resluggify']);

  Route::get('businesses/{id}/toggle-sample-menu', ['as'=>'admin.businesses.toggle.sample.menu', 'uses'=>'Administration\\BusinessesController@toggleMenuSample']);

  Route::post('businesses/copy-menu', ['as'=>'admin.businesses.copy.menu', 'uses'=>'Administration\\BusinessesController@copyItemsToVenue']);
  Route::post('businesses/copy-services', ['as'=>'admin.businesses.copy.services', 'uses'=>'Administration\\BusinessesController@copyServicesToVenue']);

  Route::any('menu_groups/bulk/allocate', ['uses'=>'Administration\\Menu_groupsController@bulk_allocate', 'as'=>'admin.menu.groups.bulk.allocate']);

  Route::resource('menu_groups', 'Administration\\Menu_groupsController', array('names' => mzk_route_helper('menu_groups', 'admin')));

  Route::resource('offers', 'Administration\\OffersController', array('names' => mzk_route_helper('offers', 'admin')));
  Route::resource('brands', 'Administration\\BrandsController', array('names' => mzk_route_helper('brands', 'admin')));
  Route::post('businesses/{business_id}/brands', ['as'=>'admin.businesses.post.brands','uses'=>'Administration\\BusinessesController@postBrands']);


  Route::get('businesses/{business_id}/deals/{type}', ['as'=>'admin.businesses.new.deal','uses'=>'Administration\\BusinessesController@createDeal']);
  Route::post('businesses/{business_id}/deals/{type}', ['as'=>'admin.businesses.create.deal','uses'=>'Administration\\BusinessesController@storeDeal']);
  Route::get('businesses/{business_id}/deals/{type}/{id}', ['as'=>'admin.businesses.edit.deal','uses'=>'Administration\\BusinessesController@editDeal']);
  Route::post('businesses/{business_id}/deals/{type}/{id}', ['as'=>'admin.businesses.update.deal','uses'=>'Administration\\BusinessesController@updateDeal']);
  Route::delete('businesses/deals/delete/{id}', array('uses' => 'Administration\\BusinessesController@destroyDeal', 'as' => 'admin.businesses.delete.deal'));
  Route::get('businesses/rate/cheat', ['as'=>'admin.businesses.cheat.rate','uses'=>'Administration\\BusinessesController@cheatRate']);
  Route::get('businesses/rate/uncheat', ['as'=>'admin.businesses.cheat.rate','uses'=>'Administration\\BusinessesController@deleteCheatRate']);



  Route::resource('categories', 'Administration\\CategoriesController', array('names' => mzk_route_helper('categories', 'admin')));
  Route::resource('zones', 'Administration\\ZonesController', array('names' => mzk_route_helper('zones', 'admin')));
  Route::resource('businesses', 'Administration\\BusinessesController', array('names' => mzk_route_helper('businesses', 'admin')));
  Route::resource('highlights', 'Administration\\HighlightsController', array('names' => mzk_route_helper('highlights', 'admin')));
  Route::resource('cities', 'Administration\\CitiesController', array('names' => mzk_route_helper('cities', 'admin')));
  Route::resource('services', 'Administration\\ServicesController', array('names' => mzk_route_helper('services', 'admin')));
  // Route::resource('promotions', 'Administration\\PromotionsController', array('names' => mzk_route_helper('promotions', 'admin')));
  

  Route::post('special_packages/bulk', [  'uses'=>'Administration\\Special_packagesController@bulk',
                                          'as'=>'admin.special_packages.bulk']);
  
  Route::post('special_packages/sort', ['uses'=>'Administration\\Special_packagesController@setOrder', 'as'=>'admin.special_packages.sort']);
  /**/

  Route::resource('jobs', 'Administration\\JobsController', array('names' => mzk_route_helper('jobs', 'admin')));

  Route::get('groups/get-outlets', ['as'=>'admin.groups.outlets', 'uses'=>'Administration\\GroupsController@getOutlets']);

  Route::resource('chains', 'Administration\\ChainsController', array('names' => mzk_route_helper('chains', 'admin')));
  Route::resource('groups', 'Administration\\GroupsController', array('names' => mzk_route_helper('groups', 'admin')));

  Route::resource('special_packages', 'Administration\\Special_packagesController', array('names' => mzk_route_helper('special_packages', 'admin')));
  Route::resource('comments', 'Administration\\CommentsController', array('names' => mzk_route_helper('comments', 'admin')));
  Route::resource('combo_items', 'Administration\\Combo_itemsController', array('names' => mzk_route_helper('combo_items', 'admin')));
  Route::post('combo_items/updatable', ['uses'=>'Administration\\Combo_itemsController@updatable', 'as'=>'admin.combo.items.update']);
  Route::post('menu_groups/updatable', ['uses'=>'Administration\\Menu_groupsController@updatable', 'as'=>'admin.menu.groups.update']);
  Route::post('invoice/items/updatable', ['uses'=>'Administration\\InvoicesController@updateInvoiceItem', 'as'=>'admin.invoices.items.update']);
  Route::post('invoice/attach', ['uses'=>'Administration\\InvoicesController@attach', 'as'=>'admin.invoices.attach']);

  Route::post('special_items/updatable', ['uses'=>'Administration\\Service_itemsController@updatable', 'as'=>'admin.special.items.update']);
  Route::post('offers/updatable', ['uses'=>'Administration\\OffersController@updatable', 'as'=>'admin.offers.items.update']);
  Route::post('comments/updatable', ['uses'=>'Administration\\CommentsController@updatable', 'as'=>'admin.comments.updatable']);

  Route::resource('service_items', 'Administration\\Service_itemsController', array('names' => mzk_route_helper('service_items', 'admin')));

  Route::post('photos/upload', ['uses'=>'Administration\\PhotosController@upload', 'as'=>'admin.photos.upload']);

  Route::post('photos/change/type', ['uses'=>'Administration\\PhotosController@changeType', 'as'=>'admin.photos.change.type']);

  Route::resource('posts', 'PostsController');
  Route::resource('posts', 'Administration\\PostsController', 
                          array('names' => mzk_route_helper('posts', 'admin')));

  Route::post('links/{id}/updare', ['uses'=>'Administration\\SelfiesController@updateUrl', 
                                    'as'=>'admin.url.update']);

  Route::resource('selfies', 'Administration\\SelfiesController', 
                          array('names' => mzk_route_helper('selfies', 'admin')));


  Route::get('service_items/list_tags', ['uses'=>'Administration\\Service_itemsController@listTags', 'as'=>'admin.special.items.listTags']);
});


Route::group(array('before' => 'admin.auth', 'prefix'=>'editor' ), function(){
  Route::get('/', 'Administration\\HomeController@editorDashboard');

  Route::get('/published', ['uses'=>'Administration\\PostsController@published', 
                    'as'=>'admin.posts.published']);
  Route::get('/rejects', ['uses'=>'Administration\\PostsController@rejects', 
                    'as'=>'admin.posts.rejects']);
  Route::get('/all', ['uses'=>'Administration\\PostsController@all', 
                    'as'=>'admin.posts.all']);
  Route::get('/pending', ['uses'=>'Administration\\PostsController@pending', 
                    'as'=>'admin.posts.pending']);
  Route::get('/drafts', ['uses'=>'Administration\\PostsController@drafts', 
                    'as'=>'admin.posts.drafts']);

  Route::get('/archives', ['uses'=>'Administration\\PostsController@archives', 
                    'as'=>'admin.posts.archives']);

  Route::get('posts/{id}/archive', ['as'=>'admin.posts.archive', 
                                    'uses'=>'Administration\\PostsController@archive']);

  Route::get('posts/{id}/unarchive', ['as'=>'admin.posts.unarchive', 
                                    'uses'=>'Administration\\PostsController@archive']);

  Route::resource('posts', 'Administration\\PostsController', 
                          array('names' => mzk_route_helper('posts', 'admin')));
});


Route::group(array('before' => 'finance.auth', 'prefix'=>'finance'), function(){
  Route::get('/', ['as'=>'finance.dashboard', 'uses'=>'Administration\\HomeController@financeDashboard']);

  Route::any('payments/{id}/allocate', ['as'=>'admin.payments.allocate', 
                                      'uses'=>'Administration\\PaymentsController@allocate']);
  Route::any('payments/{id}/deallocate', ['as'=>'admin.payments.deallocate', 
                                      'uses'=>'Administration\\PaymentsController@deallocate']);

  Route::any('invoices/{id}/allocate/payments', ['as'=>'admin.invoice.payments.allocate', 
                                      'uses'=>'Administration\\InvoicesController@allocatePaymentsApplicable']);

  Route::any('payment_applicables/{id}/delete', ['as'=>'admin.payment_applicables.delete', 
                                      'uses'=>'Administration\\InvoicesController@deletePaymentApplicables']);



  Route::get('invoices/{id}/pdf', ['as'=>'admin.invoices.pdf', 
                                      'uses'=>'Administration\\InvoicesController@getPdf']);
  Route::post('invoices/save-tax', ['as'=>'admin.invoices.save.tax', 
                                      'uses'=>'Administration\\InvoicesController@saveTax']);


  Route::post('invoices/save-charges', ['as'=>'admin.invoices.save.bounce-charges', 
                                      'uses'=>'Administration\\InvoicesController@saveBounceCharges']);

  Route::get('credit_notes/{id}/pdf', ['as'=>'admin.credit_notes.pdf', 
                                      'uses'=>'Administration\\Credit_notesController@getPdf']);

  Route::get('invoices/search', ['as'=>'admin.invoices.search', 
                                      'uses'=>'Administration\\InvoicesController@getInvoice']);

  Route::get('payments/search', ['as'=>'admin.payments.search', 
                                      'uses'=>'Administration\\PaymentsController@getPayment']);

  Route::any('payments/{id}/recieved-to-deposited', ['as'=>'admin.payments.recieved-to-deposited', 
                      'uses'=>'Administration\\PaymentsController@changeStatusRecievedToDeposited']);

  Route::any('payments/{id}/deposited-to-returned', ['as'=>'admin.payments.deposited-to-returned', 
                      'uses'=>'Administration\\PaymentsController@changeStatusDepositedToReturned']);

  Route::any('payments/{id}/returned-to-deposited', ['as'=>'admin.payments.returned-to-deposited', 
                      'uses'=>'Administration\\PaymentsController@changeStatusReturnedToDeposited']);

  Route::any('payments/{id}/deposited-to-cleared', ['as'=>'admin.payments.deposited-to-cleared', 
                      'uses'=>'Administration\\PaymentsController@changeStatusDepositedToCleared']);

  Route::resource('credit_notes', 'Administration\\Credit_notesController', 
                              array('names' => mzk_route_helper('credit_notes', 'admin')));

  Route::resource('invoices', 'Administration\\InvoicesController', 
                              array('names' => mzk_route_helper('invoices', 'admin')));
  Route::post('credit_notes/updatable', ['uses'=>'Administration\\Credit_notesController@updatable', 'as'=>'admin.credit_notes.updatable']);

  Route::resource('payments', 'Administration\\PaymentsController', 
                              array('names' => mzk_route_helper('payments', 'admin')));
  Route::get('merchants/call-logs/get-businesses', ['as'=>'admin.merchants.call_logs.businesses', 'uses'=>'Administration\\MerchantsController@getOutletsForMerchantInvoiceForm']);

  Route::get('merchants/toggle-leads-access-for-business/{id}', ['as'=>'admin.merchants.toggle_leads_access.businesses', 'uses'=>'Administration\\MerchantsController@toggleLeadsAccessForBusiness']);
  Route::get('merchants/toggle-resumes-access-for-business/{id}', ['as'=>'admin.merchants.toggle_resumes_access.businesses', 'uses'=>'Administration\\MerchantsController@toggleResumesAccessForBusiness']);
  Route::get('merchants/toggle-debug-for-business/{id}', ['as'=>'admin.merchants.toggle_debug.businesses', 'uses'=>'Administration\\BusinessesController@toggleDebug']);

  Route::get('invoices/ppl/create', ['as'=>'admin.invoices.ppl.create', 'uses'=>'Administration\\InvoicesController@createPPLForm']);
  Route::post('invoices/ppl/store', ['as'=>'admin.invoices.ppl.store', 'uses'=>'Administration\\InvoicesController@storePPL']);
  Route::post('invoices/ppl/update', ['as'=>'admin.invoices.ppl.update', 'uses'=>'Administration\\InvoicesController@updatePPL']);
  Route::get('invoices/get/call-logs', ['as'=>'admin.invoices.get.call_logs', 'uses'=>'Administration\\Call_logsController@getCallLogsForInvoiceForm']);
  Route::get('reports', ['as'=>'finance.reports', 'uses'=>'Administration\\FinanceController@reports']);
  Route::get('reports_old', ['as'=>'finance.reports', 'uses'=>'Administration\\FinanceController@reports_old']);
  Route::get('billings/user', ['as'=>'finance.billings.users', 'uses'=>'Administration\\FinanceController@billingsByPOC']);
  Route::get('billings/city', ['as'=>'finance.billings.cities', 'uses'=>'Administration\\FinanceController@billingsByCity']);
  Route::get('expenses',      ['as'=>'finance.expenses', 'uses'=>'Administration\\FinanceController@expenses']);
  Route::get('expenses/create', ['as'=>'finance.expenses.create', 'uses'=>'Administration\\FinanceController@getCreateExpense']);
  Route::post('expenses/create', ['as'=>'finance.expenses.store', 'uses'=>'Administration\\FinanceController@postCreateExpense']);
  Route::get('expenses/{id}/edit', ['as'=>'finance.expenses.edit', 'uses'=>'Administration\\FinanceController@getEditExpense']);
  Route::post('expenses/{id}/update', ['as'=>'finance.expenses.update', 'uses'=>'Administration\\FinanceController@postUpdateExpense']);
  Route::get('expenses/{id}/destroy', ['as'=>'finance.expenses.delete', 'uses'=>'Administration\\FinanceController@destroyExpense']);

});

Route::group(array('before' => 'crm.auth', 'prefix'=>'crm'), function(){
  Route::get('/', ['as'=>'crm.dashboard', 'uses'=>'Administration\\HomeController@crmDashboard']);
  Route::get('businesses/search', ['as'=>'crm.businesses.search', 'uses'=>'Administration\\BusinessesController@search']);

  Route::resource('business_zones', 'Administration\\Business_zonesController', array('names' => mzk_route_helper('business_zones', 'admin')));  

  Route::get('leads/import', ['as'=>'crm.leads.get.import', 'uses'=>'Administration\\LeadsController@getImport']);
  Route::post('leads/importing', ['as'=>'crm.leads.post.import', 'uses'=>'Administration\\LeadsController@postImport']);

  Route::post('leads/attach', ['as'=>'crm.leads.attach', 'uses'=>'Administration\\LeadsController@attachLeads']);

  Route::resource('leads', 'Administration\\LeadsController', array('names' => mzk_route_helper('leads', 'admin')));
  Route::get('adverts/get/avalable/slots', ['as'=>'crm.adverts.get.slots', 'uses'=>'Administration\\AdvertsController@getAvailableSlots']);
  Route::post('adverts/updatable', ['uses'=>'Administration\\AdvertsController@updatable', 'as'=>'admin.adverts.updatable']);

  Route::resource('adverts', 'Administration\\AdvertsController', array('names' => mzk_route_helper('adverts', 'admin')));
  Route::resource('advert_campaigns', 'Administration\\Advert_campaignsController', array('names' => mzk_route_helper('advert_campaigns', 'admin')));
  Route::resource('wikis', 'Administration\\WikisController', array('names' => mzk_route_helper('wikis', 'admin')));
 

  Route::get('resumes/import', ['as'=>'crm.resumes.get.import', 'uses'=>'Administration\\ResumesController@getImport']);
  Route::post('resumes/importing', ['as'=>'crm.resumes.post.import', 'uses'=>'Administration\\ResumesController@postImport']);
  Route::resource('resumes', 'Administration\\ResumesController', array('names' => mzk_route_helper('resumes', 'admin')));
  Route::post('resumes/attach', ['as'=>'crm.resumes.attach', 'uses'=>'Administration\\ResumesController@attachResumes']);

  Route::post('upload-file', ['as'=>'crm.file-upload', 'uses'=>'Administration\\HomeController@uploadFile']);
  Route::patch('upload-file', ['as'=>'crm.patch.file-upload', 'uses'=>'Administration\\HomeController@uploadFile']);

  Route::resource('ads', 'Administration\\AdsController', array('names' => mzk_route_helper('ads', 'admin')));
  Route::resource('campaigns', 'Administration\\CampaignsController', array('names' => mzk_route_helper('campaigns', 'admin')));

  Route::get('campaigns/{id}/get-available-slots', 
                            ['uses' => 'Administration\\CampaignsController@getAvailableSlots',
                            'as'=>'admin.campaigns.get.available.slots']);

  Route::resource('ad_zones', 'Administration\\Ad_zonesController', array('names' => mzk_route_helper('ad_zones', 'admin')));
  Route::resource('ad_sets', 'Administration\\Ad_setsController', array('names' => mzk_route_helper('ad_sets', 'admin')));

  //Route::resource('call_logs', 'Administration\\Call_logsController');
  Route::resource('virtual_number_allocations', 'Administration\\Virtual_number_allocationsController', array('names' => mzk_route_helper('virtual_number_allocations', 'admin')));

  Route::resource('virtual_numbers', 'Administration\\Virtual_numbersController', array('names' => mzk_route_helper('virtual_numbers', 'admin')));
  Route::get('businesses/{id}/call_logs', ['as'=>'admin.businesses.call_logs',
                                          'uses'=>'Administration\\Call_logsController@getIndex']);

  Route::get('featured', ['as'=>'admin.featured.index', 'uses'=>'Administration\\FeaturedController@index']);
  Route::any('featured/set', ['as'=>'admin.post.featured', 'uses'=>'Administration\\FeaturedController@postFeatured']);

  Route::get('featured/{id}/mark', ['as'=>'admin.featured.mark', 'uses'=>'Administration\\FeaturedController@setFeatured']);
  Route::get('featured/{id}/unmark', ['as'=>'admin.featured.unmark', 'uses'=>'Administration\\FeaturedController@setUnFeatured']);

  Route::get('rich-listings', ['as'=>'admin.rich-listings.index', 'uses'=>'Administration\\RichController@index']);
  Route::get('rich-listings/{id}/mark', ['as'=>'admin.rich-listings.mark', 'uses'=>'Administration\\RichController@setRich']);
  Route::get('rich-listings/{id}/unmark', ['as'=>'admin.rich-listings.unmark', 'uses'=>'Administration\\RichController@setUnrich']);

  Route::get('facebook/likeboxes', ['as'=>'admin.businesses.facebook.index', 'uses'=>'Administration\\BusinessesController@listFacebookLikeBoxed']);
  Route::get('facebook/{id}/edit', ['as'=>'admin.businesses.facebook.edit', 'uses'=>'Administration\\BusinessesController@getFacebookLikeBox']);
  Route::post('facebook/{id}/update', ['as'=>'admin.businesses.facebook.update', 'uses'=>'Administration\\BusinessesController@postFacebookLikeBox']);


  Route::get('businesses/{id}/call_logs/allocation/{allocation_id}', ['as'=>'admin.call_logs.for.virtual_number_allocation',
                                          'uses'=>'Administration\\Call_logsController@showForVirtualNumberAllocation']);


  Route::get('businesses/{id}/virtual_numbers', ['as'=>'admin.businesses.virtual_numbers.show',
                                          'uses'=>'Administration\\BusinessesController@getVirtualNumberAllocation']);

  Route::get('businesses/{id}/toggle/virtual_number', ['as'=>'admin.businesses.virtual_numbers.toggle',
                                          'uses'=>'Administration\\BusinessesController@toggleHoldVirtualNumber']);


  Route::post('virtual_numbers_allocations/item/editable', ['uses'=>'Administration\\Virtual_number_allocationsController@updateVirtualNumberAllocation', 'as'=>'admin.virtual.allocations.item.update']);

  Route::get('businesses/{id}/virtual_numbers/allocate', ['as'=>'admin.virtual_numbers.for.business.allocate',
                                'uses'=>'Administration\\Virtual_number_allocationsController@getAllocateForBusiness']);
  Route::get('businesses/{id}/virtual_numbers/deallocate', ['as'=>'admin.virtual_numbers.for.business.deallocate',
                                'uses'=>'Administration\\Virtual_number_allocationsController@deallocate']);



  Route::get('ad_sets/{adset_id}/new-ad-creative',    ['as'=>'admin.adsets.new.ad', 'uses'=>'Administration\\Ad_setsController@getCreateAd']);
  Route::post('ad_sets/{adset_id}/new-ad-creative',   ['as'=>'admin.adsets.create.ad','uses'=>'Administration\\Ad_setsController@postCreateAd']);
  Route::get('ad_sets/{ad_id}/edit-ad-creative',   ['as'=>'admin.adsets.edit.ad','uses'=>'Administration\\Ad_setsController@getEditAd']);
  Route::post('ad_sets/{ad_id}/update-ad-creative',  ['as'=>'admin.adsets.update.ad',
                                                      'uses'=>'Administration\\Ad_setsController@postEditAd']);

  Route::get('ad_sets/{ad_id}/ads/destroy',   ['as'=>'admin.adsets.delete.ad','uses'=>'Administration\\Ad_setsController@destroyAd']);

  Route::get('merchants/export', ['as'=>'admin.merchants.export', 'uses'=>'Administration\\MerchantsController@export']);
  Route::get('merchants/global', ['as'=>'admin.merchants.global', 'uses'=>'Administration\\MerchantsController@globalIndex']);
  Route::get('merchants/export/global', ['as'=>'admin.merchants.export.global', 'uses'=>'Administration\\MerchantsController@exportGlobalIndex']);

  Route::get('merchants/access/{merchant_id}/{user_id}', ['as'=>'admin.merchants.access.form', 'uses'=>'Administration\\MerchantsController@getFormForModuleAccess']);
  Route::post('merchants/access/post', ['as'=>'admin.merchants.access', 'uses'=>'Administration\\MerchantsController@postModuleAccess']);

  Route::get('merchants/get-potential-clients', ['as'=>'admin.merchants.potential.clients', 'uses'=>'Administration\\MerchantsController@getUsersForMerchant']);
  Route::get('merchants/get-clients-outlets', ['as'=>'admin.merchants.client.outlets', 'uses'=>'Administration\\MerchantsController@getOutletsForMerchant']);
  Route::resource('merchants', 'Administration\\MerchantsController', array('names' => mzk_route_helper('merchants', 'admin')));

  Route::get('merchants/call-logs/get-businesses', ['as'=>'admin.merchants.call_logs.businesses', 'uses'=>'Administration\\MerchantsController@getOutletsForMerchantInvoiceForm']);
  Route::get('newsletters', ['as'=>'admin.newsletters.index', 'uses'=>'Administration\\NewslettersController@index']);

  Route::get('newsletters/weekly/posts', ['as'=>'admin.newsletters.weekly.posts', 'uses'=>'Administration\\NewslettersController@weeklyPosts']);
  Route::post('newsletters/custom/posts', ['as'=>'admin.newsletters.custom.posts', 'uses'=>'Administration\\NewslettersController@customPosts']);



});



Route::get('/redirect/client/dashboard', 'HomeController@redirectToClientDashboard');
Route::get('the-fab-review-contest', 'HomeController@theFabReviewContest');

Route::group(array('before' => 'client.auth', 'prefix'=>'partner'), function(){
  Route::get('/{bid}', ['as'=>'client.home', 'uses'=>'client\\HomeController@dashboard']);
  Route::get('/', 'client\\HomeController@clientHomeRedirector');
  Route::get('/set-default-business/{id}', 'client\\HomeController@setCurrentBusiness');
  Route::get('/{bid}/reviews', ['as'=>'client.reviews.index', 'uses'=>'client\\ReviewsController@index']);
  Route::get('/{bid}/reviews/{id}', ['as'=>'client.reviews.show', 'uses'=>'client\\ReviewsController@show']);
  Route::get('/{bid}/photos', ['as'=>'client.businesses.show.photos', 
                          'uses'=>'client\\ProfileController@getPhotos']);
  Route::post('/{bid}/photos', 'client\\ProfileController@postPhotos');

  Route::get('/{bid}/leads', 'client\\LeadsController@index');
  Route::get('/{bid}/resumes', 'client\\ResumesController@index');

  Route::get('/{bid}/rate-cards', [ 'as'=>'client.businesses.show.rate_cards', 
                              'uses'=>'client\\ProfileController@getRateCards']);
  Route::post('/{bid}/rate-cards', 'client\\ProfileController@postRateCards');

  Route::get('/{bid}/validate/voucher/form', [ 'as'=>'client.voucher.validate.form', 
                              'uses'=>'client\\OffersController@validateVoucherForm']);

  Route::post('/{bid}/validate/voucher', [ 'as'=>'client.voucher.validate', 
                              'uses'=>'client\\OffersController@postValidateVoucher']);


  Route::post('/{bid}/voucher/{id}/redeem', [ 'as'=>'client.voucher.redeem', 
                              'uses'=>'client\\OffersController@redeemVoucher']);


  Route::any('/{bid}/api/ratings', 'client\\ReviewsController@getRatingsData');
  Route::any('/{bid}/api/views', 'client\\HomeController@getViewsDataApi');
  Route::any('/{bid}/api/calls', 'client\\HomeController@getCallsDataApi');

  Route::any('/{bid}/api/call_logs', 'client\\HomeController@getCallLogsDataApi');

  Route::get('/{bid}/change-request', 'client\\ProfileController@getChangeRequest');
  Route::post('/{bid}/change-request', [ 'as'=>'client.post.change-request', 
                              'uses'=>'client\\ProfileController@postChangeRequest']);


  Route::group(array('prefix'=>'{bid}'), function(){
    Route::resource('offers', 'client\\OffersController');
    Route::resource('offers', 'client\\OffersController', array('names' => mzk_route_helper('offers', 
      'partner')));
  });



  Route::get('users/logout', 'UsersController@logout');

  //Route::resource('call_logs', 'client\\Call_logsController');

  Route::group(array('prefix'=>'{bid}'), function(){
    Route::get('call_logs/dispute/{id}', ['uses'=>'client\\Call_logsController@createDispute', 'as'=>'partner.call_logs.disputes.store']);
    Route::post('call_logs/dispute', ['uses'=>'client\\Call_logsController@storeDispute', 'as'=>'partner.call_logs.disputes.create']);

    Route::get('call_logs/resolve/{id}', ['uses'=>'client\\Call_logsController@createResolve', 'as'=>'partner.call_logs.resolves.store']);
    Route::post('call_logs/resolve', ['uses'=>'client\\Call_logsController@storeResolve', 'as'=>'partner.call_logs.resolves.create']);
    Route::get('call_logs/reset/{id}', ['uses'=>'client\\Call_logsController@resetCallLog', 'as'=>'partner.call_logs.reset']);
    Route::get('call_logs/billable/{id}', ['uses'=>'client\\Call_logsController@clearToBilled', 'as'=>'partner.call_logs.clear.to.billed']);

    Route::group(array('before' => 'client.admin.auth'), function(){
      Route::get('call_logs/export', ['uses'=>'client\\Call_logsController@export', 'as'=>'partner.call_logs.export']);
    });

    Route::resource('call_logs', 'client\\Call_logsController');
    Route::resource('call_logs', 'client\\Call_logsController', 
        array('names' => mzk_route_helper('call_logs', 
              'partner')));
  });

  Route::get('users/logout', 'UsersController@logout');

  Route::get('/{bid}/photo/{photoId}', ['as'=>'client.businesses.show.photo','uses'=>'client\\ProfileController@getPhoto']);
  Route::post('/{bid}/photo/{photoId}', ['as'=>'client.businesses.edit.photo', 'uses'=>'client\\ProfileController@postPhoto'] );
  Route::get('/{bid}/photo/{photoId}/rotate/{degrees}', ['as'=>'client.businesses.rotate.photo',
                                                'uses'=>'client\\ProfileController@rotatePhoto']);

});

Route::get('talk', ['uses'=>'QuestionsController@index', 
                      'as'=>'questions.index']);

Route::get('talk/about/{service}', ['uses'=>'QuestionsController@service', 
                                      'as'=>'questions.service']);
Route::get('talk/{id}/{slug}', ['uses'=>'QuestionsController@show', 
                                      'as'=>'questions.show']);



Route::any('stories/to/draft/{id}', [ 'as'=>'posts.review.to.draft', 
                    'uses'=>'PostsController@reviewToDraft']);

Route::get('stories/about/{service_slug}', ['uses'=>'PostsController@index', 
                                      'as'=>'posts.service']);

Route::any('stories/to/review/{id}', [ 'as'=>'posts.draft.to.review', 
                    'uses'=>'PostsController@draftToReview']);
Route::get('stories/{id}/{slug}', [ 'as'=>'posts.show.by.id', 
                    function($id, $slug){
                      if(is_numeric($id)){
                        return Redirect::to(route('posts.show.by.slug', ['slug'=>$slug]));
                      }
                    }]);
Route::get('stories/{slug}', [ 'as'=>'posts.show.by.slug', 
                    'uses'=>'PostsController@show']);

Route::get('mobile/stories/{id}', [ 'as'=>'posts.show.mobile.view', 
                    'uses'=>'PostsController@showMobile']);


Route::get('stories', [ 'as'=>'posts.index.all', 
                    'uses'=>'PostsController@index']);


Route::post('posts/{id}/follow', 'PostsController@follow');
Route::post('posts/{id}/unfollow', 'PostsController@unfollow');


Route::when('zones', 'manage_listings');

// put all non locale links before this

Route::post('businesses/{id}/rate', 'BusinessesController@rate');
Route::post('businesses/{id}/rating/delete', 'BusinessesController@deleteRating');

Route::get('/', ['as'=>'site.home', 'uses'=>'HomeController@index']);

Route::get('/locale/{from}/{to}', ['uses'=>'HomeController@changeLocale', 'as'=>'home.change.locale']);
Route::get('/fabfinds', 'HomeController@fabFinds');

Route::get('/newsletters/subscribe', 'HomeController@newsletterSubscribe');

Route::get('/unsubscribe', 'HomeController@unsubscribe');
Route::post('/report', 'HomeController@postReport');
Route::post('/apply', 'HomeController@applyJob');
Route::get('/search', 'HomeController@search');
Route::post('/search', 'HomeController@search');
Route::get('/tos', 'HomeController@tos');
Route::get('/privacy-policy', 'HomeController@privacy');
Route::get('/cookie-policy', 'HomeController@cookie');
Route::post('/search/businesses', 'BusinessesController@searchBase');
Route::get('/add-your-business', 'HomeController@getAddBusiness');
Route::post('/add-your-business', 'HomeController@postAddBusiness');
Route::get('/faq', 'HomeController@faq');


// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');

Route::get('users/forgot_password', 'Auth\PasswordController@getEmail');    // 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'Auth\PasswordController@postEmail');  //'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'Auth\PasswordController@getReset'); //'UsersController@resetPassword');
Route::post('users/reset_password', 'Auth\PasswordController@postReset');//'UsersController@doResetPassword');

//// Password reset link request routes...
//Route::get('password/email', 'Auth\PasswordController@getEmail');
//Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::any('users/facebook/auth', ['as'=>'auth.callback.facebook', 'uses'=>'UsersController@authWithFacebook']);
Route::any('users/facebook/login', 'UsersController@loginWithFacebook');
Route::any('users/google/login', 'UsersController@loginWithGoogle');
Route::any('users/google/auth', 'UsersController@authWithGoogle');


Route::get('users/provider/register', 'UsersController@getProviderRegister');
Route::post('users/provider/register', 'UsersController@postProviderRegister');

Route::get('offers/{id}/claim', ['as'=>'offers.get.claim.form', 'uses'=>'OffersController@getClaim']);
Route::post('offers/{id}/claim',['as'=>'offers.post.claim.form', 'uses'=>'OffersController@postClaim']);

//Route::resource('photos', 'PhotosController');
//Route::resource('timings', 'TimingsController');
Route::get('users/{id}/profile', ['uses'=>'UsersController@show', 
                                  'as'=>'users.profile.show']);

Route::post('users/{id}/photo/step/1', ['uses'=>'UsersController@photoStepOne', 'as'=>'users.photos.upload.1']);
Route::post('users/{id}/photo/step/2', ['uses'=>'UsersController@photoStepTwo', 'as'=>'users.photos.upload.2']);

Route::get('users/{id}/photos', ['uses'=>'UsersController@photos', 
                                  'as'=>'users.photos.show']);
Route::get('users/{id}/videos', ['uses'=>'UsersController@videos', 
                                  'as'=>'users.videos.show']);

Route::get('search-locations', ['uses'=>'HomeController@searchZones', 
                                'as'=>'search.locations.zones']);
  Route::resource('reviews/business', 'ReviewsController@getForBusiness');

Route::group(array('before' => 'auth' ), function(){

  Route::get('gossip/ask', ['uses'=>'QuestionsController@create', 
                                        'as'=>'questions.create']);
  Route::post('gossip/create', ['uses'=>'QuestionsController@store', 
                                        'as'=>'questions.store']);

  Route::get('questions/{id}/edit', ['uses'=>'QuestionsController@edit', 
                                        'as'=>'questions.edit']);

  Route::any('questions/{id}/update', ['uses'=>'QuestionsController@update', 
                                        'as'=>'questions.update']);

  Route::resource('answers', 'AnswersController');

  Route::resource('reviews', 'ReviewsController');
  Route::resource('check_ins', 'Check_insController');
  Route::resource('favorites', 'FavoritesController');
  Route::post('businesses/{id}/checkin', 'BusinessesController@checkin');
  Route::post('businesses/{id}/checkout', 'BusinessesController@checkout');
  Route::any('businesses/{id}/follow', 'BusinessesController@favourite');
  Route::post('businesses/{id}/unfollow', 'BusinessesController@unfavourite');
  
  Route::get('businesses/{id}/review', ['as'=>'businesses.review.form', 'uses'=>'BusinessesController@getReviewForm']);
  Route::post('review/{id}/update', ['as'=>'businesses.review.update', 'uses'=>'ReviewsController@update']);

  Route::post('users/{id}/follow', 'UsersController@follow');
  Route::post('users/{id}/unfollow', 'UsersController@unfollow');

  Route::post('share', 'HomeController@share');

  Route::post('sitemap', 'HomeController@sitemap');

  Route::get('feed', 'HomeController@feed');
  Route::post('photos/upload', ['uses'=>'PhotosController@upload', 'as'=>'photos.upload']);

  // Route::post('deals/{id}/follow', 'DealsController@favourite');
  // Route::post('deals/{id}/unfollow', 'DealsController@unfavourite');

  Route::any('users/connect/facebook', 'UsersController@attachFacebookAccount');
  Route::any('users/connect/google', 'UsersController@attachGoogleAccount');

  Route::any('users/disconnect/facebook', 'UsersController@detachFacebookAccount');
  Route::any('users/disconnect/google', 'UsersController@detachGoogleAccount');

  Route::resource('comments', 'CommentsController',
                array('only' => array('create', 'store', 'update', 'destroy')));

  Route::get('users/edit', 'UsersController@edit');
  Route::get('users/vouchers', 'OffersController@my');
  Route::get('vouchers/{id}/pdf', ['uses'=>'OffersController@pdf', 'as'=>'users.vouchers.pdf']);
  Route::post('users/edit', 'UsersController@update');
  Route::post('users/update-account', 'UsersController@updateAccount');
  Route::get('users/logout', 'UsersController@logout');
  Route::resource('posts', 'PostsController');
  Route::resource('selfies', 'SelfiesController');
  Route::resource('videos', 'VideosController');
});

Route::resource('posts', 'PostsController', array('only' => array('index', 'show')));
Route::resource('selfies', 'SelfiesController', array('only' => array('index', 'show')));
Route::resource('videos', 'VideosController', array('only' => array('index', 'show')));


Route::get('email', 'HomeController@email');

Route::resource('vouchers', 'VouchersController');

Route::resource('slugs', 'SlugsController');

Route::resource('invoice_items', 'Invoice_itemsController');

Route::resource('service_zone_counters', 'Service_zone_countersController');

Route::resource('shares', 'SharesController');

});

Route::get('/info/get/pagination', ['as'=>'views.get.pagination', 'uses'=>function(){
  return response()->view('elements.pagination.angular');
}]);

Route::any('human/prove', 'HomeController@captcha');
Route::any('suspicious', 'HomeController@suspicious');
Route::any('blocked', 'HomeController@blocked');

