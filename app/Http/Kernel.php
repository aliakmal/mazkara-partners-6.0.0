<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
    		
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
     \App\Http\Middleware\Minify::class,		
     //'notBlocked' => 
     \App\Http\Middleware\Blocked::class,
		// 'App\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' =>  'App\Http\Middleware\RedirectIfAuthenticated',//'App\Http\Middleware\Authenticate',
		'auth.basic' => 'App\Http\Middleware\RedirectIfAuthenticated',//'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
		//'shieldsquare' => '\App\Http\Middleware\ShieldSquare',
	];

}
