<?php namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Auth, Request, Redirect, MazkaraHelper, Response;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;

//include_once app_path().'/../shieldsquare/ss2.php';

class RouteServiceProvider extends ServiceProvider {

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);

		//
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => $this->namespace], function($router)
		{
			require app_path('Http/routes.php');
		});
	}

}







Route::filter('mazkara.cache', function($route, $request, $response = null)
{
  //$key = MazkaraHelper::getCurrentPageCacheName();

// /  if(is_null($response) && Cache::has($key)){
// /    return Cache::get($key);
// /  }elseif(!is_null($response) && !Cache::has($key)){
// /    Cache::put($key, $response->getContent(), 10);
// /  }
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('users/login');
		}
	}
});

Route::filter('auth.api.admin', function()
{

  $key = Input::get('key');
  $validator = Validator::make([
          'key' => $key,
      ],
      [
          'key' => 'required'
      ]
  );

  if ($validator->fails()) {
  	return Response::make('Unauthorized', 401);
  }

  $apiKey = ApiKey::where('key', '=', $key);

  if($apiKey->count()==0){
      return Response::make('Unauthorized', 401);
  }



  $user = User::where('id', $apiKey->first()->user_id)->first();
  if (!isset($user)){
		return Response::make('Unauthorized', 401);
  } 

  if(!($user->hasRole('admin') || $user->hasRole('moderator'))){
    return Response::make('Unauthorized', 401);
  }

});

Route::filter('auth.api.user', function()
{

  $key = Input::get('key');
  $validator = Validator::make([
          'key' => $key,
      ],
      [
          'key' => 'required'
      ]
  );

  if ($validator->fails()) {
    return Response::make('Unauthorized Error', 401);
  }

  $apiKey = ApiKey::where('key', '=', $key);

  if($apiKey->count()==0){
    return Response::make('Unauthorized Key', 401);
  }

  $user = User::where('id', $apiKey->first()->user_id)->first();
  if (!isset($user)){
    return Response::make('Unauthorized User', 401);
  } 


});



Route::filter('auth.api.black', function()
{

  $key = Input::get('key');
  $validator = Validator::make([
          'key' => $key,
      ],
      [
          'key' => 'required'
      ]
  );

  if ($validator->fails()) {
    return Response::make('Unauthorized', 401);
  }

  $apiKey = ApiKey::where('key', '=', $key);

  if($apiKey->count()==0){
      return Response::make('Unauthorized', 401);
  }



  $user = User::where('id', $apiKey->first()->user_id)->first();
  if (!isset($user)){
    return Response::make('Unauthorized', 401);
  } 

  if(!($user->hasRole('client') )){
    return Response::make('Unauthorized', 401);
  }

});



Route::filter('admin.auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}else{
			return Redirect::guest('admin/users/login');
		}
	}else{
    $user = Auth::user();
		if(!($user->hasRole('admin')||$user->hasRole('moderator')|| $user->hasRole('sales-admin')||$user->hasRole('finance'))){		
			if (Request::ajax()){
				return Response::make('Unauthorized', 401);
			}else{
				return Redirect::guest('/');
			}
		}else{
      if(!$user->hasRole('admin')){
        if(!$user->hasDefaultLocale()){
          return (Request::ajax()) ? Response::make('Unauthorized', 401) : Redirect::guest('/');
        }else{
          // set the default domain
          MazkaraHelper::affixDefaultAdminLocale();
        }
      }
    }
	}
});

/*
Route::filter('shieldsquare', function()
{
  // only show it to non logged in users
  if (Auth::guest()):
    if (\App::environment('not-production')):
      $shieldsquare_username = "roy@mazkara.com"; // Enter the UserID of the user
      $shieldsquare_calltype = 1;
      $shieldsquare_pid = "";
      $shieldsquare_config_data = new \shieldsquare_config();

      $prefix = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
      $current_page = $prefix.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

      if(isset($_COOKIE[md5("captchaResponse")]) && $_COOKIE[md5("captchaResponse")]==md5("1".$current_page.$_SERVER[$shieldsquare_config_data->_ipaddress].$shieldsquare_config_data->_sid)){
        $shieldsquare_calltype = 5;
        //Unset the cookie variable
        setcookie(md5("captchaResponse"),null,-1,"/");
        setcookie("currentPagename",null,-1,"/");
      } else {
        //set calltype as 1
        $shieldsquare_calltype = 1;
      }
      $shieldsquare_response=shieldsquare_ValidateRequest($shieldsquare_userid, $shieldsquare_calltype, $shieldsquare_pid);
      if ($shieldsquare_response->responsecode == 2){
        //setting the current page name to the cookie for later use
        setcookie("currentPagename",$current_page);
         //Redirect to the Captcha page
        return Redirect::guest('/human/prove');
      }elseif ($shieldsquare_response->responsecode == -1){
        return Redirect::guest('/human/prove');
      }

      //  $shieldsquare_response = shieldsquare_ValidateRequest($shieldsquare_username, $shieldsquare_calltype, $shieldsquare_pid);
      //  if ($shieldsquare_response->responsecode != '0'){
      //    return Redirect::guest('/suspicious');
      //  }
    endif;
  endif;
});

*/
Route::filter('crm.auth', function()
{
  if (Auth::guest())
  {
    if (Request::ajax())
    {
      return Response::make('Unauthorized', 401);
    }else{
      return Redirect::guest('admin/users/login');
    }
  }else{
    $user = Auth::user();
    if(!($user->hasRole('admin') || $user->hasRole('sales-admin') || $user->hasRole('sales')) ){    
      if (Request::ajax()){
        return Response::make('Unauthorized', 401);
      }else{
        return Redirect::guest('/');
      }
    }else{
      if(!$user->hasRole('admin')){
        if(!$user->hasDefaultLocale()){
          return (Request::ajax()) ? Response::make('Unauthorized', 401) : Redirect::guest('/');
        }else{
          // set the default domain
          MazkaraHelper::affixDefaultAdminLocale();
        }
      }
    }
  }
});

Route::filter('finance.auth', function()
{
  if (Auth::guest())
  {
    if (Request::ajax())
    {
      return Response::make('Unauthorized', 401);
    }else{
      return Redirect::guest('admin/users/login');
    }
  }else{
    $user = Auth::user();
    if(!($user->hasRole('admin') || $user->hasRole('finance')  || $user->hasRole('sales-admin') || $user->hasRole('sales')) ){    
      if (Request::ajax()){
        return Response::make('Unauthorized', 401);
      }else{
        return Redirect::guest('/');
      }
    }else{
      if(!$user->hasRole('admin')){
        if(!$user->hasDefaultLocale()){
          return (Request::ajax()) ? Response::make('Unauthorized', 401) : Redirect::guest('/');
        }else{
          // set the default domain
          MazkaraHelper::affixDefaultAdminLocale();
        }
      }
    }
  }
});


Route::filter('content.auth', function()
{
  if (Auth::guest())
  {
    if (Request::ajax())
    {
      return Response::make('Unauthorized', 401);
    }else{
      return Redirect::guest('admin/users/login');
    }
  }else{
    $user = Auth::user();
    if(!($user->hasRole('admin') || $user->hasRole('moderator'))){    
      if (Request::ajax()){
        return Response::make('Unauthorized', 401);
      }else{
        return Redirect::guest('/');
      }
    }else{
      if(!$user->hasRole('admin')){
        if(!$user->hasDefaultLocale()){
          return (Request::ajax()) ? Response::make('Unauthorized', 401) : Redirect::guest('/');
        }else{
          // set the default domain
          MazkaraHelper::affixDefaultAdminLocale();
        }
      }
    }
  }
});




Route::filter('client.auth', function()
{
  if (Auth::guest())
  {
    if (Request::ajax())
    {
      return Response::make('Unauthorized', 401);
    }else{
      return Redirect::guest('admin/users/login');
    }
  }else{
    $user = Auth::user();
    if(!($user->hasRole('client') || $user->can('access_client_dashboards')))
    {   
      if (Request::ajax())
      {
        return Response::make('Unauthorized', 401);
      }else{
        return Redirect::guest('/');
      }
    }
  }
});

Route::filter('client.admin.auth', function()
{
  if (Auth::guest())
  {
    if (Request::ajax())
    {
      return Response::make('Unauthorized', 401);
    }else{
      return Redirect::guest('admin/users/login');
    }
  }else{
    $user = Auth::user();
    if(!($user->hasRole('client') || $user->hasRole('admin')))
    {   
      if (Request::ajax())
      {
        return Response::make('Unauthorized', 401);
      }else{
        return Redirect::guest('/');
      }
    }
  }
});



Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});


Route::filter('allowOrigin', function($route, $request, $response)
{
    $response->header('access-control-allow-origin','*');
});

