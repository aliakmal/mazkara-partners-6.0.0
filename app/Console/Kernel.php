<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		Commands\GenerateRoutesText::class,
		Commands\ClearAppStorage::class,
		Commands\ClearCacheBot::class,
		Commands\ClearStorageForGood::class,
		Commands\DatabaseBackup::class,
		Commands\GenerateItemsToExcel::class,
		Commands\ResluggifyModels::class,
		Commands\UpdateAllBusinessCache::class,
		Commands\UpdateAllMetaBusinessCache::class,
		Commands\UpdateAllOffersCache::class,
		Commands\UpdateAllOffersState::class,
		Commands\UpdateAllVouchersState::class,
		Commands\UpdateCategoryCount::class,
		Commands\UpdateHighlightCount::class,
		Commands\UpdateServiceCount::class,
		Commands\UpdateUserCounters::class,
		Commands\UpdateZoneCount::class,
		Commands\CheatPageViews::class,
		Commands\add_roles::class,
		Commands\assign_stock_to_coverless_businesses::class,
		Commands\downloadCallLogs::class,
		Commands\downloaderCallLogs::class,
		Commands\fixBannerPhoto::class,
		Commands\fixPhoto::class,
		Commands\fixBannerPhoto::class,
		Commands\fixBannerPhoto::class,
		Commands\test_emailer::class,
		Commands\AllocateLuxurySpas::class,
		Commands\ResetAdzones::class,
		Commands\set_items_to_ads::class,
		Commands\ReallocateSalesPOCsToInvoices::class,
		Commands\reallocate_nails_hairs_new_category::class,
		Commands\setup_depth_in_zones::class,
		Commands\allocate_lng_lat::class,
		Commands\setup_settings::class,
		Commands\reset_businesses_popularity::class,
		Commands\populate_counters_with_todays_counts::class,
		Commands\populate_counters_with_todays_counts_bground::class,
		Commands\reallocate_home_new_category::class,
		Commands\cheat_rates_undo::class,
		Commands\cheat_rates_bulk_bg::class,
		Commands\cheat_rates_bulk::class,
		Commands\add_tags::class,
		Commands\toggle_active_cities::class,
		Commands\reminder_expired_billables::class,
		Commands\reset_states_for_facebook::class,
		Commands\reset_states_for_virtual_numbers::class,
		Commands\allocate_bridals_to_bridals_category::class,
		Commands\UpdateRichnessMatrix::class,
		Commands\reset_invoice_counters::class,
		Commands\fix_invoice_numbers::class,
		Commands\populate_empty_offer_price_with_number_from_offer_meta::class,
		Commands\generate_json_assets::class,
		Commands\reset_businesses_popularity_bg::class,
		Commands\UpdateUsersMeta::class,
		Commands\regenerate_global_merchant_ids::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
//		$schedule->command('inspire')
//				 ->hourly();

//		$schedule->command('mazkara:photos.fix')
//				 ->hourly()
//				 ->withoutOverlapping()
//         ->sendOutputTo(storage_path().'/output.photos.fix')
//         ->emailOutputTo('ali@fabogo.com');



		$schedule->command('mazkara:set.ads.items')
				 ->hourly()
				 ->withoutOverlapping()
         ->name('Output Set Ads')
         ->sendOutputTo(storage_path().'/output.set.ads')
         ->emailOutputTo('ali@fabogo.com');

		$schedule->command('mazkara:populate.counters.bg')
				 ->daily()
				 ->withoutOverlapping()
         ->name('Counters Database Updated')
         ->sendOutputTo(storage_path().'/output.populate.fix')
         ->emailOutputTo('ali@fabogo.com');

//		$schedule->command('mazkara:cheat.ratings.undo')
//				 ->hourly()
//				 ->withoutOverlapping()
//         ->sendOutputTo(storage_path().'/output.cheat.rates')
//         ->emailOutputTo('ali@fabogo.com');



//		$schedule->command('mazkara:business.cache.update')
//				 ->twiceDaily(3, 23);

		$schedule->command('mazkara:business.cache.meta')
				 ->twiceDaily(3, 23)
				 ->withoutOverlapping()
         ->sendOutputTo(storage_path().'/output.business.cache.meta')
         ->name('Update Business Meta/Cache')
         ->emailOutputTo('ali@fabogo.com');
		$schedule->command('mazkara:popularity.update.bg')
				 ->twiceDaily(3, 23)
				 ->withoutOverlapping()
         ->sendOutputTo(storage_path().'/output.popularity.update.bg')
         ->name('Update popularity bg')
         ->emailOutputTo('ali@fabogo.com');


		$schedule->command('mazkara:offers.cache.update')
				 ->twiceDaily(3, 23);

		$schedule->command('mazkara:offers.state.update')
				 ->twiceDaily(3, 23);

//		$schedule->command('mazkara:vouchers.state.update')
//				 ->twiceDaily(3, 23);

		$schedule->command('mazkara:categories.businesses.count.update')
				 ->twiceDaily(8, 22);

		$schedule->command('mazkara:highlights.businesses.count.update')
				 ->twiceDaily(8, 22);
		$schedule->command('mazkara:services.businesses.count.update')
				 ->twiceDaily(8, 22);
		$schedule->command('mazkara:update.users.counters')
				 ->twiceDaily(8, 22);
		$schedule->command('mazkara:zones.businesses.count.update')
				 ->twiceDaily(8, 22);

		$schedule->command('mazkara:call_logs.download')
				 ->cron('0 */3 * * *')//twiceDaily(8, 22)
         ->sendOutputTo(storage_path().'/output.call_logs.downloader')
         ->name('Call Logs Downloader')
         ->emailOutputTo('ali@fabogo.com');

		$schedule->command('mazkara:businesses.richness')
				 ->twiceDaily(8, 22);

	}

}
