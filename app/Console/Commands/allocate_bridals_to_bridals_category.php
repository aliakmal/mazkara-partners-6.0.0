<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Image, File;

use App\Models\Category as Category;
use App\Models\Business as Business;

class allocate_bridals_to_bridals_category extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:allocate.bridals';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Allocate Bridals.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$bridals_category_id = 14;
		$bridals_services = [128,113];

		$bids = \DB::table('business_service')->whereIn('service_id', $bridals_services)->lists('business_id','business_id');
		$businesses = Business::query()->whereIn('id', $bids)->get();
    foreach($businesses as $business){
    	$business->categories()->attach($bridals_category_id);
      $business->save();
			$this->line($business->name.' allocated as bridals.');
    }
		$this->info(count($businesses).' spas and centers updated - All done :)');

		$this->line(' :) All done');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
			array('b_id', null, InputOption::VALUE_OPTIONAL, 'Business ID.', null),
		);
	}

}
