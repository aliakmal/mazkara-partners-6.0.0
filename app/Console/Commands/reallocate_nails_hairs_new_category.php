<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Business;
use App\Models\Service;
use App\Models\Category;


class reallocate_nails_hairs_new_category extends Command {
    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $name = 'mazkara:allocate.nails.hairs';

    protected $description = 'Allocate Nails Hairs.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $nail_category = Category::where('slug', '=', 'nail-service')->get()->first();
        $hair_category = Category::where('slug', '=', 'hair-service')->get()->first();

        $nails_services = Service::where('parent_id', '=', 45)->orWhere('id', '=', 45)->lists('id', 'id')->all();
        $hairs_services = Service::where('parent_id', '=', 21)->orWhere('id', '=', 21)->lists('id', 'id')->all();

        // all businesses ids in the business_service table with one or any of the above services
        $nails_business_ids = \DB::table('business_service')
                                ->whereIn('service_id', $nails_services)
                                ->lists('business_id', 'business_id');

        $hairs_business_ids = \DB::table('business_service')
                                ->whereIn('service_id', $hairs_services)
                                ->lists('business_id', 'business_id');

        $this->line(count($nails_business_ids).' venues for nails category');
        $this->line(count($hairs_business_ids).' venues for hairs category');

        // make an entry for business_id
        $hs = [];
        foreach($hairs_business_ids as $vv){
            $hs[] = ['category_id'=>$hair_category->id, 'business_id'=>$vv];
        }

        $ns = [];
        foreach($nails_business_ids as $vv){
            $ns[] = ['category_id'=>$nail_category->id, 'business_id'=>$vv];
        }

        DB::table('business_category')->insert($ns);
        DB::table('business_category')->insert($hs);

        $this->line(count($nails_business_ids).' venues updated for nails category');
        $this->line(count($hairs_business_ids).' venues updated for hairs category');
    }
}
