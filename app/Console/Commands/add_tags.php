<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\Zone;
use App\Models\Review;
use App\Models\User;
use App\Models\Business;

class add_tags extends Command
{
  /**
   * Create a new command instance.
   *
   * @return void
   */
  protected $name = 'tagging:add';

  protected $description = 'Toggly Active City.';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle(){
    $tag_groups = \DB::table('tagging_tag_groups')->select('id', 'name')->get();
    $choices = [];
    $choice_ids = [];
    $tag_name = $this->argument('name');

    foreach($tag_groups as $tag_group){
      $choices[] = $tag_group->name;
      $choice_ids[] = $tag_group->id;
    }

    $choice = $this->choice('Whice tag group to attach here?', $choices);
    $choice = array_search($choice, $choices);
    $tag_group_id = $choice_ids[$choice];
    echo $tag_group_id;
    $tag = \Conner\Tagging\Model\Tag::firstOrCreate(['name'=>$tag_name, 'tag_group_id'=>$tag_group_id]);
    $tag->setGroup($choices[$choice]);
    $tag->save();
    $this->info('All done :)');
  }


  protected function getArguments()
  {
    return array(
      array('name', InputArgument::REQUIRED, 'finance'),
    );
  }


}
