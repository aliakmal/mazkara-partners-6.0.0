<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Zone;
use App\Models\Review;
use App\Models\User;
use App\Models\Business;

class toggle_active_cities extends Command
{
  /**
   * Create a new command instance.
   *
   * @return void
   */
  protected $name = 'mazkara:toggle.city';

  protected $description = 'Toggly Active City.';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle(){
    $cities = Zone::select()->cities()->get();
    $choices = [];
    $choice_ids = [];

    foreach($cities as $city){
      $choices[] = $city->name.'('.$city->state.')';
      $choice_ids[] = $city->id;
    }

    $choice = $this->choice('Whice city here?', $choices);
    $choice = array_search($choice, $choices);
    $city_id = $choice_ids[$choice];
    $city = Zone::find($city_id);
    if($city->isActive()){
      $this->info($city->name.' is active - it will be marked as inactive as well as its subzones');
      $city->state = 'inactive';
      $city->save();
      $sql = 'UPDATE zones set state = "inactive" where city_id = '.$city->id;
    }else{
      $this->info($city->name.' is inactive - it will be marked as active as well as its subzones');
      $city->state = 'active';
      $city->save();
      $sql = 'UPDATE zones set state = "active" where city_id = '.$city->id;
    }

    \DB::update($sql);

    $this->info('All done :)');
  }
}
