<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Image, File;

use GuzzleHttp\Client as HttpClient;

use App\Models\Category as Category;
use App\Models\Business as Business;
use App\Models\Virtual_number_allocation as Virtual_number_allocation;
use App\Models\Virtual_number as Virtual_number;

class reset_states_for_virtual_numbers extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.acd.states';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Mark Expired ACD as inactive.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([3,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// get all allocations that have expired but are still active
		$virtuals = Virtual_number_allocation::select()
																						->justActive()->whereRaw('valid_until <= NOW()')
																						->orderby('id', 'asc')
																						->get();
		$this->line(count($virtuals).' records of virtual numbers to be deactived');

		foreach($virtuals as $virtual_number_allocation){
//			$virtual_number_allocation->deallocate();
	    $virtual_number = Virtual_number::find($virtual_number_allocation->virtual_number_id);
//	    $client = new HttpClient();
//	    $url = 'http://etsintl.kapps.in/webapi/mazkara/api/mazkara_agent_mapping.py?auth_key=51c41934-94b1-4be7-82c2-6531251a76ab';
//	    $url .='&knowlarity_number='.$virtual_number->body.
//	                      '&mapped_number=None'; 
//	    $response = $client->get($url);
	    if($virtual_number){
		    $msg = 'Number '.$virtual_number->body;
		  }else{
		    $msg = 'Number ID '.$virtual_number_allocation->virtual_number_id;
		  }

	    $msg .=' deallocated from business ';
	    if($virtual_number_allocation->business):
		    $msg .=' '.$virtual_number_allocation->business->name;
		    $msg .=', '.$virtual_number_allocation->business->zone_cache;
	    endif;
	    $msg .='('.$virtual_number_allocation->business_id.')';

	    $this->info($msg);
		}

    $this->line('All Done :)');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
			array('b_id', null, InputOption::VALUE_OPTIONAL, 'Business ID.', null),
		);
	}

}
