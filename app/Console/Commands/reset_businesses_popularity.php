<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Zone;
use App\Models\Business;

class reset_businesses_popularity extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:popularity.update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update Popularity.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');

    $city = Zone::select()->where('slug', '=', 'mumbai')->get()->first();
		$businesses = Business::select()->byLocale($city->id)->hasCategories()->hasHighlights()->hasPhotos()->take($count)->skip($page*$count)->get();

		//$businesses = Business::select()->take($count)->skip($page*$count)->get();
		$this->line($count.' records taken from page '.$page);

		foreach($businesses as $business){
			$business->setPopularity();
			$this->info($business->id.' - '.$business->name.' popularity updated');
		}
		$this->line(count($businesses).' records updated');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 1000),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
