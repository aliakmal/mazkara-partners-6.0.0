<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Image, File;

use App\Models\Category as Category;
use App\Models\Business as Business;
use App\Models\Group as Group;
use App\Models\Virtual_number_allocation as Virtual_number_allocation;

class reminder_expired_billables extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reminder.expirations';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reminder emails about expiring billables';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([3,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$businesses = Business::select()->isDisplayable()
											->where('facebook_like_box_status', '=', 'active')
											->whereRaw('DATE_ADD(NOW(), INTERVAL 5 DAY) >= `facebook_like_box_valid_until`')
											->whereRaw('NOW() <= `facebook_like_box_valid_until`')
											->orderby('id', 'asc')->get();
		$line = '';
		for($i=0;$i<10;$i++){
			$line.='-------------';
		}
		$this->line($line);
		$this->line(count($businesses).' whose facebook pending deactivation in the next 5 days');
		$this->line($line);

		foreach($businesses as $business){
			$this->line($business->name.','.$business->zone_cache.'('.$business->id.')'.' facebook expiration on  '.$business->facebook_like_box_valid_until);
		}


		$groups = Group::select()->isCustomActive()
											->whereRaw('DATE_ADD(NOW(), INTERVAL 5 DAY) >= `valid_until`')
											->whereRaw('NOW() <= `valid_until`')
											->orderby('id', 'asc')->get();

		$this->line($line);
		$this->line(count($groups).' ccp pending deactivation in the next 5 days');
		$this->line($line);

		foreach($groups as $group){
			$this->line($group->name.'('.$group->id.')'.' ccp expiration on  '.$group->valid_until);
		}

		$virtuals = Virtual_number_allocation::with('business')->select()
											->justActive()->whereRaw('NOW() <= `valid_until`')
											->whereRaw('DATE_ADD(NOW(), INTERVAL 5 DAY) >= `valid_until`')
											->orderby('id', 'asc')
											->get();

		$this->line($line);
		$this->line(count($virtuals).' acd pending deactivation in the next 5 days');
		$this->line($line);


		foreach($virtuals as $virtual){
			if($virtual->business){
				$this->line($virtual->business->name.'('.$virtual->business_id.')'.' Virtual number expiration on  '.$virtual->valid_until);
			}
		}

		$this->line(' :) All done');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
			array('b_id', null, InputOption::VALUE_OPTIONAL, 'Business ID.', null),
		);
	}

}
