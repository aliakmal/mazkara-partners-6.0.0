<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\Offer as Offer;
use App\Models\Voucher as Voucher;

class UpdateAllVouchersState extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:vouchers.state.update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update states of vouchers - mark the expired offers ones as invalid.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler){
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([4,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = 500;//$this->argument('count');

		$offers_recently_expired = Offer::query()->byValidDateRange(100)->lists('id','id');
		$this->info('OFFERS EXPIRING IN LAST WEEK IDS - '.join(',',$offers_recently_expired));
		$total = Voucher::whereIn('offer_id', $offers_recently_expired)->get()->count();
		$pages = ceil($total/$count);
		$this->info($total.' VOUCHERS TO BE RESTATED');
		

		for($i=0;$i<=$pages;$i++):
			$vouchers = Voucher::query()->whereIn('offer_id', $offers_recently_expired)->take($count)->skip($count*$i)->get();
			$this->line($count.' records taken from page '.$i);

			foreach($vouchers as $voucher){
				$voucher->resetState();
				$voucher->save();
				$this->info($voucher->id.' ID VOUCHER - updated');
				$voucher = null;
			}
			$this->line(count($vouchers).' records updated from offset '.$i);
			$vouchers = null;
			$this->info(memory_get_usage().' bytes allocated');
		endfor;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
