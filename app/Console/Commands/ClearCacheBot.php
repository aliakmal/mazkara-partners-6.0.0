<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;
use File;

class ClearCacheBot extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:force.clear.cache';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear Cache.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
   *  public function schedule(Schedulable $scheduler)
	 *	{
	 *			return $scheduler->daily()->hourly()->everyMinutes(5);
	 *	}
	*/
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$path = storage_path().'/framework/cache';
		$directories = File::directories($path);
		$num_directories = count($directories);
		foreach($directories as $directory){
			File::deleteDirectory($directory);
		}
    $this->info($num_directories.' directories have been deleted from '.$path);		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];

	}

}
