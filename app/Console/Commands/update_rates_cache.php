<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Zone;
use App\Models\Review;
use App\Models\User;
use App\Models\Business;

class update_rates_cache extends Command
{
  /**
   * Create a new command instance.
   *
   * @return void
   */
  protected $name = 'mazkara:update.ratings.cache';

  protected $description = 'Add cache ratings.';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle(){
    //$c = Counter::orderBy('dated', 'asc')->first();
    //$date_to_copy = $c->dated;
    //$counts = Counter::select()->where('dated', '=', $date_to_copy)->whereRaw('dated < DATE_ADD(NOW(), INTERVAL -2 MONTH)')->get();
    //$this->line(count($counts).' entries for the date '.$date_to_copy.' selected');

    // get guys to cheat on

    $cities = Zone::select()->showActive()->cities()->get();
    $choices = [];
    $choice_ids = [];

    foreach($cities as $city){
      $choices[] = $city->name;
      $choice_ids[] = $city->id;
    }


    $choice = $this->choice('Whice city here?', $choices);
    $choice = array_search($choice, $choices);
    $city_id = $choice_ids[$choice];

    // select venues that have photos, highlights and category and are active
    $businesses = Business::select()->byLocale()->hasCategories()->hasHighlights()->hasPhotos()->where('total_ratings_count', '<', 3)->get();
    $this->line(count($businesses).' venues to be bulk rated in '.$choices[$choice].' - minimum 2 - 5 rating');

    foreach($businesses as $business){
      $num_ratings = rand(4, 7);
      $ratings = [];

      for($i = 0; $i < $num_ratings; $i++){
        $rating = rand(2, 5);
        $rand_keys = array_rand($users, 2);

        $data = ['rating'=>$rating];
        $usr = $users[$rand_keys[0]];
        $data['ip'] = $usr['id'].time();
        $this->line($business->name." rating in process - rated ".$rating."");
        $business->rate($data, true);

        //$sql = "INSERT INTO `reviews` ";
        //$sql.="(`body`,`user_id`,`business_id`,`created_at`,`updated_at`,`rating`,`flags`,`ip`,
        //`type`,`is_cheat`,`number_of_likes`,`number_of_comments`,`number_of_shares`)";
        //$sql.="VALUES";
        //$sql.="(' ',0,".$business->id.", UTC_TIMESTAMP(), UTC_TIMESTAMP(),".$rating.",'',".$data['ip'].",";
        //$sql.="'business',1, 0,0,0);";
        //\DB::insert($sql);


        //$review = Review::create([ 'user_id'=>'0',
        //                            'ip'=>$data['ip'], 
        //                            'business_id'=>$business->id]);
        //$review->user_id =  0;
        //$review->business_id =  $business->id;
        //$review->ip =  $data['ip'];
        //$review->body = ' ';
        //$review->rating = $rating;
        //$review->is_cheat =  1;
        //$review->save();
        // $business->updateRatingAndReviewsCount();
        $ratings[] = $rating;

      }

      $this->info($business->name." given ".$num_ratings.' cheat ratings of '.join(',', $ratings));

    }


    $this->info('All done :)');
  }
}
