<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \Invoice as Invoice;
use \Payment as Payment;

class repair_invoice_payments_deallocation extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:invoices.allocations.reset';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Repair busted allocations of invoices.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//

		$id = $this->option('id');
		if($id){
			$invoices = [Invoice::find($id)];

		}else{
			$invoices = Invoice::all();

		}


		$payments = Payment::all();
		foreach($payments as $payment){
			$payment->reset();
		}

		foreach($invoices as $invoice){

			$this->info('INVOICE ID '.$invoice->id.' PICKED');
			$payments = $invoice->payments;
			
			foreach($payments as $payment){
				$this->line('PAYMENT ID '.$payment->id.' DETACHED FROM INVOICE ID '.$invoice->id);
				$payment->reset();
				$invoice->payments()->detach($payment->id);
				$invoice->payments()->attach([$payment->id=>['amount'=>$payment->getApplicableAmountForInvoice($invoice)]]);
			}

			$invoice->reset();
			$invoice->resetAmountApplicable();

		}




	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('id', null, InputOption::VALUE_OPTIONAL, 'ID.', null),
		);
	}

}
