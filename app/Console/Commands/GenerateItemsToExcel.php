<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Excel, DB;

class GenerateItemsToExcel extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:export.items';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Export all business service items to excelsheets.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$directory = storage_path().'/media/';
		$filename = 'items-dump-'.time();
		Excel::create($filename, function($excel) {
	    $excel->sheet('Businesses', function($sheet) {
				$sql = ("SELECT service_items . * , group_concat( services.name SEPARATOR ',' ) AS `service_names`
											FROM service_items
											LEFT JOIN service_service_item ON service_items.id = service_service_item.service_item_id
											LEFT JOIN services ON service_service_item.service_id = services.id
											GROUP BY service_items.id");
				$result = DB::select(DB::raw($sql));
				$bar = $this->output->createProgressBar(count($result));

				$data = [];
				foreach($result as $row){
					$data[] = (array)$row;
			    $bar->advance();					
				}
				$bar->finish();
		
				$this->info('...');
				$this->info('Saving to excel file - please wait...');


        $sheet->fromArray($data);
	    });
		})->save();
		$this->info('Excel file created at '.$directory.$filename);

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
