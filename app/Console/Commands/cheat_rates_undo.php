<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Zone;
use App\Models\Review;
use App\Models\User;
use App\Models\Business;

class cheat_rates_undo extends Command
{
  /**
   * Create a new command instance.
   *
   * @return void
   */
  protected $name = 'mazkara:cheat.ratings.undo';

  protected $description = 'Undo messed up excessive dummy ratings.';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle(){

    $businesses = Business::select()->hasCategories()->hasHighlights()->hasPhotos()->where('total_ratings_count', '>', 40)->take(100)->get();
    $this->line(count($businesses).' venues to ratings to be audited');

    foreach($businesses as $business){

      $counted = Review::select()->byBusiness($business->id)->isRating()->isCheatRate()->count();
      if($counted > 20){
        $counted = $counted - (20 - rand(2, 7));
        $deletables = Review::select()->byBusiness($business->id)->isRating()->isCheatRate()->take($counted)->lists('id', 'id')->all();

        if(count($deletables)>0){
          $this->line($business->name." audited rating in progress - removing ".$counted." cheats");
          $sql = 'DELETE FROM reviews WHERE id in ('.join(',', $deletables).')';
          \DB::delete($sql);
        }else{
          $this->info($business->name." seems to have not too many cheat ratings");
        }
      }
      
      $business->updateRatingsCount();
      $business->updateAverageRating();
      $this->info($business->name." rating count audited to ".$business->total_ratings_count);

    }


    $this->info('All done :)');
  }
}
