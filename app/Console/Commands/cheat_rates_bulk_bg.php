<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Zone;
use App\Models\Review;
use App\Models\User;
use App\Models\Business;

class cheat_rates_bulk_bg extends Command
{
  /**
   * Create a new command instance.
   *
   * @return void
   */
  protected $name = 'mazkara:cheat.ratings.bg';

  protected $description = 'Add dummy ratings.';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle(){

    // get guys to cheat on
    //$users = User::select()->where('email', 'like', '%@mazkara.com')->get()->toArray();

    $city = Zone::select()->where('slug', '=', 'dubai')->get()->first();
    
//    $choices = [];
//    $choice_ids = [];
//
//    foreach($cities as $city){
//      $choices[] = $city->name;
//      $choice_ids[] = $city->id;
//    }


    //$choice = 2;//$this->choice('Whice city here?', $choices);
    //$choice = array_search($choice, $choices);
    $city_id = $city->id;//$choice_ids[$choice];
    // select venues that have photos, highlights and category and are active
    $businesses = Business::select()->byLocale($city_id)->hasCategories()->hasHighlights()->hasPhotos()->where('total_ratings_count', '<', 60)->get()->take(100);
    $this->line(count($businesses).' venues to be bulk rated in '.$city->name.' - minimum 2 - 5 rating');

    foreach($businesses as $business){
      $num_ratings = mt_rand(60, 240);
      $ratings = [];
        $this->line($business->name." random ".$num_ratings."");

       $sql = "INSERT INTO `reviews` ";
       $sql.="(`body`,`user_id`,`business_id`,`created_at`,`updated_at`,`rating`,`flags`,`ip`,
       `type`,`is_cheat`,`number_of_likes`,`number_of_comments`,`number_of_shares`)";
       $sql.="VALUES";
       $vals = [];
      for($i = 0; $i < $num_ratings; $i++){
        $rating = rand(3, 5);
        //$rand_keys = array_rand($users, 2);

        $data = ['rating'=>['rating'=>$rating]];
        //$usr = $users[$rand_keys[0]];
        $data['ip'] = 'cheat'.$i.'@'.time();//$usr['id'].'@'.time();
        //$this->line($business->name." rating in process - rated ".$rating."");
        //$business->rate($data, true);

         $vals[]="(' ',0,".$business->id.", UTC_TIMESTAMP(), UTC_TIMESTAMP(),".$rating.",'','".$data['ip']."','business',1, 0,0,0)";
         //\DB::insert($sql);


        // $review = Review::create([ 'user_id'=>'0',
        //                             'ip'=>$data['ip'], 
        //                             'business_id'=>$business->id]);
        // $review->user_id =  0;
        // $review->business_id =  $business->id;
        // $review->ip =  $data['ip'];
        // $review->body = ' ';
        // $review->rating = $rating;
        // $review->is_cheat =  1;
        // $review->save();
        $ratings[] = $rating;

      }
      $sql.=join(',', $vals);
      $vals=[];
       \DB::insert($sql);

      $business->updateRatingsCount();
      $business->updateAverageRating();

      //$this->info($business->name." given ".$num_ratings.' cheat ratings of '.join(',', $ratings));

    }


    $this->info('All done :)');
  }
}
