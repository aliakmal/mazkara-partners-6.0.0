<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Bus\SelfHandling;
use Offer;

class populate_empty_offer_price_with_number_from_offer_meta extends Command implements SelfHandling
{


    protected $name = 'mazkara:populate.empty.metas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate offers empty metas.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
      $offers = Offer::select()->where('offer_price','=', '')->get();

      // check and change if they are cost based ones only
      foreach($offers as $offer):
        $offer->legacy_price_meta = strtolower($offer->legacy_price_meta);
        if(strstr_array($offer->legacy_price_meta, ['inr', 'aed'])){
          $offer->offer_price = trim(str_replace(['inr', 'aed'], '', $offer->legacy_price_meta));
          $offer->save();
          $this->info($offer->id.' - '.$offer->title.' updated');
        }else{
          $this->line($offer->id.' - '.$offer->title.' SKIPPED');
        }
      endforeach;
    }


    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];

    }




}
