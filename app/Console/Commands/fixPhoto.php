<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Image, File;
use App\Models\Photo as Photo;
class fixPhoto extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:photos.fix';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Photos fixerupper resizes and compression ONLY.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		//$count = $count > 100 ? 100 : $count;
		$counter_file = storage_path().'/media/counter-photo-fix';
		$skip_id = $this->option('skip_id');

		$photos_done = [];
		$photos_missed = [];

		$main_time_start = microtime(true);

		if(!$skip_id):

			if(File::exists($counter_file)){
				$skip_id = trim(File::get($counter_file));
			}
		endif;

		if($skip_id){
			$photos = Photo::select()->where('imageable_type', '=', 'Business')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' photos records after photo id '.$skip_id.' taken from page '.$page);
		}else{
			$photos = Photo::select()->where('imageable_type', '=', 'Business')->orderby('id', 'asc')->take($count)->skip($page*$count)->get();
			$this->line($count.' photos records taken from page '.$page);
		}
		foreach($photos as $photo):

			if(trim($photo->image_file_name)==''){
				$this->line('Photo ID '.$photo->id.' skipped empty---');
				continue;
			}

			$time_start = microtime(true);


			
			try {
				$img = Image::make($photo->image->url());
			} catch (Intervention\Image\Exception\NotReadableException $e) {
				$this->error('--- Photo ID '.$photo->id.' not readable ---');
				$photos_missed[] = '--- Photo ID '.$photo->id.' not readable ---';
				continue;	
			}

			$url = $photo->image->url();
			$url = explode('/', $photo->image->url());
			$fname = $url[count($url)-1];
			$dir = storage_path().'/media/photo_'.$photo->type.'_'.$photo->id.md5(time());
			mkdir($dir);

			$f_name = explode('.', $fname);

			$fname = str_slug($f_name[0], '-').'.'.strtolower(array_pop($f_name));
			File::put($counter_file, $photo->id);

			$filename = $dir.'/'.$fname;
			$this->line($filename);

			try {
				$img->save($filename);
			} catch (Intervention\Image\Exception\NotReadableException $e) {
				$photos_missed[] = '--- Photo ID '.$photo->id.' not readable ---';
				continue;	
			}

			$time_end = microtime(true);
			$time = round($time_end - $time_start, 4);

			$this->line('Photo ID '.$photo->id.' downloaded to '.$filename.' in '.$time.' seconds');

			$time_start = microtime(true);
			$photo->image = $filename ;
			$photo->save();
			$time_end = microtime(true);
			$time = round($time_end - $time_start, 4);
			$this->info('Photo ID '.$photo->id.' saved in '.$time.' seconds');
			$photos_done[] = 'Photo ID '.$photo->id.' saved in '.$time.' seconds';

		endforeach;
		$main_time_end = microtime(true);

		$total_time = round($main_time_end - $main_time_start, 4);
		$this->line($count.' photos fixed in '.$total_time.' seconds.... :)');
//    Mail::send('emails.notify-fix-photo', ['server'=>$_SERVER, 'photos_done'=>$photos_done, 'photos_missed'=>$photos_missed, 'job_details'=>'Fixed '.count($photos_done).' Photos '], function($message){
//      $message->to('ali@fabogo.com', 'Mazkara')->from('no-reply@mazkara.com')
//              ->subject('Photos Fixer Upper done '.date('Y-m-d H:i'));
//    });

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 60),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
