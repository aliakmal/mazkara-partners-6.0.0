<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;
use File;

class ClearCacheBase extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:rest.base.cache';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear Base Cache.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
   *  public function schedule(Schedulable $scheduler)
	 *	{
	 *			return $scheduler->daily()->hourly()->everyMinutes(5);
	 *	}
	*/
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(){
    $this->call('cache:clear');
    $this->info($num_directories.' directories have been deleted from '.$path);		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];

	}

}
