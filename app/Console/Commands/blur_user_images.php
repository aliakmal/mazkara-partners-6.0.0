<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\Photo;
use Image;

class blur_user_images extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:users.reset';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'reset the users pictures.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		//$count = $count > 100 ? 100 : $count;

		$skip_id = $this->option('skip_id');
		if($skip_id){
			$photos = Photo::select()->where('imageable_type', '=', 'User')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' photos records after photo id '.$skip_id.' taken from page '.$page);

		}else{
			$photos = Photo::select()->where('imageable_type', '=', 'User')->orderby('id', 'asc')->take($count)->skip($page*$count)->get();
			$this->line($count.' photos records taken from page '.$page);
		}
		foreach($photos as $photo):
			
			if(trim($photo->image_file_name)==''){
				$this->line('Photo ID '.$photo->id.' skipped empty---');
				continue;
			}

			$time_start = microtime(true);

			$img = Image::make($photo->image->url());
			$url = $photo->image->url();
			$url = explode('/', $photo->image->url());
			$fname = $url[count($url)-1];
			$dir = storage_path().'/media/photo_user_'.$photo->type.'_'.$photo->id.md5(time());
			mkdir($dir);

			$filename = $dir.'/'.$fname;
			$img->save($filename);
			$time_end = microtime(true);
			$time = round($time_end - $time_start, 4);

			$this->line('Photo ID '.$photo->id.' downloaded to '.$filename.' in '.$time.' seconds');

			$time_start = microtime(true);
			$photo->image = $filename ;
			$photo->save();
			$time_end = microtime(true);
			$time = round($time_end - $time_start, 4);
			$this->line('User Photo ID '.$photo->id.' saved in '.$time.' seconds');
		endforeach;

		$this->line($count.' photos fixed.... :)');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 10),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
