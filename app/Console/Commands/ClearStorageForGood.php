<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;

class ClearStorageForGood extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:views.flush';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear the Views Cache.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(){
		parent::__construct();
		$this->files = new \Illuminate\Filesystem\Filesystem;		
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$yesterday = strtotime(date("F j, Y", time() - 60 * 60 * 24));

		foreach ($this->files->files(storage_path().'/views') as $file){
			$this->files->delete($file);
		}

		$this->info('Temp Files and Views older than one day deleted from views cache');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
