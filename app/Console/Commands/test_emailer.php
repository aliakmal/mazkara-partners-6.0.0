<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\User;
use App\Models\Post;
use Mail;

class test_emailer extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:test.email';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Hack to test email.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

    $user = User::where('email', '=', 'ali@mazkara.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()->take(5)->get();


//    Mail::send('site.users.emails.signup', ['user'=>$user->toArray(), 'data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
//      $message->to('ali@mazkara.com', 'Mazkara')->subject('Mazkara: New User Signup ['.time().']');
//    });


    Mail::send(
      'emails.weekly.stories',
      compact('user', 'posts'),
      function ($message) use ($user) {
        $message->to($user->email, $user->username)
                ->subject('Howdy there('.microtime().')');
      }
    );

		$this->info('Email sent');

	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
