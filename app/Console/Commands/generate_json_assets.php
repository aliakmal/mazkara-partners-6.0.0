<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\Zone;
use App\Models\Category;
use App\Models\Service;
use File, Mail;
class generate_json_assets extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:generate.json';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate Json assets.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->daily()->hours([3, 12, 19]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$categories = Category::all()->toArray();
		$cities = Zone::query()->cities()->get()->toArray();


    $dir = storage_path('/json/');

    $filename = $dir.'/categories.json';
    $contents = json_encode($categories);
    $bytes_written = File::put($filename, $contents);
		if ($bytes_written === false){
			$this->error('Could not write categories.json file - something went wrong');
		}else{
			$this->line('Wrote to categories.json file');
		}

    $filename = $dir.'/cities.json';
    $contents = json_encode($cities);
    $bytes_written = File::put($filename, $contents);
		if ($bytes_written === false){
			$this->error('Could not write cities.json file - something went wrong');
		}else{
			$this->line('Wrote to cities.json file');			
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
