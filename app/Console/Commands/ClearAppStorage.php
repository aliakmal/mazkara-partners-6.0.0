<?php

namespace App\Console\Commands;

use App\User;
use App\DripEmailer;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;
use File;

class ClearAppStorage extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:storage.clear';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Storage Clear.';
	protected $files;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->files = new \Illuminate\Filesystem\Filesystem;		
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(){
		$yesterday = strtotime(date("F j, Y", time() - 60 * 60 * 24));

		$directories = File::directories(storage_path().'/media');

		$force = $this->option('force');
	
		foreach($directories as $directory){
			if(($force)||(filemtime($directory) < $yesterday)){			
				File::deleteDirectory($directory);
			}
		}

		foreach ($this->files->files(storage_path().'/views') as $file){
			if(($force)||(filemtime($file) < $yesterday)){
				$this->files->delete($file);
			}
		}


		foreach($this->files->files(public_path().'/data/temp') as $file){
			if(($force)||(filemtime($file) < $yesterday)){
				$this->files->delete($file);
			}
		}


		
		$this->info('Temp Files and Views deleted from cache');
	}

	protected function getOptions()
	{
		return array(
			array('force', null, InputOption::VALUE_OPTIONAL, 'Force to clear all.', null),
		);
	}

}
