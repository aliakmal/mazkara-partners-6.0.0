<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Image, File;

use App\Models\Category as Category;
use App\Models\Business as Business;

class UpdateAllBusinessCache extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:business.cache.update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update entire businesses cache.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler){
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([3,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = 500;//$this->argument('count');

		$total = Business::count();
		$pages = ceil($total/$count);

		for($i=0;$i<=$pages;$i++):
			$businesses = Business::select()->take($count)->skip($count*$i)->get();
			$this->line($count.' records taken from page '.$i);

			foreach($businesses as $business){
				$business->updateAllCache();
				$this->info($business->id.' - '.$business->name.' updated');
				$business = null;
			}
			$this->line(count($businesses).' records updated from offset '.$i);
			$businesses = null;
			$this->info(memory_get_usage().' bytes allocated');
		endfor;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
