<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Invoice;
use App\Models\Zone;


class fix_invoice_numbers extends Command implements SelfHandling
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $name = 'mazkara:reset.invoice_numbers';

    protected $description = 'Update Invoice numbers.';

    public function __construct()
    {
        parent::__construct();

    }
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
      $cities = Zone::cities()->get();
      foreach ($cities as $city){
          $invoices = Invoice::select()->where('city_id', '=', $city->id)->orderby('id', 'asc')->get();
          $increment = 0;
          foreach ($invoices as $invoice){
            $increment++;

            $num = explode('/', $invoice->title);
            $num[0] = 'PF';
            $num[1] = $invoice->city_id;
            $num[2] = date('my', strtotime($invoice->created_at));

            $num[3] = str_pad(($increment), 10, "0", STR_PAD_LEFT);//str_pad($this->id, 10, "0", STR_PAD_LEFT);
            $invoice->title = join('/', $num);

            $invoice->save();
            $this->line($invoice->id.' of city id '.$invoice->city_id.'has been reset to the number '.$invoice->title);
          }

      }

      $this->line('All Done :)');
    }

    protected function getOptions()
    {
        return array(
        );
    }


}
