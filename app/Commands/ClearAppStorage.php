<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;
class ClearAppStorage extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:storage.clear';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Storage Clear.';
	protected $files;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->files = new \Illuminate\Filesystem\Filesystem;		
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->daily()->hours([3,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(){
		$yesterday = strtotime(date("F j, Y", time() - 60 * 60 * 24));

		$directories = File::directories(storage_path().'/media');

		$force = $this->option('force');
	
		foreach($directories as $directory){
			if(($force)||(filemtime($directory) < $yesterday)){			
				File::deleteDirectory($directory);
			}
		}

		foreach ($this->files->files(storage_path().'/views') as $file){
			if(($force)||(filemtime($file) < $yesterday)){
				$this->files->delete($file);
			}
		}

		$this->info('Temp Files and Views deleted from cache');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('force', null, InputOption::VALUE_OPTIONAL, 'Force to clear all.', null),
		);
	}

}
