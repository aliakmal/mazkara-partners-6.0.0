<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \Service_item as Service_item;
use \Menu_group as Menu_group;

class reset_item_services extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:items.reset';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reset all Items services.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$items = Service_item::all();
		foreach($items as $item){
			$service = $item->service_id;
			if($service==0){
				continue;
			}
			$services = [$service];
			$item->services()->sync($services);
			$group_name = $item->grouping;

			if(strlen(trim($group_name))>0){
				$menu_group = Menu_group::firstOrCreate(['name'=>$group_name, 'business_id'=>$item->business_id]);
				$item->menu_group_id = $menu_group->id;
				$this->line($group_name.' GROUP created/allocated ');
			}

			$item->save();
			$this->line($item->id.' ITEM HAS BEEN SAVED ');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
