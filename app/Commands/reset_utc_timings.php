<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class reset_utc_timings extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.timings';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reset UTC timings.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function fire(){

		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');

		$skip_id = $this->option('skip_id');
		if($skip_id){
			$timings = Timing::select()->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' timings records after id '.$skip_id.' taken from page '.$page);

		}else{
			$timings = Timing::select()->orderby('id', 'asc')->take($count)->skip($page*$count)->get();
			$this->line($count.' timings records after id '.$skip_id.' taken from page '.$page);
		}

		foreach($timings as $timing){
			$timing->save();
			$this->line($timing->id.' ID TIMING saved ');
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 1000),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
