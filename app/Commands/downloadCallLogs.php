<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
//use \Call_log;

class downloadCallLogs extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:call_logs.download';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download All Call Logs.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		
		$skip_id = $this->option('skip_id');

		$total = Call_log::query()->count();
		$pages = ceil($total/$count);

		if($skip_id){
			//$photos = Photo::select()->where('imageable_type', '=', 'Business')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$call_logs = Call_log::select()->withUrl()->onlyNotDownloaded()->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' records after photo id '.$skip_id.' taken from page '.$page);
			foreach($call_logs as $call_log){
				if($call_log->download()==true){
					$this->info($call_log->id.' - call log downloaded and updated');
				}else{
					$this->error($call_log->id.' - call log could not be downloaded');
					$call_log->markNotDownloaded();
				}
				$call_log = null;
			}

			$this->line(count($call_logs).' records updated from offset ');
			$call_logs = null;
			$this->info(memory_get_usage().' bytes allocated');

		}else{

			for($i=0;$i<=$pages;$i++):

				$call_logs = Call_log::select()->withUrl()->onlyNotDownloaded()->take($count)->skip($count*$i)->get();
				$this->line($count.' call log records taken from page '.$i);

				foreach($call_logs as $call_log){
					if($call_log->download()==true){
						$this->info($call_log->id.' - call log downloaded and updated');
					}else{
						$this->error($call_log->id.' - call log could not be downloaded');
					}

					$this->info($call_log->id.' - '.$call_log->title.' updated');
					$call_log = null;
				}

				$this->line(count($call_logs).' records updated from offset '.$i);
				$call_logs = null;
				$this->info(memory_get_usage().' bytes allocated');
			endfor;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
