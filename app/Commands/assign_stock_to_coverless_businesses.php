<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class assign_stock_to_coverless_businesses extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:cover.allocate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Allocate stock images to coverless businesses.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		$businesses = Business::select()->hasNoPhotos()->onlyActive()->skip($page*$count)->take($count)->get();
		$this->line($count.' records taken from page '.$page);

		$clinical = [6,24,28];

		$spa = [5,30,31,34,36,38];
		$fitness = [7,8,10,11,12,13,15,16];
		$massage = [4,25,33,40,42];


		$salon = [1,2,3,9,14,17,18,19,20,21,22,23,26,27,29,32,35,37,39,41,43,44,45,46,47];

		$salon_ladies = [1,9,18,43];

		$salon_gents = [22,23,27];

		$salon_nails = [1,18,47];
		$salon_hair_removal = [14,39,46];
		$salon_hair = [17,19,20,21,29,41,43];
		$salon_face = [3,9,26,32,44];
		$salon_body = [2,35,37,45];
		$salon_massage = $massage;

		$all_salons = array_merge($salon_nails, $salon_hair_removal, $salon_hair, $salon_face, $salon_body);

		$body_services = array_merge([1], Service::query()->where('parent_id', '=', 1)->lists('id','id'));
		$nails_services = array_merge([45], Service::query()->where('parent_id', '=', 45)->lists('id','id'));
		$hair_services = array_merge([21], Service::query()->where('parent_id', '=', 21)->lists('id','id'));
		$hair_removal_services = array_merge([31], Service::query()->where('parent_id', '=', 31)->lists('id','id'));
		$massage_services = array_merge([36], Service::query()->where('parent_id', '=', 36)->lists('id','id'));

		$s = Service::query()->showParents()->get();
    $services = [];
    foreach($s as $service):
    	$services[$service->id] = [];
      if(count($service->children) > 0):
      	$services[$service->id] = $service->children->lists('id','id');
      endif;
    endforeach;

		foreach($businesses as $business){
			if($business->isSpa()){
				$business->has_stock_cover_image = $spa[array_rand($spa)];
			}elseif($business->isFitness()){
				$business->has_stock_cover_image = $fitness[array_rand($fitness)];
			}elseif($business->isSalon()){
				if($business->isForGents()){
					$business->has_stock_cover_image = $salon_gents[array_rand($salon_gents)];
				}elseif($business->isForLadies()){
					// check what services does this salon has
					$services = $business->services->lists('id', 'id');
					// check for 
					$body = array_intersect($services, $body_services);
					$nails = array_intersect($services, $nails_services);
					$hair = array_intersect($services, $hair_services);
					$hair_removal = array_intersect($services, $hair_removal_services);
					$massage = array_intersect($services, $massage_services);

					$max = max($body,$nails,$hair,$hair_removal, $massage);
					
					foreach( array('body','nails','hair','hair_removal','massage') as $v) {
				    if ($$v == $max) {
				    	break;
				    }
				  }

					$business->has_stock_cover_image = ${'salon_'.$v}[array_rand(${'salon_'.$v})];
				}
			}elseif($business->isMassage()){
				$business->has_stock_cover_image = $massage[array_rand($massage)];
			}elseif($business->isClinic()){
				$business->has_stock_cover_image = $clinical[array_rand($clinical)];
			}
			
			$business->save();

			$this->info($business->id.' - '.$business->name.' cover updated with stock image '.$business->has_stock_cover_image.' updated');
		}
		$this->line(count($businesses).' records updated');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
