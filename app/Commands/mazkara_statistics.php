<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class mazkara_statistics extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:statistics';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get Statistics.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('action');
		switch($page){
			case 'total.call.clicks':
			default:
				$day = $this->option('day');
				if($day==null){
					$data = Counter::select()->byType('call-views')->sum('views');
					$this->line($data.' call to book clicks in total ');
				}else{
					if($day == 0){
						$data = Counter::select()->byType('call-views')->today()->sum('views');
						$this->line($data.' call to book clicks in total today ');
					}elseif($day<0){
						$start_date = \Carbon\Carbon::now()->subDays(($day*-1));
						$end_date  = \Carbon\Carbon::now();
						$data = Counter::select()->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');
						$this->line($data.' call to book clicks in total since '.($day*-1).' days ago.');
					}
				}
			break;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('action', InputArgument::REQUIRED, 'total.call.clicks'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('day', null, InputOption::VALUE_OPTIONAL, 'Today0, -1 day before etc.', null),
		);
	}

}
