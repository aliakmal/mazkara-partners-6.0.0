<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateItemsToExcel extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:export.items';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$directory = storage_path().'/media/';

		Excel::create('items-dump-'.time(), function($excel) {

	    $excel->sheet('Businesses', function($sheet) {
			$sql = ("SELECT service_items . * , group_concat( services.name SEPARATOR ',' ) AS `service_names`
										FROM service_items
										LEFT JOIN service_service_item ON service_items.id = service_service_item.service_item_id
										LEFT JOIN services ON service_service_item.service_id = services.id
										GROUP BY service_items.id");
			$result = DB::select(DB::raw($sql));
			$data = [];
			foreach($result as $row){
				$data[] = (array)$row;
			}

        $sheet->fromArray($data);

    });

		})->save();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
