<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use \Category as Category;
use \Business as Business;

class UpdateAllMetaBusinessCache extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:business.cache.meta';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update entire businesses cache.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([3,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		$skip_id = $this->option('skip_id');
		$b_id = $this->option('b_id');

		if($count == 0){
			$count = 1000;
			$total = Business::count();
			$pages = ceil($total/$count);

			for($i=0; $i<=$pages;$i++):
				$businesses = Business::select()->take($count)->skip($count*$i)->get();
				$this->line($count.' records taken from page '.$i);

				foreach($businesses as $business){
					$business->updateAllCache();
					$this->info($business->id.' - '.$business->name.' updated');
					unset($business);
				}
				$this->line(count($businesses).' records updated from offset '.$i);
				unset($businesses);
				$this->info(memory_get_usage().' bytes allocated');
			endfor;


		}else{

			if($b_id){
				//$photos = Photo::select()->where('imageable_type', '=', 'Business')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
				$businesses = [Business::find($b_id)];
				$this->line($count.' records after photo id '.$skip_id.' taken from page '.$page);
			}elseif($skip_id){
				//$photos = Photo::select()->where('imageable_type', '=', 'Business')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
				$businesses = Business::select()->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
				$this->line($count.' records after photo id '.$skip_id.' taken from page '.$page);
			}else{
				$businesses = Business::select()->take($count)->skip($page*$count)->get();
				$this->line($count.' records taken from page '.$page);
			}
	
			foreach($businesses as $business){
				$business->updateAllCache();
				$this->info($business->id.' - '.$business->name.' updated');
			}
			$this->line(count($businesses).' records updated');

		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
			array('b_id', null, InputOption::VALUE_OPTIONAL, 'Business ID.', null),
		);
	}

}
