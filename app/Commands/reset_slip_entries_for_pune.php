<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Maatwebsite\Excel\ExcelServiceProvider;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Filesystem\Filesystem;

class reset_slip_entries_for_pune extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:slip.location';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set up the Slip coordinates.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$directory = storage_path().'/media';
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');

		$reader = Excel::load($directory.'/pune.xlsx');	
		$listings = $reader->skip($page*$count)->take($count)->get();
		foreach($listings as $listing){
			$business = Business::select()->where('ref', '=', $listing->slip_id)->first();
			if($business){
				$business->geolocation_latitude = $listing->latitude;
				$business->geolocation_longitude = $listing->longitude;
				$business->save();
				$this->line('Slip ID '.$business->ref.' - '.$business->name.' updated');
			}else{
				$this->info('Slip ID '.$listing->slip_id.' was not found and not updated');
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
