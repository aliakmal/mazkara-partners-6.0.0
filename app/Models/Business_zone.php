<?php
namespace App\Models;

use Eloquent;

use App\Models\Zone;
use App\Models\Ad_set;
use Watson\Rememberable\Rememberable;

class Business_zone extends Eloquent {
  use Rememberable;
  
	protected $guarded = array();
  protected $morphClass = 'Business_zone';

	public static $rules = array(
		'name' => 'required'
	);

  public function zones(){
    return $this->hasMany('App\Models\Zone');
  }

  public function adsets(){
    return $this->hasMany('App\Models\Ad_set', 'business_zone_id');
  }

  public function updateAssociatedZones($zone_ids){
    foreach($this->zones as $current_zone){
      $current_zone->business_zone()->dissociate($this);
      $current_zone->save();
    }

    $zones = Zone::whereIn('id', $zone_ids)->get();

    foreach($zones as $zone){
      $zone->business_zone()->associate($this);
      $zone->save();
    }
  }


  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function scopeByZones($query, $zone_ids){
      return $query->whereHas('zones', function ($q) use ($zone_ids) 
      {
         $q->whereIn('zones.id',  $zone_ids);
      });

  }



  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }


}
