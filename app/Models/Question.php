<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Question extends Post implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;
  protected $morphClass = 'Question';

  public static $rules = array(
    'title' => 'required',
    'body'=>'required',
    //'services'=>'required'
  );

  public static $fields = array(
    'title', 'caption', 'body'
  );

  public function url(){
    return route('questions.show', array($this->id, $this->slug));
  }

  public function answers(){
    return $this->belongsToMany('App\Models\Answer');
  }
  public function scopeOfServices($query, $services){
    if(!is_array($services)){
      $services = [$services];
    }

    $with_services = \DB::table('question_service')
                            ->whereIn('service_id', $services)
                            ->lists('question_id');

    return $query->whereIn('id', $with_services);
  }

  public function scopeOfSearch($query, $search){
    return $query->where('title', 'like', '%'.$search.'%');
  }

  protected static $singleTableType = 'question';
  protected static $persisted = [ 'title','slug','caption','body',
                                  'author_id','state','published_on'];

  public function view(){
    foreach($this->answers as $answer){
      $answer->view();
    }
    $this->incrementViewCount();
    $this->save();
  }


}
