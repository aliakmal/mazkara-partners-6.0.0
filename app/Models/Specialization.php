<?php
namespace App\Models;

use Eloquent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use Auth, File;
use LaravelArdent\Ardent\Ardent;

use Cviebrock\EloquentTaggable\Taggable;


class Specialization extends Eloquent{
  protected $guarded = array();

  public function resume(){
    return $this->belongsTo('\App\Models\Resume');
  }



}
