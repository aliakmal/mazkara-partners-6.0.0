<?php
namespace App\Models;

use Eloquent;

use App\Models\Zone;
use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;

class Ad extends Eloquent {
	protected $guarded = array();
  protected $morphClass = 'Ad';

  protected $classes = [
    'Activity'=>'App\Models\Activity',
    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Invoice'=>'App\Models\Invoice',
    'Highlight'=>'App\Models\Highlight',
    'Photo'=>'App\Models\Photo',
    'Post'=>'App\Models\Post',
    'Selfie'=>'App\Models\Selfie',
    'Video'=>'App\Models\Video',
    'Favorite'=>'App\Models\Favorite',
    'Group'=>'App\Models\Group',
    'Review'=>'App\Models\Review',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Zone'=>'App\Models\Zone',
  ];

  public function getItemableTypeAttribute($cls) {
      
      $cls = ucwords($cls);
      // to make sure this returns value from the array
      return array_get($this->classes, $cls, $cls);
      // which is always safe, because new 'class'
      // will work just the same as new 'Class'
  }

  public function hasPhoto($size = 'banner'){
    //$cover = $this->cover()->get();
    if(count($this->photo) > 0){
      return true;
    }else{
      return false;
    }
  }

  public function getPhotoUrl($size = 'banner'){
    if($this->hasPhoto($size)){
      $photo = $this->photo()->first();
      return $photo->image->url($size);
    }else{
      return false;
    }
  }


  public function itemable()
  {
      return $this->morphTo();
  } 
  public function ad_set(){
    return $this->belongsTo('App\Models\Ad_set');
  }

  public function photo(){
    return $this->morphOne('App\Models\Photo', 'imageable');
  }

  public function getLabelAttribute(){
    return $this->id.' '.$this->title;
  }

  public function hasHeadLine(){
    if(trim($this->headline)==""){
      return false;
    }else{
      return true;
    }
  }

  public function saveImage($image = null){
    if($image){
      $this->removeImage();
    }

    $photo = new Photo();
    $photo->image = $image;
    $photo->type = Photo::IMAGE;
    $photo->save();
    $this->photo()->save($photo);
  } 

  public function removeImage(){
    if($this->photo){
      $this->photo->deleteBasic();
    }
  }
  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }

	public static $rules = array(
		'title' => 'required',
		'caption' => 'required',
		//'url' => 'required',
		//'itemable_type' => 'required',
		//'itemable_id' => 'required'
	);

  public function shares(){
    return $this->morphMany('App\Models\Share', 'sharable');
  }

  public function incrementViewCount(){


    $c = Counter::firstOrCreate(['countable_type'=>'Ad', 'countable_id'=>$this->id, 'type'=>Counter::IMPRESSIONS, 'dated'=>date('Y-m-d')]);
    $c->incrementViewCount();
  }

  public function incrementCTRCount(){
    $c = Counter::firstOrCreate(['countable_type'=>'Ad', 'countable_id'=>$this->id, 'type'=>Counter::CTR, 'dated'=>date('Y-m-d')]);

    $c->incrementViewCount();
  }




}
