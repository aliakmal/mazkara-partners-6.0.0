<?php
namespace App\Models;

use Eloquent;

use App\Models\Zone;
use App\Models\Category;


class Ad_zone extends Eloquent {
	protected $guarded = array();
  protected $morphClass = 'Ad_zone';

  public function zone(){
    return $this->belongsTo('App\Models\Zone', 'zone_id');
  }

  public function category(){
    return $this->belongsTo('App\Models\Category', 'category_id');
  }

	public static $rules = array(
		'title' => 'required',
		//'caption' => 'required',
		'zone_id' => 'required',
		'category_id' => 'required'
	);
}
