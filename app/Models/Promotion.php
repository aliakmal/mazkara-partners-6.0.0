<?php
namespace App\Models;


use LaravelArdent\Ardent\Ardent;

class Promotion extends Ardent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required',
		'description' => 'required',
		'starts' => 'required',
		'ends' => 'required',
		'type' => 'required',
		'business_id' => 'required',
		'status' => 'required',
		'amount' => 'required',
		'discounted_as' => 'required'
	);


  protected $fillable = array('title', 'description', 'starts', 'ends', 'type', 'business_id', 'status', 'amount', 'discounted_as'); 

  public static $types = array('discount'=>'Discount', 'promotion'=>'Promotion');
  public static $statuses = array('active'=>'Active', 'inactive'=>'Inactive');

  public function artwork(){
    return $this->morphOne('App\Models\Photo', 'imageable')->where('type', Photo::PROMOTION);
  }
  public function business(){
    return $this->belongsTo('App\Models\Business');
  }

  public function saveImage($image){
    if(is_null($image))
      return;

    $this->artwork()->delete();
    
    $photo = new Photo();
    $photo->image = $image;
    $photo->type = Photo::PROMOTION;
    $photo->save();
    $this->artwork()->save($photo);
  } 

}
