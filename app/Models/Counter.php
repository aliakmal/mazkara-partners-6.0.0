<?php
namespace App\Models;

use Eloquent;
use App\Models\Business;
use DB;

class Counter extends Eloquent {
	protected $guarded = array();
  const PAGE_VIEWS = 'page-views';
  const CALL_VIEWS = 'call-views';
  const CTR = 'ctr';
  const IMPRESSIONS = 'impressions';

	public static $rules = array();

  public function business(){
    $this->belongsTo('App\Models\Business');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale ? $locale : mzk_get_localeID();
    $countable = 'Business';
    return $query->join('businesses', function($join) use($locale){
      $join->on('businesses.id', '=', 'counters.countable_id');
      $join->where('businesses.city_id', '=', $locale);
    })->where('countable_type', '=', $countable);
  }

  public function scopeToday($query){
    return $query->where(DB::raw('DATE(counters.dated)'), date('Y-m-d'));
  }

  public function scopeByBusiness($query, $business_id){
    return $query->where('countable_type', '=', 'Business')
                  ->where('countable_id', '=', $business_id);
  }

  public function scopeByCountable($query, $countable_type, $countable_id){
    return $query->where('countable_type', '=', $countable_type)
                  ->where('countable_id', '=', $countable_id);
  }


  public function scopeByType($query, $type){
    return $query->where('counters.type', '=', $type);
  }
  
  public function scopeBetweenDates($query, $start, $end){
    return $query->where('dated', '>=', $start)->where('dated', '<=', $end);
  }

  public function incrementViewCount(){
    $this->views = $this->views + 1;
    $this->save();
  }
}
