<?php
namespace App\Models;

use App\Models\Zone;
use App\Models\Role;

use Eloquent, Request, Auth;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Comment;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract{ 
//implements ConfideUserInterface, SluggableInterface{
  use EntrustUserTrait;
  use SluggableTrait;
  use Authenticatable, CanResetPassword;
  protected $morphClass = 'User';

  protected $sluggable = array(
    'build_from' => 'slugname',
    'save_to'    => 'slug',
    'max_length' => 200
  );

  protected $table = 'users';
  protected $hidden = ['password', 'remember_token'];


  //use ConfideUser;
   // Add this trait to your user model

  public static $non_selectable_roles = ['client'];

  public static function getRepoDefaults($email){
    $p = str_random(12);
    return ['email'=>$email, 'password'=>$p, 'username'=>'', 'password_confirmation'=>$p];
  }
  
  public function getSlugnameAttribute(){
    return empty($this->name) ? $this->username : $this->name;
  }

  public function avatar(){
    return $this->morphOne('Photo', 'imageable');
  }

  public function isRecievingNewsletter(){
    if($this->recieve_newsletters == 1){
      return true;
    }else{
      return false;
    }
  }

  public function allowNewsletter(){
    $this->recieve_newsletters = 1;
  }

  public function unallowNewsletter(){
    $this->recieve_newsletters = 0;
  }



  public function getSafeAttributes(){
    $fields = [ 'email', 'name', 'about', 'location', 'designation', 
                'designation_at', 'designated_at_url', 'contact_email_address',
                'twitter', 'phone', 'instagram', 'gender'];
    $data = [];
    foreach($fields as $field){
      $data[$field] = $this->$field;
    }
    return $data;
  }

  function isEditableBy($user){
    if($user->hasRole('admin')){
      return true;
    }

    if($this->id == $user->id){
      return true;
    }

    return false;
  }

  public function markBan(){
    $this->is_banned = 1;
  }

  public function markUnban(){
    $this->is_banned = 0;
  }

  public function isBlocked(){
    return $this->is_banned == 1 ? true : false;
  }
  public function getMetaAttribute($value){
    return (array)json_decode($value);
  }

  public function setMetaAttribute($value){
    $this->attributes['meta'] = json_encode($value);
  }

  public function saveImage($image){
    if(is_null($image)){
      return false;
    }

    $this->removeImage();

    $photo = new Photo();
    $photo->image = $image;
    $photo->type = Photo::AVATAR;
    $photo->save();
    $this->avatar()->save($photo);
    $this->addMeta('photo', $photo->image->url('micro'));
    $this->save();
  }

  public function updateMetaCache(){
    if(count($this->avatar)>0){
      $this->addMeta('photo', $this->avatar->image->url('micro'));    
    }
  }

  public function removeImage(){
    if(count($this->avatar)>0){
      $this->avatar()->first()->deleteBasic();
      $this->addMeta('photo', null);
    }
  }

  public function canAccessRoles(){
    if($this->hasRole('admin')){
      return true;
    }
    return false;
  }

  public function canAccessDashboard($dashboard){
    if($this->hasRole('admin')){
      return true;
    }
    switch($dashboard){
      case 'finance':
        return $this->hasRole('finance') || $this->can("manage_invoices");
      break;
      case 'editor':
        return $this->hasRole('editorial');
      break;
      case 'content':
        return $this->hasRole('moderator') || $this->can("manage_listings");
      break;
      case 'crm':
        return $this->hasRole('sales-admin') || $this->hasRole('sales');
      break;
    }

    return false;
  }
  public function comments(){
    return $this->hasMany('App\Models\Comment', 'user_id');
  }

  public function quickSetRegistered($attrs){
    $this->confirmed = 1;
    $this->save();
    $role = Role::where('name', '=', 'user')->first();
    $this->attachRole($role);             
    $this->update($attrs);
  }

  public static $profile_rules = array(
    'name' => 'required',
    //'username' => 'required'

  );

  public static $profile_fields= array(
    'name', 'about', 'twitter', 'phone', 'instagram', 'gender', 'contact_email_address',
    'location', 'designation', 'designated_at_text', 'designated_at_url'
  );

  public function vouchers(){
    return $this->hasMany('App\Models\Voucher');
  }

  public function hasNoVouchers(){
    return count($this->vouchers) >0 ? false : true;
  }


  public function zones(){
    return $this->belongsToMany('App\Models\Zone');
  }

  public function getUsersLocationAttribute(){
    return trim($this->location) == '' ? 'undefined' : $this->location;
  }



  public function allowedZones(){
    if($this->hasRole('admin')){
      return Zone::all();
    }else{
      return $this->zones()->get();
    }
  }

  public function isContributorable(){
    return $this->hasRole('admin') || $this->hasRole('contributor');
  }

  public function getAuthorsDesignationAttribute(){
    //$d = $this->designation.' at '.$this->designated_at_text;
    //if(trim($d)=='at'){
    //  return $this->hasRole(['contributor','admin']) ? 'Author' : '';
    //}

    $d = $this->designation;
    if(trim($d)==''){
      $d = 'Author' ;
    }
    if(empty($this->designated_at_url)){
      $d.=(trim($this->designated_at_text)!=''?' at '.$this->designated_at_text:'');  
    }else{
      $d.=' at <a href="http://'.ltrim($this->designated_at_url, 'http://').'">'.$this->designated_at_text.'</a>';  
    }

    return $d;
  }



  public function hasDefaultLocale(){
    return ($this->allowedZones()->count() > 0 ? true : false );
  }

  public function defaultLocale(){
    if($this->hasDefaultLocale()){
      return $this->allowedZones()->first();
    }

    return false;
  }

  protected $fillable = array('username', 'email', 'password', 'name', 'about', 'confirmed', 'confirmation_token', 'remember_token', 'twitter', 'instagram', 'gender');

  public function reviews(){
    return $this->hasMany('App\Models\Review');
  }

  public function completedReviews(){
    return $this->reviews()->where('body', '<>', ' ');//hasMany('Review');
  }

  public function getStockImage(){
    return asset('assets/users-covers/'.rand(1, 4).'.jpg');
  }

  public function hasContactDetails(){
    return empty(trim($this->phone.$this->contact_email_address.$this->twitter.$this->instagram)) ? false : true;
  }

  public function activities(){
    return $this->hasMany('App\Models\Activity');
  }

  public function favorites(){
    return $this->hasMany('App\Models\Favorite');
  }

  public function followsUsers(){
    return $this->favorites()->where('favorable_type', '=', 'User')->groupBy('favorable_id');
  }

  public function followsBusinesses(){
    return $this->favorites()->where('favorable_type', '=', 'Business')->groupBy('favorable_id');
  }

  public function followedBusinessesByIds($ids = false){
    $ids = $ids ? $ids : [];
    return $this->favorites()->where('favorable_type', '=', 'Business')->whereIn('favorable_id', $ids);
  }


  public function followsDeals(){
    return $this->favorites()->where('favorable_type', '=', 'Deal');
  }

  public function updateAllCache(){
    $this->updateFavoriteCount();
    $this->updateCheckInCount();
    $this->updateReviewsCount();
    $this->updateRatingsCount();
    $this->updateFollowersCount();
    $this->updateFollowsCount();
  }


  public function updateFavoriteCount(){
    $this->favorites_count = count($this->favourites);
    $this->save();
  }

  public function accounts(){
    return $this->hasMany('App\Models\Account', 'user_id');
  }

  public function scopeExcludeUser($query, $user_id){
    return $query->where('id', '<>', $user_id);
  }

  public function roleLabel(){
    $s = 'A Fabogo User';
    if($this->hasRole('admin')){
      $s = 'Administrator';
    }
    if($this->hasRole('contributor')){
      $s = 'Expert Contributor';
    }

    return $s;

  }


  public function follows(){
    return $this->morphMany('App\Models\Favorite', 'favorable');
  }

  public function isFollowed($user_id){
    return $this->follows()->where('user_id', '=', $user_id)->count() > 0;
  }

  public function followers(){
    return $this->follows()->where('favorable_id', '=', $this->id)
                          ->where('favorable_type', '=', 'User');
  }

  public function getFollowersCountLabelAttribute(){
    $c = $this->followers()->count();
    $str = 'No ';
    if($c > 0){
      $str = $c;
    }

    $str.=' followers';
    return $str;
  }

  public function getArticlesCountLabelAttribute(){
    $c = $this->posts()->count();
    $str = 'No ';
    if($c > 0){
      $str = $c;
    }

    $str.=' Articles';
    return $str;
  }


  public function posts(){
    return $this->hasMany('App\Models\Post', 'author_id');
  }

  public function isUserMeSignedIn(){
    if(!Auth::check()){
      return false;
    }

    if(Auth::user()->id == $this->id){
      return true;
    }else{
      return false;
    }
  }

  public function isUserBoring(){
    $sum = count($this->posts)+count($this->completedReviews()->get());//+count($this->followsBusinesses);
    if($sum == 0){
      return true;
    }else{
      return false;
    }
  }

  public function merchants(){
    return $this->belongsToMany('App\Models\Merchant'); 
  }

  public function module_accesses(){
    return $this->belongsToMany('App\Models\Merchant_module_access'); 
  }

  public function canManageBusiness($business_id){
    $businesses = $this->businesses();
    foreach($businesses as $business){
      if($business->id == $business_id){
        return true;
      }
    }

    return false;
  }

  public function businesses(){
    $businesses = [];
    foreach($this->merchants as $merchant){
      foreach($merchant->businesses as $business){
        $businesses[] = $business;
      }
    }
    return $businesses;
  }


  public function isMerchantForBusiness($business_id){

    foreach($this->businesses() as $business){
      if($business_id == $business->id){
        return true;
      }
    }

    return false;
  }


  public static function findByProvider($provider, $_id){
    $result = Account::where('provider', '=', $provider)->where('provider_id', '=', $_id)->get();
    return $result->count() > 0 ? User::find($result->first()->user_id) : false;
  }

  public function findByFacebookId($fb_id, $except = false){
    $result = Account::where('provider', '=', Account::FACEBOOK)->where('provider_id', '=', $fb_id);
    if($except){
      $result = $result->where('user_id', '<>', $except);
    }
    $result = $result->get();
    return $result->count() > 0 ? User::find($result->first()->user_id) : false;
  }

  public function findByGoogleId($gl_id, $except = false){
    $result = Account::where('provider', '=', Account::GOOGLE)->where('provider_id', '=', $gl_id);
    if($except){
      $result = $result->where('user_id', '<>', $except);
    }
    $result = $result->get();

    return $result->count() > 0 ? User::find($result->first()->user_id) : false;
  }

  public function findByEmail($email){
    $result = $this->where('email', '=', $email)->get();
    return $result->count() > 0 ? $result->first() : false;
  }

  public function hasFacebookAccount(){
    return count($this->accounts()->where('provider','=', Account::FACEBOOK)->get())>0;
  }

  public function check_ins(){
    return $this->hasMany('App\Models\Check_in');
  }

  public function hasAbout(){
    return empty($this->about)? false:true;
  }

  public function hasLocation(){
    return empty($this->location)? false:true;
  }

  public function getFullNameAttribute(){
    return empty($this->name)?(empty($this->username)?substr(trim($this->email), 0, 3):$this->username):$this->name;
  }

  public function getSelectableFullNameAttribute(){
    return $this->id.' - '.(empty($this->name)?$this->username:$this->name).'('.$this->email.')';
  }

  public function scopeByName($query, $search){
    return $query->where('name', 'like', $search.'%')
                ->orWhere('email', 'like', $search.'%');
  }

  public function scopeByRoles($query, $role_ids){
    $user_ids = \DB::table('assigned_roles')->whereIn('role_id', $role_ids)->lists('user_id', 'user_id');

    return $query->whereIn('id', $user_ids);
  }

  public function scopeOnlyAuthors($query){
    $role_ids = Role::whereIn('name',['admin', 'contributor'])->lists('id','id')->all();

    return $query->byRoles($role_ids);
  }

  public function scopeByPOCs($query){
    $role_ids = Role::whereIn('name',['sales-admin', 'sales'])->lists('id','id')->all();

    return $query->byRoles($role_ids);
  }
  public function scopeSearch($query, $search){
    return $query->where('name', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%');
  }


  public function getInitials(){
    $initial = substr(trim($this->full_name), 0, 2);

    $initial = trim($initial) == '' ? substr(trim($this->email), 0, 2) : $initial;
    return $initial;
  }

  public function getGravatarAttribute($size = 80){
    $meta_photo = $this->getMeta('photo-micro');
    if($meta_photo!=null){
      return $meta_photo;
    }
    if(count($this->avatar)>0){
      return $this->avatar->image->url('small');

    }

    //elseif(count($this->accounts)>0){
    //  $account = $this->accounts()->where('provider', '=', 'facebook')->get();
    //  if(count($account)>0){
    //    $account = $account->first();
    //    return 'http://avatars.io/facebook/'.$account->provider_id;
    //  }
    //}
    $hash = md5(strtolower(trim($this->attributes['email'])));
    return "http://www.gravatar.com/avatar/$hash?s=".$size."&d=identicon";//.urlencode(asset('assets/avatar.jpg'));
  }

  public function hasFavouritedMultiple($type = 'Business', $ids){
    return $this->favorites()->whereIn('favorable_id', $ids)
                ->whereIn('favorable_type', [$type])->lists('favorable_id', 'favorable_id')->all();

  }


  public function hasFavourited($type = 'App\Models\Business', $id){
    $type = strstr($type, 'App\Models')? $type : 'App\Models\\'.$type;

    $posts_type = ['App\Models\Selfie', 'App\Models\Article', 'App\Models\Post',
                    'App\Models\Video','App\Models\Answer','App\Models\Question'];

    if(in_array($type, $posts_type)){
      return $this->favorites()->where('favorable_id', '=', $id)
                  ->whereIn('favorable_type', $posts_type)->count() > 0;

    }else{
      return $this->favorites()->where('favorable_id', '=', $id)
                ->where('favorable_type', '=', $type)->count() > 0;
    }
  }

  public function hasCheckedIn($business_id){
    return $this->check_ins()->where('business_id', '=', $business_id)->count() > 0;
  }

  

  public function updateCheckInCount(){
    $this->check_ins_count = $this->check_ins()->count();
    $this->save();
  }

  public function updateReviewsCount(){
    $this->reviews_count = $this->reviews()->isReview()->count();
    $this->save();
  }

  public function updateRatingsCount(){
    $this->ratings_count = $this->reviews()->isRating()->count();
    $this->save();
  }
  public function updateFollowersCount(){
    $this->followers_count = $this->followers()->count();
    $this->save();
  }

  public function updateFollowsCount(){
    $this->follows_count = $this->follows()->count();
    $this->save();
  }

  public function setLastLoginIP(){
    $this->last_login_ip = Request::getClientIp();
    $this->save();
  }

  public function addMeta($item, $value){
    $meta = $this->meta;
    $meta[$item] = $value;
    $this->meta = $meta;
  }

  public function getMeta($item, $default = false){
    $meta = $this->meta;
    return isset($meta[$item])? (is_object($meta[$item])?(array)$meta[$item]:$meta[$item]):$default;
  }


  public static function loggedIn(){
    if(Auth::check()){
      $user = User::find(Auth::user()->id);
      $ip = Request::getClientIp();
      $ip = trim($ip);
      if(empty($ip)){
        $ip = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:$ip;
      }

      $locale_id = mzk_get_localeID();
      if(!empty($ip)){
        if($user->last_login_ip != $ip){
          $user->last_login_ip = $ip;
          $user->save();
        }
      }

      if(!empty($locale_id)){
        if($user->current_locale != $locale_id){
          $user->current_locale = $locale_id;
          $user->save();
        }
      }
      return true;
    }else{
      return false;
    }
  }


  public function countersArray(){
    $result = [];
    $result[] = $this->reviews_count>0 ? $this->reviews_count.' Reviews' : null;
//    $result[] = $this->followers_count>0 ? $this->followers_count.' Followers':null;
    //$result[] = $this->check_ins_count>0? $this->check_ins_count.' Check Ins':null;
    return array_filter($result);
  }

}


// Replaced below code with above

// use Illuminate\Auth\UserTrait;
// use Illuminate\Auth\UserInterface;
// use Illuminate\Auth\Reminders\RemindableTrait;
// use Illuminate\Auth\Reminders\RemindableInterface;
// 
// class User extends Eloquent implements UserInterface, RemindableInterface {
// 
// 	use UserTrait, RemindableTrait;
// 
// 	/**
// 	 * The database table used by the model.
// 	 *
// 	 * @var string
// 	 */
// 	protected $table = 'users';
// 
// 	/**
// 	 * The attributes excluded from the model's JSON form.
// 	 *
// 	 * @var array
// 	 */
// 	protected $hidden = array('password', 'remember_token');
// 
// }
