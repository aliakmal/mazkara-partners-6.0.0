<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;


class Offer extends Ardent {
	protected $guarded = array();
  protected $morphClass = 'Offer';

  public function afterCreate(){
    $this->business->updateMetaOffers();
  }

  public function afterSave(){
    $this->business->updateMetaOffers();
  }

	public static $rules = array(

	);

  public static $fields = array("title", "description", "original_price", "legacy_price_meta", "discount_type", "offer_price", "start_date",
    "valid_until", "max_num_vouchers", "business_id","toc", "state", "cost_range");

  public function business(){
    return $this->belongsTo('App\Models\Business', 'business_id');
  }

  public function isNonPrice(){
    return $this->discount_type == 'non-price-based-deal' ? true : false;
  }

  public function menu_groups(){
    return $this->belongsToMany('App\Models\Menu_group');
  }

  public function services(){
    return $this->belongsToMany('App\Models\Service');
  }

  public function scopeOfServices($query, $services){
    $with_services = \DB::table('offer_service')
                          ->whereIn('service_id', $services)
                          ->lists('offer_id');

    return $query->whereIn('id', $with_services);
  }

  public function service_items(){
    return $this->belongsToMany('App\Models\Service_item');
  }

  public static function activeStates(){
    return array( 'active' => 'Active', 'pending'=>'Pending',
                  'inactive' => 'Inactive');
  }
  public function getTocAttribute($value){
    return (array)json_decode($value);
  }

  public function setTocAttribute($value){
    $this->attributes['Toc'] = json_encode($value);
  }

  public static function activeStatesForEditable(){
    $tags = self::activeStates();
    $result = [];
    foreach($tags as $ii=>$vv){
      $result[] = ['value'=>$ii, 'text'=>$vv];
    }
    return $result;
  }

  public static function tocOptions(){
    $tocs = array('Prior appointment required','Non transferable',
                'Valid on all days','All services to be availed in one sitting',
                'You can claim only 2 vouchers from Mazkara' );
    return $tocs;
  }

  public function photo(){
    return $this->morphOne('App\Models\Photo', 'imageable');
  }


  public function saveImage($image){
    if(is_null($image)){
      return false;
    }

    $this->removeImage();

    $photo = new Photo();
    $photo->image = $image;
    $photo->save();
    $this->photo()->save($photo);
  } 

  public function removeImage(){
    if(count($this->photo)>0){
      $this->photo()->deleteBasic();
    }
  }

  public function isValid(){
    return ($this->state == 'active') && ($this->isExpired() == false);
  }

  public function isExpired(){
    return (strtotime($this->valid_until) < strtotime(date('Y-m-d')))?true:false;
  }




  public function resetState(){
    // is today is before the start date
    $this->state = 'active';

    if(time() < strtotime($this->start_date)) {
      $this->state = 'pending';
    }

    // valid date has passed
    if(strtotime($this->valid_until) < time()) {
      $this->state = 'inactive';
    }

    $this->save();
  }

  public static $tags = [
                          ['id'=>'special', 'label'=>'Special', 'css'=>'label-info'],
                          ['id'=>'hot', 'label'=>'Hot', 'css'=>'label-danger'],
                          ['id'=>'special', 'label'=>'Special', 'css'=>'label-warning']
                        ];

  public static function tagsList(){
    $tags = self::$tags;
    $result = [];
    
    foreach($tags as $tag){
      $result[$tag['id']] = $tag['label'];
    }

    return $result;
  }

  public static function discountPercentages(){
    $result = [];
    for($i=10; $i<=90; $i+=10){
      $result[$i] = $i;
    }

    return $result;
  }

  public function scopeByBusiness($query, $b_id){
    return $query->where('business_id', '=', $b_id);//->Where('body', '<>', ' ');
  }

  public function scopeByState($query, $state){
    return $query->where('state', '=', $state);//->Where('body', '<>', ' ');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->join('businesses', function($join){
              $join->on('businesses.id', '=', 'offers.business_id');
            })->where('businesses.city_id', '=', $locale);
  }


  public function scopeByStates($query, $states){
    return $query->whereIn('state', $states);//->Where('body', '<>', ' ');
  }

  public function scopeOnlyActive($query){
    $valid_until = date('Y-m-d');
    return $query->byStates(['active'])->where('valid_until', '>=', $valid_until);
  }

  public function scopeByValidDateRange($query, $days = 7){
    $yesterday = \Carbon\Carbon::yesterday();
    $week_ago = \Carbon\Carbon::yesterday()->subDays($days);

    // get offers which have expired in the last 7 days
    return $query->where('valid_until', '>=', $week_ago->toDateString())
                ->where('valid_until', '<=', $yesterday->toDateString());
  }

  public function cssForTag(){
    return Offer::tagsValue($this->tag, 'css');
  }

  public function labelForTag(){
    return Offer::tagsValue($this->tag, 'label');
  }

  public static function tagsValue($tg, $value='id'){
    $tags = self::$tags;
    $result = [];
    
    foreach($tags as $tag){
      if($tg==$tag['id']){
        return $tag[$value];
      }
    }

    return $tags[0][$value];//'label-default';
  }

  public static function tagsForEditable(){
    $tags = self::tagsList();
    $result = [];
    foreach($tags as $ii=>$vv){
      $result[] = ['value'=>$ii, 'text'=>$vv];
    }
    return $result;
  }

  public function getPriceAttribute(){
    return $this->original_price;
  }

  public function getBodyAttribute(){
    return $this->title;
  }

  public static function areClaimable($offer){
    return false; // remove this
    
    if($offer->state != 'active'){
      return false;
    }

    return ($offer->max_num_vouchers == $offer->used_vouchers) ? false : true;
  }

  public function isClaimable(){
    return false; // remove this
    if(!$this->isValid()){
      return false;
    }
    return ($this->max_num_vouchers == $this->used_vouchers) ? false : true;
  }

  public function bookVoucherCount($count = 1){
    $new_count = $this->used_vouchers + $count;
    if($new_count > $this->max_num_vouchers){
      return false;
    }

    $this->used_vouchers = $new_count;

    $this->save();

    return true;
  }

  public function getDisplayableOfferPriceAttribute(){
    if($this->discount_type == 'non-price-based-deal'){
      return $this->legacy_price_meta;
    }else{
      return $this->offer_price;
    }
  }

  public function getPriceMetaAttribute(){
    if(strlen(trim($this->legacy_price_meta))>0){
      return $this->legacy_price_meta;
    }else{
      return $this->discount_amount > 0 ? $this->discount_amount.($this->discount_type == 'percentage' ? '% OFF' : ' '.$this->discount_type) : '';
    }
  }

  public function getAvailableVouchersAttribute(){
    return ($this->max_num_vouchers - $this->used_vouchers);
  }


}
