<?php
namespace App\Models;

use Eloquent;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Business;
use App\Models\Photo;


class Group extends Eloquent implements SluggableInterface{
  use SluggableTrait;
  protected $morphClass = 'Group';

  protected $sluggable = array(
      'build_from' => 'name',
      'save_to'    => 'slug',
      'max_length' => 200

  );

	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		//'type' => 'required'
	);

  public function cover(){
    return $this->morphOne('Photo', 'imageable')->where('type', Photo::COVER);
  }

  public function hasCover($size = ''){
    //$cover = $this->cover()->get();
    if(count($this->cover) > 0){
      return true;

    }else{
      return false;
    }
  }

  public function getCoverUrl($size = null){
    if($this->hasCover()){
      $cover = $this->cover()->first();
      return $cover->image->url($size);
    }else{
      return false;
    }
  }

  public function isCustomActivated(){
    if(!($this->is_custom_active > 0)){
      return false;
    }

    if($this->hasBanner() && $this->hasCover()){
      return true;
    }

    return false;
  }


  public function photos(){
    return $this->morphMany('Photo', 'imageable');
  }

  public function banner(){
    return $this->morphOne('Photo', 'imageable')->where('type', Photo::PROMOTION);
  }

  public function hasBanner($size = ''){
    //$cover = $this->cover()->get();
    if(count($this->banner) > 0){
      return true;
    }else{
      return false;
    }
  }

  public function getBannerUrl($size = null){
    if($this->hasBanner()){
      $cover = $this->banner()->first();
      return $cover->image->url($size);
    }else{
      return false;
    }
  }


  public function saveBanner($image){
    if(!is_null($image)){
      $this->cover()->delete();
      $this->saveAllImages([$image], Photo::PROMOTION);
    }
  } 

  public function saveCover($image){
    if(!is_null($image)){
      $this->cover()->delete();
      $this->saveAllImages([$image], Photo::COVER);
    }
  } 

  public function removeAllImages($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $p = Photo::findOrFail($id);
      $p->deleteBasic($id);
    }
  }


  protected function saveAllImages($images, $type){
    $result = [];
    if(!is_array($images)){
      return $result;
    }

    foreach($images as $image)
    {
      if(is_null($image)){
        continue;
      }
      $photo = new Photo(['type'=>$type]);
      $photo->image = $image;
      $photo->type = $type;
      $photo->save();
      $this->photos()->save($photo);
      $result[] = $photo;
    }
    return $result;
  }




  public function getLabelAttribute(){
    return $this->id.' - '.$this->name;
  }

  public function businesses(){
    return $this->hasMany('App\Models\Business', 'chain_id');
  }

  public function numActiveBusinesses(){
    return $this->businesses()->onlyActive()->get()->count();
  }

  public function grouped_businesses(){
    return $this->belongsToMany('App\Models\Business');
  }


  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }


  public function scopeSelector($query, $title = 'Select') {
    $selectVals[''] = $title;
    $selectVals += $this->lists('name', 'id')->all();
    return $selectVals;
  }

  public function scopeIsCustomActive($query){
    return $query->where('is_custom_active', '=', 1);
  }

  public function deactivateCCP(){
    $this->is_custom_active = 0;
  }

  public function isExpired(){
    return (strtotime($this->valid_until) < strtotime(date('Y-m-d')))?true:false;
  }


  public function scopeSearch($query, $search){
    return $query->where('name', 'like', $search.'%');
  }

  public function scopeByType($query, $type){
    return $query->where('type', $type);
  }

}
