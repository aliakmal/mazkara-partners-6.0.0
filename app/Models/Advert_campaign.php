<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

use App\Models\Zone;
use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;

class Advert_campaign extends Eloquent{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_campaign';
  }

  public static $rules = [
    'description'=>'required',
    'start_date' => 'required|date_format:Y-m-d|before:end_date',
    'end_date' => 'required|date_format:Y-m-d|after:start_date',
    'merchant_id'=>'required'
  ];

  public static $fields = array('description','merchant_id','start_date','end_date');
}
