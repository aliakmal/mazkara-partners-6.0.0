<?php
namespace App\Models;

use Kalnoy\Nestedset\Node as Node;

/**
* Zone
*/
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Photo as Photo;
use App\Models\Service as Service;
use App\Models\Category;
use App\Models\Highlight;
use Watson\Rememberable\Rememberable;

    
class Zone extends Node implements SluggableInterface{
  protected $morphClass = 'Zone';

  use SluggableTrait;
  use Rememberable;

  protected $sluggable = array(
      'build_from' => 'name',
      'save_to'    => 'slug',
      'max_length' => 200
      
  );

  public function business_zone(){
    return $this->belongsTo('App\Models\Business_zone', 'business_zone_id');
  }

  /**
   * Table name.
   *
   * @var string
   */
  protected $table = 'zones';
  public static $rules = array('name');
  public static $rules_for_zone = array('name', 'parent_id');

  public static $fields = array('name',  'description', 'state', 'parent_id');

  public static $fields_for_city = array('name',  'description', 'country_code');

  const LFT = 'lft';

  const RGT = 'rgt';

  const PARENT_ID = 'parent_id';

  protected $guarded = [ 'lft', 'rgt' ];

  public function updateBusinessesCount(){
    $this->business_count = count($this->businesses);
    $this->save();
  }

  public function hasThisBusinessZone($business_zone_id){
    return $business_zone_id == $this->business_zone_id;
  }

  public function scopeSearch($query, $search){
    return $query->where('name', 'like', '%'.$search.'%');
  }

  public function categories(){
    return $this->belongsToMany('App\Models\Category')->withPivot('state');
  }

  public function activeCategories(){
    return $this->categories()->wherePivot('state', 'active');//belongsToMany('Category')->withPivot('state');
  }

  public function scopeHasBusinessZone($query){
    return $query->where('business_zone', '>', 0);
  }

  public function scopeIsDisplayable($query){
    return $query->where('is_displayable', '>', 0);
  }


  public function scopeHasNoBusinessZone($query, $except = false){
    if($except){
      return $query->whereIn('business_zone_id', [0, $except]);
    }else{
      return $query->where('business_zone_id', '=', 0);
    }
  }
  public function zone_counters(){
    return $this->hasMany('App\Models\Service_zone_counter');
  }


  public function locale($locale = false){
    if($locale == false){
      $locale = mzk_get_localeID();
    }
    return $this->zones()->where('zone_id', '=', $locale)->first();
  }

  public function highlights(){
    return $this->belongsToMany('App\Models\Highlight')->withPivot('state');
  }

  public function activeHighlights(){
    return $this->highlights()->wherePivot('state', 'active');
  }

  public function services(){
    return $this->belongsToMany('App\Models\Service')->withPivot('state');
  }

  public function activeServices(){
    return $this->services()->wherePivot('state', 'active');
  }

  public function activeParentServices(){
    $s = Service::where('parent_id', null)->get()->lists('id', 'id')->all();
    return $this->services()->wherePivot('state', 'active')->whereIn('service_id', $s);
  }

  public function getChildIds(){
    return array_merge([$this->id], $this->getDescendants()->lists('id', 'id')->all());
  }


  public function isSubZone(){

    if($this->depth > 1){
      return true;
    }else{
      return false;
    }
    // is the parent id zero? i.e. is this Dubai
    if($this->parent_id==0){
      return false;
    }
    // is its parents parent id_zero i.e. Barsha
    $p = Zone::find($this->parent_id);
    if($p->parent_id == 0){
      return false;
    }

    return true;
  }

  public function setCity($locale = false){
    $locale = $locale ? $locale : mzk_get_localeID();
    $this->city_id = $locale;
    $this->save();
  }

  public function isZone(){
    // is the parent id zero? i.e. is this Dubai
    if($this->parent_id==0){
      return false;
    }
    // is its parents parent id_zero i.e. Barsha
    if($this->parent()->get()->first()->parent_id == 0){
      return true;
    }

    return false;
  }



  public function photos(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', Photo::IMAGE);
  }

  public function thumbnail(){
    $photos = $this->photos();
    if($photos->count()>0){
      return $photos->first()->image->url('medium');
    }else{
      return 'http://lorempixel.com/400/275/city/'.rand(1,6);
    }
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('parent_id', '=', $locale)
                  ->orWhere('city_id', '=', $locale);

  }


  public function scopeCities($query){
    return $query->where('parent_id', '=', '0')->orWhere('parent_id', '=', null);

  }
  public function scopeNotCities($query){
    return $query->where('parent_id', '<>', '0')->orWhere('parent_id', '<>', 'null');

  }


  public function businesses()
  {
    return $this->hasMany('App\Models\Business');
  }

  public function saveImages($images){
    if(!is_array($images))
    {
        $images = [$images];
    }

    foreach($images as $image)
    {
      $photo = new Photo();
      $photo->image = $image;
      $photo->type = Photo::IMAGE;
      $photo->save();
      $this->photos()->save($photo);
    }
  } 
  public function removeAllImages($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $p = Photo::findOrFail($id);
      $p->deleteBasic($id);
    }
  }


  public function scopeSelector($query, $title = 'Select') {
      $selectVals[''] = $title;
      $selectVals += $this->lists('name', 'id')->all();
      return $selectVals;
  }


  public static function getCitiesArray(){
    return ['dubai'=>'Dubai, UAE', 'pune'=>'Pune, India', 'mumbai'=>'Mumbai, India', 'bengaluru'=>'Bengaluru, India', 'delhi'=>'Delhi, India'];
  }

public function isActive(){
  return $this->state == 'active' ? true : false;
}


public function scopeShowActive($query){
  return $query->where('state', '=', 'active');
}
public function scopeShowNotActive($query){
  return $query->where('state', '<>', 'active');
}

protected $stringPath;

public function isCity(){
  return $this->parent_id == 0;
}

public function countryFromCountryCode(){
  $codes = ['AE'=>'United Arab Emirates', 'IN'=>'India'];
  return isset($codes[$this->country_code])?$codes[$this->country_code]:$this->country_code;
}

public function stringPath()
{
   if ($this->stringPath) return $this->stringPath;

   $parent = $this->parent;
   return $this->stringPath = $parent ? $parent->stringPath().' > '.$this->name : $this->name;
}

protected $aPath;

public function aPath()
{
   if ($this->aPath) return $this->stringPath;

   $parent = $this->parent;
  if($parent){
    $parent->aPath()[$this->name] = [$this->id, $this->name];
  }else{
    $parent->aPath()[$this->name] = [];

  }
   return $this->aPath;// = $parent ? $parent->stringPath().' > '.$this->name : $this->name;
}


  //////////////////////////////////////////////////////////////////////////////

  //
  // Below come the default values for Baum's own Nested Set implementation
  // column names.
  //
  // You may uncomment and modify the following fields at your own will, provided
  // they match *exactly* those provided in the migration.
  //
  // If you don't plan on modifying any of these you can safely remove them.
  //

  // /**
  //  * Column name which stores reference to parent's node.
  //  *
  //  * @var string
  //  */
  // protected $parentColumn = 'parent_id';

  // /**
  //  * Column name for the left index.
  //  *
  //  * @var string
  //  */
  // protected $leftColumn = 'lft';

  // /**
  //  * Column name for the right index.
  //  *
  //  * @var string
  //  */
  // protected $rightColumn = 'rgt';

  // /**
  //  * Column name for the depth field.
  //  *
  //  * @var string
  //  */
  // protected $depthColumn = 'depth';

  // /**
  //  * Column to perform the default sorting
  //  *
  //  * @var string
  //  */
  // protected $orderColumn = null;

  // /**
  // * With Baum, all NestedSet-related fields are guarded from mass-assignment
  // * by default.
  // *
  // * @var array
  // */
  // protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

  //
  // This is to support "scoping" which may allow to have multiple nested
  // set trees in the same database table.
  //
  // You should provide here the column names which should restrict Nested
  // Set queries. f.ex: company_id, etc.
  //

  // /**
  //  * Columns which restrict what we consider our Nested Set list
  //  *
  //  * @var array
  //  */
  // protected $scoped = array();

  //////////////////////////////////////////////////////////////////////////////

  //
  // Baum makes available two model events to application developers:
  //
  // 1. `moving`: fired *before* the a node movement operation is performed.
  //
  // 2. `moved`: fired *after* a node movement operation has been performed.
  //
  // In the same way as Eloquent's model events, returning false from the
  // `moving` event handler will halt the operation.
  //
  // Below is a sample `boot` method just for convenience, as an example of how
  // one should hook into those events. This is the *recommended* way to hook
  // into model events, as stated in the documentation. Please refer to the
  // Laravel documentation for details.
  //
  // If you don't plan on using model events in your program you can safely
  // remove all the commented code below.
  //

  // /**
  //  * The "booting" method of the model.
  //  *
  //  * @return void
  //  */
  // protected static function boot() {
  //   // Do not forget this!
  //   parent::boot();

  //   static::moving(function($node) {
  //     // YOUR CODE HERE
  //   });

  //   static::moved(function($node) {
  //     // YOUR CODE HERE
  //   });
  // }

}
