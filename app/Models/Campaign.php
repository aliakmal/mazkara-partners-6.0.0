<?php
namespace App\Models;

use Eloquent;

use App\Models\Zone;
use App\Models\Ad_set;
use App\Models\Ad;

class Campaign extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required',
		//'caption' => 'required',
		//'starts' => 'required',
		//'ends' => 'required'
	);

  public function adsets(){
    return $this->hasMany('App\Models\Ad_set')->orderby('slot', 'asc'); 
  }

  public function ads(){
    return $this->hasManyThrough('App\Models\Ad','App\Models\Ad_set');
  }

  public function scopeCurrent($query){
    return $query->whereRaw('campaigns.starts <= CURDATE()')
          ->whereRaw('campaigns.ends >= CURDATE()');
  }

  public function scopeByCategory($query, $categories){

  }

  public function isValid(){
    return (strtotime($this->ends) < strtotime(date('Y-m-d')))?true:false;

  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function scopeBySearch($query, $search){
    return $query->where('title', 'LIKE', '%'.$search.'%');
  }

  public function scopeByMerchants($query, $merchant_ids){
    return $query->whereIn('merchant_id', $merchant_ids);
  }


  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }



}
