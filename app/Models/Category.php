<?php
namespace App\Models;

use Kalnoy\Nestedset\Node as Node;
/**
* Category
*/
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Watson\Rememberable\Rememberable;

use App\Models\Zone;
use App\Models\Business;
use App\Models\Photo;
use App\Models\Service;
use App\Helpers\MazkaraHelper;

class Category extends Node implements SluggableInterface{
  use SluggableTrait;
  use Rememberable;

  protected $sluggable = array(
    'build_from' => 'name',
    'save_to'    => 'slug',
    'max_length' => 200
  );

  /**
   * Table name.
   *
   * @var string
   */

  protected $table = 'categories';
  public static $rules = array('name');
  public static $fields = array('name', 'description', 'parent_id', 'css');
  const LFT = 'lft';

  const RGT = 'rgt';

  const PARENT_ID = 'parent_id';

  protected $guarded = [ 'lft', 'rgt' ];

  public function updateBusinessesCount($locale = false){
    // update the business count for all locales
    if(!$locale){
      return;
    }
    $c = count($this->businesses()->byLocale($locale)->get());
    $this->zones()->updateExistingPivot($locale, ['business_count'=>$c]) ;

//    $this->business_count = count($this->businesses);
//    $this->save();
  }

  public function get_plural_name(){
    return MazkaraHelper::getPluralName($this->name);
  }

  public function scopeShowActive($query){
    return $query->where('state', '=', 'active');
  }

  public function zones(){
    return $this->belongsToMany('App\Models\Zone')->withPivot('business_count', 'displayable', 'state');
  }

  public function locale($locale = false){
    if($locale == false){
      $locale = mzk_get_localeID();
    }
    return $this->zones()->where('zone_id', '=', $locale)->first();
  }

  public function scopeByLocale($query, $locale = false){
    return $this->byLocaleParams($locale);
  }

  public function scopeByLocaleActive($query, $locale = false){
    return $this->byLocaleParams($locale, 'active');
  }

  public function scopeByLocaleParams($query, $locale = false, $state = false){

    $locale = $locale ? $locale : mzk_get_localeID();
    $locale = Zone::find($locale);

    $categories = $state ? $locale->categories()->where('category_zone.state', '=', $state)->lists('category_id','category_id')->all() : $locale->categories()->lists('category_id','category_id')->all();
    return $query->where(function($q) use($categories){
      $q->whereIn('id', $categories);
    });
  }

  public function scopeByLocaleActiveDisplayable($query, $locale = false){

    $locale = $locale ? $locale : mzk_get_localeID();
    $locale = Zone::find($locale);

    $categories = $locale->categories()->where('category_zone.state', '=', 'active')->where('category_zone.displayable', '=', 1)->lists('category_id','category_id')->all();
    return $query->where(function($q) use($categories){
      $q->whereIn('id', $categories);
    });
  }



  public function businesses() {
    return $this->belongsToMany('App\Models\Business'); 
  }

  public function photos(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', 'image');
  } 

  public function attachCities(){
    $cities = MazkaraHelper::getCitiesSlugList();
    foreach($cities as $ii=>$v){
      $cities[$ii] = ['state'=>'inactive'];
    }

    $this->zones()->sync($cities);
  }

  public function saveImages($images){
    if(is_null($images)){
      return;
    }
    if(!is_array($images))
    {
        $images = [$images];
    }

    foreach($images as $image)
    {
      $photo = new Photo();
      $photo->image = $image;
      $photo->type = Photo::IMAGE;
      $photo->save();
      $this->photos()->save($photo);
    }
  } 
  public function scopeSelector($query, $title = 'Select') {
      $selectVals[''] = $title;
      $selectVals += $this->lists('name', 'id')->all();
      return $selectVals;
  }

  public function scopeSearch($query, $search){
    return $query->where('name', 'like', $search.'%');
  }

  public function services(){
    return $this->belongsToMany('App\Models\Service');
  }

  public function getState(){
    return $this->locale()->pivot->state;
  }

public function isActive(){
  return $this->getState() == 'active' ? true : false;
  return $this->state == 'active' ? true : false;
}

  public function getDisplayable(){
    return $this->locale()->pivot->displayable;
  }

public function isDisplayable(){
  return $this->getDisplayable() == 1 ? true : false;
}

  //////////////////////////////////////////////////////////////////////////////

  //
  // Below come the default values for Baum's own Nested Set implementation
  // column names.
  //
  // You may uncomment and modify the following fields at your own will, provided
  // they match *exactly* those provided in the migration.
  //
  // If you don't plan on modifying any of these you can safely remove them.
  //

  // /**
  //  * Column name which stores reference to parent's node.
  //  *
  //  * @var string
  //  */
  // protected $parentColumn = 'parent_id';

  // /**
  //  * Column name for the left index.
  //  *
  //  * @var string
  //  */
  // protected $leftColumn = 'lft';

  // /**
  //  * Column name for the right index.
  //  *
  //  * @var string
  //  */
  // protected $rightColumn = 'rgt';

  // /**
  //  * Column name for the depth field.
  //  *
  //  * @var string
  //  */
  // protected $depthColumn = 'depth';

  // /**
  //  * Column to perform the default sorting
  //  *
  //  * @var string
  //  */
  // protected $orderColumn = null;

  // /**
  // * With Baum, all NestedSet-related fields are guarded from mass-assignment
  // * by default.
  // *
  // * @var array
  // */
  // protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

  //
  // This is to support "scoping" which may allow to have multiple nested
  // set trees in the same database table.
  //
  // You should provide here the column names which should restrict Nested
  // Set queries. f.ex: company_id, etc.
  //

  // /**
  //  * Columns which restrict what we consider our Nested Set list
  //  *
  //  * @var array
  //  */
  // protected $scoped = array();

  //////////////////////////////////////////////////////////////////////////////

  //
  // Baum makes available two model events to application developers:
  //
  // 1. `moving`: fired *before* the a node movement operation is performed.
  //
  // 2. `moved`: fired *after* a node movement operation has been performed.
  //
  // In the same way as Eloquent's model events, returning false from the
  // `moving` event handler will halt the operation.
  //
  // Below is a sample `boot` method just for convenience, as an example of how
  // one should hook into those events. This is the *recommended* way to hook
  // into model events, as stated in the documentation. Please refer to the
  // Laravel documentation for details.
  //
  // If you don't plan on using model events in your program you can safely
  // remove all the commented code below.
  //

  // /**
  //  * The "booting" method of the model.
  //  *
  //  * @return void
  //  */
  // protected static function boot() {
  //   // Do not forget this!
  //   parent::boot();

  //   static::moving(function($node) {
  //     // YOUR CODE HERE
  //   });

  //   static::moved(function($node) {
  //     // YOUR CODE HERE
  //   });
  // }

}
