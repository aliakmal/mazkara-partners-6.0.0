<?php
namespace App\Models;

use Eloquent;

class Mzk_meta extends Eloquent {
	protected $guarded = array();
    protected $table = 'meta';
	public static $rules = array();
  public function getBodyAttribute($value){
    return (array)json_decode($value);
  }

  public function setBodyAttribute($value){
    $this->attributes['body'] = json_encode($value);
  }
  public function scopeByLocale($query, $locale = false){
    $locale = $locale ? $locale : mzk_get_localeID();
    return $query->where('city_id', '=', $locale);
  }

  public function scopeByCallViews($query){
    return $query->where('type', '=', 'call-views');
  }



  public static function addCallEntry($data){
    $meta = self::firstOrCreate(['type'=>'call-views', 'city_id'=>mzk_get_localeID()]);

    if((count($meta->body)==0)  ){
      $meta->body = [$data];
    }else{

      $body = $meta->body;

      if(!is_string($body)):
        $cbody = ($body);

        // if the last ip is the same as this one and the time between the last two ips is less than a minute
        // dont add
        foreach($cbody as $vv){
          if(!is_string($vv)):

            if($vv->ip == $data['ip']){
              break;
            }
          endif;
        }


        if(!is_string($vv)):
          if($vv->ip == $data['ip']){
            $interval  = abs($data['time'] - $vv->time);
            if($interval < 60){
              return false;
            }
          }
        endif;
      endif;

      if(count($body)>=200){
        array_pop($body);
      }

      array_unshift($body, $data);
      $meta->body = $body;
    }

    $meta->save();
    return true;

  }
}
