<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

use App\Models\User;
use Auth, Image;

use Intervention\Image\ImageManager;


class Account extends Ardent {
	protected $guarded = array();
  protected $morphClass = 'Account';

	public static $rules = array(
		//'username' => 'required|unique:users',
		//'password' => 'required',
		//'email' => 'required',
		//'confirmed' => 'required'
	);

  public static $provider_rules = array(
    'provider' => 'required',
    'provider_id' => 'required',
  );

  const FACEBOOK = 'facebook';
  const GOOGLE = 'google';

  public function afterCreate(){
    switch($this->provider):
      case 'facebook':
        $this->setProfileImageFromFacebook();
      break;
      case Account::GOOGLE:
      break;
    endswitch;
  }

  public function setProfileImageFromGoogle(){

  }

  public function setProfileImageFromFacebook(){
    $p_url = 'https://graph.facebook.com/'.$this->provider_id.'/picture?type=large';
    $manager = new ImageManager();

    $img = $manager->make($p_url);
    $url = $p_url;
    $dir = storage_path().'/media/facebook_image_for_'.$this->id.md5(time());
    mkdir($dir);
    $filename = $dir.'/fb_image_'.$this->provider_id.'.jpeg';
    $img->save($filename);
    $this->user->saveImage($filename);
  }

  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function getMetaAttribute($value){
    return json_decode($value);
  }

  public function setMetaAttribute($value){
    $this->attributes['meta'] = json_encode($value);
  }

}
