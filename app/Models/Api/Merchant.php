<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;


class Merchant extends Eloquent
{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_merchant';
  }

  public static $rules = [];


  public $fields = array( 'description','name','ref','email','phone','global_id','old_id','city_id');

  public $fillables = array( 'description','name','ref','email','phone','global_id','old_id','city_id');
  
  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    $zone = \App\Models\Zone::find($locale);

    $city = \App\Models\Api\City::select()->where('slug', '=', $zone->slug)->first();

    return $query->where('city_id', '=', $city->id);
  }
  public function getDisplayableAttribute(){
    return $this->id.' - '.$this->name;
  }

}
