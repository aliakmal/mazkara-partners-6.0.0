<?php
namespace App\Models;
use App\Models\Zone;

use Eloquent;

class Service_zone_counter extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

  public function service(){
    return $this->belongsTo('App\Models\Service', 'service_id');
  }

  public function zone(){
    return $this->belongsTo('App\Models\Zone', 'zone_id');
  }

  public function scopeOfZones($query, $zones){
    return $query->whereIn('zone_id', $zones);
  }

  public function scopeOfServices($query, $services){
    return $query->whereIn('service_id', $services);
  }

}
