<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use DateTime, DateTimeZone;

class Timing extends Ardent {
	protected $guarded = array();

	public static $rules = array();
  public function business() {
    return $this->belongsTo('App\Models\Business', 'business_id'); 
  }


  public function beforeCreate(){
    $this->setTimeZone();
    $this->setUTCTimings();
  }

  public function beforeSave(){
    $this->setTimeZone();
    $this->setUTCTimings();
  }

  public function getTimezone(){
    if($this->timezone==''){
      return $this->business ? ($this->business->zone ? $this->business->zone->time_zone : ($this->business->city_id ? Zone::find($this->business->city_id)->time_zone : Zone::find(1)->time_zone)):Zone::find(1)->time_zone;
    }else{
      return $this->timezone;
    }
  }

  public function setTimeZone(){
    $this->timezone = $this->getTimezone();
  }

  public function setUTCTimings(){
    $timezone = $this->getTimezone();
    if(is_null($timezone)||($timezone=='')){
      return;
    }

    $open = new DateTime('2000-01-01 '.$this->open, new DateTimeZone($timezone));

    $open->setTimezone(new DateTimeZone('UTC'));
    $this->open_utc = $open->format('H:i');

    $close = new DateTime('2000-01-01 '.$this->close, new DateTimeZone($timezone));

    $close->setTimezone(new DateTimeZone('UTC'));
    $this->close_utc = $close->format('H:i');

  }


}
