<?php
namespace App\Models;

use Eloquent;
use App\Models\Service_item;
use App\Models\Business;

class Combo_item extends Eloquent {
	protected $guarded = array();

	public function items(){
		return $this->belongsToMany('App\Models\Service_item');
	}
	
  public function scopeByGroup($query, $group){
    return $query->where('grouping', '=', $group);
  }

  public function scopeByBusiness($query, $business_id){
    return $query->where('business_id', '=', $business_id);
  }


	public static $rules = array(
		'business_id' => 'required',
		'name' => 'required',
		//'description' => 'required',
		//'cost' => 'required',
		//'duration' => 'required',
		//'cost_type' => 'required',
		//'duration_type' => 'required',
		//'state' => 'required'
	);
}
