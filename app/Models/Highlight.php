<?php
namespace App\Models;

use Eloquent;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Watson\Rememberable\Rememberable;


use App\Models\Zone;
use App\Helpers\MazkaraHelper;

class Highlight extends Eloquent implements SluggableInterface{
  use SluggableTrait;
  use Rememberable;

  protected $morphClass = 'Highlight';

  protected $sluggable = array(
      'build_from' => 'name',
      'save_to'    => 'slug',
      'max_length' => 200
      
  );


  public function updateBusinessesCount($locale = false){
    if(!$locale){
      return;
    }
    $c = count($this->businesses()->byLocale($locale)->get());
    $this->zones()->updateExistingPivot($locale, ['business_count'=>$c]) ;

    //$this->business_count = count($this->businesses);
    //$this->save();
  }
  
  public function attachCities(){
    $cities = MazkaraHelper::getCitiesSlugList();
    foreach($cities as $ii=>$v){
      $cities[$ii] = ['state'=>'inactive'];
    }

    $this->zones()->sync($cities);
  }

public function scopeShowActive($query){
  return $query->where('state', '=', 'active');
}

  public function getState($locale = false){
    return $this->locale($locale)->pivot->state;
  }

public function isActive($locale = false){
  return $this->getState($locale) == 'active' ? true : false;
  return $this->state == 'active' ? true : false;
}

  public function zones(){
    return $this->belongsToMany('App\Models\Zone')->withPivot('business_count','state');
  }

  public function locale($locale = false){
    if($locale == false){
      $locale = mzk_get_localeID();
    }
    return $this->zones()->where('zone_id', '=', $locale)->first();
  }



  public function scopeByLocale($query, $locale = false){
    return $this->byLocaleParams($locale);
  }

  public function scopeByLocaleActive($query, $locale = false){
    return $this->byLocaleParams($locale, 'active');
  }

  public function scopeByLocaleParams($query, $locale = false, $state = false){

    $locale = $locale ? $locale : mzk_get_localeID();

    //$locale = Zone::find($locale);

    if($state){

      return $query->join('highlight_zone', function($join){
                $join->on('highlights.id', '=', 'highlight_zone.highlight_id');
              })->where('highlight_zone.state', '=', $state)->where('zone_id', '=', $locale);

      //$highlights = $locale->highlights()->where('highlight_zone.state', '=', $state)->lists('highlight_id','highlight_id')->all();
    }else{

      return $query->join('highlight_zone', function($join){
                $join->on('highlights.id', '=', 'highlight_zone.highlight_id');
              })->where('zone_id', '=', $locale);

      //$highlights = \DB::table('highlight_zone')->lists('highlight_id','highlight_id');
      //$highlights = $locale->highlights()->lists('highlight_id','highlight_id')->all();
    }


    return $query->where(function($q) use($highlights){
      $q->whereIn('id', $highlights);
    });
  }






	protected $guarded = array();
  public function businesses() {
    return $this->belongsToMany('App\Models\Business'); 
  }

	public static $rules = array(
		'name' => 'required',
	);
}
