<?php
namespace App\Models;

use Eloquent;
use App\Models\Business;
use DB;

class Counterbase extends Eloquent {
	protected $guarded = array();
  const PAGE_VIEWS = 'page-views';
  const CALL_VIEWS = 'call-views';
  const CTR = 'ctr';
  const IMPRESSIONS = 'impressions';
  protected $connection = 'mysql_counters';
	public static $rules = array();
  public function __construct(){
    parent::__construct();
    $this->table = env('DB_DATABASE_COUNTERS_TABLE');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale ? $locale : mzk_get_localeID();
    return $query->where('locale_id', '=', $locale);

//    $countable = 'Business';
//    return $query->join('businesses', function($join) use($locale){
//      $join->on('businesses.id', '=', 'counters.countable_id');
//      $join->where('businesses.city_id', '=', $locale);
//    })->where('countable_type', '=', $countable);
  }

  public function scopeToday($query){
    return $query->where(DB::raw('DATE('.$this->table.'.dated)'), date('Y-m-d'));
  }

  public function scopeByBusiness($query, $business_id){
    return $query->where('countable_type', '=', 'Business')
                  ->where('countable_id', '=', $business_id);
  }

  public function scopeByCountable($query, $countable_type, $countable_id){
    return $query->where('countable_type', '=', $countable_type)
                  ->where('countable_id', '=', $countable_id);
  }

  public function scopeByType($query, $type){
    return $query->where($this->table.'.type', '=', $type);
  }
  
  public function scopeBetweenDates($query, $start, $end){
    return $query->where('dated', '>=', $start)->where('dated', '<=', $end);
  }

}
