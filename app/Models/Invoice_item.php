<?php
namespace App\Models;

use Eloquent;

class Invoice_item extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'desc' => 'required',
		'price' => 'required',
		'qty' => 'required',
		'total' => 'required',
		'invoice_id' => 'required',
		'state' => 'required'
	);

  public function call_logs(){
    return $this->hasMany('App\Models\Call_log');
  }

  public function scopeOnlyItems($query){
    return $query->where('type', '=', 'item');
  }

  public function scopeNotItems($query){
    return $query->where('type', '<>', 'item');
  }

  public function scopeOnlyBounceCharges($query){
    return $query->where('type', '=', 'bounce-charges');
  }



}
