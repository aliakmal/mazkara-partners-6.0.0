<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Question;


class Answer extends Post implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;
  protected $morphClass = 'Answer';

  public static $rules = array(
    'body' => 'required',
  );

  public static $fields = array(
    'title', 'caption', 'body'
  );

  protected static $singleTableType = 'answer';
  protected static $persisted = [ 'title','slug', 'caption','body',
                                  'author_id','state','published_on'];

  public function questions(){
    return $this->belongsToMany('App\Models\Question');
  }


}
