<?php
namespace App\Models;

use Eloquent;

use App\Models\Campaign;
use App\Models\Business_zone;
use App\Models\Category;
use App\Models\Zone;



class Ad_set extends Eloquent {
	protected $guarded = array();
  protected $morphClass = 'Ad_set';


  public function campaign(){
    return $this->belongsTo('App\Models\Campaign', 'campaign_id');
  }

  public function category(){
    return $this->belongsTo('App\Models\Category', 'category_id');
  }

  public function business_zone(){
    return $this->belongsTo('App\Models\Business_zone', 'business_zone_id');
  }

  protected static $slots = [ 1 =>'Slot 1', 2 =>'Slot 2', 3 =>'Slot 3', 4 =>'Slot 4', 5 =>'Slot 5', 
                              6 =>'Slot 6', 7 =>'Slot 7', 8 =>'Slot 8', 9 =>'Slot 9', 10 =>'Slot 10',
                              11 =>'Slot 11', 12 =>'Slot 12', 13 =>'Slot 13', 14 =>'Slot 14', 15 =>'Slot 15'
                            ];


  public static function getAvailableSlots($business_zone_id, $category_id, $campaign, $exclude = 0){
    $slots = self::$slots;
    $booked_slots = Ad_set::join('campaigns', 'ad_sets.campaign_id','=', 'campaigns.id')->where('business_zone_id', '=',$business_zone_id)
                          ->where('category_id', '=',$category_id)
                          ->where('campaigns.starts', '<=', $campaign->ends)
                          ->where('campaigns.ends', '>=', $campaign->starts)
//                          ->whereRaw('((? <= campaigns.starts OR ? <= campaigns.ends) OR (? >= campaigns.starts OR ? <= campaigns.ends))', [$campaign->starts, $campaign->starts, $campaign->ends, $campaign->ends])
                        ->where('ad_sets.id', '<>',$exclude)
                        ->get()->toArray();
    foreach($booked_slots as $b){
      unset($slots[$b['slot']]);
    }
    return $slots;

  }


  public function ads(){
    return $this->hasMany('App\Models\Ad');
  }

  public function getLabelAttribute(){
    return $this->id.' '.$this->title;
  }

  public function hasAdZone(){
    return $this->adZone;
  }


  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }
  

	public static $rules = array(
		'campaign_id' => 'required',
		'business_zone_id' => 'required',
		'slot' => 'required',
		//'ad_id' => 'required',
		//'status' => 'required'
	);
}
