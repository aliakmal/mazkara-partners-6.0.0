<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Link extends Post implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;
  protected $morphClass = 'Link';

  public static $rules = array(
    'title' => 'required',
    'url' => '',
  );

  public static $fields = array(
    'title', 'caption', 'body', 'url', 'share_url_title', 'share_url_image'
  );
  protected static $singleTableType = 'url';
  protected static $persisted = [ 'title','slug','caption','body',
                                  'author_id','state','published_on'];

  public function services(){
    return $this->belongsToMany('App\Models\Service', 'post_service', 'post_id', 'service_id');
  }
  
  public function comments(){
    return $this->morphMany('App\Models\Comment', 'commentable');
  }

  public function likes(){
    return $this->morphMany('App\Models\Favorite', 'favorable')->where('type', '=', Favorite::FAVOURITE);
  }

  public function bookmarks(){
    return $this->morphMany('App\Models\Favorite', 'favorable')->where('type', '=', Favorite::BOOKMARK);
  }


}
