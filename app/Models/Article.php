<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Post;


class Article extends Post implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;
  protected $morphClass = 'Post';

  public static $rules = array(
    'title' => 'required',
    //'slug' => 'required',
    'cover'=>'required',
    'caption' => 'required',
    'body' => 'required',
    //'author_id' => 'required',
    //'state' => 'required',
    //'published_on' => 'required'
  );

  public static $fields = array(
    'title', 'caption', 'body'
  );

  //protected static $singleTableType = 'post';
  protected static $persisted = [ 'title','slug','caption','body',
                                  'author_id','state','published_on'];

}
