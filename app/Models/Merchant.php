<?php
namespace App\Models;
use App\Models\Zone;

use Eloquent;

class Merchant extends Eloquent {
	protected $guarded = array();
  protected $morphClass = 'Merchant';

	public static $rules = array(
		'name' => 'required',
		'email' => 'required',
		//'ref' => 'required'
	);

  public function users(){
    return $this->belongsToMany('App\Models\User')->withPivot('role')->where('role', '=', 'merchant'); 
  }

  public function pocs(){
    return $this->belongsToMany('App\Models\User')->withPivot('role')->where('role', '=', 'poc'); 
  }

  public function payments(){
    return $this->hasMany('App\Models\Payment');
  }

  public function generateGlobalID(){
    $str = 'FB'.str_pad($this->id, 10, "0", STR_PAD_LEFT);
    $this->global_id = $str;
    $this->save();
  }

  public function getDisplayableMerchantID(){
    return $this->global_id;
    
    $str = 'FB'.str_pad($this->id, 10, "0", STR_PAD_LEFT);
    return $str;
  }

  public function poc(){
    return $this->pocs()->get()->count()>0 ? $this->pocs()->get()->first():false;
  }

  public function pocId(){
    return $this->poc() ? $this->poc()->id:'';
  }


  public function businesses(){
    return $this->belongsToMany('App\Models\Business'); 
  }

  public function allocateUsers($user_ids){

    if(is_null($user_ids)){
      return false;
    }

    if(!is_array($user_ids)){
      $user_ids = [$user_ids];
    }
    $data = [];
    foreach($user_ids as $user_id){
      $data[$user_id] = ['role'=>'merchant'];
    }



    $this->users()->sync($data);

    foreach($this->users as $user){
      $role = Role::where('name', '=', 'client')->first();
      $user->attachRole($role);
      $user->save();
    }
  }


    public function allocateUsersNPocs($user_ids, $pocs){

    if(is_null($user_ids)){
      $user_ids = [];
      // deallocate current allocated users
      foreach($this->users as $user){
        if($user->hasRole('client')){
          $user->detachRole('client');
          $user->save();
        }
      }
    }

    if(!is_array($user_ids)){
      $user_ids = [$user_ids];
    }

    if(!is_array($pocs)){
      $pocs = [$pocs];
    }

    $data = [];
    foreach($user_ids as $user_id){
      $data[$user_id] = ['role'=>'merchant'];
    }
    foreach($pocs as $user_id){
      $data[$user_id] = ['role'=>'poc'];
    }

    $this->users()->sync($data);
    $role = Role::where('name', '=', 'client')->first();

    foreach($this->users as $user){
      $user->attachRole($role);
      $user->save();
    }
  }






  public function allocatePOCs($user_ids){

    if(is_null($user_ids)){
      return false;
    }

    if(!is_array($user_ids)){
      $user_ids = [$user_ids];
    }

    $data = [];
    foreach($user_ids as $user_id){
      $data[$user_id] = ['role'=>'poc'];
    }

    $this->pocs()->sync($data);
  }



  public static $fillables = array('name','description','email', 'phone', 'global_id'); 




  public function allocateBusinesses($business_ids){
    if(is_null($business_ids)){
      return false;
    }

    if(!is_array($business_ids)){
      $business_ids = [$business_ids];
    }

    $this->businesses()->sync($business_ids);
  }

  public function allocateBusinessesNAttributes($businesses){
    if(is_null($businesses)){
      return false;
    }

    $this->businesses()->sync($businesses);
  }

  public function canActivateDeactivateVirtualNumbers(){
    // you can only activate or deactivate virtual numbers when you have them
    return $this->getCountOfVenuesWithVirtualNumbers() > 0 ? true : false;
  }

  public function isAnyVirtualNumberActive(){
    $v = $this->getVenuesWithVirtualNumbers();
    return count($v)>0 ? true : false;
  }


  public function getCountOfVenuesWithVirtualNumbers(){
    $r = $this->getVenuesWithVirtualNumbers();
    return ($r!=false) ? count($r) : 0;
  }

  public function getVenuesWithVirtualNumbers(){
    $businesses = $this->businesses();//->lists('business_id', 'business_id');
    // get list of venues with virtual numbers that are active
    $with_virtual = [];
    foreach($businesses as $business){
      $r = $business->current_virtual_number_allocation();
      if($r!=false){
        $with_virtual[] = $r;
      }
    }

    return $with_virtual;
  }


  public function city_name(){
    return $this->city_id > 0 ? $this->city->name : '';
  }

  public function poc_name(){
    return $this->pocs()->count() > 0 ? $this->pocs()->first()->name : '';
  }


  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function scopeBySearch($query, $search){
    return $query->where('name', 'LIKE', '%'.$search.'%');
  }

  public function scopeByVirtualNumber($query, $num){
    $numbers = Virtual_number::query()->bySearch($num)->lists('id','id')->all();
    $allocations = Virtual_number_allocation::select()->byNumbers($numbers)->justActive()->lists('business_id','business_id')->all();
    // find businesses belonging to merchants
    $merchant_ids = \DB::table('business_merchant')->whereIn('business_id', $allocations)->lists('merchant_id','merchant_id');
    return $query->whereIn('id', $merchant_ids);
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }
  



}
