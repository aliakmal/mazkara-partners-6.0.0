<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

use App\Models\Invoice;

class Credit_note extends Ardent {
	protected $guarded = array();

	public static $rules = array();

  public function invoice(){
    return $this->belongsTo('App\Models\Invoice', 'invoice_id');
  }

  public function getType(){
    return $this->type;
  }

  public function afterCreate(){
    $this->invoice->amount_due = $this->invoice->amount_due - $this->amount;
    $this->invoice->setState();
    $this->assignNumber();
    $this->invoice->save();
    $this->setInitDates();
    $this->save();
  }

  public function scopeByInvoices($query, $invoices){
    return $query->whereIn('invoice_id', $invoices);
  }

  public function setInitDates(){
    if($this->isBadDebt()){
      $this->start_date = $this->invoice->start_date;
    }
    $this->end_date = $this->invoice->end_date;    
  }

  public function assignNumber(){
    $this->title = $this->generateNumber();
  }

  public function isBadDebt(){
    return ($this->type == 'bad-debt') || ($this->type == 'bad debt') ? true : false;
  }

  public function generateNumber(){
    $num = $this->type == $this->isBadDebt() ? ['BD'] : ['CN'];
    $num[] = mzk_get_localeID();
    $num[] = date('my');
    $num[] = str_pad($this->id, 10, "0", STR_PAD_LEFT);
    return join('/', $num);
  }

  public function getAmountInrAttribute(){
    if($this->currency == 'INR'){
      return $this->amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'INR');
    }

  }

  public function getAmountAedAttribute(){
    if($this->currency == 'AED'){
      return $this->amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'AED');
    }

  }

  public function getAmountUsdAttribute(){
    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'USD');
    }

  }

  public function monthlyAverage(){
    return round($this->amount/(ceil(mzk_date_intervals($this->start_date, $this->end_date)/30)), 2);
  }


  public function currentMonthContribution($m = false, $y = false){

    $avg = $this->monthlyAverage();
    $m = $m ? $m : date('n');
    $y = $y ? $y : date('Y');
    
    // is this starting in the mid of the current month
    //date('nY')
    if($m.$y == date('nY', strtotime($this->start_date)) && ($m.$y!=date('nY', strtotime($this->end_date))) && (date('d', strtotime($this->start_date))>1)){
      return $avg/2;
    }

    if($m.$y == date('nY', strtotime($this->end_date))  && ( $m.$y!=date('nY', strtotime($this->start_date))) && (  date('d', strtotime($this->end_date))!=(date('t', strtotime($this->end_date)))   )){
      return $avg/2;
    }

    // is this within the current month to begin with?
    // loop from 1st month to the last month
    $start_from = strtotime($this->start_date);
    $end_at = strtotime($this->end_date);


    $start_month = date('Y-m-d', strtotime($y.'-'.$m.'-15'));
    $end_month = date('Y-m-t', strtotime($y.'-'.$m.'-15'));


    // loop through intervals
    $overlap = mzk_dates_overlap($this->start_date, $this->end_date, $start_month, $end_month);
    if($overlap>0){
      return round($avg, 2);
    }



    return 0;

  }

  public function getCurrencyAttribute(){
    if($this->invoice){
      return $this->invoice->currency;
    }else{
      return 'AED';
    }
  }

  public function currentMonthContributionINR($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);
    if($this->currency == 'INR'){
      return $amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'INR');
    }

  }
  public function currentMonthContributionAED($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);

    if($this->currency == 'AED'){
      return $amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'AED');
    }

  }


  public function currentMonthContributionUSD($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'USD');
    }
  }

}
