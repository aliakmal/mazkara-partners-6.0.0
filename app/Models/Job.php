<?php
namespace App\Models;
use Eloquent;

class Job extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required',
		'body' => 'required',
		'location' => 'required',
		'state' => 'required'
	);
}
