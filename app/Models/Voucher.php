<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Voucher extends Ardent {
	protected $guarded = array();

	public static $rules = array(
		'offer_id' => 'required',
		'user_id' => 'required',
		//'code' => 'required',
		//'state' => 'required'
	);
  protected $morphClass = 'Voucher';

  const STATE_VALID   = 'VALID';
  const STATE_EXPIRED = 'EXPIRED';
  const STATE_CLAIMED = 'CLAIMED';

  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function offer(){
    return $this->belongsTo('App\Models\Offer', 'offer_id');
  }

  public function provider(){
    return $this->offer->business;
  }

  public function scopeByUser($query, $user_id){
    return $query->where('user_id', '=', $user_id);
  }

  public function scopeByOffer($query, $offer_id){
    return $query->where('offer_id', '=', $offer_id);
  }

  public function scopeByState($query, $state){
    return $query->where('state', '=', $state);//->Where('body', '<>', ' ');
  }

  public function claim(){
    $this->state = self::STATE_CLAIMED;
    $this->claimed_at = mzk_f_date(date('Y-m-d'));
    $this->save();
  }

  public function isClaimed(){
    return $this->state == self::STATE_CLAIMED ? true : false;
  }




  public function resetState(){
    // if the voucher has been used then it cannot be reset
    if(in_array($this->state, [self::STATE_CLAIMED])){
      return false;
    }

    $offer = $this->offer()->first();

    // if the offer is still valid
    if($offer->isValid()){
      $this->state = self::STATE_VALID;
    }else{
      $this->state = self::STATE_EXPIRED;
    }
  }

  public function afterCreate(){
    $this->setupCode();
    $this->resetState();
    $this->save();
    $this->offer->bookVoucherCount(1);
  }

  public function getBarCodePNG(){
    return 'data:image/png;base64,'.DNS1D::getBarcodePNG($this->code, "C39+",3,100,array(1,1,1));
  }

  public function setupCode(){
    // code format
    // [LOCALECODE][BUSINESSID][OFFERID zero padded to min 5 digits][VOUCHERID zero padded to min 5 digits]
    $code = [];

    $provider = $this->provider();
    $code[] = $provider->city_id;
    $code[] = str_pad($this->offer_id, 3, "0", STR_PAD_LEFT);
    $code[] = str_pad($this->id, 5, "0", STR_PAD_LEFT);
    $code[] = $this->user_id;

    $code = join(' ', $code);
    $this->code = $code;
  }
}
