<?php
namespace App\Models;

use Kalnoy\Nestedset\Node as Node;

/**
* Service
*/
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Watson\Rememberable\Rememberable;

use App\Models\Zone;
use App\Models\Business;
use App\Models\Offer;
use App\Models\Category;
use App\Models\Service_zone_counter;
use App\Models\Photo;
use App\Helpers\MazkaraHelper;

class Service extends Node implements SluggableInterface{
  use SluggableTrait;
  use Rememberable;

  
  protected $morphClass = 'Service';

  protected $sluggable = array(
      'build_from' => 'name',
      'save_to'    => 'slug',
      'max_length' => 200

  );

  /**
   * Table name.
   *
   * @var string
   */
  protected $table = 'services';
  public static $rules = array('name');
  public static $fields = array('name', 'description', 'parent_id');
  public static $editable_fields = array('name', 'description', 'parent_id', 'slug');
  const LFT = 'lft';

  const RGT = 'rgt';

  const PARENT_ID = 'parent_id';

  protected $guarded = [ 'lft', 'rgt' ];

  public function businesses() {
    return $this->belongsToMany('App\Models\Business'); 
  }
  
  public function getBySlug($slug){
    $service = $this->where('slug', '=', $slug)->first();
    if($service){
      return $service;
    }
    return false;
  }

  public function posts(){
    return $this->belongsToMany('App\Models\Post');
  }

  public function reviews() {
    return $this->belongsToMany('App\Models\Review'); 
  }

  public function thumbnail(){
    $photos = $this->photos();
    if($photos->count()>0){
      return $photos->first()->image->url('medium');
    }else{
      return 'http://lorempixel.com/400/275/nature/'.rand(1,6);
    }
  }

  public function kids(){
    return $this->hasMany('App\Models\Service', 'parent_id');
  }

  public function attachCities(){
    $cities = MazkaraHelper::getCitiesSlugList();
    foreach($cities as $ii=>$v){
      $cities[$ii] = ['state'=>'inactive'];
    }

    $this->zones()->sync($cities);
  }

  public function offers(){
    return $this->belongsToMany('App\Models\Offer');
  }

  public function zones(){
    return $this->belongsToMany('App\Models\Zone')->withPivot('business_count','state');
  }
  public function locale($locale = false){
    if($locale == false){
      $locale = mzk_get_localeID();
    }
    return $this->zones()->where('zone_id', '=', $locale)->first();
  }
  public function scopeByLocale($query, $locale = false){
    return $this->byLocaleParams($locale);
  }

  public function scopeByLocaleActive($query, $locale = false){
    return $this->byLocaleParams($locale, 'active');
  }

  public function scopeByLocaleParams($query, $locale = false, $state = false){

    $locale = $locale ? $locale : mzk_get_localeID();

    $locale = Zone::find($locale);

    if($state){
      $services = $locale->services()->where('service_zone.state', '=', $state)->lists('service_id','service_id')->all();
    }else{
      $services = $locale->services()->lists('service_id','service_id')->all();
    }

    return $query->where(function($q) use($services){
      $q->whereIn('id', $services);
    });
  }
  
  public function categories(){
    return $this->belongsToMany('App\Models\Category');
  }

  public function scopeShowOnly($query){
    return $query->where('parent_id', '<>', 'null');
  }
  public function scopeShowParents($query){
    return $query->where('parent_id', '=', null);
  }


  public function updateBusinessesCount($locale = false){
    if(!$locale){
      return;
    }
    $c = count($this->businesses()->byLocale($locale)->get());
    $this->zones()->updateExistingPivot($locale, ['business_count'=>$c]) ;

    //$this->business_count = count($this->businesses);
    //$this->save();
  }

  public function getState($locale = false){
    if($this->locale($locale) && $this->locale($locale)->pivot && $this->locale($locale)->pivot->state)
      return $this->locale($locale)->pivot->state;
    else
      return '';
  }

  public static function getBusinessTotalCount($locale = false){
    if($locale == false){
      $locale = mzk_get_localeID();
    }

    $results =  \DB::table('service_zone')
                 ->select(\DB::raw('`service_zone`.`service_id`, `service_zone`.`business_count` as `business_count`'))
                 ->where('zone_id', '=', $locale)
                 ->lists('business_count','service_id');
    return $results;
  }


  public function getBusinessCount($locale = false){
    if($locale == false){
      $locale = mzk_get_localeID();
    }

    $sql = sprintf("select `service_zone`.`business_count` as `business_count`
                 from `service_zone` where `service_zone`.`service_id` = '%s' 
                 and `service_zone`.`zone_id` = '%s'", $this->id, $locale);

    $results = \DB::select($sql);

    if(count($results)==0){
      return ;
    }else{
      $result = array_pop($results);
      return $result->business_count;
    }
    
    if($this->locale($locale) && $this->locale($locale)->pivot && $this->locale($locale)->pivot->business_count)
      return $this->locale($locale)->pivot->business_count;
    else
      return 0;
  }

  public function getBusinessCountByZone($zone = false){
    if($zone):
      $c = $this->zone_counters()->ofZones(is_array($zone)?$zone:[$zone])->sum('business_count');
      return $c;

    endif;
    return 0;
  }

  public function zone_counters(){
    return $this->hasMany('App\Models\Service_zone_counter');
  }

public function isActive($locale = false){
  return $this->getState($locale) == 'active' ? true : false;
  return $this->state == 'active' ? true : false;
}

  public function photos(){
    return $this->morphMany('Photo', 'imageable')->where('type', 'image');
  } 

  public function saveImages($images){
    if(!is_array($images))
    {
        $images = [$images];
    }

    foreach($images as $image)
    {
      if(is_null($image)){
        continue;
      }
      
      $photo = new Photo();
      $photo->image = $image;
      $photo->type = Photo::IMAGE;
      $photo->save();
      $this->photos()->save($photo);
    }
  } 
  public function scopeSearch($query, $search){
    return $query->where('name', 'like', '%'.$search.'%');
  }


  public function removeAllImages($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $p = Photo::findOrFail($id);
      $p->deleteBasic($id);
    }
  }

  public function scopeSelector($query, $title = 'Select') {
      $selectVals[''] = $title;
      $selectVals += $this->lists('name', 'id')->all();
      return $selectVals;
  }



protected $stringPath;

public function scopeShowActive($query){
  return $query->where('state', '=', 'active');
}


public function hasStartingPrice(){
  $price = $this->pivot->starting_price;
  if(is_null($price)){
    return false;
  }

  if($price==0){
    return false;
  }

  return true;
}

public function isParent(){
  if($this->parent_id > 0){
    return false;
  }else{
    return true;
  }
}


public function getStartingPrice($locale=false){
  $price = $this->pivot->starting_price;
  if(is_null($price)){
    return ' ';
  }

  if($price==0){
    return 'On consultation';
  }

  return mzk_currency_symbol($locale).' '.$price.'+';

}

public function getDisplayablePrice($locale=false){
  $price = $this->pivot->starting_price;
  if(is_null($price)){
    return '';
  }

  if($price==0){
    return '';
  }

  return mzk_currency_symbol($locale).' '.$price.'+';
}


public function stringPath()
{
   if ($this->stringPath) return $this->stringPath;

   $parent = $this->parent;
   return $this->stringPath = $parent ? $parent->stringPath().' > '.$this->name : $this->name;
}

protected $aPath;

public function aPath()
{
   if ($this->aPath) return $this->stringPath;

   $parent = $this->parent;
  if($parent){
    $parent->aPath()[$this->name] = [$this->id, $this->name];
  }else{
    $parent->aPath()[$this->name] = [];

  }
   return $this->aPath;// = $parent ? $parent->stringPath().' > '.$this->name : $this->name;
}

  //////////////////////////////////////////////////////////////////////////////

  //
  // Below come the default values for Baum's own Nested Set implementation
  // column names.
  //
  // You may uncomment and modify the following fields at your own will, provided
  // they match *exactly* those provided in the migration.
  //
  // If you don't plan on modifying any of these you can safely remove them.
  //

  // /**
  //  * Column name which stores reference to parent's node.
  //  *
  //  * @var string
  //  */
  // protected $parentColumn = 'parent_id';

  // /**
  //  * Column name for the left index.
  //  *
  //  * @var string
  //  */
  // protected $leftColumn = 'lft';

  // /**
  //  * Column name for the right index.
  //  *
  //  * @var string
  //  */
  // protected $rightColumn = 'rgt';

  // /**
  //  * Column name for the depth field.
  //  *
  //  * @var string
  //  */
  // protected $depthColumn = 'depth';

  // /**
  //  * Column to perform the default sorting
  //  *
  //  * @var string
  //  */
  // protected $orderColumn = null;

  // /**
  // * With Baum, all NestedSet-related fields are guarded from mass-assignment
  // * by default.
  // *
  // * @var array
  // */
  // protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

  //
  // This is to support "scoping" which may allow to have multiple nested
  // set trees in the same database table.
  //
  // You should provide here the column names which should restrict Nested
  // Set queries. f.ex: company_id, etc.
  //

  // /**
  //  * Columns which restrict what we consider our Nested Set list
  //  *
  //  * @var array
  //  */
  // protected $scoped = array();

  //////////////////////////////////////////////////////////////////////////////

  //
  // Baum makes available two model events to application developers:
  //
  // 1. `moving`: fired *before* the a node movement operation is performed.
  //
  // 2. `moved`: fired *after* a node movement operation has been performed.
  //
  // In the same way as Eloquent's model events, returning false from the
  // `moving` event handler will halt the operation.
  //
  // Below is a sample `boot` method just for convenience, as an example of how
  // one should hook into those events. This is the *recommended* way to hook
  // into model events, as stated in the documentation. Please refer to the
  // Laravel documentation for details.
  //
  // If you don't plan on using model events in your program you can safely
  // remove all the commented code below.
  //

  // /**
  //  * The "booting" method of the model.
  //  *
  //  * @return void
  //  */
  // protected static function boot() {
  //   // Do not forget this!
  //   parent::boot();

  //   static::moving(function($node) {
  //     // YOUR CODE HERE
  //   });

  //   static::moved(function($node) {
  //     // YOUR CODE HERE
  //   });
  // }

}
