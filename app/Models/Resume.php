<?php
namespace App\Models;

use Eloquent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use Auth, File;
use LaravelArdent\Ardent\Ardent;

use Cviebrock\EloquentTaggable\Taggable;


class Resume extends Ardent implements StaplerableInterface{
	protected $guarded = array();
  protected $morphClass = 'Resume';
  use EloquentTrait;
  use Taggable;



	public static $rules = array(
		'name' => 'required', 
    'location' => 'required', 
    'nationality' => 'required', 
    'phone' => 'required', 
    'experience' => 'required', 
    'salary' => 'required', 
	);

  public static $fields = array('name', 'location',
                                'nationality','uploaded_by', 
                                'city_id', 'phone', 'experience', 
                                'salary');

  public function __construct(array $attributes = array()) {
    $this->hasAttachedFile('doc', [
      'storage' => 's3',
      's3_client_config' => [
        'key' => 'AKIAI37S25ETHXHU7YMQ',
        'secret' => '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',

        // 'key' => 'AKIAJB6C26YXI6SME7EQ',
        // 'secret' => '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',
        'region' => 'us-east-1',
            'version'=> 'latest',
        
        'credentials'=>[
          'key' => 'AKIAI37S25ETHXHU7YMQ',
          'secret' => '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',

          // 'key' => 'AKIAJB6C26YXI6SME7EQ',
          // 'secret' => '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',
        ]

      ],
      's3_object_config' => [
        'Bucket' => 'mazkaracdn'
      ],
      'default_url' => '/defaults/:style/missing.png',
      'keep_old_files' => true        
    ]);

    // IMPORTANT:  the call to the parent constructor method
    // should always come after we define our attachments.
    parent::__construct($attributes);
  }

    function deleteBasic(){
      $sql ='delete from resumes where id = '.$this->id;

      $d = \DB::delete($sql);
    }



  function getMediaMatter(){
    $url = $this->media->url();
    return strstr($url, 'missing.png')?'none':$url;
  }

  function getSpecializations(){
    $specializations = array('Spa Manager','Masseuse','Masseur','Spa Therapists',
      'Spa Receptionist','Spa Advisor','Salon Manager','Salon Receptionist',
      'Hair Stylist','Esthetician','Makeup Artist','Nail Technician',
      'Beautician','Beauty Therapist','Beauty Advisor');
    asort($specializations);
    $data = array();
    foreach($specializations as $vv){
      $data[str_slug($vv)] = $vv;
    }

    return $data;
  }

  function hasCVAttached(){
    return $this->doc_file_name == '' ? false : true;
  }


  function getExperiences(){
    $experiences = array(
      'No Experience',
      '6 months or less',
      '6 to 12 months',
      '1 to 2 years',
      '2 to 5 years',
      '5 years and above');
    $data = array();
    foreach($experiences as $vv){
      $data[str_slug($vv)] = $vv;
    }

    return $data;
  }



  public function specializations(){
    return $this->hasMany('\App\Models\Specialization');
  }

  public function businesses(){
    return $this->belongsToMany('\App\Models\Business')->withPivot('allocated_at');;
  }

  public function scopeBetweenDates($query, $start, $end){
    return $query->where('created_at', '>', $start)->where('created_at', '<=', $end);
  }

  public function scopeByBusiness($query, $bid){
    return $query->whereHas('businesses', function ($query) use ($bid) {
                        $query->where('businesses.id', '=', $bid);
                });  
  }

  public function scopeBySpecializations($query, $specializations){
    return $query->whereHas('specializations', function ($query) use ($specializations) {
              $query->whereIn('specializations.specialization', $specializations);
            });  
  }

  public function city_name(){
    return $this->city_id > 0 ? $this->city->name : '';
  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }

}
