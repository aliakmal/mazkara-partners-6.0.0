<?php
namespace App\Models;
use Eloquent;

class Menu_group extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'business_id' => 'required',
	);

  public function business(){
    return $this->belongsTo('App\Models\Business', 'business_id');
  }

  public function scopeByBusiness($query, $business_id){
    return $query->where('business_id', '=', $business_id);
  }

  public function service_items(){
    return $this->hasMany('App\Models\Service_item');
  }

  public static function optionables($params = false){
    $params = $params == false? [] : $params;
    $q = self::query();
    if(isset($params['business_id'])){
      $q->byBusiness($params['business_id']);
    }
    $d = $q->get()->lists('name', 'id')->all();
    return (['0'=>'No Header'] + $d);
  }
}
