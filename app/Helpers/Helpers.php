<?php 
use Vinelab\Http\Client as HttpClient;
use App\Models\Zone;
use App\Models\Service;
use App\Models\Category;
use App\Models\Business;
use App\Models\Campaign;
use App\Models\Ad;
use App\Models\Setting;
use App\Models\Photo;
use App\Models\Call_log;
use App\Models\Offer;
//use Session, Cache, Request, Response, Input;

global $__MAZKARA_TIMER__;
global $__MAZKARA_CONSOLE__;


if ( ! function_exists('mzkIsApiDomain'))
{
  function mzkIsApiDomain()
  {
    return strstr(Request::root(), 'api.mazkara.local') || strstr(Request::root(), 'sandbox.mazkara.com') || strstr(Request::root(), 'api.mazkara.com');
  }
}

function mzk_categories_as_associative_from_json($index = 'id', $value = 'slug'){
  $dir = storage_path('/json/');
  $filename = $dir.'/categories.json';
  $contents = File::get($filename);
  $categories = json_decode($contents);
  return mzk_associative_from_object_array($categories, $index, $value);
}

function mzk_cities_as_associative_from_json($index = 'id', $value = 'slug'){
  $dir = storage_path('/json/');
  $filename = $dir.'/cities.json';
  $contents = File::get($filename);
  $cities = json_decode($contents);
  return mzk_associative_from_object_array($cities, $index, $value);
}

function mzk_associative_from_object_array($a, $index, $value){
  $result = [];
  foreach($a as $one_item){
    $result[$one_item->$index] = $one_item->$value;
  }

  return $result;
}


function mzk_seo_name(){
  return 'Fabogo';
}


function mzk_format_rating($rating){
  return number_format($rating, 1, '.', '');
}


function mzk_post_profile_wizrocket($id, $profile){
  return;
  $data = [];
  $data["type"] = "profile";
  $data["profileData"] = $profile;
  $data["ts"] = time();
  $data["Identity"] = $id;
  mzk_post_wizrocket($data);
}


function mzk_assets($file){
  $str = 'https://s3.amazonaws.com/mazkaracdn/';
  return $str.$file;
}

function mzk_is_mobile_phone($phone){
  $phone = ltrim($phone, '+');
  $phone = ltrim($phone, '0');
  $mobile_nos_in = ['917','918','919'];
  $mobile_nos_ae = ['9715'];

  if(in_array(substr($phone, 0, 3), $mobile_nos_in)){
    return true;
  }

  if(in_array(substr($phone, 0, 4), $mobile_nos_ae)){
    return true;
  }

  return false;
}


function strstr_array($haystack, $needles=array(), $offset=0) {
  $chr = array();
  foreach($needles as $needle) {
    $res = strstr($haystack, $needle, $offset);
    if ($res !== false) {
      $chr[$needle] = $res;
    }
  }

  if(empty($chr)){
    return false;
  }

  return min($chr);
}

function mzk_js_tag($f, $min = false){
  //$min = false;
  $url = $min ? /*mzk_cloudfront_image*/('https://s3.amazonaws.com/mazkaracdn/js/'.$f.'.min.js?v=9.03') : '/js/'.$f.'.js?v='.time();

  $tag = '<script src="'.$url.'"></script>';

  return $tag;
}

function mzk_is_counters_divided(){
  return true;
}

function mzk_stylesheet_link_tag($f, $min = false){
  //$min = false;
  $url = $min ? ('https://s3.amazonaws.com/mazkaracdn/css/'.$f.'.min.css?v=12.01') : '/css/'.$f.'.css?v=1.01';//.time();
  $tag = '<link type="text/css" rel="stylesheet" href="'.$url.'">';

  return $tag;
}

function mzk_post_wizrocket($data){
  return;
  $request = [
                'url' => 'https://api.wzrkt.com/up',
                'params' => [ 'id' =>  '844-6KZ-R44Z', 
                              'payload' => ['p'=>'SAA-SWD-MAAL', 
                                          'd'=>$data]]
              ];
  $response = HttpClient::post($request);
}

function mzk_array_to_object($array) {
    return (object) $array;
}

function mzk_object_to_array($array) {
  $array = json_decode(json_encode($array), true);
}

function mzk_secure_iterable($var)
{
  return mzk_is_iterable($var) ? $var : array();
}

function _apfabtrack($var){
  //$params = mzk_get_fabtrack_parameters($var);
  // return '';
  return 'fabtrack='._paramsfabtrack($var);
}

function _paramsfabtrack($var, $concat='+'){

  $params = mzk_get_fabtrack_parameters($var);
  return join($concat, $params);
}


function mzk_get_fabtrack_parameters($var){
  $params = is_array($var)?$var:[$var];

  // $params[] = mzk_get_route_hash();
  return $params;
}

function mzk_get_route_hash(){
  $route = \Route::current();
  if(!is_object($route)){
    return 'site';
  }
  $current = \Route::current()->getName();
  $route = 'site';
  if(strstr($current, 'businesses.list')){
    $route = 'list';
  }

  if(strstr($current, 'angular')){
    $route = 'list';
  }

  if(strstr($current, 'businesses.view.by.slug')){
    $route = 'venue';
  }

  switch($current){
    case 'site.home':
      $route = 'home';
    break;
  }

  return $route;
}

function mzk_is_iterable($var){
  return $var !== null && (is_array($var) 
          || $var instanceof Traversable 
          || $var instanceof Iterator 
          || $var instanceof IteratorAggregate
          );
}

function mzk_percentage($o, $t){
  if(($o==0)||($t==0)){
    return 0;
  }

  return floor(($o/$t)*100).'%';
}

function mzk_timer_init(){
  global $__MAZKARA_TIMER__, $__MAZKARA_CONSOLE__;

  $__MAZKARA_TIMER__ = is_null($__MAZKARA_TIMER__)?[]:$__MAZKARA_TIMER__;
  $__MAZKARA_CONSOLE__ = is_null($__MAZKARA_CONSOLE__)?[]:$__MAZKARA_CONSOLE__;
}

function mzk_timer_start($index=''){
  global $__MAZKARA_TIMER__;
  mzk_timer_init();
  $__MAZKARA_TIMER__[$index] = microtime(true);
}

function mzk_timer_stop($index = ''){
  global $__MAZKARA_TIMER__, $__MAZKARA_CONSOLE__;
  mzk_timer_init();
  $__MAZKARA_CONSOLE__[]= 'console.log("'.($index.' :: '.round((microtime(true)-$__MAZKARA_TIMER__[$index])*1000,3)." ms").' - at - '.microtime().'");';
}

function mzk_console($lbl = ''){
  //   return '';
  global $__MAZKARA_CONSOLE__;
  mzk_timer_init();
  $__MAZKARA_CONSOLE__[]= 'console.log("'.($lbl).'");';
}

function mzk_dump_console(){
  // return '';
  global $__MAZKARA_TIMER__, $__MAZKARA_CONSOLE__;
  mzk_timer_init();
  if (!App::environment('production')){
    echo '<script language="javascript">'.join("\n", $__MAZKARA_CONSOLE__).'</script>';
  }
}

function mzk_route_helper($resource, $prefix = false, $skip = false){
  $a = ['index','create','store','show','edit','update','destroy'];
  if(is_array($skip)){
    foreach($skip as $vv){
      $a = array_diff($a, [$vv]);
    }
  }
  $r = [];
  foreach($a as $v){
    $r[$v] = ($prefix ? $prefix.'.' : '').$resource.'.'.$v;
  }

  return $r;
}

function mzk_slug_to_words($str){
  return ucwords(str_replace(['-', '_'], ' ', $str));
}


function mzk_in_admin(){
  $l = Request::segment(1);
  return in_array($l, ['admin', 'content', 'data', 'crm', 'sms', 'restricted', 'finance']) ? true : false;
}

function mzk_clear_all_cache(){
  Cache::flush();
}

function mzk_reset_all_highlights_cache(){
  MazkaraHelper::setHighlightsList(true);
}

function mzk_reset_all_service_cache(){
  MazkaraHelper::setServicesHierarchyList(true);
  MazkaraHelper::setServicesList(true);
  MazkaraHelper::setServicesSlugList(true);
}

function mzk_reset_all_category_cache(){
  MazkaraHelper::setAllCategoriesList(true);
  MazkaraHelper::setCategoriesList(true);
}

function mzk_reset_all_zone_cache(){
  MazkaraHelper::setCitiesList(true);
  MazkaraHelper::setCitiesSlugList(true);
  ViewHelper::setZonesComboAsArray(true);
}

function mzk_get_localeID(){
  if(mzk_in_admin()){
    return MazkaraHelper::getAdminDefaultLocaleID();
  }else{
    return MazkaraHelper::getLocaleID();
  }
}

function mzk_round_to_hundred($theNumber) {
    if (strlen($theNumber)<3) {
    $theNumber=$theNumber;
    } else {
    $theNumber=substr($theNumber, 0, strlen($theNumber)-2) . "00";

    }
    return $theNumber;

}

function mzk_url_city_change($url, $city){
  $replace = MazkaraHelper::getLocale();
  $replacers = [  
                  Request::root() =>  Request::root()."/".$city,
                  Request::root()."/" =>  Request::root()."/".$city,
                  Request::root()."/".$replace  =>  Request::root()."/".$city,
                  Request::root()."/".$replace."/"  =>  Request::root()."/".$city,
                ];  
  if(isset($replacers[$url])){
    $url = $replacers[$url];
  }else{
    $url = 'javascript:void(0)';
  }

  return $url;  
}

 function mzkGetInitials($primary, $secondary = 'USER'){
  $initial = substr(trim($primary), 0, 2);

  $initial = trim($initial) == '' ? substr(trim($secondary), 0, 2) : $initial;
  return $initial;
}




function mzk_get_children_for($ary, $id)
{
  $results = array();

  foreach ($ary as $el){
    if ($el['id'] == $id){
      $results[] = $el;
    }

    if (count($el['children']) > 0 && ($children = mzk_get_children_for($el['children'], $id)) !== FALSE){
      $results = array_merge($results, $children);
    }
  }

  return count($results) > 0 ? $results : FALSE;
}


function mzk_get_name_from_email($email){
  $e = explode('@', $email);
  return mzk_slug_to_words($e[0]);
}

function mzk_cache_name($key){
  $map = [
            'zones.cities.slug.list'  =>  'zones.cities.slug.list.mazkara',
            'zones.subzones.slug.list'=>  'zones.subzones.slug.list.mazkara'
          ];

  return isset($map[$key])?$map[$key]:$key;
}

function mzk_get_class($o){
  $cls = get_class($o);
  return str_replace('App\Models\\', '', $cls);
}


function mzk_get_countries_list(){
  return config('countries');
}

function mzk_get_country_from_list($code){
  $countries = mzk_get_countries_list();
  return isset($countries[$code])?$countries[$code]:$code;
}

function mzk_get_code_from_list($country){
  $countries = mzk_get_countries_list();
  return array_search($country, $countries);
}



function mzk_cache_locale_name($prepend){
  $slug = MazkaraHelper::getLocale();
  return $prepend.'.for.city.'.$slug;
}

function mzk_is_session_valid($name){
  return Session::has($name) && !is_null(Session::get($name));
}

function mzk_is_cache_valid($name){
  return Cache::has($name) && !is_null(Cache::get($name));
}



function mzk_call_logs_state_helper($state){

  $html = '<span ';
  $class = '';
  switch($state){
    case Call_log::STATE_CLEAR:
      $class="label label-info";
    break;
    case Call_log::STATE_UNBILLED:
      $class="label label-warning";
    break;
    case Call_log::STATE_DISPUTED:
      $class="label label-danger";
    break;
    case Call_log::STATE_BILLED:
      $class="label label-success";
    break;
    case Call_log::STATE_RESOLVED:
      $class="label-primary";
    break;

  }
  $html.=' class="'.$class.' label">';
  $html.= strtoupper($state);
  $html.='</span>';

  return $html;

}

function mzk_offer_tag($offer){

  if(is_null($offer)){
    return '';
  }




  $html = '<span ';
  $html.=' class="'.Offer::tagsValue($offer->tag, 'css').' dpb text-left label">';
  $html.= strtoupper(str_replace('offer', 'special', Offer::tagsValue($offer->tag, 'label')));
  $html.='</span>';

  return $html;
}

function mzk_offer_title($offer){
  if(isset($offer->body)){
    return $offer->body;
  }
  if(isset($offer->title)){
    return $offer->title;
  }

}

function mzk_show_pending_post_states($state){
  $css = '';
  switch($state){
    case 'draft':
      $css = 'label-default';
    break;
    case 'publish-for-review':
      $css = 'label-warning';
    break;
    case 'reject':
      $css = 'label-danger';
    break;
  }

  $html = '<span ';
  $html.=' class="label '.$css.' dpb text-left label">';
  $html.= strtoupper($state);
  $html.='</span>';

  return $html;
}

function mzk_status_tag($state, $css = 'dpb  text-left', $tag = 'span', $label = ' label'){
  $css = $css;
  $state = strtolower($state);
  switch($state){
    case 'active':
    case 'valid':
    case 'published':
      $css .= $label.'-success';
    break;
    case 'inactive':
    case 'expired':
    case 'draft':
      $css .= $label.'-default';
    break;
    case 'claimed':
      $css .= $label.'-pink';
    break;
    case 'invalid':
    case 'pending':
      $css .= $label.'-warning';
    break;
    case 'rejected':
      $css .= $label.'-danger';
    break;
  }

  $html = '<'.$tag.' ';
  $html.=' class=" '.$label.' '.$css.'  ">';
  $html.= strtoupper($state);
  $html.='</'.$tag.'>';

  return $html;
}

function mzk_price($price, $breaker = ' ', $css = 'dpb '){

  if(is_null($price)||($price<=0)){
    return '';
  }

  $html = mzk_currency_symbol().$breaker;
  $html.=(integer)$price;

  return $html;
}


function mzk_offer_preprice($offer){

  if(is_null($offer)){
    return '';
  }
  if(!isset($offer->price_meta)){
    return '';
  }

  $html = '<span ';
  $html.=' class="fw500  dpb">';
  $html.=str_replace(' ', '&nbsp;', $offer->price_meta);
  $html.='</span>';

  return $html;
}

function mzk_offer_price($offer, $breaker = ' ', $css = 'dpb '){

  if(is_null($offer)){
    return '';
  }

  if($offer->discount_type == 'non-price-based-deal'){
    return str_replace(' ', '&nbsp;', $offer->legacy_price_meta);
  }

  $price = 0;

  if(isset($offer->offer_price)){
    $price = $offer->offer_price;
  }

  $html = '<span ';
  $html.=' class="'.$css.'">';
  if(isset($offer->cost_range)  && ($offer->cost_range == 'starting-from')){
    $html.='Starting&nbsp;from&nbsp;';
  }
  $html.=mzk_currency_symbol().$breaker;
  $html.=(integer)$price;
  $html.='</span>';

  return $html;
}



function mzk_api_response($data, $status_code = 200, $status = true, $message){
  $result = array(
              'data' => $data,
              'status_code' => $status_code,
              'status' => $status,
              'message' => $message
    );

  return $result;

}

function mzk_api_success_response($data = false, $message = 'Success!'){
  $data = $data ? $data : [];
  return mzk_api_response($data, 200,  true, $message);

}

function mzk_api_fail_response($data = false, $message = 'Uh Oh - Something went wrong!'){
  $data = $data ? $data : [];
  return mzk_api_response($data, 200,  true, $message);

}




function mzk_str_trim($str, $num = 30, $append = '..'){
  return strlen($str)>$num ? substr($str, 0, $num ).$append:$str;
}

function mzk_show_ads_for_single($business){
  // get all running campaigns where today is within the date
  // where the category is the same as for the business
  // and the zone is the same as the business zone
}

function mzk_currency_locale($locale = false){
  $locale = $locale ? $locale : strtolower(MazkaraHelper::getLocale());
  $sym = 'AED';
  switch($locale){
    case 'dubai':
      $sym = 'AED';
    break;  
    case 'pune':
    case 'bangalore':
    case 'delhi':
    case 'mumbai':
      $sym = 'INR';
    break;
  }

  return $sym;
}


function mzk_currency_name_from_sym($sym){
  $r = '';
  switch($sym):
    case 'INR':
      $r = 'Indian Rupees';
    break;
    case 'AED':
      $r = 'Dirhams';
    break;
  endswitch;

  return $r;
}


function mzk_is_pune(){
  $locale = strtolower(MazkaraHelper::getLocale());
  return ($locale=='pune') ? true : false;
}


 function mzk_get_starting_price($price, $locale=false){

  if(is_null($price)){
    return ' ';
  }

  if($price==0){
    return 'On consultation';
  }

  return mzk_currency_symbol($locale).' '.$price.'+';

}


function mzk_label($lbl, $locale=false){
  $str = $lbl;
  $locale = $locale ? $locale : strtolower(MazkaraHelper::getLocale());
  switch($lbl){
    case 'specials':
    case 'offers':
      if($locale=='pune'){
        $str = 'offers';
      }
      if($locale=='mumbai'){
        $str = 'offers';
      }

      if($locale=='dubai'){
        $str = 'specials';
      }

    break;
  }

  return $str;
}

function mzk_quick_day($s){
  $days = ['Sun'=>'Sunday', 'Mon'=>'Monday', 'Tue'=>'Tuesday', 
    'Wed'=>'Wednesday','Thu'=>'Thursday', 
    'Fri'=>'Friday', 
    "Sat"=>'Saturday'
    ];

    return $days[$s];
}

function mzk_is_dubai(){
    $l = Request::segment(1);
    if($l == 'dubai'){
      return true;
    }else{
      return false;
    }
}

function mzk_currency_symbol($locale = false){
  $locale = $locale ? $locale : strtolower(MazkaraHelper::getLocale());
  $sym = 'AED';
  switch($locale){
    case 'dubai':
    case '1':
    case 1:
      $sym = 'AED';
    break;  
    case 'pune':
    case 'delhi':
    case 'bangalore':
    case 'mumbai':
    case '88':
    case 88:
      $sym = 'INR';
    break;
  }

  return $sym;
}

function mzk_time_convert_from_ist_to_utc($date_time){
  $dt = Carbon\Carbon::parse($date_time);
  $dt->subHours(5);
  $dt->subMinutes(30);
  return $dt;
}

function mzk_time_convert_from_ist_to_uae($date_time){
  $dt = mzk_time_convert_from_ist_to_utc($date_time);
  $dt->addHours(4);
  return $dt;
}

function mzk_current_datetime(){
  return Carbon\Carbon::now();
}

function mzk_current_date_time(){
  return Carbon\Carbon::now();
}

function mzk_client_set_default_business($business_id = false){
  $session = 'mazkara.clients.business';
  if(!Auth::user()->can('access_client_dashboards')):
    $users_businesses = Auth::user()->businesses();
    $ids = [];
    foreach($users_businesses as $business){
      $ids[] = $business->id;
    }


    if((!$business_id)){
      $business_id = current($ids);
    }
    if(!in_array($business_id, $ids)){
      $business_id = current($ids);
    }
  endif;


  Session::put($session, $business_id);
}

function mzk_client_get_default_business(){
  $session = 'mazkara.clients.business';

  if(!Session::has($session)){
    mzk_client_set_default_business();
  }
  return  Session::get($session);

}


function mzk_client_bid(){
  return Route::current()->getParameter('bid');
}

function mzk_client_get_default_business_name(){
  $business = Business::find(mzk_client_bid());//mzk_client_get_default_business());
  return $business->name.','.$business->zone_cache;
}
function mzk_client_get_default_business_object(){
  return Business::find(mzk_client_bid());//mzk_client_get_default_business());
}

function mzk_get_weeks_between_range_array($start, $end){
  $dates = range(strtotime($start), strtotime($end),604800);
  $weeks = [];
  $ws = array_map(function($v){ 
              $w = date('W', $v);
              $week_start = new DateTime();
              $week_start->setISODate(date('Y', $v), $w);
              return  $week_start->format('d-M-Y');
            }, $dates); // Requires PHP 5.3+
  $lastElementKey = key($ws);
  foreach($ws as $k=>$week):
    if( next( $ws ) ) {
      $weeks[] = [$week, $ws[$k+1]];
    }
  endforeach;

  return $weeks;
}


function mzk_f_date($dt, $format = 'Y-m-d'){
  return date($format, strtotime($dt));
}

function mzk_create_month_range_array($start, $end){
  $start    = (new DateTime($start))->modify('first day of this month');
  $end      = (new DateTime($end))->modify('first day of next month');
  $interval = DateInterval::createFromDateString('1 month');
  $period   = new DatePeriod($start, $interval, $end);
  $range = [];

  foreach ($period as $dt) {
    $range[] = $dt->format("Y-m");
  }

  return $range;
}

function mzk_create_date_range_array($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

$start    = new DateTime($strDateFrom);
$end      = new DateTime($strDateTo);
$interval = DateInterval::createFromDateString('1 day');
$period   = new DatePeriod($start, $interval, $end);
return $period;

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}


function mzk_reset_counter(){
  $session = 'mazkara.search.paginator';
  mzk_init_counter();
}

function mzk_init_counter(){
  $session = 'mazkara.search.paginator';
  //if(!Session::has($session)){
    Session::put($session, 0);
  //}
}

function mzk_get_counter(){
  $session = 'mazkara.search.paginator';
  if(!Session::has($session)){
    mzk_init_counter();
  }
  return Session::get($session);
}

function mzk_increment_counter($max = false){
  $session = 'mazkara.search.paginator';
  $counter = Session::get($session);

  if(($max!==false) && ($counter >= $max)){
    $counter = 0;
  }else{
    $counter++;

  }
  Session::put($session, $counter);
}

function mzk_get_search_session(){
  $session = 'mazkara.search.parameter';

  if(!Session::has($session)){
    mzk_set_search_session();
  }

  $result =   Session::get($session);


  return  Session::get($session);
}

function mzk_set_search_session($categories = false, $business_zones = false){
  $session = 'mazkara.search.parameter';
  $search_vars = mzk_prepare_search_session_variable($categories, $business_zones);

  Session::put($session, $search_vars);
}

function mzk_prepare_search_session_variable($categories = false, $business_zones = false){

  $categories = $categories !=false ? array_unique($categories) : [];
  $business_zones = $business_zones!=false ? array_unique($business_zones) : [];
  sort($categories);sort($business_zones);

  $result = join(',', $business_zones).'|'.join(',',$categories);
  return $result;
}

function mzk_get_categories_from_search_session_variable(){
  $v = mzk_get_search_session();
  $v = split('|', $v);
  $categories = split(',', $v[1]);
  return $categories;
}

function mzk_get_business_zones_from_search_session_variable(){
  $v = mzk_get_search_session();
  $v = split('|', $v);
  $business_zones = split(',', $v[0]);
  return $business_zones;
}

function mzk_get_ad_lists_to_show_today($params){

  if (App::environment('production')){
    //return [];
  }

  $categories = isset($params['categories'])?$params['categories']:[];
  $business_zones = isset($params['business_zones'])?$params['business_zones']:[];

  $campaigns_ids = Campaign::select('id')->current()->byLocale()->lists('id','id')->all();

  
  // which campaigns ads should we be showing btw here?


  // get adsets running for these campaigns which are for the parameters
  // add in parameters

  // these adsets are for currently running campaigns
  $ad_sets = Ad::select('ads.*', 'ad_sets.slot as slot', 'campaigns.merchant_id as merchant_id')
            ->join('ad_sets', 'ad_sets.id', '=', 'ads.ad_set_id')
            ->join('campaigns', 'ad_sets.campaign_id', '=', 'campaigns.id');

  if(count($categories)>0){
    // now they are for the categories being searched
    $ad_sets->whereIn('ad_sets.category_id', $categories);
  }


  if(count($business_zones)>0){
    // now they are for the business zones being searched
    $ad_sets->whereIn('ad_sets.business_zone_id', array_values($business_zones));
  }

  $ad_sets = $ad_sets->whereIn('ad_sets.campaign_id', $campaigns_ids);
  $ads = $ad_sets->orderby('slot', 'asc')->orderby('category_id', 'asc')->orderby('campaign_id', 'asc')->get();

  $ad_ids = $ads->lists('id','id')->all();
  $photos = Photo::where('imageable_type', '=', 'Ad')->where('image_file_size', '<>', '0')->whereIn('imageable_id', $ad_ids)->get();

  $result = array();
  $pics_keys = array();
  $pics_values = array();
  foreach($photos as $photo){
//    $pics_keys[] = $photo->imageable_id;
    $pics_values[$photo->imageable_id] = $photo->image->url('banner'); 
    //$pics_values[$photo->imageable_id] = $photo->image->url('banner'); 
  }




  $most_banners_count = 0;
  $chk = [];
  foreach($ads as $ad){
    //$i = array_search($ad->id, $pics_keys);
    $ad->photo_url = isset($pics_values[$ad->id]) ? $pics_values[$ad->id]:'';//$i?$pics_values[$i]:'';
    //get rid of ads that are not supposed to be here in this business zone to begin with
    if(!isset($result[$ad->slot])){
      $result[$ad->slot] = [];
      $chk[$ad->slot] = [];
    }
    $result[$ad->slot][] = $ad;
    $chk[$ad->slot][] = $ad->id;
  }

  //dump($categories,$business_zones,$campaigns_ids,$ad_ids, $chk);


  foreach($result as $banners){
    $most_banners_count = count($banners)>$most_banners_count ? count($banners):$most_banners_count;
  }

  // check the current search
  // has the new search changed from what is in the session
  if(mzk_prepare_search_session_variable($categories, $business_zones)!=mzk_get_search_session()){
    // reset counter
    mzk_reset_counter();
  }else{
    // increment counter if we can increment further
    mzk_increment_counter(($most_banners_count-1));
  }
  // just set the search session
  mzk_set_search_session($categories, $business_zones);
  // which page to show the ads for
  $page = mzk_get_counter();

  $displays = [];
  // loop through the ads and set it up
  foreach($result as $slot => $banners){
    $index = $page;
    // increment it
    if(isset($banners[$index])){
      $displays[$slot] = $banners[$index];
    }else{
     $index = $index%count($banners);
     $displays[$slot] = $banners[$index];
    }
  }

  // filter to remove adjacent similar clients here
  $all_merchants_shown_today = [];
  if(count($business_zones)!=1){ // run for multiple business zones
    $ds = array();
    $previous = 0;
    foreach ($displays as $display) {
      //if (($previous !== $display->merchant_id) /* || ($display->merchant_id == 18)*/){ // hack for dummy
      if (!in_array($display->merchant_id, $all_merchants_shown_today)){ // downt show more times for same merchant
        $ds[] = $display;
      }

      $previous = $display->merchant_id;
      $all_merchants_shown_today[] = $display->merchant_id;
    }
    $displays = $ds;
  }

  ksort($displays);

  return $displays;
}

function mzk_categories_from_services($service_ids){
  $categories = \DB::table('category_service')
                      ->whereIn('service_id', $service_ids)
                      ->lists('category_id');
  $categories = array_unique($categories);
  return $categories;
}


function mzk_is_acd_active($locale = false){
  $locale = $locale ? $locale : mzk_get_localeID();
  $s = Setting::query()->byLocale($locale)->where('name', '=', 'enable_acd')->first();
  if($s){
    return $s->value == 1 ? true : false;
  }
  return true;
}


function mzk_get_active_services(){
  //remember(60, 'parents.of.services')->
  $parents = Service::select()->showParents()->remember(180)->orderby('name', 'asc')->get();
  // get ids of all service ids that are active for this locale
  
  $locally_active_child_services = \DB::table('service_zone')
                                        ->where('state', '=', 'active')
                                        ->where('zone_id', '=', MazkaraHelper::getLocaleID())
                                        ->lists('service_id');

  $children = Service::query()->whereIn('id', $locally_active_child_services)->get();
  $data = array();
  $key = array();
  foreach($parents as $parent){
    $data[$parent->name] = array();
    $key[$parent->id] = $parent->name;
  }

  foreach($children as $child){
    if($child->parent_id <> null){
      if(!isset($key[$child->parent_id])){
        //continue;
      }
      $data[$key[$child->parent_id]][] = array('id'=>$child->id, 'name'=>$child->name);
    }
  }

  return $data;
}

function mzk_get_active_services_ids(){
  
  $locally_active_services_ids = \DB::table('service_zone')
                                        ->where('state', '=', 'active')
                                        ->where('zone_id', '=', MazkaraHelper::getLocaleID())
                                        ->lists('service_id','service_id');


  return $locally_active_services_ids;
}


function mzk_favorite_big_badge($business){
  $html = '';
  $html.='<div class="search-item-stars hide-only-mobile " style="margin-top:160px;">';
  $html.='<span class="">';
  $html.='<span class="text-center fs300 dpb  ">';
  $html.='<i class="fa fa-heart " style="color:'.($business->favorites_count>4 ?'#D9534F':'#CCC').'"></i>';
  if($business->favorites_count>4){
    $html.='<div style="color:#CCC; font-size:14px;" class=" dpb">('.$business->favorites_count.' Likes)</div>'; 
  }else{
    $html.='<div style="color:#CCC; font-size:14px;" class=" dpb">Not enough Likes</div>'; 

  }
  $html.='</span>';
  $html.='<div class="clear"></div>';
$html.='</span>';
$html.='</div>';
$html.='<div class="clear"></div>';
return $html;
}

function mzk_favorite_small_badge($business){
  $html = '';
  $html.='<div class="search-item-stars hide-only-mobile " style="margin-top:160px;">';
  $html.='<span class="">';
  $html.='<span class="text-center fs300 dpib  ">';
  $html.='<i class="fa fa-heart " style="color:'.($business->favorites_count>4 ?'#D9534F':'#CCC').'"></i>';
  if($business->favorites_count>4){
    $html.='<div style="color:#CCC; font-size:12px;" class=" dpib">'.$business->favorites_count.' Likes</div>'; 
  }else{
    $html.='<div style="color:#CCC; font-size:12px;" class=" dpib">Not enough Likes</div>'; 
  }
  $html.='</span>';
  $html.='<div class="clear"></div>';
$html.='</span>';
$html.='</div>';
$html.='<div class="clear"></div>';
return $html;
}


function mzk_favorite_badge($business){
  $html = '';
  $html.='<div class="search-item-stars hide-only-mobile ">';
  $html.='<span class="">';
  $html.='<span class=" fs125 dpib  ">';
  $html.='<i class="fa fa-heart " style="color:'.($business->favorites_count>4 ?'#D9534F':'#CCC').'"></i>';
  if($business->favorites_count>4){
    $html.='<span class=" dpib">('.$business->favorites_count.')</span>'; 
  }else{
    $html.='<span style="color:#CCC;font-size:11px; margin-top:-3px;" class=" dpib">(None)</span>'; 
  }
  $html.='</span>';
  $html.='<div class="clear"></div>';
$html.='</span>';
$html.='</div>';
$html.='<div class="clear"></div>';
return $html;
}

function mzk_cloudfront_image($url){
  $s = '/s3.amazonaws.com/mazkaracdn/'; 
  $r = '/d187hsnmfgmmaz.cloudfront.net/';
  $url = str_replace($s, $r, $url);
  $s = 'http://'; 
  $r = 'https://';

  return str_replace($s, $r, $url);
}


function mzk_haversine($lat = 0, $lon = 0, $distance = 0, $lon_label = 'geolocation_longitude', $lat_label = 'geolocation_latitude')
{
  $sql = '';
  if($distance > 0)
  {
    $sql.="((6372.797 * (2 * ";
    $sql.="ATAN2(";
    $sql.="SQRT(";
    $sql.="SIN((".($lat*1)." * (PI()/180)-".$lat_label."*(PI()/180))/2) * ";
    $sql.="SIN((".($lat*1)." * (PI()/180)-".$lat_label."*(PI()/180))/2) + ";
    $sql.="COS(".$lat_label." * (PI()/180)) * ";
    $sql.="COS(".($lat*1)." * (PI()/180)) * ";
    $sql.="SIN((".($lon*1)." * (PI()/180)-".$lon_label."*(PI()/180))/2) * ";
    $sql.="SIN((".($lon*1)." * (PI()/180)-".$lon_label."*(PI()/180))/2) ";
    $sql.="),";
    $sql.="SQRT(1-( " ;
    $sql.="SIN((".($lat*1)." * (PI()/180)-".$lat_label."*(PI()/180))/2) * ";
    $sql.="SIN((".($lat*1)." * (PI()/180)-".$lat_label."*(PI()/180))/2) + ";
    $sql.="COS(".$lat_label." * (PI()/180)) * ";
    $sql.="COS(".($lat*1)." * (PI()/180)) * ";
    $sql.="SIN((".($lon*1)." * (PI()/180)-".$lon_label."*(PI()/180))/2) * ";
    $sql.="SIN((".($lon*1)." * (PI()/180)-".$lon_label."*(PI()/180))/2) ";
    $sql.=")) ";
    $sql.=") ";
    $sql.=")) <= ".($distance/1000). ")";
  }
  return $sql;
}


/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles (default)                         :*/
/*::                  'K' is kilometers                                      :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at http://www.geodatasource.com                          :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: http://www.geodatasource.com                        :*/
/*::                                                                         :*/
/*::         GeoDataSource.com (C) All Rights Reserved 2015              :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
function mzk_distance_between($lat1, $lon1, $lat2, $lon2, $unit = 'K') {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

function mzk_invoice_state_label_css($state){
  $css = 'default';
  switch($state){
    case 'pending':
    case 'recieved':
    case 'deposited':
      $css = 'warning';
    break;
    case 'cleared':
      $css = 'success';
    break;
    case 'credit note':
    case 'bad debt':
    case 'credit-note':
    case 'bad-debt':
    case 'returned':
      $css = 'danger';
    break;
  }

  return $css;
}

  function mzk_shuffle_assoc(&$array) {
    $keys = array_keys($array);

    shuffle($keys);

    $new = array();

    foreach($keys as $key) {
        $new[] = $array[$key];
    }

    $array = $new;

    return true;
  }

function mzk_duration($seconds, $max_periods) {
    $periods = array('year' => 31536000, 'month' => 2419200, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60, 'second' => 1);
    $i = 1;
    foreach ( $periods as $period => $period_seconds ) {
        $period_duration = floor($seconds / $period_seconds);
        $seconds = $seconds % $period_seconds;
        if ( $period_duration == 0 ) continue;
        $duration[] = $period_duration .' '. $period . ($period_duration > 1 ? 's' : '');
        $i++;
        if ( $i > $max_periods ) break;
    }
    if (is_null($duration)) return 'just now';
    return implode(' ', $duration);
}

function mzk_date_intervals($start, $end){

  $date1 = new DateTime($start);
  $date2 = new DateTime($end);
  $interval = $date1->diff($date2);
  return $interval->days;
}

function mzk_month_array(){
  $months = array();
  for ($i = 0; $i < 12; $i++) {
    $timestamp = mktime(0, 0, 0, $i, 1);
    $months[date('n', $timestamp)] = date('F', $timestamp);
  }

  return $months;
}

function mzk_currency_exchange($amount, $from ='AED', $to = 'USD'){
  $converted = 0;

  if($from == 'AED'){
    if($to == 'USD'){
      $converted = round($amount/3.67, 2);
    }
    if($to == 'INR'){
      $converted = round($amount*18, 2);
    }
  }

  if($from == 'INR'){
    if($to == 'USD'){
      $converted = round(($amount/3.67)/18, 2);
    }
    if($to == 'AED'){
      $converted = round(($amount/18), 2);
    }
  }

  return $converted;

}


function mzk_dates_overlap($start_one,$end_one,$start_two,$end_two) {

  $start_one = new DateTime($start_one);
  $end_one = new DateTime($end_one);
  $start_two = new DateTime($start_two);
  $end_two = new DateTime($end_two);

   if($start_one <= $end_two && $end_one >= $start_two) { //If the dates overlap
        return min($end_one,$end_two)->diff(max($start_two,$start_one))->days + 1; //return how many days overlap
   }

   return 0; //Return 0 if there is no overlap
}

function mzk_year_array(){
  $years = [];
  for ($i = (date('Y')+2); $i > 2013; $i--) {
    $years[$i] = $i;
  }

  return $years;
}




function mzk_convert_number_to_words($number) {
   
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'mzk_convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . mzk_convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . mzk_convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = mzk_convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= mzk_convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
    return $string;
}

function mzk_get_entities($id = false){
  $data = [];
  $data[1] = [
                'name'=>'Mazkara FZ LLC', 
                'address'=>['17th & 18th Floor',
                            'Creative Tower',
                            'Fujairah (UAE)',
                            'www.fabogo.com']
              ];

  $data[2] = [
                'name'=>'Mazkara Advertising LLC', 
                'address'=>['1701 Al Saqr Business Tower',
                            'Sheikh Zayed Road',
                            'Dubai (UAE)',
                            'www.fabogo.com']
              ];

  $data[3] = [
                'name'=>'Mazkara Media Pvt Ltd', 
                'address'=>['D-8 Hindustan Estate',
                            'Yerwada',
                            'Pune - 411006',
                            'www.fabogo.com',
                            'Service Tax No. AAJCM9267DSD001',
                            'PAN No.  AAJCM9267D']
              ];

  return $id == false ? $data : $data[$id];
}

function mzk_is_entity_india($id){
  if($id==3){
    return true;
  }

  return false;
}

function mzk_get_entities_as_array(){
  $e = mzk_get_entities();
  $r = [];
  foreach($e as $ii=>$vv){
    $r[$ii] = $vv['name'];
  }

  return $r;
}


function mzk_generate_business_thumbnail($business, $params=null){
  $url = false;
  $str = '';
  $size = isset($params['size']) ? $params['size'] : 'large';

  $sizes = ['thumbnail' => ['x'=>'100', 'y'=>'100'],
            'small' => ['x' => '175', 'y' => '175'],
            'xlarge' => ['x' => '1600', 'y'=>'1200'],
            'xlargeCropped' => ['x' => '1600', 'y'=>'1200'],
            'largeCropped' => ['x' => '800', 'y'=>'600'],
            'xlargeCroppedPortrait' => ['x' => '1200', 'y'=>'1600'],
            'largeCroppedPortrait' => ['x' => '600', 'y'=>'800'],
            'medium' => ['x' => '400', 'y'=>'275'],
            'mediumPortrait' => ['x' => '275', 'y'=>'400#'],
            'micro'     => ['x' => '50', 'y'=>'50']];

  if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage()):
    $url = $business->getCoverUrl($size);
  else:
    if($business->hasMetaThumbnail($size) && !isset($params['no-meta'])){
      $url = $business->getMetaThumbnailUrl($size);
    }elseif($business->hasThumbnail()){
      $url = $business->getThumbnailUrl($size);
    }
  endif;

  $params = $params?$params: ['width'=>'40', 'height'=>'40', 'class'=>'img-rounded', 
                              'style'=>'width:40px; height:40px; padding-top:6px;'];
  foreach($params as $ii=>$vv){
    if(($ii=='style')||($ii=='no-meta')||($ii=='class'))
      continue;
    $str.=$ii.'="'.$vv.'" ';
  }

    $class =(isset($params['class'])?$params['class']:'');

    if($url!=''){
      $url = mzk_cloudfront_image($url);
      return '<img src="'.$url.'" '.$str.' alt="'.$business->name.' photo" class="'.$class.'" />';
    }

    $css =(isset($params['style'])?$params['style']:'');
    $css.='background-color:'.(ViewHelper::genColorCodeFromText($business->name.$business->id)).';';

    $class.= ' dumatar ';

    $html = '<span class="'.$class.'" '.$str.' style="'.$css.'">';

    $html .=strtoupper($business->getInitials());

    $html .='</span>';

    return $html;
  }



function mzk_slug_to_icon_css($slug){
  $css = ['salon'=>'parenticon parenticon-salon-01-01', 
          'spa'=>'parenticon parenticon-salon-02',
          'massage'=>'parenticon parenticon-salon-03',
          'massage-center'=>'parenticon parenticon-salon-03',
          'personal-fitness-center'=>'parenticon parenticon-salon-04',
          'bridal'=>'highlights highlights-bridal',
          'freelancer'=>'highlights highlights-freelancer',
          'cosmetic-clinic'=>'parenticon-salon-05'
          ];
  return isset($css[$slug]) ? $css[$slug] : '';
}

function mzk_icon_highlights($highlight){
  $css = ['ac'=>'highlights-ac highlights',
          'air-conditioned'=>'highlights-ac highlights',
          'credit-card'=>'highlights-credit-card highlights-credit-cards highlights',
          'deal'=>'highlights-deal highlights',
          'economical'=>'highlights-economical highlights',
          'home-services'=>'highlights-home-services highlights',
          'kids-services'=>'highlights-kids-services highlights',
          '4-or-5-star-luxury-hotel'=>'fa fa-star-o',// 'highlights-4-or-5-star-luxury-hotel highlights',
          'luxury'=>'highlights-luxury highlights',
          'mens-only'=>'highlights-mens-only  highlights-mens highlights',
          'premium'=>'highlights-premium highlights',
          'tv'=>'highlights-tv highlights',
          'products-sold'=>'fa fa-shopping-cart',
          'unisex'=>'highlights-unisex highlights',
          'walk-ins-allowed'=>'highlights-walk-ins-allowed highlights',
          'ladies-only'=>'highlights-ladies-only highlights-womens highlights',
          'wi-fi'=>'highlights-wi-fi'];

  return isset($css[$highlight]) ? $css[$highlight] : '';
}

function mzk_icon_cost_estimate($cost){
  $css = ['1'=>'highlights-economical',
          '2'=>'highlights-premium',
          '3'=>'highlights-luxury'];

  return isset($css[$cost]) ? $css[$cost] : '';
}


function mzk_xml2array(SimpleXMLElement $parent)
{
    $array = array();

    foreach ($parent as $name => $element) {
        ($node = & $array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = & $node[];

        $node = $element->count() ? XML2Array($element) : trim($element);
    }

    return $array;
}



// darn fixes

function obStart(){
  return ob_start();
}

function ob_getClean(){
  return ob_get_clean();
}