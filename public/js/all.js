$(function(){


//$('.dropdown').on('show.bs.dropdown', function(e){
//    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
//  });
//
//  // ADD SLIDEUP ANIMATION TO DROPDOWN //
//  $('.dropdown').on('hide.bs.dropdown', function(e){
//    e.preventDefault();
//    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100, function(){
//      $('.dropdown').removeClass('open');
//        $('.dropdown').find('.dropdown-toggle').attr('aria-expanded','false');
//    });
    //
//  });

  $('.submit-parent-form').click(function(){
    $(this).parents().first().submit();
  });

  $('form.confirmable').submit(function(){
    return confirm('Are you sure?');  
  });

  //if(jQuery.isFunction('magnificPopup'))
  {
    $('.ajax-popup-link').magnificPopup({
      type: 'ajax',
      showCloseBtn:true,
      closeBtnInside:false,
      fixedContentPos: 'auto',
      closeOnBgClick:true,
    });    
  }

  $('[data-toggle="tooltip"]').tooltip();

  //if(jQuery.isFunction('fakeLoader'))
  {
    $(document).on('click', '.fakeloader-link', function(){
      $("#fakeloader").fakeLoader({
        timeToHide:5000,
        zIndex: '89',
        bgColor:"#fff",
        spinner:"spinner6"
      });
    });

    $('#sidebar-navbar-filter a').click(function(){
      $("#fakeloader").fakeLoader({
        timeToHide:5000,
        zIndex: '89',
        bgColor:"#fff",
        spinner:"spinner6"
      });
    });
  }



  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });

  var MZK_PRELOAD = $('a.image').map(function() {
    return $( this ).attr('href');
  }).get();

  $.imgpreload(MZK_PRELOAD);

  /*
    Handler for pushpop History states

  */

  // we get a normal Location object

  /*
   * Note, this is the only difference when using this library,
   * because the object window.location cannot be overriden,
   * so library the returns generated "location" object within
   * an object window.history, so get it out of "history.location".
   * For browsers supporting "history.pushState" get generated
   * object "location" with the usual "window.location".
   */
//  var location = window.history.location || window.location;
//
//  // looking for all the links and hang on the event, all references in this document
//  $(document).on('click', 'a.link-single-venue', function() {
//    // keep the link in the browser history
//    history.pushState(null, null, this.href);
//
//    // here can cause data loading, etc.
//    // make ajax call to the link
//    // link returns json data
//    // select what the view will be here
//    // 
//
//    return false;
//  });
//
//  // hang on popstate event triggered by pressing back/forward in browser
//  $(window).on('popstate', function(e) {
//    // here can cause data loading, etc.
//    // just post
//  });

  /*
  $('.navbar-nav.yamm').on('show.bs.dropdown', function () {
    $('#fullscreen-overlay').show();
  });

  $('.navbar-nav.yamm').on('hide.bs.dropdown', function () {
    $('#fullscreen-overlay').hide();
  });
  */

$.smartbanner({
  title: 'Fabogo - Beauty and Wellness', // What the title of the app should be in the banner (defaults to <title>)
  author: null, // What the author of the app should be in the banner (defaults to <meta name="author"> or hostname)
  price: 'FREE', // Price of the app
  appStoreLanguage: 'us', // Language code for App Store
  inGooglePlay: 'Available in Google Play', // Text of price for Android
  button: 'INSTALL', // Text for the install button
  speedIn: 300, // Show animation speed of the banner
  speedOut: 400, // Close animation speed of the banner
  daysHidden: 1, // Duration to hide the banner after being closed (0 = always show banner)
  daysReminder: 1, // Duration to hide the banner after "VIEW" is clicked *separate from when the close button is clicked* (0 = always show banner)
  hideOnInstall: true, // Hide the banner after "VIEW" is clicked.
})

});

$(function(){

  // assign values to the hidden form when you click on a data entry in the search drop down
  // then submit the form
  $('#search-bar-button').click(function(){
    $('#search-bar-form').find('#search-selector-name').val($("#search-selector").val());
    $('#search-bar-form').submit();
  });

//  $('#search-bar-form').submit(function(){
//    sdt = $('#search-selector').data('required');
//    if(sdt){
//      src = $('#location-selector-service').val();
//      if(src==''){
//        $('#search-selector').popover('show');
//        return false;
//      }else{
        //
//        return true;
//      }
//    }else{
//      return true;
//    }
//  })

  var cities = _MAZKARA_CITIES_; 
  var __MZK_SEARCH_LBL__ = $('#search-selector').val();
  var __MZK_SEARCH_VAL__ = $('#search-selector-name').val();

  $('#search-selector').autocomplete({
    search  : function(){ $('.search-box-preloader').show(); },
    open    : function(){ $('.search-box-preloader').hide();},    
    source:  function(request, response) {
      $.ajax({
        url: '/search',
        minLength: 2,
        data: {
          zone: $('#location-selector-name').val(),
          term: request.term
        },
        success:function(data){
          response(data);
          $('.search-box-preloader').hide();
        }
      });
    },

    select: function(event, ui){
      if(ui.item.type=='OUTLET'){
        $('#search-selector').val(ui.item.name);
        window.location.href = ui.item.url;
        return false;
      }else if(ui.item.type=='CATEGORY'){
        $('#location-selector-category').val(ui.item.id);
        $('#search-selector').val(ui.item.name);
        //$('#search-selector').attr('placeholder', ui.item.name);
        $("#search-selector-name").val( ui.item.name ); 
        return false;         
      }else{
        $('#location-selector-service').val(ui.item.id);
        $('#search-selector').val(ui.item.name);
        //$('#search-selector').attr('placeholder', ui.item.name);
        $("#search-selector-name").val( ui.item.name ); 
        return false;         
      }
      //$('#location-selector').focus();

    },
    focus: function( event, ui ) {
      //$( "#search-selector" ).val( ui.item.name );
      if(ui.item.id){
        return false;
      }else{
        return false;
      }
    }

  }).click(function(){
      $('#search-selector').val('');
      $('#location-selector-category').val('');

  }).focus(function(){
    if (this.value == ""){
      $("#search-selector-name").val( '' ); 
      $('#location-selector-category').val('');
    }else{
      __MZK_SEARCH_LBL__ = $('#search-selector').val();
      __MZK_SEARCH_VAL__ = $('#search-selector-name').val();
    }
  }).keypress(function(e){

    if(e.which==13){
      if($('#search-selector').val()!=$('#search-selector-name').val()){
        // he is making a search for random - so unset the services selected
        $('#search-selector-name').val($('#search-selector').val());
      }

      $('#search-bar-form').submit();
      return false;
    }else{

    }
  }).keyup(function (e) {
    if ($(this).val() == '') {
      $("#search-selector-name").val( '' ); 
    } 
  }).blur(function (e){
    if($(this).val()==''){
      //$('#search-selector').val(__MZK_SEARCH_LBL__);
      //$('#search-selector-name').val(__MZK_SEARCH_VAL__);
      //$(this).autocomplete('search', "");
    }


    $(this).autocomplete('search', "");
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    if(item.id){
      label = item.name;

      var re = new RegExp( "(" + this.term + ")", "gi" ),
      cls = 'highlight-ui',
      template = "<span class='" + cls + "'>$1</span>",
      label = label.replace( re, template ),

      html = '<a href="javascript:void(0)" class=" dpb">';
      html = html + '<div class=" ">' + label;
      html = html + '<div class="pull-right "><small class="gray">SERVICE</small></div><div class="clearfix" style="display:none"></div>' + '</div>';
      html = html +"</a>";
      return $( "<li>" )
          .append( html )
          .appendTo( ul );
    }else{
      if(item.type == 'OUTLET'){

        label = item.name;

        var re = new RegExp( "(" + this.term + ")", "gi" );
        cls = 'highlight-ui';
        template = "<span class='" + cls + "'>$1</span>";
        label = label.replace( re, template );

        label_zone = item.zone;

        htm = '<a href="' + item.url + '" class=" fakeloader-link dpb">';
        htm = htm + '<div class=" ">';
        htm = htm + label + ' <small class="gray">' + label_zone + '</small>';
        htm = htm + '<div class="pull-right "><small class="gray">VENUE</small></div><div class="clearfix" style="display:none"></div>';
        htm = htm + '</div>';
        htm = htm + '</a>';

        return $( "<li>" )
          .append( htm)
          .appendTo( ul );

      }else if(item.type == 'CATEGORY'){

        label = item.name;

        var re = new RegExp( "(" + this.term + ")", "gi" );
        cls = 'highlight-ui';
        template = "<span class='" + cls + "'>$1</span>";
        label = label.replace( re, template );
        htm = '<a href="javascript:void(0)' + /*item.url +*/ '" class="fakeloader-link dpb">';
        htm = htm + '<div>' + label;
        htm = htm + '<div class="pull-right "><small class="gray">CATEGORY</small></div><div class="clearfix" style="display:none"></div>';
        htm = htm + '</div></a>';

//        label_zone = item.zone;
//
//        htm = '<a href="' + item.url + '" class=" fakeloader-link dpb">';
//        htm = htm + '<div class=" ">';
//        htm = htm + '<div class="pull-right "><small class="gray">CATEGORY</small></div><div class="clearfix" style="display:none"></div>';
//        htm = htm + '</div>';
//        htm = htm + '</a>';
//
        return $( "<li>" )
          .append( htm)
          .appendTo( ul );
      }else if(item.type == 'CHAIN'){

        label = item.name;

        var re = new RegExp( "(" + this.term + ")", "gi" );
        cls = 'highlight-ui';
        template = "<span class='" + cls + "'>$1</span>";
        label = label.replace( re, template );
        htm = '<a href="' + item.url + '" class="fakeloader-link dpb">';
        htm = htm + '<div>' + label;
        htm = htm + '<div class="pull-right "><small class="gray">CHAIN</small></div><div class="clearfix" style="display:none"></div>';
        htm = htm + '</div></a>';

//        label_zone = item.zone;
//
//        htm = '<a href="' + item.url + '" class=" fakeloader-link dpb">';
//        htm = htm + '<div class=" ">';
//        htm = htm + '<div class="pull-right "><small class="gray">CATEGORY</small></div><div class="clearfix" style="display:none"></div>';
//        htm = htm + '</div>';
//        htm = htm + '</a>';
//
        return $( "<li>" )
          .append( htm)
          .appendTo( ul );

      }else{
        htm = '<a href="javascript:void(0)" class="clearfix dpb">';
        htm = htm + '<div class="pull-left ">' + item.name + '</div>';
        htm = htm + '<div class="pull-right "><small class="gray">SERVICE</small></div>';
        htm = htm + '</a>';

        return $( "<li>" )
          .append(htm)
          .appendTo( ul );        
      }      
    }
  };
  // location box handler
  var __MZK_LOCALE_LBL__ = $('#location-selector').val();
  var __MZK_LOCALE_VAL__ = $('#location-selector-name').val();

  $('#location-selector').autocomplete({
    search  : function(){$(this).addClass('ui-autocomplete-loading');},
    open: function(event, ui) {
      $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
      $(this).removeClass('ui-autocomplete-loading');        
    },
    source:  function(request, response) {
      if(request.term == '*'){
        mzk_response = _MAZKARA_CITIES_;
        mzk_response[0] = ({ type:'heading', label:'POPULAR LOCATIONS' });

        response(mzk_response);
      }else{
      $.ajax({
        url: '/search-locations',
        minLength: 1,
        data: {
          term: request.term
        },
        success:function(data){
          response(data);
        }
      });
      }
    },

    /* source: '/search-locations', function(request, response) {
      var results = $.ui.autocomplete.filter(cities, request.term);
      results = results.slice(0, 5);
      //results.unshift({type:'heading', label:'POPULAR LOCATIONS'});
      response(results);
    },*/

    minLength: 1,
    select: function(event, ui){
      $("#location-selector").val( ui.item.label );
      $("#location-selector-name").val( ui.item.value );

      __MZK_LOCALE_LBL__ = $('#location-selector').val();
      __MZK_LOCALE_VAL__ = $('#location-selector-name').val();

      if(ui.item.type == 'city'){

        if(ui.item.url == 'javascript:void(0)'){
          $('#search-bar-form').submit();
        }
      }

      //$('#search-selector').focus();
      return false;         
    },
    focus: function( event, ui ) {
      $( "#location-selector" ).val( ui.item.label );
      return false;
    }
  }).keypress(function(e){
    if(e.which==13){
      if($('#search-selector').val()!=$('#search-selector-name').val()){
        // he is making a search for random - so unset the services selected
        $('#search-selector-name').val($('#search-selector').val());
      }
      
      $('#search-bar-form').submit();
      return false;

    }
  }).blur(function(){
    if($(this).val()==''){
      $('#location-selector').val(__MZK_LOCALE_LBL__);
      $('#location-selector-name').val(__MZK_LOCALE_VAL__);
    }
  }).focus(function(){
    if($(this).val()==''){
      $(this).val('');
    }else{
      __MZK_LOCALE_LBL__ = $('#location-selector').val();
      __MZK_LOCALE_VAL__ = $('#location-selector-name').val();
      $('#location-selector').val('');
      $('#location-selector-name').val('');

      $(this).autocomplete('search', "*");
    }
  }).click(function(){
    if($(this).val()==''){
      $(this).val('');
    }
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    if(item.type == 'heading'){
      //html = '<a href="javascript:void(0)" class="dpb"><div ><b>'+item.label+'</b></div></a>';

      html = '<a href="javascript:void(0);" class=" dpb">';
      html = html + '<div class="text-left "><b>' + item.label + '</b></div>';
      html = html + '</a>';

    }else{
      if(item.type == 'city'){
        html = '<a href="'+(item.url!=false ? item.url : 'javascript:void(0)')+'" class=" dpb">';
        html = html + '<div class="text-left ">' + item.label + '</div>';
        html = html + '</a>';

      }else{

        html = '<a href="javascript:void(0)" class=" dpb">';
        html = html + '<div class="text-left ">' + item.label + '</div>';
        html = html + '</a>';

      }
    } 
    return $( "<li>" )
          .append( html )
          .appendTo( ul );

  };

  $('#lnk-to-change-location').click(function(){
    $('#location-selector').focus();
    $('#location-selector').autocomplete('search', "*");
  });

});
//# sourceMappingURL=all.js.map
