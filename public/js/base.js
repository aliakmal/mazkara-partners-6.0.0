// Define the module
// var mazkaraApp = angular.module('mazkaraApp', [], function($interpolateProvider) {
//   $interpolateProvider.startSymbol('<{');
//   $interpolateProvider.endSymbol('}>');
// });
// 
// mazkaraApp.controller('NavigationController', function NavigationController($scope) {
// 
// });
// Define the module

  


var mzk_get_cities_array = function(){
  all_cities = __MZK_CITIES__;
  if(!(all_cities.constructor === Array)){
    all_cities = [];
  }
  return all_cities;
};

var mzk_get_city_attr = function(city_id, attrib){
  if(attrib == null){
    attrib = 'slug';
  }

  cities = mzk_get_cities_array();
  for(i in cities){
    if(cities[i].id == city_id){
      return cities[i][attrib];
    }
  }
};

var mzk_slug_single = function(business){
  str = '';

  if(business.city_id>0){
    str += '/' + mzk_get_city_attr(business.city_id, 'slug');
  }

  str += '/' + business.slug;
  console.log(str);
  return str;
};
