@extends('layouts.email-no-box')

@section('content')
<div class='movableContent'>
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="container">
    <tr>
      <td width="100%"  align="center" style="padding-bottom:10px;;">
        <div class="contentEditableContainer contentTextEditable">
          <p style="text-align:center;font-size:20px;" >
            Hey {{ $user['name']}}!<br/>
            Come see what stories are on Fabogo!

          </p>
        </div>
      </td>
    </tr>
    <tr>
      <td width="100%" style="padding:0px 15px 0px 15px;" >
          @foreach($posts as $post)
        <table width="100%" style="margin-bottom:10px;">
            <tr >
              <td style="vertical-align: top;padding-top:0px;"><?php
                  ?><a title="{{{ $post->title }}}" href="{{ $post->url() }}" style="border:0px; text-decoration:none;" >
                    <img src="{{ $post->cover->image->url('medium') }}"  class="" style="border:1px solid #AAA;max-width:190px;"  />
                  </a><?php
              ?></td>
              <td valign="top" style="width: 100%;max-width: 350px;vertical-align: top;" >
                <div style=" margin:0px 5px 5px 0px;border-bottom:1px solid #CCC;">
                  <table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
                    <tr>
                      <td style="color:#aaa;padding-left:5px; font-size:10px;font-family:sans-serif;">
                        {{ strtoupper(join(',',$post->services()->lists('name','name')->all())) }}
                      </td>
                      <td style="text-align:right">
                        <span style="color:#aaa;font-size:10px; font-family: sans-serif;">
                        <img style="width:15px;" src="https://s3.amazonaws.com/mazkaracdn/css/images/icon-views.png" />
                        {{ $post->views }} Views
                        <img style="width:12px;" src="https://s3.amazonaws.com/mazkaracdn/css/images/icon-likes.png" />
                        {{ $post->num_likes() }} Likes
                        </span>
                      </td>
                    </tr>
                  </table>
                </div>
                <div style="margin-left:5px;font-family:sans-serif;">
                  <p style="font-weight:500;font-size:15px;margin-top:0px;margin-bottom:5px;">
                    <a title="{{{ $post->title }}}" href="{{ $post->url() }}" style="display:block;color:black; text-decoration:none;border:0px;">
                      {{{ mzk_str_trim($post->title, 50) }}}
                    </a>
                  </p>
                  <p style="margin:0px 0px 5px 0px;font-size:12px;color:#000;line-height:1.5em;">
                    {{{ mzk_str_trim($post->caption, 120) }}}
                  </p>
                  <table>
                    <tr>
                      <td>{{ ViewHelper::userAvatar($post->author, ['width'=>'30', 'height'=>'30', 'size'=>'micro', 'style'=>'width:30px; height:30px;'])}}</td>
                      <td style="font-family:sans-serif; padding-left:5px;">
                          <a style="color:#000;display:block; padding-bottom:3px; text-decoration:none;border:0px;font-size:12px;" href="{{route('users.profile.show', $post->author->id)}}">
                          {{ $post->authors_full_name }}
                          </a>
                          <span style="display:block;line-height:1; text-decoration:none;border:0px;font-size:11px;padding:0px;margin:0px;color:#AAA;">{{ $post->authors_designation }}</span>
                      </td>
                    </tr>
                  </table>
                </div>



              </td>
            </tr>


        </table>
          @endforeach
      </td>
    </tr>
    <tr>
      <td width="100%" colspan="2" align="center">
        <p style="padding:10px;margin:10px;font-size:12px;">For more details write to hello@fabogo.com</p>
      </td>
    </tr>
  </table>
</div>

@stop