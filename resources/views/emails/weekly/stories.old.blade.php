
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN""http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<base target="_blank">
<title>Story Email Template</title>
<style type="text/css">

body *{font-family: 'Open Sans', Arial, sans-serif !important}

div, p, a, li, td { -webkit-text-size-adjust:none; }

*{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
td{word-break: break-word;}
a{word-break: break-word; text-decoration: none; color: inherit;}

body .ReadMsgBody
{width: 100%; background-color: #ffffff;}
body .ExternalClass
{width: 100%; background-color: #ffffff;}
body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{ background-color:#ffffff; width: 100%;}   

body img {user-drag: none; -moz-user-select: none; -webkit-user-drag: none;}
body .hover:hover {opacity:0.85;filter:alpha(opacity=85);}
body td img:hover {opacity:0.85;filter:alpha(opacity=85);}

body .image600 img {width: 600px; height: auto;}
body .logo img {width: 160px; height: auto;}
body .image16 img {width: 16px; height: auto;}
body .image274 img {width: 276px; height: auto;}
body .avatar27 img {width: 27px; height: auto;}
body .image276 img {width: 276px; height: auto;}
body .image76 img {width: 76px; height: auto;}
body .imgCenter img {width: 150px; height: auto;}
body .image198 img {width: 198px; height: auto;}
body .image176 img {width: 176px; height: auto;}
</style>

<style type="text/css">@media only screen and (max-width: 640px){
    body body{width:auto!important;}
    body *[class=erase] {display: none;}
    body *[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
    body .image600 img {width: 100%!important; height: auto!important;}
    body .h25 {height: 25px!important;}
    body .imgCenter {display:inline-block!important; width: 100%!important; text-align: center!important;}
    body .imgCenter img {float: none!important; text-align: center!important; display: inline-block!important; clear: both; padding-right: 0px!important; padding-bottom: 20px!important;}
}</style>

<style type="text/css">@media only screen and (max-width: 479px){
    body body{width:auto!important;}
    body *[class=erase] {display: none;}
    body *[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
    body .image600 img {width: 100%!important; height: auto!important;}
    body .h25 {height: 25px!important;}
    body .imgCenter {display:inline-block!important; width: 100%!important; text-align: center!important;}
    body .imgCenter img {float: none!important; text-align: center!important; display: inline-block!important; clear: both; padding-right: 0px!important; padding-bottom: 20px!important;}
}</style>


</head>
<body style='margin: 0; padding: 0;'>



<table width="100%" border="0" cellpadding="0" bgcolor="#313131" cellspacing="0" align="center" class="full">
  <tbody><tr>
    <td align="center" style="" bgcolor="#313131">
    <!--[if gte mso 9]> <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;"><v:fill type="tile" src="images/header_bg.jpg"/><v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0"><![endif]--><div>
    
      <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
        <tbody><tr>
          <td width="100%" align="center">
            <!-- Start Header Text -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
              <tbody><tr>
                <td width="100%" height="30"></td>
              </tr>
              <tr>
                <td width="100%" valign="middle" class="logo" align="center">
                  <!-- Header Text --> 
                  <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                    <tbody>
                    <tr>
                      <td valign="middle" width="100%" style="text-align: center; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 14px; color: #ffffff; line-height: 24px; font-weight: 400;" class="fullCenter">
                        <a href="http://www.fabogo.com">
                          <img src="https://s3.amazonaws.com/mazkaracdn/assets/fabogo-logo.png" alt='Fabogo'    />
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="30">
                        <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/email-header-filler.jpg" height="1" style="width:600px;" />
                      </td>
                    </tr>
                  </tbody></table>
                                  
                </td>
              </tr>
            </tbody></table><!-- End Header Text -->
            
          </td>
        </tr>
      </tbody></table>
      
      </div><!--[if gte mso 9.]>
          </v:textbox>
          </v:fill></v:rect>
          <![endif]-->
      
    </td>
  </tr>
</table>

<table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" class="full">
  <tbody><tr>
                      <td width="100%" height="30"></td>
                    </tr>
</tbody></table>

                  <table cellspacing="0" cellpadding="0" width="600" border="0" align="center" class="fullCenter" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                    <tbody><tr>
                      <td width="100%" valign="middle" class="fullCenter" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 30px; line-height: 38px; font-weight: 800;">
                        Hey {{ $user['name']}}!
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="10"></td>
                    </tr>

                    <tr>
                      <td width="100%" valign="middle" class="fullCenter" style="text-align: center; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px; font-weight: 400;">
                        Come see what stories are on Fabogo!
                      </td>
                    </tr>
                                        <tr>
                      <td width="100%" height="30"></td>
                    </tr>

                    <!-- Button Center -->
                  </tbody></table>

          @foreach($posts as $ii=>$post)

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  <tbody><tr>
    <td width="100%" valign="top" bgcolor="#ffffff" align="center">
    
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
        <tbody><tr>
          <td align="center">
            
            <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
              <tbody><tr>
                <td width="100%" align="center">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                  <tbody>
                    <tr>
                      <td width="20" align=""></td>

                      <td width="201" align="right">
                  <!-- Image 101 - 2 -->
                  <table width="201" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="">
                    <tbody><tr>
                      <td width="100%" >
                        <a title="{{{ $post->title }}}" href="{{ $post->url() }}" style="border:0px; text-decoration:none;" >
                          <img src="{{ $post->cover->image->url('medium') }}"  class=" " width="201" style="border:1px solid #AAA;max-width:201px;"  />
                        </a>
                      </td>
                    </tr>
                  </tbody></table>
                      </td>
                      <td width="9">
                        &nbsp;
                      </td>
                      <td width="350">                  <!-- Text -->
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;width:300px;" class="">
                    <tbody><tr>
                      <td valign="top" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 18px; color: #3a3a3a; line-height: 26px; font-weight: 600; " class="">
                        <a title="{{{ $post->title }}}" href="{{ $post->url() }}" style="display:block;color:black; text-decoration:none;border:0px;">
                      {{{ mzk_str_trim($post->title, 50) }}}
                    </a>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="5"></td>
                    </tr>
                    <tr>
                      <td valign="top" width="100%" style="text-align: left; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 13px; color: #3a3a3a; line-height: 22px; font-weight: 400;" class="fullCenter">
                        <i><a style="color:#000;display:inline-block; text-decoration:none;border:0px;" href="{{route('users.profile.show', $post->author->id)}}">
                          {{ $post->authors_full_name }}
                          </a></i> - <span style="font-weight: 700; color: #d1a663;">{{ $post->authors_designation }}</span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="5"></td>
                    </tr>
                    <tr>
                      <td valign="top" width="100%" style="text-align: left; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 13px; color: #a3a3a3; line-height: 22px; font-weight: 400;" class="fullCenter">
                        {{{ mzk_str_trim($post->caption, 125) }}} <a href="{{ $post->url() }}" style="text-decoration: none; color: #3a3a3a; font-weight: 600; font-style: italic;" class="underline">Continue Reading</a>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="5"></td>
                    </tr>
                    <tr>
                      <td width="100%" class="image16">
                        <span style="font-size:11px;"><img style="width:22px;" src="https://s3.amazonaws.com/mazkaracdn/css/images/icon-views.png" /> {{ $post->views }} Views</span>
                        &nbsp;
                        <span style="font-size:11px;"><img style="width:16px;" src="https://s3.amazonaws.com/mazkaracdn/css/images/icon-likes.png" />{{ $post->num_likes() }} Likes</span>
                      </td>
                    </tr>
                  </tbody></table>
</td>
                      <td width="19" align=""></td>

                    </tr>
                  </tbody>
                </table>                </td>
              </tr>
            </tbody></table>
            
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
              <tbody><tr>
                <td width="100%" height="10"></td>
              </tr>
              <tr>
                <td width="100%" height="1" bgcolor="#f1f1f1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody></table>
            
          </td>
        </tr>
      </tbody></table>
    
    </td>
  </tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
  <tbody><tr>
    <td width="100%" height="10">
      <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/email-space-filler.jpg" height="1" style="width:600px;" />

    </td>
  </tr>
</tbody></table>

@endforeach


<table width="100%" border="0" cellpadding="0"  bgcolor="#e7e7e7" cellspacing="0" align="center" class="full">
  <tbody>
    <td width="100%" valign="top" bgcolor="#e7e7e7" align="center">
    
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
        <tbody>
            <tr><td height="30">
                        <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/email-footer-filler.jpg" height="1" style="width:600px;" />

            </td></tr><tr>
            <tr>
              <td width="100%" align="center" style="padding-top:15px;">

                  
             
              <a style="text-decoration:none;" href="https://play.google.com/store/apps/details?id=com.mazkara.user" target="_blank">
                <img height="45" src="https://s3.amazonaws.com/mazkaracdn/assets/home/google-store.png">
              </a>
              <a style="text-decoration:none;" href="https://itunes.apple.com/us/app/id1086643190" target="_blank">
                <img height="45" src="https://s3.amazonaws.com/mazkaracdn/assets/home/apple-store.png">
              </a>
              </td>
              </tr>
            <tr><td height="10"></td></tr><tr>


              <tr>
                <td height="70" valign="middle" align="center" style="padding:10px;">
                  <div class="contentEditableContainer contentTextEditable">
                    <div class="contentEditable" align="center" >
                      <table border="0" cellspacing="0" cellpadding="0" align='center'>
                        <tr>
                          <td valign="top"  width='55'>
                            <div class="contentEditableContainer contentFacebookEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://www.facebook.com/gofabogo" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/facebook.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='facebook' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://www.linkedin.com/company/fabogo" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/linkedin.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='linkedin' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://instagram.com/gofabogo?" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/instagram.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='instagram' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentTwitterEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://twitter.com/gofabogo"data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/twitter.jpg"  data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='twitter' style='margin-right:40x;'>
                              </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a target='_blank' href="https://plus.google.com/+Fabogo" data-default="placeholder"  style="text-decoration:none;">
                                  <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/google.jpg"  width="40" height="40" data-max-width="40" alt='Google plus' style='margin-right:0px;' />
                                </a>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>



                    </div>
                  </div>
                </td>
              </tr>
              <tr><td height="20"></td></tr>
      </tbody></table>
    </td>
  </tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  <tbody><tr>
    <td width="100%" valign="top" bgcolor="#ffffff" align="center">
    
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
        <tbody><tr>
          <td align="center">
            
            <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
              <tbody><tr>
                <td width="100%" align="center">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                    <tbody><tr>
                      <td width="100%" height="15"></td>
                    </tr>
                  </tbody></table>
                  
                  <!-- Left -->
                  <table width="280" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                    <tbody><tr>
                      <td valign="top" width="100%" style="text-align: left; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 13px; color: #a3a3a3; line-height: 22px; font-weight: 400; font-style: italic;" class="fullCenter">
                        © 2016 Mazkara FZ LLC - All Rights Reserved
                        <p>This newsletter was sent to [Email] from www.fabogo.com because you subscribed. </p>
                      </td>
                    </tr>
                  </tbody></table>
                  
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                    <tbody><tr>
                      <td width="100%" height="15"></td>
                    </tr>
                  </tbody></table>
                  
                  <!-- Right -->
                  <table width="280" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                    <tbody><tr>
                      <td valign="top" width="100%" style="text-align: right; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 13px; color: #a3a3a3; line-height: 22px; font-weight: 400; font-style: italic;" class="fullCenter">
                        Rather not receive our newsletter anymore? [unsubscribe] instantly.
                      </td>
                    </tr>
                  </tbody></table>
                  
                </td>
              </tr>
            </tbody></table>
            
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
              <tbody><tr>
                <td width="100%" height="14"></td>
              </tr>
              <tr>
                <td width="100%" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody></table>
            
          </td>
        </tr>


      </tbody></table>
    
    </td>
  </tr>
</tbody></table>
</body>
</html>