@extends('layouts.scaffold')

@section('main')

<h1>All Deals</h1>

<p>{{ link_to_route('deals.create', 'Add New Deal', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($deals->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Caption</th>
				<th>Description</th>
				<th>Fine_print</th>
				<th>Offer_amount</th>
				<th>Original_amount</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($deals as $deal)
				<tr>
					<td>{{{ $deal->title }}}</td>
					<td>{{{ $deal->caption }}}</td>
					<td>{{{ $deal->description }}}</td>
					<td>{{{ $deal->fine_print }}}</td>
					<td>{{{ $deal->offer_amount }}}</td>
					<td>{{{ $deal->original_amount }}}</td>
					<td>{{{ $deal->starts }}}</td>
					<td>{{{ $deal->ends }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('deals.destroy', $deal->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('deals.edit', 'Edit', array($deal->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no deals
@endif

@stop
