@section('header')
  <script type="text/javascript">var switchTo5x=true;</script>
  <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
  <script type="text/javascript">stLight.options({publisher: "9693b6f5-c590-4134-a42a-29b6a775ceab", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
@stop
<div class="row">
  <div class="col-md-9">
    <div class="row">
      <div class="col-md-12">
  <div class="header pb10 mb10">
    <h1 class="item-name fs200 green">
      {{ $deal->title }}
    </h1>
  </div>

      <div class="pull-right">
        <span><b>{{$business->reviews_count}}</b> votes</span>
          {{ ViewHelper::rateBusinessNiblet($business)}}<br/>

      </div>
      <h2 class="lead mb5">
        <a  href="{{ MazkaraHelper::slugSingle($business) }}">{{$business->name}}</a>
      </h2>

      <p class="fs125"><i class="fa fa-pencil" style="width:30px"></i> {{ count($business->reviews) }} Reviews</p>
      <p class="fs125"><i class="fa fa-camera" style="width:30px"></i> {{ count($business->photos) }} Photos</p>
      <p class="fs125"><i class="fa fa-map-marker" style="width:30px"></i> {{ count($business->check_ins) }} Check Ins</p>
    <div class="row hidden">
      <div class="col-md-12">
        <div class="mt5">
          @if(Auth::check())
            <a href="#" class="btn btn-small {{ Auth::check()? ($deal->favourited(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default small-text" rel="{{$deal->id}}" data-route="deals">
              <i class="fa fa-heart"></i> 
            </a>
          @else
            <a href="/users/login" class="btn btn-small ajax-popup-link btn-default small-text" rel="{{$deal->id}}">
              <i class="fa fa-heart"></i> 
            </a>
          @endif
        </div>
      </div>
    </div>

      </div>
    </div>
    <div class="row">
    <div class="col-md-3">
      <div class="well p15 alert-danger bg-white fs200">
        <span class="green ">{{ ViewHelper::money($deal->offer_amount) }}</span>
      </div>
      @if($deal->type!='package')
        <div class="well p15 alert-danger bg-white fs125">
          <p class="">
            @if($deal->hasEnded())
            <i class="fa fa-stop"></i> 
              Deal has ended
            @else
            <i class="glyphicon glyphicon-time"></i> 
              Deal ends {{{ Date::parse($deal->ends)->ago()  }}}
            @endif
          </p>
        </div>
      @elseif(!$deal->isActive)
        <div class="well p15 alert-danger bg-white fs125">
          <p class="">
            <i class="fa fa-stop"></i> 
              Deal has ended
          </p>
        </div>
      @endif

        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
        <div id="item-map" class="ba item-map  mb10" style="height:400px"></div>
        
        @include('site.businesses.partials.show.categories', ['business'=>$business])
        @include('site.businesses.partials.show.services', ['business'=>$business])


        @include('site.businesses.partials.show.timings', ['business'=>$business])
        @include('site.businesses.partials.show.highlights', ['business'=>$business])

  </div>


    	<div class="col-md-9">
        @if($deal->artworks->count() >0)
        <h4 class="item-headers  pb10 txttrans fw300">Photos</h4>
          <div class="photos-holder">
            <div class="pb10 pt10 lead">

              @foreach($deal->artworks as $one_photo)
                <a href="{{ $one_photo->image->url() }}" class="mr10 ba mb15" style="display:inline-block"  ><img src="{{ $one_photo->image->url('thumbnail') }}" class="img-rounded" /></a>
              @endforeach
            </div>  
        </div>
        @endif
    		<h4 class="item-headers pt10 pb10 txttrans fw300">Description</h4>
    		<div class="pb10 pt10 html-fix">
		      {{ $deal->description }}
		    </div>


		  	<h4 class="item-headers pt10 pb10 txttrans fw300">Terms and Conditions</h4>
    		<div class="pb10 pt10  html-fix ">
		  		{{ $deal->fine_print }}
				</div>
    @if(count($business->deals) > 0)
    <div class="row">
      <div class="col-md-12">
      <h4 class="item-headers pt10 pb10 txttrans fw300   pb10 mb10">Other Deals</h4>
      <p></p>
      </div>
    </div>
    <div class="deals-holder row list-group">
      @foreach($business->deals as $one_deal)
                @include('site.businesses.partials.deal-niblet')
        @endforeach
    </div>
    @endif


    	</div>
    </div>
  </div>
  <div class="col-md-3">
    <small class=" pb10 mb5">FEATURED</small>
    @foreach($suggested_spas as $business)
      @include('site.businesses.partials.native-ad')
    @endforeach


  </div>
</div>
@section('js')

<script type="text/javascript">
$(function(){
  $('.__carousel').carousel();
  map = new GMaps({
    div: '#item-map',
    lat:{{ $business->geolocation_latitude }},
    lng:{{ $business->geolocation_longitude }}
  });

  map.addMarker({
    lat:{{ $business->geolocation_latitude }},
    lng:{{ $business->geolocation_longitude }}
  });


  $('.photos-holder').magnificPopup({
    delegate: 'a', 
    type: 'image',
    gallery: {
      enabled: true
    }
  });





});

</script>
@stop