<?php 
  $user = $review->user;
  $show_links = isset($show_links)?$show_links:true;
  $show_comments = isset($show_comments)?$show_comments:true;
  $show_border = isset($show_border)?$show_border:true;
  $show_business = isset($show_business)?$show_business:false;

?>
@if(Auth::check())
  <?php $usr = User::find(Auth::user()->id);?>
@else
  <?php $usr = false;?>
@endif
<div class="media  pb10 {{ $show_border?'mb10 mt0 bb':'mt0'}}">
    @if($show_links)
    <div class="pull-right">

        @if(Auth::check())
          <a href="javascript:void(0)" class="sharable btn-xs btn-default btn" data-sharable="Review" data-sharableid="{{$review->id}}"  title="Share on Facebook">
            <i class="fa fa-share"></i>
          </a>
          @if($user->id == Auth::user()->id)
            <a href="#reviews" class="review-btn btn btn-xs btn-default"><i class="fa fa-edit"></i></a>

              {{ Form::open(array('style' => 'display: inline-block;', 
                                  'class'=>'pull-right confirmable',  'method' => 'DELETE', 
                                  'route' => array('reviews.destroy', $review->id))) }}
                &nbsp;<a href="javascript:void(0)" class="btn btn-xs btn-default submit-parent-form"><i class="fa fa-times"></i></a>

              {{ Form::close() }}

          @endif
        @endif

    </div>
    @endif

  <div class="mb10 mt10 bg-lite-gray p10 pb5">
    @include('site.reviews.partials.user-niblet', ['user'=>$user, 'review'=>$review])
  </div>
  @if($show_business)
  <div class="pb10 ">
        <span class="pull-left mr10">
          <a href="{{MazkaraHelper::slugSingle($business)}}">

            {{ViewHelper::businessThumbnail($business, [ 'meta'=>'medium', 'width'=>'60',  
                                  'style'=>'width:60px; padding-top:3px;'])}}
          </a>
        </span>
        <h5 class="search-item-name media-heading mb0 notransform bolder ">
          <a title="{{{ $business->name }}}" href="{{ MazkaraHelper::slugSingle($business) }}" class="result-title fs80">{{{ $business->getTrimmedName(26) }}} </a>
        </h5>

        <span title="{{ $business->zone_cache }}" class="search-item-address fs90">
          › {{ $business->zone_cache }}
        </span>


  </div>
  <div class="clearfix"></div>
  @endif
  <div>
    <div class="pb5">
    <b>Rated: {{ ViewHelper::starRateSmallBasic($review, 'xs')}} 
      @if(count($review->services)>0)
        for 
        {{ join(', ',$review->services()->lists('name', 'name')->all())}}
      @endif
    </b></div>
    <div>
    {{ nl2br($review->body) }}
  </div>
  </div>
  @if($show_comments)
    <div class="mt10 pt10 fs90">
      @foreach($review->comments as $comment)
        <div class="media pb10 pt10 bb">
          @if(Auth::check())
            @if($comment->isDeletableBy(Auth::user()))
              {{ Form::open(array('style' => 'display: inline-block;', 
                                  'class'=>'pull-right confirmable',  'method' => 'DELETE', 
                                  'route' => array('comments.destroy', $comment->id))) }}
                <a href="javascript:void(0)" class="submit-parent-form"><i class="fa fa-times"></i></a>
              {{ Form::close() }}
            @endif
          @endif
        <div class="media-left">
          <a href="/users/{{$comment->user_id}}/profile">
            {{ ViewHelper::userAvatar($comment->user, ViewHelper::$avatar35) }}
          </a>
        </div>
        <div class="media-body dark-gray">
          <b>{{$comment->getDisplayableUsersName()}}</b> <span class="medium-gray">{{$comment->body}}</span>
          <p>
            <small class="gray">
              {{{ Date::parse($comment->updated_at)->ago() }}}
            </small>
          </p>
        </div>
      </div>
      @endforeach
    </div>
    @endif
  @if((1==2))
    <div class="media pb10 pt10 ">
  <div class="media-left">
    <a href="/users/{{$user->id}}/profile" >
      {{ViewHelper::userAvatar($usr, ViewHelper::$avatar35)}}
    </a>
  </div>
  <div class="media-body" style="width: 100%;">

    {{ Form::open(array('route' => 'comments.store')) }}
      {{ Form::hidden('commentable_type', 'Review')}}
      {{ Form::hidden('commentable_id', $review->id)}}
      {{ Form::hidden('type', 'comment')}}
      {{ Form::text('body', '', ['class'=>'form-control count-limiter', 'placeholder'=>'Write a comment'])}}
    {{ Form::close()}}
  </div>
</div>
  @endif

</div>
