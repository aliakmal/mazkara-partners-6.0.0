<div class="media pb5 mt5 ">
  <div class="pull-right">
  </div>
  <div class="media-left">
    <a href="/users/{{$user->id}}/profile" >
      {{ViewHelper::userAvatar($user)}}
    </a>
  </div>
  <div class="media-body text-left">
    <b class="media-heading   pt5">
      <a title="{{{ $user->full_name }}}" href="/users/{{$user->id}}/profile" class="color-black result-title">
        <span >
          <span >{{{ $user->full_name }}}</span>
        </span> 
      </a>
    </b>
    <small class="medium-gray">
      &nbsp;&nbsp;<i>Posted {{{ Date::parse($review->created_at)->ago() }}}</i>
    </small>

    <br/>
    <small class=" ">{{ join(', ', $user->countersArray())}}</small>

  </div>
</div>