@extends('layouts.master')
@section('content')
<div class="container">    
  <div id="loginbox" class="mainbox ">                    
    <div class="" >
      <div class="row">
        <div class="col-md-4 col-md-offset-1 hide-only-mobile">

          <div class="p10 m10">
          <div class="p30">
            <div class="dpib va-middle p10" ><img style="width:30px;"  src="https://s3.amazonaws.com/mazkaracdn/assets/home/01.png"/></div>
            <div class="dpib  va-middle pl10">Discover Salons, Spas and Fitness<br/>Centers in your Neighbourhood.  </div>
          </div>
          <div class=" p30">
            <div  class="dpib va-middle" ><img style="width:50px;" src="https://s3.amazonaws.com/mazkaracdn/assets/home/02.png"/></div>
            <div class="dpib va-middle pl10  va-middle">Search for exclusives specials and<br/>offers published directly by the venue.</div>
          </div>
          <div class="p30">
            <div class="dpib va-middle"><img  style="width:50px;" src="https://s3.amazonaws.com/mazkaracdn/assets/home/03.png"/></div>
            <div class="dpib va-middle pl10">Write reviews for your favourite spas<br/>salons and fitness centers.</div>
          </div>
          <div class="p30">
            <div class="dpib va-middle"><img  style="width:50px;" src="https://s3.amazonaws.com/mazkaracdn/assets/home/04.png"/></div>
            <div class="dpib  pl10  va-middle">Stay tuned, with beauty trends, tricks<br/>and tips through Fabogo Stories. </div>
          </div>
        </div>
        </div>
        <div class="col-md-4 col-md-offset-2">


      <div class="p30 mr15" >
      <div class="p10 text-center">REGISTER ON FABOGO</div>
        @include('elements.messages')
        <div class=" text-center mt10 ">
          <a id="btn-fblogin" href="/users/facebook/login" class="dpb no-border-radius mb10 facebook-login-link btn btn-primary">
            <i class="fa fa-facebook-square"></i> FACEBOOK
          </a>
          <p>Highly recommended - also we will never post anything without your permission.</p>
          <a id="btn-gllogin" href="/users/google/login" class="btn no-border-radius btn-danger dpb mb10">
            <i class="fa fa-google"></i> GOOGLE
          </a>
        </div>
        <div class=" text-center mb10 ">
          <p>or</p>
        </div>

        {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->action('/users')->encodingType('multipart/form-data') }}
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="text" tabindex="1" placeholder="FULL NAME" name="name" value="{{{ Input::old('name') }}}" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="text" tabindex="1" placeholder="EMAIL ADDRESS" name="email" value="{{{ Input::old('email') }}}" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="password" tabindex="1" placeholder="PASSWORD" name="password" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="password" tabindex="1" placeholder="PASSWORD CONFIRMATION" name="password_confirmation" />

          {{ BootForm::token() }}
            <div class="mt10   ">
              <div class="mt10  ">
                <button id="btn-login" style="width:100%"  class="btn dpb btn-turquoise p10 text-center no-border-radius" >SIGN UP</button>
              </div>
            </div>

              <div class="text-center p10 text-danger" >
                Have an account? 
                <a href="/users/login" >
                  Login Here
                </a>
              </div>

      {{ BootForm::close() }}

              <div class="">
                <div class="text-center  fs80">By signing in, you agree to Fabogo's <a href="/tos" class="text-muted">Terms of Service</a>, 
                  <a href="/cookie-policy" class="text-muted">Cookie Policy</a>, <a class="text-muted" href="/privacy-policy">Privacy Policy</a> and
                  <a href="/content-policy" class="text-muted">Content Policies</a>.
                </div>

              </div>


      </div>  
    </div>
  </div>                   
    </div>  
  </div>
</div>
@endsection