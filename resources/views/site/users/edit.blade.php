@extends('layouts.master')
@section('content')
<div class="row">
  <div class="mainbox col-md-6 col-md-offset-2">
    <h1 class="notransform ">Edit your profile</h1>
  </div>                    
</div>
<div class="row">    
<script src="https://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>

  <div id="loginbox" class="mainbox col-md-6 col-md-offset-2">                    

   @if (Session::get('error'))
      <div class="alert alert-error alert-danger">
        {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
      </div>
    @endif
    @if (Session::get('notice'))
      <div class="alert alert-success">
        {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
      </div>
    @endif
    {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->action('/users/update-account')->encodingType('multipart/form-data') }}
        {{ BootForm::bind($user) }}
    <div class="panel panel-default" >

      <div class="panel-heading">
        <div class="panel-title">Profile</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
        {{ BootForm::text('Full name', 'name')->placeholder('Full Name') }}
        {{ BootForm::file('Avatar', 'avatar') }}
        @if(isset($user))
          @if(count($user->avatar)>0)
            <a href="{{ $user->avatar->image->url('medium') }}" class="lightbox"><img src="{{ $user->avatar->image->url('thumbnail') }}" class="img-thumbnail" /></a>
            {{ Form::checkbox("deletablePhotos", '1', false, array('class'=>'deletable') ) }} Check to Delete
          @endif
        @endif
        {{ BootForm::textarea('About', 'about')->placeholder('A bit about you') }}
        {{ BootForm::text('Where are you', 'location')->placeholder('Your location') }}

        {{ BootForm::select('Gender', 'gender', array('male'=>'Male', 'female'=>'Female')) }}
        @if(isset($user))
          @if($user->hasROle('admin') || $user->can('manage_posts'))
            {{ BootForm::text('Designation', 'designation')->placeholder('Designation') }}
            {{ BootForm::text('Designation At', 'designated_at_text')->placeholder('Company name, website, blog, twitter handle?') }}
            {{ BootForm::text('Link to it', 'designated_at_url')->placeholder('Url to above if any') }}
            {{ BootForm::text('Contact Email Address', 'contact_email_address')->placeholder('Email Address to contact you? Optional') }}
          @endif
        @endif

        {{ BootForm::text('Twitter Handle', 'twitter')->placeholder('Twitter Handle') }}
        {{ BootForm::text('Instagram', 'instagram')->placeholder('Instagram') }}
      </div>
      <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn  btn-primary')) }}
      </div>

    </div>
      {{ BootForm::close() }}

  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->action('/users/edit')->encodingType('multipart/form-data') }}
      {{ BootForm::bind($user) }}
    <div class="panel panel-default" >

      <div class="panel-heading">
        <div class="panel-title">Account</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >

          {{ BootForm::email('Email', 'email')->placeholder('Email address') }}
          {{ BootForm::password('Password', 'password')->placeholder('Password') }}
          {{ BootForm::password('Confirm Password', 'password_confirmation')->placeholder('password confirmation') }}

          {{ BootForm::token() }}


      </div>
                  <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn  btn-primary')) }}
      </div>
          
    </div>       
    {{ BootForm::close() }}
 
  </div>
  <div class="col-md-3">
    <h3 class="mt0 pt0 bb pb10">Connected Accounts</h3>
    @if(count($user->accounts) == 12220)
      <div class="well">
        <p class="lead">No social media accounts attached? Connect to a social media account and share your discoveries with your friends!</p>
      </div>
    @endif
    @foreach($user->accounts as $account)
      @if($account->provider == 'facebook')
        <p>
          <span class="btn btn-primary"><i class="fa fa-facebook"></i></span> Connected Facebook Account 
          <a href="/users/disconnect/facebook"><i class="fa fa-times"></i></a>
        </p>
      @endif
      @if($account->provider == 'google')
        <p>
          <span class="btn btn-danger"><i class="fa fa-google"></i></span> Connected Google Account
          <a href="/users/disconnect/google"><i class="fa fa-times"></i></a>
        </p>
      @endif
    @endforeach
    @if(1==2)
    <p>
      <a href="/users/connect/facebook" class="btn btn-primary dpb"><i class="fa fa-facebook"></i> Connect Facebook Account</a>
    </p>
    <p>
      <a href="/users/connect/google" class="btn btn-danger dpb"><i class="fa fa-google"></i> Connect Google Account</a>
    </p>
    @endif
  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$(function(){
  input = $('input#location');
  console.log(input);
  var autocomplete = new google.maps.places.Autocomplete(input);
})
</script>
@stop