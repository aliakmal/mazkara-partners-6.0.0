@if(isset($post))
  {{ Form::model($post, array('class' => 'form-vertical', 'files'=>true, 
                              'method' => 'PATCH', 
                              'route' => array('videos.update', $post->id))) }}
@else
  {{ Form::open(array('route' => 'videos.store', 'files'=>true, 'class' => 'form-vertical')) }}
@endif


@if ($errors->any())
  <div class="alert alert-danger">
      <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
  </div>
@endif



<div class="form-group">
  {{ Form::label('title', 'Title:', array('class'=>' float-none control-label')) }}
    {{ Form::text('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
</div>

@if(isset($post))
  <div class="form-group">
    {{ Form::label('slug', 'Slug:', array('class'=>'float-none control-label')) }}
      {{ Form::text('slug', Input::old('slug'), array('class'=>'form-control', 'placeholder'=>'Slug')) }}
  </div>
@endif

@if(isset($post))
<div class="well">
  {{ $post->body }}
</div>
@else
<div class="form-group">
  {{ Form::label('body', 'YouTube URL:', array('class'=>'float-none control-label')) }}
  {{ Form::text('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'YouTube URL')) }}
</div>
@endif

<div class="form-group">
  {{ Form::label('caption', 'Caption:', array('class'=>'float-none control-label')) }}
  {{ Form::textarea('caption', Input::old('caption'), array('class'=>'form-control', 'maxlength'=>'300', 'placeholder'=>'Caption')) }}
</div>

<div class="form-group">
  {{ Form::label('services', 'Services', array('class'=>'float-none control-label')) }}
  <select name="services[]" multiple="multiple" data-live-search="true" id="service_ids" class="form-control">
    @foreach (Service::query()->showParents()->orderby('name', 'asc')->get() as $parent)
      <optgroup label="{{$parent->name}}">
        @foreach ($parent->kids()->showActive()->orderby('name', 'asc')->get() as $service)
          @if($service->isActive())
            <option value="{{$service->id}}" >{{$service->name}}</option>
          @endif
        @endforeach
      </optgroup>
    @endforeach
  </select>
</div>


<div class="form-group pull-left">
  {{ Form::submit('Save Post', array('class' => 'btn dpb btn-lg btn-primary')) }}
</div>

{{ Form::close() }}

@if(isset($post))
  @if($post->isEditableBy(Auth::user()->id))
    {{ Form::open(array('style' => 'float:right;display: inline-block;', 'method' => 'DELETE', 'route' => array('posts.destroy', $post->id))) }}
      {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
  @endif
@endif

@section('js')
<script type="text/javascript">
$(function(){

  $('#service_ids').selectpicker({maxOptions:1});

  @if(isset($post))
    $('#service_ids').selectpicker('val', [{{ join(',', $post->services()->lists('service_id','service_id')->all()) }}]);
  @endif


  $('.dateinput').datepicker({ dateFormat: "yy-mm-dd", autoclose:true });
});


</script>

@stop