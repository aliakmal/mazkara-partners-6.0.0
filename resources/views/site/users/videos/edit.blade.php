@extends('layouts.parallax')
@section('content')


<div class="container">
<div class="row">
  <div class="col-md-9">
    <div class="row">
      <div class="col-md-3 pr0 pt20 br">
        <div class="header show-on-mobile text-center">
          {{ViewHelper::userAvatar($user, ViewHelper::$avatar80)}}

          <h3 class="item-name burgundy">
            {{ $user->name }}
          </h3>

        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="mt10 pr10">
              @if(Auth::check())
                @if($user->id != Auth::user()->id)
                @else
                  <a href="/users/edit" class="btn btn-small btn-block btn-default small-text">
                    <i class="fa fa-cog"></i> Settings
                  </a>

                @endif
                @if(Auth::user()->hasRole('admin'))
                  <a href="/content/accounts/{{$user->id}}" class="btn btn-small btn-block btn-default small-text">
                    <i class="fa fa-shield"></i> Edit as Admin
                  </a>
                @endif
              @endif

            </div>
          </div>
        </div>


        <div role="tabpanel ">
        </div>
      </div>
      <div class="col-md-8 bg-white pt20">
        <h3 class="item-name burgundy ">Edit Video</h3>
        @include('site.users.videos.form')
      </div>
    </div>
  </div>
  <div class="col-md-3 pt20">

  </div>
</div>
</div>
@stop

@section('js')

<script type="text/javascript">
$(function(){
  $('.jscroll').jscroll({
      padding: 20,
      loadingHtml: '<p><b>Loading more...</b></p>',

      nextSelector: '.pagination a[rel=next]',
      contentSelector: '.single-activity-element'
  });
  
  $(".popover-mobile").popover({
        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });


  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });



})
</script>
@stop
