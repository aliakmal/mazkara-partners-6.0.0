<div class="media pt5 pb10 mb10 mt0 bb ">
  <div class="mb5">
  <div class="media pb5 mt5 ">
  <div class="media-left">
    <a href="{{MazkaraHelper::slugSingle($business)}}">
          {{ViewHelper::businessThumbnail($business, ['width'=>'40', 'height'=>'40', 'meta'=>'small', 'class'=>'img-rounded', 
                                'style'=>'width:40px; height:40px; padding-top:3px;'])}}
    </a>
  </div>
  <div class="media-body">
    <b class="media-heading   pt5">
      <a title="{{{ $business->name }}}"  href="{{MazkaraHelper::slugSingle($business)}}" class="ovf-hidden  result-title">
        {{{ $business->name }}} 
      </a>
    </b><small class="medium-gray">
      &nbsp;&nbsp;<i>Posted {{{ Date::parse($review->created_at)->ago() }}}</i>
    </small><br/>
    <small title="{{ $business->geolocation_address }}" class="search-item-address">
      {{ $business->zone_cache }}
    </small><br/>
    
</div>
  </div>
</div>
  <div>
    <div class="pb5">
    <b>
      <span class="dpib"><span class="dpib">RATED:</span>&nbsp;&nbsp;{{ ViewHelper::starRateSmallBasic($review, 'xs')}}</span>
      @if(count($review->services)>0)
        for 
        @foreach($review->services()->lists('name', 'name')->all() as $s)
          <span class="fs90 dpib p5 pt0 pb0 medium-gray bayellow border-radius-10 mr5">{{ $s }}</span>
        @endforeach
      @endif
    </b></div>
    <div>
    {{ nl2br($review->body) }}
  </div>
  </div>
</div>

