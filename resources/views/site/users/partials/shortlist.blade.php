<div class="media pb10 ">
  <div class="pull-right">
    <a href="javascript:void(0)" title="Favorite {{ $business->name }}" class="btn btn-circle {{ Auth::check()? ($business->favourited(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default small-text" data-route="businesses" rel="{{$business->id}}">
      <i class="fa fa-heart"></i> 
    </a>


  </div>
  <div class="media-left">
    <a href="{{MazkaraHelper::slugSingle($business)}}">
      {{ViewHelper::businessThumbnail($business, ['height'=>'60', 'width'=>'90', 'meta'=>'medium', 'class'=>' ', 
                            'style'=>'height:60px;width:90px; padding-top:3px;'])}}
    </a>
  </div>
  <div class="media-body">
    <b class="media-heading   pt5">
      <a title="{{{ $business->name }}}"  href="{{MazkaraHelper::slugSingle($business)}}" class="ovf-hidden result-title">
        {{{ $business->name }}} 
      </a>
    </b><br/>
    <small title="{{ $business->geolocation_address }}" class="search-item-address">
      {{ $business->zone_cache }}
    </small><br/>
     {{ ViewHelper::starRateSmallBasic($business->average_rating)}}

  </div>
  <div class="clearfix"></div>
</div>
