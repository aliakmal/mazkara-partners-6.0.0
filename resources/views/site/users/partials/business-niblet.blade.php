<div class="media pb10 bb">
  <div class="pull-right">
    <a href="javascript:void(0)" title="Favorite {{ $business->name }}" class="btn btn-circle {{ Auth::check()? ($business->favourited(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default small-text" data-route="businesses" rel="{{$business->id}}">
      <i class="fa fa-heart"></i> 
    </a>


  </div>
  <div class="media-left">
    <a href="{{MazkaraHelper::slugSingle($business)}}">
          {{ViewHelper::businessThumbnail($business, ['width'=>'40', 'height'=>'40', 'meta'=>'small', 'class'=>'img-rounded', 
                                'style'=>'width:40px; height:40px; padding-top:3px;'])}}
    </a>
  </div>
  <div class="media-body">
    <b class="media-heading   pt5">
      <a title="{{{ $business->name }}}"  href="{{MazkaraHelper::slugSingle($business)}}" class="ovf-hidden burgundy-item result-title">
        {{{ $business->name }}} 
      </a>
    </b><br/>
    <small title="{{ $business->geolocation_address }}" class="search-item-address">
      {{ $business->zone_cache }}
    </small><br/>

  </div>
</div>
