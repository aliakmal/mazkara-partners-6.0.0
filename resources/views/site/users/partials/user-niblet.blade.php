<div class="media pb5 mt5 ">
  <div class="pull-right">
<!--     
    @if(Auth::check())
      @if($user->id != Auth::user()->id)
        <a href="javascript:void(0)" class="btn btn-circle mt5 user-follow {{ $user->isFollowed(Auth::user()->id) ? 'favourited' : 'favourite' }} btn-default small-text" title="{{ ($user->isFollowed(Auth::user()->id)? 'Following' : 'Follow ') }} {{$user->full_name}}"  data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
          <i class="fa fa-user-plus"></i> 
        </a>
      @endif
    @else
      <a href="javascript:void(0)" class="btn btn-circle mt5 user-follow btn-default small-text" title="Follow {{$user->full_name}}"  data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
        <i class="fa fa-user-plus"></i> 
      </a>      
    @endif
  -->
  </div>
  <div class="media-left">
    <a href="/users/{{$user->id}}/profile" >
      {{ViewHelper::userAvatar($user)}}
    </a>
  </div>
  <div class="media-body text-left">
    <b class="media-heading   pt5">
      <a title="{{{ $user->full_name }}}" href="/users/{{$user->id}}/profile" class="result-title">
        <span >
          <span >{{{ $user->full_name }}}</span>
        </span> 
      </a>
    </b><br/>
    <small class=" ">{{ join(', ', $user->countersArray())}}</small>

  </div>
</div>