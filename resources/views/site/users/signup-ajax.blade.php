<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">                    
  <div class="background-login-screen" >
    <div class="row">
      <div class="col-md-6 hide-only-mobile">
        <div class="p10 m10">
          <div class="p30">
            <div class="dpib va-middle p10" ><img style="width:30px;"  src="https://s3.amazonaws.com/mazkaracdn/assets/home/01.png"/></div>
            <div class="dpib  va-middle pl10">Discover Salons, Spas and Fitness<br/>Centers in your Neighbourhood.  </div>
          </div>
          <div class="p30">
            <div class="dpib va-middle"><img  style="width:50px;" src="https://s3.amazonaws.com/mazkaracdn/assets/home/02.png"/></div>
            <div class="dpib pl10  va-middle">Search for exclusives specials and<br/>offers published directly by the venue.</div>
          </div>
          <div class="p30">
            <div class="dpib va-middle"><img  style="width:50px;" src="https://s3.amazonaws.com/mazkaracdn/assets/home/03.png"/></div>
            <div class="dpib va-middle pl10">Write reviews for your favourite spas<br/>salons and fitness centers.</div>
          </div>
          <div class="p30">
            <div class="dpib va-middle"><img  style="width:50px;" src="https://s3.amazonaws.com/mazkaracdn/assets/home/04.png"/></div>
            <div class="dpib  pl10  va-middle">Stay tuned, with beauty trends, tricks<br/>and tips through Fabogo Stories. </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
      <div class="p30 mr15" style="padding-left:70px" >
        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
        <div class=" text-center mt10 ">
          <a id="btn-fblogin" href="/users/facebook/login" class="dpb no-border-radius mb10 facebook-login-link btn btn-primary">
            <i class="fa fa-facebook-square  pull-left mt2"></i> FACEBOOK
          </a>
          <p>Highly recommended - also we will never post anything without your permission.</p>
          <a id="btn-gllogin" href="/users/google/login" class="btn no-border-radius btn-danger dpb mb10">
            <i class="fa fa-google  pull-left mt2"></i> GOOGLE
          </a>
        </div>
        <div class=" text-center mb10 ">
          <p>or</p>
        </div>

        {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->action('/users')->encodingType('multipart/form-data') }}

         @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
              {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
            </div>
          @endif
          @if (Session::get('notice'))
            <div class="alert alert-success">
              {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
            </div>
          @endif
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="text" tabindex="1" placeholder="FULL NAME" name="name" value="{{{ Input::old('name') }}}" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="text" tabindex="1" placeholder="EMAIL ADDRESS" name="email" value="{{{ Input::old('email') }}}" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="password" tabindex="1" placeholder="PASSWORD" name="password" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb10 fs90" data-validation="required" type="password" tabindex="1" placeholder="PASSWORD CONFIRMATION" name="password_confirmation" />
          <input type="hidden" name="redirectto" value=" {{ ($_SERVER['HTTP_REFERER']) }}" />
          {{ BootForm::token() }}
            <div class="mt10   ">
              <div class="mt10  ">
                <button id="btn-login" style="width:100%"  class="btn dpb btn-turquoise p10 text-center no-border-radius" >SIGN UP</button>
              </div>
            </div>

              <div class="text-center p10 text-danger" >
                Have an account? 
                <a href="/users/login"  id="lnk-sign-in"  >
                  Login Here
                </a>
              </div>

      {{ BootForm::close() }}

              <div class="">
                <div class="text-center  fs80">By signing in, you agree to Fabogo's <a href="/tos" class="text-muted">Terms of Service</a>, 
                  <a href="/cookie-policy" class="text-muted">Cookie Policy</a>, <a class="text-muted" href="/privacy-policy">Privacy Policy</a> and
                  <a href="/content-policy" class="text-muted">Content Policies</a>.
                </div>

              </div>


      </div>  
    </div>
  </div>                   
    </div>  
  </div>
<script type="text/javascript">
$(function(){
  $('#lnk-sign-in').magnificPopup({
      type: 'ajax',
      showCloseBtn:     true,
      closeBtnInside:   false,
      fixedContentPos:  'auto',
      closeOnBgClick:   true,
    });    




})

</script>


