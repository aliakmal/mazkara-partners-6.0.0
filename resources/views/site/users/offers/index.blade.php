
<div class="container">
  <div class="row">
    <div class="col-md-9 ">

      @if($user->isUserMeSignedIn() && $user->hasNoVouchers())
        <div class="p30 mt15 bg-lite-gray text-center">
          <div class="fs220"><span class="fs175"><i class="flaticon flaticon-plant59 "></i></span></div>
          <div class="">No Vouchers yet? Whaddup?</div>
        </div>
      @else
        <h1>{{isset($params['state'])?$params['state']:'ALL'}} Vouchers</h1>
        <hr/>
      @endif

      <p class="pb10">
        <a href="?" class="dpib pr15 {{ !isset($params['state'])?'pink':''}}" >ALL</a>
        <a href="?state=valid"  class="dpib pr15 {{ isset($params['state']) && (strtolower($params['state']) == 'valid')?'pink':''}}">VALID</a>
        <a href="?state=expired" class="dpib pr15 {{ isset($params['state']) && (strtolower($params['state']) == 'expired')?'pink':''}}">EXPIRED</a>
        <a href="?state=claimed" class="dpib pr15 {{ isset($params['state']) && (strtolower($params['state']) == 'claimed')?'pink':''}}">CLAIMED</a>
      </p>

      @foreach($vouchers as $voucher)
      <div class="p5 mb5 bg-lite-gray ba">
        <div class="row">
          <div class="col-md-9">
            <p class="fs125 mb5">{{ $voucher->offer->title}}</p>
            <p class="mb0">
              <b>Claimed on:</b> {{ mzk_f_date($voucher->created_at) }}
            </p>
            <p>
              <b>Valid Until:</b> {{ mzk_f_date($voucher->offer->valid_until) }}
            </p>
            <p class="mb0">
              {{ mzk_status_tag($voucher->state, 'dpib no-border-radius fs125 p5') }}
            </p>
          </div>
          <div class="col-md-3 text-right">
            <p class="mt75 mb0"><a href="{{ route('users.vouchers.pdf', [$voucher->id])}}" class="btn btn-sm no-border-radius btn-default">DOWNLOAD VOUCHER</a></p>
          </div>
        </div>
      </div>
      @endforeach
      {{ $vouchers->appends($params)->render()}}






    </div>

  <div class="col-md-3 pt20">
    <div class="p10 bg-lite-gray">
      <h2 class="item-name">
        {{ $user->name }}
      </h2>
      @if($user->isContributorable() == true)
        <p class="">
          {{ $user->authors_designation }}
        </p>
      @endif
      @if($user->hasLocation())
        <p>
          Location: {{ $user->users_location }}
        </p>
      @endif

      @if(Auth::check())
        @if($user->id != Auth::user()->id)
          <a href="javascript:void(0)" class="btn no-border-radius btn-default mt5 user-follow {{ $user->isFollowed(Auth::user()->id) ? 'favourited' : 'favourite' }} btn-default small-text" title="{{ ($user->isFollowed(Auth::user()->id)? 'Following' : 'Follow ') }} {{$user->full_name}}"  
            data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
            <span class="show-when-followed">FOLLOWING</span>
            <span class="hide-when-followed">FOLLOW</span>
          </a>
        @endif
      @else
        <a href="javascript:void(0)" class="btn no-border-radius btn-default mt5 user-follow btn-default small-text" title="Follow {{$user->full_name}}"  data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
          <i class="fa fa-user-plus"></i> 
            FOLLOW
        </a>
      @endif


      @if(Auth::check())
        @if($user->id != Auth::user()->id)
        @else
          <a href="/users/edit" class="btn btn-small btn-default mt5 no-border-radius small-text">
            <i class="fa fa-cog"></i> SETTINGS
          </a>
        @endif
        @if(Auth::user()->hasRole('admin'))
          <a href="/content/accounts/{{$user->id}}" class="btn mt5 no-border-radius btn-small btn-default small-text">
            <i class="fa fa-shield"></i> EDIT ADMIN
          </a>
        @endif
      @endif

      <div class="text-danger pt10 pb10 ">{{ $user->roleLabel() }}</div>

      <div class="row ">
        <div class="col-md-6 fs90 fw300 ">
          <p>
            <i class="fa fa-user fs125 medium-gray"></i> {{ $user->followers()->count() }} Follower(s)
          </p>
          <p>
            <i class="fa fa-camera fs125 medium-gray"></i> {{ $user->posts()->onlySelfies()->count() }} Photo(s)
          </p>
          <p>
            <i class="fa fa-pencil-square-o fs125 medium-gray"></i> {{ $user->completedReviews()->count() }} Review(s)
          </p>
        </div>
        <div class="col-md-6 fs90 fw300">
          <p>
            <i class="fa fa-file fs125 medium-gray"></i> {{ $user->posts()->count() }} Storie(s)
          </p>
          <p>
            <i class="fa fa-comment-o fs125 medium-gray"></i> {{ $user->comments()->count() }} Comment(s)
          </p>
        </div>
      </div>

      @if($user->hasAbout())
        <p>
          <b>ABOUT ME</b>
        </p>
        <p>
          {{ $user->about }}
        </p>
      @endif
    </div>

    @if($user->hasContactDetails())
      <div class=" pt10 pb10 "><b>CONTACT</b></div>
      <div class="row ">
        <div class="col-md-12 ">
          @if(!empty($user->phone))
            <p><i class="fa fa-phone"></i> {{ $user->phone }}</p>
          @endif

          @if(!empty($user->contact_email_address))
            <p><i class="fa fa-envelope"></i> <a href="mailto:{{ $user->contact_email_address }}">{{ $user->contact_email_address }}</a></p>
          @endif

          @if(!empty($user->twitter))
            <p><i class="fa fa-twitter"></i> <a href="http://www.twitter.com/{{ $user->twitter }}"> {{ '@'.$user->twitter }}</a></p>
          @endif

          @if(!empty($user->instagram))
            <p><i class="fa fa-instagram"></i> <a href="http://www.instagram.com/{{ $user->instagram }}">/{{ $user->instagram }}</a></p>
          @endif
        </div>
      </div>
    @endif



  </div>
  </div>
</div>

@section('js')

<script type="text/javascript">
$(function(){
  $('.show-hidden').click(function(){
    p = $(this).parents('.panel-body').first();
    console.log(p);
    $(p).find('.hidden').removeClass('hidden');
    $(this).hide();
  })

  $('.link-display-followed-businesses').click(function(){
    $('.followed-businesses').removeClass('hidden');
    $(this).hide();
  });

  $('.jscroll').jscroll({
      padding: 20,
      loadingHtml: '<p><b>Loading more...</b></p>',

      nextSelector: '.pagination a[rel=next]',
      contentSelector: '.single-activity-element'
  });
  
  $(".popover-mobile").popover({
        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });


  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });



})
</script>
@stop