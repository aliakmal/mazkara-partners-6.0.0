@extends('layouts.master')
@section('content')
<div class="container">    
  <div id="" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">                    
    <h1 class="text-center notransform">Reset Password?</h1>
    <hr/>
    <p class="lead text-center">Awesome, set a brand new password for your account! Something easy to remember but hard to guess!</p>
    @if (Session::get('error'))
        <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
    @endif

    @if (Session::get('notice'))
        <div class="alert">{{{ Session::get('notice') }}}</div>
    @endif

    <form method="POST" action="{{{ URL::to('/users/reset_password') }}}" accept-charset="UTF-8">
    <input type="hidden" name="token" value="{{{ $token }}}">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

    <div class="form-group">
        <input class="form-control" placeholder="PASSWORD" type="password" name="password" id="password">
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="PASSWORD CONFIRMATION" type="password" name="password_confirmation" id="password_confirmation">
    </div>


    <div class="form-actions form-group">
        <button type="submit" class="btn form-control btn-primary">RESET MY PASSWORD</button>
    </div>
</form>
</div></div>
<br/><br/><br/><br/><br/>
@endsection