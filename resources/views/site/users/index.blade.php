<div class="tf-section pt0">
  <div class="container">
    <div class="row">
      @if(count($user->followsUsers)>0)
        <div class="col-md-3 pt20">
      @foreach($buttons as $b)
          @include('elements.collection', ['b'=>$b])
      @endforeach




   



    <small class=" pb10 mb5">NEW ON FABOGO</small>
    @foreach($suggested_salons as $business)
      @include('site.businesses.partials.business-niblet', ['business'=>$business])
    @endforeach


        </div>
        <div class="col-md-6 bg-white pb5  ">
          <h3 class="item-name ">
            Latest on Fabogo
          </h3>
          <hr/>
          <div class="jscroll">
          <div id="activities-holder">
          @foreach ($activities as $activity)
            @include('elements.activities', array('activity'=>$activity))
          @endforeach
          </div>
          <?php Paginator::setViewName('elements.pagination.next');?>
          <div class=" single-activity-element">{{ $activities->render() }}</div>
        </div>

        </div>
        <div class="col-md-3">
          <h3 class="item-name ">
            Mazkarian's to follow
          </h3>
          <hr/>
          @foreach($suggested_users_to_follow->get() as $f)
            @include('site.users.partials.user-niblet', ['user'=>$f])
          @endforeach
        </div>
      @else
        <div class="col-md-6 pb5 bg-white col-md-offset-3 pt20 text-center">
          <h2 class="color fw300 lnh notransform">Your feed looks a bit empty?</h2>
          <p class="lead">
            You haven't followed anyone yet - here are some awesome dudes and dudettes to follow:
          </p>
          <div role="tabpanel" class="tab-pane" id="follows">
            <hr/>
            @foreach($suggested_users_to_follow->get() as $f)
              @include('site.users.partials.user-niblet', ['user'=>$f])
            @endforeach
          </div>
        </div>
      @endif            
    </div>
  </div>
</div>
@section('js')

<script type="text/javascript">
$(function(){
 
  $('.jscroll').jscroll({
      padding: 20,
      autoTriggerUntil:2,
      loadingHtml: '<p><b>Loading more...</b></p>',
      nextSelector: '.pagination a[rel=next]',
      contentSelector: '.single-activity-element'
  });

})
</script>
@stop