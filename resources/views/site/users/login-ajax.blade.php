<div>
  <div id="loginbox" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">                    
    <div class="background-login-screen pt15 pb15 p25" >
      <div>
        <div class="p5 text-center">LOGIN TO FABOGO</div>
      </div>
      <div>
        <div class=" text-center mt10 ">
          <a id="btn-fblogin" href="/users/facebook/login" class="dpb no-border-radius mb10 facebook-login-link btn btn-primary">
            <i class="fa fa-facebook-square  pull-left mt2"></i> FACEBOOK
          </a>
          <p>Highly recommended - also we will never post anything without your permission.</p>
          <a id="btn-gllogin" href="/users/google/login" class="btn no-border-radius btn-danger dpb mb10">
            <i class="fa fa-google  pull-left mt2"></i> GOOGLE
          </a>
        </div>
        <div class="text-center mb10">
          <p>or</p>
          <p>Alternatively you can also use your email address.</p>
        </div>
        <div style="display:none" id="error-message-wrapper" class="alert alert-danger "></div>
        <form id="frm-authentication" role="form" method="POST" class="form-horizontal" action="{{{ URL::to('/users/login') }}}" accept-charset="UTF-8">
          @if(Session::get('error'))
            <div class="alert alert-error alert-danger">
              {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
            </div>
          @endif
          @if(Session::get('notice'))
            <div class="alert alert-success">
              {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
            </div>
          @endif
          <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
          <input class="form-control no-border-radius p5 pl10 pr10 mb5 fs90" data-validation="required" type="text" tabindex="1" placeholder="EMAIL ADDRESS" name="email" id="txt-email" value="{{{ Input::old('email') }}}" />
          <input class="form-control no-border-radius p5 pl10 pr10 mb5 fs90" data-validation="required" tabindex="2" placeholder="PASSWORD" type="password" name="password" id="txt-password">
          <input type="hidden" name="redirectto" value=" {{ ($_SERVER['HTTP_REFERER']) }}" />
          <div class="text-right fs90">
            <a href="{{{ URL::to('/users/forgot_password') }}}">Forgot password?</a>
          </div>

          <div class="mt10 pt10  ">
            <div class="mt10  ">
              <button id="btn-login" style="width:100%"  class="btn dpb btn-turquoise p10 text-center no-border-radius" >LOGIN</button>
            </div>
          </div>

          <div class="clearfix">
            <div style="padding-top:10px;" class="checkbox pt10  pull-left" >
              <label class=" clearfix " style="float:none;">
                <input type="hidden" name="remember" value="0">
                <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> Remember me
              </label>
            </div>                  
            <div class="pt10 text-right ">
              Don't have an account!
              <a href="/users/create" id="lnk-sign-up"  class="text-danger">
                Sign Up Here
              </a>
            </div>
          </div>

          <div class="fs90 pt10 mt10">
            <div class="text-center  fs80">By logging in, you agree to Fabogo's <a href="/tos" class="text-muted">Terms of Service</a>, 
              <a href="/cookie-policy" class="text-muted">Cookie Policy</a> and <a class="text-muted" href="/privacy-policy">Privacy Policy</a>.
            </div>
          </div>    
        </form>     
      </div>                     
    </div>  
  </div>
</div>
<script type="text/javascript">
$(function(){
  $('#lnk-sign-up').magnificPopup({
      type: 'ajax',
      showCloseBtn:     true,
      closeBtnInside:   false,
      fixedContentPos:  'auto',
      closeOnBgClick:   true,
    });    




})

</script>


