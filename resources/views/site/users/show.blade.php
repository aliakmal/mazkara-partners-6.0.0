@extends('layouts.parallax')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9 ">
      <center>
        <p class="p10 mb10">
          <div class="dpib hoverable relative">
            @if(Auth::check())
              @if($user->isEditableBy(Auth::user()))
                <div class="absolute  dpb" style="width:100%; height:100%; background-color:rgba(0,0,0, 0.2);">
                  <p class="absolute force-white text-center" style="width:100%;top:40%;">
                    <a id="lnk-update-profile-photo" class="inline-popup-link" href="#edit-profile-photo">Click to change photo</a>
                  </p>
                </div>
              @endif
            @endif
          {{ViewHelper::userAvatar($user, ViewHelper::$avatar325Square)}}
          </div>
        </p>
      </center>

      @if(!$user->isUserMeSignedIn() && $user->isUserBoring())
        <div class="p30 bg-lite-gray text-center">
          <div class="fs220"><span class="fs175"><i class="flaticon flaticon-plant59 "></i></span></div>
          <div class="">{{$user->full_name}}'s profile is dry as a desert!</div>
        </div>
      @endif

<div class="hidden">
  <div id="edit-profile-photo">
    <div class="  col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 ">
      <div class="row">
        <div class="bg-white p0 col-md-10 col-md-offset-1">
        <div class="bg-yellow p10 text-center mt0 ">
          <h3>CHANGE PROFILE PICTURE</h3>
        </div>
        <div class="p5 mb10 mt10 text-left">
          <div class="p15">
          <p>
           {{ Form::open(['file' => true, 'Method' => 'POST', 'id' => 'profile-image-upload']) }}

            <div class="alert alert-danger  error" style="display:none">
                <p>File must be an image</p>
            </div>

            <div class="form-group">
              {{ Form::label('upload', 'Upload Photo') }}
              {{ Form::file('avatar', ['id'=>'photo-upload', 'class' => 'form-control upload-box']) }}
            </div>

            {{ Form::close() }}


          </p>
          </div>
          <div class="p10 dark-gray">
            <p class=" p15">
            <ul class="p15 mr10">
              <li>Make sure image is a jpeg, png, gif</li>
              <li>Image must be atleast 600 pixels by 600 pixels</li>
              <li>Maximum size not more than 1 MB</li>
              <li>Please no toilet selfies, they stink...</li>
            </ul>
            </p>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>



      @if($user->isContributorable())
        <?php $posts_count = $user->posts()->onlyPosts()->isViewable()->count();?>
        <div class="pt15 ">
          <p class="mt15 mb15">
            @if($posts_count>0)
              <h4 class="item-headers ">
                {{ strtoupper($user->full_name) }}'S STORIES({{$posts_count}})
                @if(Auth::check())
                  @if(Auth::user()->id == $user->id)
                    @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                      @if($posts_count>0)
                        <a href="{{route('posts.create')}}" class="mt10 fs75 fw300 pull-right">
                          <i class="fa fa-edit"></i>
                          Add a Story
                        </a>
                      @else
                      @endif
                    @endif
                  @endif
                @endif

              </h4>
            @endif


          </p>
          <div class="clearfix"></div>

            @if(Auth::check())
              @if(Auth::user()->id == $user->id)
                @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                  @if($posts_count==0)
                    <a style="clear:both" class="p30 mb15 dpb hover-bg-yellow text-center bg-lite-gray" href="{{ route('posts.create') }}" >
                      <div class="fs220"><i class="fa fa-edit "></i></div>
                      <div class="">Add Your First Story</div>
                    </a>
                  @endif
                @endif
              @endif
            @endif

          <div class="row">
            @foreach($user->posts()->onlyPosts()->isViewable()->take(4)->get() as $post)
              <div class="col-md-3">
                @include('site.posts.partials.minimal')
              </div>
            @endforeach
          </div>
        </div>
        @if($user->posts()->onlyPosts()->isViewable()->get()->count() > 4)
          <p class="pb10 text-right">
            <small><a href="{{ route('posts.index.all', ['author'=>$user->id])}}">See all {{$user->posts()->onlyPosts()->isViewable()->count()}} stories</a></small>
          </p>
        @endif
      @endif


      <div class="pt15 ">
        <p class="mt15 mb15">
          <?php $selfies_count = $user->posts()->onlySelfies()->isViewable()->count();?>
          @if($selfies_count > 0)
            <h4 class="item-headers  ">
              {{ strtoupper($user->full_name) }}'S PHOTO'S({{ $selfies_count }})
              @if(Auth::check())
                @if(Auth::user()->id == $user->id)
                  @if($selfies_count>0)
                    <a href="{{route('selfies.create')}}" class="mt10 fs75 fw300 pull-right">
                      <i class="fa fa-camera"></i>
                      Add a Selfie
                    </a>
                  @endif
                @endif
              @endif

            </h4>
          @endif

        </p>
        <div class="clearfix"></div>

          @if(Auth::check())
            @if(Auth::user()->id == $user->id)
              @if($selfies_count==0)
                <a style="clear:both" class="p30 mb15 dpb hover-bg-yellow text-center bg-lite-gray" href="{{route('selfies.create')}}" >
                  <div class="fs220"><i class="fa fa-camera "></i></div>
                  <div class="">Add Your First Photo</div>
                </a>
              @endif
            @endif
          @endif

        <div class="row">
          @foreach($user->posts()->onlySelfies()->isViewable()->take(4)->get() as $post)
            <div class="col-md-3">
              @include('site.posts.partials.selfie')
            </div>
          @endforeach
        </div>
      </div>
      @if($user->posts()->onlySelfies()->isViewable()->get()->count() > 4)

      <p class="pb10 text-right">
        <small>
          <a href="{{ route('users.photos.show', $user->id)}}">
            See all {{$user->posts()->onlySelfies()->isViewable()->count()}} pictures
          </a>
        </small>
      </p>
      @endif

      @if($user->isContributorable())
        <div class="pt15 ">
          <p class="mt15 mb15">
            <?php $videos_count = $user->posts()->onlyVideos()->isViewable()->count();?>
            @if($videos_count>0)
              <h4 class="item-headers">
                {{ strtoupper($user->full_name) }}'S VIDEOS({{$videos_count}})
    
                @if(Auth::check())
                  @if(Auth::user()->id == $user->id)
                    @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                      @if($videos_count>0)
                        <a href="{{ route('videos.create') }}" class="mt10 fs75 fw300 pull-right">
                          <i class="fa fa-video-camera"></i>
                          Add a Video
                        </a>
                      @endif
                    @endif
                  @endif
                @endif


              </h4>
            @endif

          </p>
          <div class="clearfix"></div>
          @if(Auth::check())
            @if(Auth::user()->id == $user->id)
              @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                @if($videos_count==0)
                  <a style="clear:both" class="p30 mb15 dpb hover-bg-yellow text-center bg-lite-gray" href="{{ route('videos.create') }}" >
                    <div class="fs220"><i class="fa fa-video-camera "></i></div>
                    <div class="">Add Your First Video</div>
                  </a>
                @endif
              @endif
            @endif
          @endif

          <div class="row">
            @foreach(Video::ofAuthors([$user->id])->isViewable()->take(4)->get() as $post)
              <div class="col-md-3">

                @include('site.posts.partials.video')
              </div>
            @endforeach
          </div>
        </div>
        @if($user->posts()->onlyVideos()->isViewable()->get()->count() > 4)
          <p class="pb10 text-right">
            <small>
              <a href="{{ route('users.videos.show', $user->id)}}">
                See all {{$user->posts()->onlyVideos()->isViewable()->count()}} videos
              </a>
            </small>
          </p>
        @endif
      @endif


      @if(Auth::check() && (Auth::user()->id == $user->id))
        @if($user->followsBusinesses()->count()>0)
        <div class="pt15 ">
          <p class="mt15 mb15">
            <h4 class="item-headers ">
              {{ strtoupper($user->full_name) }}'S SHORTLIST({{$user->followsBusinesses()->get()->count()}})
            </h4>
          </p>
          <div class="row">
              @foreach($user->followsBusinesses()->get() as $ii=>$favourite)
              <?php $business = $favourite->favorable;?>
              <div class="col-md-3 {{ $ii > 7 ? 'hidden followed-businesses' : '' }}">
                @include('site.posts.partials.business')
              </div>
            @endforeach
          </div>
          @if($ii > 7)
            <p class="pb10 text-right">
              <small>
                <a href="javascript:void(0)" class="link-display-followed-businesses">
                  See all {{($ii+1)}} Shortlisted
                </a>
              </small>
            </p>
          @endif
        </div>
        @endif
      @endif
      @if($user->completedReviews()->count() >0)
      <div class="pt15 ">
        <p class="mt15 mb15">
          <h4 class="item-headers ">
            {{ strtoupper($user->full_name) }}'S REVIEWS({{$user->completedReviews()->count()}})
          </h4>
        </p>
        <div class="row">
          @foreach($user->completedReviews()->orderby('id', 'desc')->get() as $ii=>$review)
            <?php $business = $review->business;?>
            @if($business)
              <div class="col-md-12">
                @include('site.users.partials.review-no-favorite', ['review'=>$review, 'business'=>$business])
              </div>
            @endif

          @endforeach
        </div>
      </div>
      @endif
    </div>

  <div class="col-md-3 pt20">
    <div class="p10 bg-lite-gray">
      <h2 class="item-name">
        {{ $user->name }}
      </h2>
      @if($user->isContributorable() == true)
        <p class="">
          {{ $user->authors_designation }}
        </p>
      @endif
      @if($user->hasLocation())
        <p>
          Location: {{ $user->users_location }}
        </p>
      @endif

      @if(Auth::check())
        @if($user->id != Auth::user()->id)
          <a href="javascript:void(0)" class="btn no-border-radius hide-only-mobile btn-default mt5 user-follow {{ $user->isFollowed(Auth::user()->id) ? 'favourited' : 'favourite' }} btn-default small-text" title="{{ ($user->isFollowed(Auth::user()->id)? 'Following' : 'Follow ') }} {{$user->full_name}}"  
            data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
            <span class="show-when-followed">FOLLOWING</span>
            <span class="hide-when-followed">FOLLOW</span>
          </a>
        @endif
      @else
        <a href="/users/create" id="ajax-login-link" class=" hide-only-mobile ajax-popup-link btn no-border-radius btn-default mt5 user-follow btn-default small-text" title="Sign in to follow {{$user->full_name}}"  data-toggle="tooltip" data-placement="left"  data-route="users">
          <i class="fa fa-user-plus"></i> 
            FOLLOW
        </a>
      @endif


      @if(Auth::check())
        @if($user->id != Auth::user()->id)
        @else
          <a href="/users/edit" class="btn btn-small btn-default mt5 no-border-radius small-text">
            <i class="fa fa-cog"></i> SETTINGS
          </a>
        @endif
        @if(Auth::user()->hasRole('admin'))
          <a href="/content/accounts/{{$user->id}}" class="btn mt5 no-border-radius btn-small btn-default small-text">
            <i class="fa fa-shield"></i> EDIT ADMIN
          </a>
        @endif
      @endif

      <div class="text-danger pt10 pb10 ">{{ $user->roleLabel() }}</div>

      <div class="row ">
        <div class="col-md-6 fs90 fw300 ">
          <p>
            <i class="fa fa-user fs125 medium-gray"></i> {{ $user->followers()->count() }} Follower(s)
          </p>
          <p>
            <i class="fa fa-camera fs125 medium-gray"></i> {{ $user->posts()->onlySelfies()->count() }} Photo(s)
          </p>
          <p>
            <i class="fa fa-pencil-square-o fs125 medium-gray"></i> {{ $user->completedReviews()->count() }} Review(s)
          </p>
        </div>
        <div class="col-md-6 fs90 fw300">
          <p>
            <i class="fa fa-file fs125 medium-gray"></i> {{ $user->posts()->count() }} Storie(s)
          </p>
          <p>
            <i class="fa fa-comment-o fs125 medium-gray"></i> {{ $user->comments()->count() }} Comment(s)
          </p>
        </div>
      </div>

      @if($user->hasAbout())
        <p>
          <b>ABOUT ME</b>
        </p>
        <p>
          {{ $user->about }}
        </p>
      @endif
    </div>

    @if($user->hasContactDetails())
      <div class=" pt10 pb10 "><b>CONTACT</b></div>
      <div class="row ">
        <div class="col-md-12 ">
          @if(!empty($user->phone))
            <p><i class="fa fa-phone"></i> {{ $user->phone }}</p>
          @endif

          @if(!empty($user->contact_email_address))
            <p><i class="fa fa-envelope"></i> <a href="mailto:{{ $user->contact_email_address }}">{{ $user->contact_email_address }}</a></p>
          @endif

          @if(!empty($user->twitter))
            <p><i class="fa fa-twitter"></i> <a href="http://www.twitter.com/{{ $user->twitter }}"> {{ '@'.$user->twitter }}</a></p>
          @endif

          @if(!empty($user->instagram))
            <p><i class="fa fa-instagram"></i> <a href="http://www.instagram.com/{{ $user->instagram }}">/{{ $user->instagram }}</a></p>
          @endif
        </div>
      </div>
    @endif



  </div>
  </div>
</div>
@stop
@section('js')

<script type="text/javascript">
$(function(){
  $('.show-hidden').click(function(){
    p = $(this).parents('.panel-body').first();
    console.log(p);
    $(p).find('.hidden').removeClass('hidden');
    $(this).hide();
  });

  $('.hoverable').mouseover(function(){
    $(this).find('.show-on-hover').show();
  });

  $('.hoverable').mouseout(function(){
    $(this).find('.show-on-hover').hide();
  });


  $('.link-display-followed-businesses').click(function(){
    $('.followed-businesses').removeClass('hidden');
    $(this).hide();
  });

    $('input[type=file]').change(function () {

        $('#load').show();

        var formData = new FormData($('#profile-image-upload')[0])

        $('.upload-container').hide();

        $.ajax({
            type: 'POST',
            url: '{{ route('users.photos.upload.1', $user->id) }}',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
              if (data != 'false'){

                $.magnificPopup.instance.items[0] = { src: data, type: 'inline' };
                $.magnificPopup.instance.updateItemHTML();

                $('#sample_picture').cropper({
                  aspectRatio: 1 / 1,
                  minCropBoxWidth:200,
                  minCropBoxHeight:200,
                  crop: function(e) {
                    $('#picture_x').val(e.x);
                    $('#picture_y').val(e.y);
                    $('#picture_width').val(e.width);
                    $('#picture_height').val(e.height);
                    $('#picture_scale_x').val(e.scaleX);
                    $('#picture_scale_x').val(e.scaleY);

                  }
                });

                $('.lnk-save-avatar').click(function(){
                  vals = {};
                  $('#crop-area').find('input').each(function(i, v){
                    index = $(v).attr('name');
                    val = $(v).val();
                    vals[index] = val;
                  });
                  
                  $('.form-crop').addClass('hidden');
                  $('.form-crop-submit-message').removeClass('hidden');

                  $.ajax({
                    type: 'POST',
                    url: '{{ route('users.photos.upload.2', $user->id) }}',
                    data: vals,
                    dataType: 'json'
                  }).done(function(){
                    location.reload(true);
                  });
                });
              }
              if (data == 'false'){
                $('.upload-container').show();
                $('.error').show();
              }
            },
            error: function (data) {
                $('#load').hide();
                console.log("error");
                console.log(data);
            }
        });
    });


//  $('#photo-upload').fileinput({
//      uploadUrl: "{{ route('users.photos.upload.1', $user->id) }}", // server upload action
//      uploadAsync: false,
//      showPreview: false,
//      allowedFileExtensions: ['jpg', 'png', 'gif'],
//      maxFileCount: 1,
//      elErrorContainer: '.error-container'
//  }).on('filebatchuploadsuccess', function(event, data) {
//    console.log(222, data);
//    var out = '';
//    $.each(data.files, function(key, file) {
//        var fname = file.name;
//        out = out + '<li>' + 'Uploaded file # ' + (key + 1) + ' - '  +  fname + ' successfully.' + '</li>';
//    });
//    $('#kv-success-2 ul').append(out);
//    $('#kv-success-2').fadeIn('slow');
//  });





//  $('.jscroll').jscroll({
//      padding: 20,
//      loadingHtml: '<p><b>Loading more...</b></p>',
//
//      nextSelector: '.pagination a[rel=next]',
//      contentSelector: '.single-activity-element'
//  });
  
  $(".popover-mobile").popover({
        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });


  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $('.inline-popup-link').magnificPopup({
      type: 'inline',
      showCloseBtn:true,
      closeBtnInside:false,
      fixedContentPos: 'auto',
      closeOnBgClick:true,
    });    

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });



})
</script>
@stop