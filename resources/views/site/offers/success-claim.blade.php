<div class="  col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 ">
  <div class="row">
    <div class="bg-white p0 col-md-10 col-md-offset-1">
  <div class="bg-yellow p10 text-center mt0 ">
    <h3>GREAT GOING!</h3>
  </div>
  <div class="p10 text-center">
    <p class="">USE IT NOW! DON'T SHAVE IT FOR LATER!</p>
    <div>
      <p class="fs125 mb0 text-center pink">
        <b>{{$offer->title}}</b>
      </p>
      <p class="fs125 text-center ">
        at {{ (join(', ', array_filter([$business->name, $business->zone_cache]))) }}
      </p>

      <p>This is your offer redemption code - you'll need this when you avail the {{ (mzk_label('special')) }}.</p>
      <div class="p10 text-center">
        <center>
          <img style="width:100%" src="{{ $voucher->getBarCodePNG() }}" alt="barcode" />
        </center>
        <p>
          <small>{{ $voucher->code }}</small>
        </p>
      </div>
      <p class="text-center fs90 mb0">
        You don't need to print to look favulous! <span class="green">Go Green!</span> Save paper. 
        We've also emailed this voucher to you incase you do want to take a printout. All your vouchers are accessible from your profile.</p>
    </div>
  </div>
</div>
</div>
</div>

