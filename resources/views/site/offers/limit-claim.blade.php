<div class="  col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 ">
  <div class="row">
    <div class="bg-white p0 col-md-10 col-md-offset-1">
    <div class="bg-yellow p10 text-center mt0 ">
      <h3>OOPSY DAISY!</h3>
    </div>
    <div class="p5 mb10 mt10 text-center">
      <img src="{{ mzk_assets('assets/oopsy.jpg') }}" />
        <p class="fs125 p10">
        You've used your limit of {{ (mzk_label('specials')) }}
        </p>
    </div>
  </div>
  </div>
</div>
