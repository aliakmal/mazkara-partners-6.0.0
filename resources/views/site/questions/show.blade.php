@extends('layouts.parallax')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="pt10 mt10 no-text-shadow mb10 pb10">
        {{ $breadcrumbs->render() }}
      </div>

    </div>
  </div>
  <div class="row">
    <div class="col-md-8 ">

      <div class="pt10   bb  clearfix fw300 ">
        <div class="pull-left mb10 mr10">
          {{ViewHelper::userAvatar($question->author, ViewHelper::$avatar35)}}
        </div>
        <div class=" ">
            <b>
              <a href="{{route('users.profile.show', $question->author->id)}}">
                {{$question->author->name}}
              </a>

            </b><br/>
            {{$question->author->authors_designation }}
        <div class="pull-right mb10 mr0">
          POSTED ON {{ strtoupper(mzk_f_date($question->updated_at, 'jS M'))}}

        </div>
        </div>
      </div>
      <div class="pt10">
        @foreach($question->services as $service)
          <a href="{{ route('questions.service', [$service->slug])}}" class="gray">{{ strtoupper($service->name) }}</a>&nbsp;&nbsp;
        @endforeach

      <div class="pull-right text-right mb15">
        <div class="">

          @if(Auth::check())
            <a href="javascript:void(0)" class="btn btn-xs  {{ Auth::check()? ($question->liked(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default" rel="{{$question->id}}"  data-route="posts">
              <i class="fa fa-heart "></i> 
            </a>
            @if($question->isEditableBy(Auth::user()->id))
              @if($question->isEditable())
                <a href="{{ route('questions.edit', [$question->id]) }}" class="btn btn-xs btn-default " rel="{{$question->id}}">
                  <i class="fa fa-pencil"></i>
                </a>
              @endif
            @endif

          @else
            <a href="/users/login" class="btn btn-xs ajax-popup-link btn-default " rel="{{$question->id}}">
              <i class="fa fa-heart"></i> 
            </a>
          @endif

        </div>
      </div>

      </div>



      <h2 class="pt10 item-name fw500 mt20">{{$question->title}}</h2>
      <div class="pt10 fw300">
        {{ nl2br($question->body)}}
      </div>
      @if($question->cover)
        <p class="mt10 text-center mb10">
          <img src="{{ $question->cover->image->url('medium') }}" class="img-thumbnail" width="75%" />
        </p>
      @endif

      <hr />
      <div class="mt10 pt10 clearfix fs90">
      </div>
      @foreach($question->answers as $answer)
      <div class="pt10 mb10 mt0 media  bb  clearfix fw300 ">
        <div class="media-left mb10 mr10">
          {{ViewHelper::userAvatar($answer->author, ViewHelper::$avatar35)}}
          <div class="gray text-center pt10">
            <i class="fa fa-flip-vertical fa-mail-forward"></i>
          </div>
        </div>
        <div class="media-body ">
        <div class="pull-right gray mb10 mr0">
          {{{ Date::parse($answer->updated_at)->ago() }}}

        </div>

            <b>
              <a href="{{route('users.profile.show', $answer->author->id)}}">
                {{$answer->author->name}}
              </a>

            </b><br/>
            <div class="gray">{{$answer->author->authors_designation }}</div>
            <div >
              <div class="pt10 fw300">
                {{ nl2br($answer->body)}}
              </div>
              @if($answer->cover)
                <p class="mt10 mb10">
                  <img src="{{ $answer->cover->image->url('medium') }}" class="img-thumbnail" width="75%" />
                </p>
              @endif

            </div>


            <div class="text-right mb10">
              @if(Auth::check())
                <a href="javascript:void(0)" class="btn btn-xs  {{ Auth::check()? ($answer->liked(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default" rel="{{$answer->id}}"  data-route="posts">
                  <i class="fa fa-heart "></i> 
                </a>
                @if($answer->isEditableBy(Auth::user()->id))
                  @if($answer->isEditable())

                    {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 
                              'route' => array('answers.destroy', $answer->id))) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
                    {{ Form::close() }}

                    <!-- <a href="/questions/{{$answer->id}}/edit" class="btn btn-xs btn-default " rel="{{$answer->id}}">
                      <i class="fa fa-pencil"></i> Edit Question
                    </a> -->
                  @endif
                @endif

              @else
                <a href="/users/login" class="btn btn-xs ajax-popup-link btn-default " rel="{{$answer->id}}">
                  <i class="fa fa-heart"></i> 
                </a>
              @endif


            </div>

        </div>
      </div>


      @endforeach
      @if(Auth::check())
        <?php $user = Auth::user();?>
        <a href="javascript:void(0)" class="btn btn-answer btn-default btn-sm">Answer the Question</a>
        {{ Form::open(array('route' => 'answers.store', 'id'=>'answer-form', 'style'=>'display:none;', 'files'=>true,  'class' => 'form-vertical')) }}
          <p class=""><b>RESPOND</b></p>
          <div class="pt10 pb10  media clearfix fw300 ">
            <div class="media-left ">
              {{ViewHelper::userAvatar($answer->author, ViewHelper::$avatar35)}}
            </div>
            <div class="media-body ">

              <div class="form-group">
                {{ Form::textarea('body', Input::old('body'), array('class'=>'form-control wysiwyg', 'id'=>'post-body', 'style'=>'width:120%', 'placeholder'=>'Write a Comment')) }}
              </div>
              <div class="" style="width:120%">
              <div class="form-group pull-left">
                <i class="fa fa-camera"></i> {{ Form::label('cover', 'Attach a Photo', array('class'=>'float-none control-label')) }}
                {{ Form::file('cover', array( 'accept'=>"image/*", 'capture'=>'camera')) }}

              </div>
                <div class=" pull-right">
                  {{ Form::submit('POST', array('class' => 'btn dpb btn-sm btn-default')) }}
                </div>
              </div>

              {{ Form::hidden('question_id', $question->id) }}

            </div>
          </div>
        {{ Form::close() }}

      @else
        <div class="well mt10">
          Sign in to answer
        </div>
      @endif
    </div>
    <div class="col-md-4  pt20">

    <small class=" pb10 mb10">BROWSE VIA TOPICS</small>
    <hr/>
    @foreach($services as $servce)
      <div class="pb5">
        <a href="{{route('questions.service', [$servce->slug])}}">{{$servce->name}}</a>
      </div>
    @endforeach

    @if($service)
    <div class="mt10 pt10  mb10">
      <small>POPULAR VENUES FOR {{strtoupper($service->name)}}</small></div>
      <hr/>

      @foreach($businesses_with_prices as $business)
        @include('site.businesses.partials.business-service-niblet')



      @endforeach

    @endif

    </div>
  </div>
</div>
@stop
@section('js')

<script type="text/javascript">
$(function(){



  $('.btn-answer').click(function(){
    $(this).hide();
    $('#answer-form').show();
  })


  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });


});
</script>
@stop
