@extends('layouts.parallax')
@section('content')
<?php $no_rating = true;?>
@include('site.businesses.partials.cover-or-parallax-desktop-mobile', ['business'=>$business, 'phones'=>$phones])
<div class="hide-only-mobile">
  @include('site.businesses.show-rate-cards-desktop', ['business'=>$business, 'phones'=>$phones])
</div>
<div class="show-only-mobile">
  @include('site.businesses.show-rate-cards-mobile', ['business'=>$business, 'phones'=>$phones])
</div>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))
@stop
@endsection

