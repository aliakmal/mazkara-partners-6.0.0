@extends(isset($layout)?$layout:'layouts.master-open-angular')
@section('content')
<div >

<div class="row bg-white" >
  <div class="col-md-12 ">

    <div class="no-text-shadow preangular hidden fs90 hide-only-mobile">
      <ul class="breadcrumbs breadcrumb">
        <li >
          <a href="/">Home</a>
        </li>
        <li ng-repeat="breadcrumb in venuesCtrl.breadcrumbs">
          <span class="divider">›</span>
          <a href="<{ breadcrumb.url }>" class="pr0 pl5"><{ breadcrumb.label }></a>
        </li>
      </ul>
    </div>

      <div class="row">
        <div class="col-md-9 col-xs-9">
          <div class="p10 show-only-mobile"></div>
          <h1 class="preangular hidden item-heading pb10 mt0 mb5">
            <!--title -->
            <{ venuesCtrl.titleText }>
            <small class=" fw500" >
              <a id="lnk-to-change-location" href="javascript:void(0)" class="hide-only-mobile hover-underline fire">CHANGE LOCATION</a>
            </small>
          </h1>
        </div>
        <div class="col-md-3  col-xs-3">
          <p class="pt15  show-only-mobile"></p>
            <div class="text-right gray preangular hidden">(<{ venuesCtrl.totalItems }>&nbsp;venues)</div>
        </div>
      </div>
    <div class="p5 show-only-mobile"></div>
  </div>
</div>
<div class="row bt">
  <div class="row-height">
  <div class="col-md-9  col-height ">
    <div  class=" -inside-full-height">
      <div class=" fake-load-it bg-white bb " style="margin-left:-15px; margin-right:-20px;" >
        <!-- horizontal filter goes here -->
        <div class="hide-only-mobile">
          <div ng-include src="'/js/angular/views/site/businesses/horizontal-categories-filter.html'"></div>
        </div>
        <div class="show-only-mobile force-gray">
          <ul class="mobile-category-filter hidden padded12 mb0" >
            <li class="dpb text-center">
              <a href="{{ MazkaraHelper::slugCityZone((isset($sub_zone) && ($sub_zone!=false)?$sub_zone:null), []) }}" 
                class="fs110  color-black  pb10 pt10 bbb-turquoise">All</a>
            </li>
            <li ng-repeat="category in venuesCtrl.filters.categories" class="dpb text-center">
              <a class="fs110 color-black pb10 pt10 bbb-gray-on-hover">
                <b><{ category.name }></b>
              </a>
              <div class="clearfix"></div>
            </li>
            <!-- show selected category here -->
            <!-- show rest of categories here -->
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="hide-only-mobile ">
          <div class="col-md-3 pr10 pl10 br filter-column fake-load-it li-pl0 ">
            <div class="p15 pt5 mt5 pb5 bg-white  ___border-radius-3 mb10">
              <!--vertical filter goes here-->
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-sort.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-zones.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-services.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-gender.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-price.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-highlights.html'"></div>
            </div>
          </div>
        </div>
        <div class="show-only-mobile mt_5 p0 pt0 pb0">
          <div class="navbar-header show-only-mobile ba  bg-white ba  mb0">
            <div class="dpb">
              <button data-target="#vertical-navbar-filter" data-toggle="collapse" class="navbar-toggle p0 mb0 collapsed mb0" type="button">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-filter "></i>
              </button>
              <span class="visible-xs navbar-brand p10 " style="height:auto;"><small>Filters</small></span>
            </div>
          </div>
        </div>
        <div class="show-only-mobile">
          <div id="vertical-navbar-filter" >
          <div class="col-md-3 filter-column fake-load-it li-pl0 ">
            <div class="p15 pt5 pb5 bg-white  border-radius-3 mb10">
              
              <!--vertical filter goes here-->
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-sort.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-zones-mobile.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-services.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-gender.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-price.html'"></div>
              <div ng-include src="'/js/angular/views/site/businesses/vertical-filter-highlights.html'"></div>

            </div>
          </div>

          </div>
        </div>
        <div class="col-md-9 pl0-mobile mt0 pl10 pr10 pr0-mobile li-pl0 " id="venues-listing-holder" style="min-height:200px;">
          <div id="fakeloader" ></div>
    <div id="msg-pre-page-loader" style="text-align:center; padding-top:160px;padding-bottom:160px;">
      Loading Venues...
    </div>



          <div class="show-on-load p15 pb0 pt5" style="" >
            <div class="show-only-mobile">
              <!--ads for mobile go here-->
            </div>
          </div>
          <div ng-show="venuesCtrl.venues.length > 0" class="list-group fake-load-it" >
            <div ng-repeat="venue in venuesCtrl.venues" class="listing" >
              <div ng-include src="'/js/angular/views/site/businesses/single.html'"></div>
            </div>
            <div style="margin-bottom:20px;" >
              <ul uib-pagination items-per-page="venuesCtrl.numPerPage" max-size="venuesCtrl.maxSize" template-url="{{ route('views.get.pagination') }}" total-items="venuesCtrl.totalItems" ng-model="venuesCtrl.currentPage" boundary-link-numbers="true" rotate="false" ng-change="venuesCtrl.pageChanged()"></ul>
            </div>
          </div>
          <div id="msg-no-results" ng-show="(venuesCtrl.venues.length == 0) && (venuesCtrl.initialized == 1)"  class="hidden mt60 mb20">
            <div class="text-center"><img src="{{mzk_assets('assets/no-results.png')}}"/>
              <div class="p10 mt20">
                <p><b>NO RESULTS FOUND</b></p>
                <p>
                  <a class="yellow" href="{{ MazkaraHelper::slugCity() }}">
                    <b>Browse all results in {{ ucwords(MazkaraHelper::getLocaleLabel()) }}</b>
                  </a>
                </p>
              </div>
            </div>
          </div>
          <!-- businesses listed here -->
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 bg-lite-gray-3 hide-only-mobile col-height pt10">
    <div class=" -inside-full-height ">
      <div ng-include src="'/js/angular/views/site/businesses/vertical-ads.html'"></div>
    </div>
  </div>
</div>
</div>
</div>

@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.3/angular.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-route.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.1/ui-bootstrap.js"></script>
<script type="text/javascript" src="https://s3.amazonaws.com/mazkaracdn/js/loadingoverlay.min.js"></script>
<script type="text/javascript">


(function(){


  var app = angular.module('mazkaraApp', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<{');
    $interpolateProvider.endSymbol('}>');
  }).controller('NavigationController', ['$scope', '$location', function($scope, $location){
    $scope.goToUrl = function(url){
      $location.path(url);
    };
  }]).controller('VenuesListingController', 
                  ['$scope', '$http', '$log', '$routeParams', '$route', 
                  function($scope, $http, $log, $routeParams, $route){
    var venues_listing = this;
    venues_listing.venues = [];
    venues_listing.current_zone = '<?php echo isset($sub_zone) && ($sub_zone!=false) ? $sub_zone->id : $zone->id;?>';
    venues_listing.current_zone_name = '<?php echo isset($sub_zone) && ($sub_zone!=false) ? $sub_zone->name : $zone->name;?>';
    venues_listing.current_zone_slug = '<?php echo isset($sub_zone) && ($sub_zone!=false) ? $sub_zone->slug : $zone->slug;?>';
    venues_listing.current_city = '<?php echo $zone->id;?>';
    venues_listing.current_city_name = '<?php echo $zone->name;?>';
    venues_listing.current_city_slug = '<?php echo $zone->slug;?>';
    venues_listing.filters_applied = [];
    venues_listing.ads = [];

    this.categories = [];

    this.breadcrumbs = [];

    filters = {};
    filters.highlights = [];
    filters.genders = [];
    filters.categories = [];
    filters.zones = <?php echo json_encode($zones);?>;
    filters.services = <?php echo json_encode($services);?>;
    filters.prices = [{id:1, name:'Economical'}, {id:2, name:'Premium'}, {id:3, name:'Luxury'}];
    filters.sortables = { rating:{ label:'rating', value:'Rating (High to Low)' },
                          name:{ label:'name', value:'Name (High to Low)' },
                          popularity:{ label:'popularity', value:'Popularity (High to Low)' }
                        };
    filters.currentSort = filters.sortables['popularity'];
    
    venues_listing.filters = filters;
    venues_listing.selects = {  highlights:[], 
                                prices:[], 
                                categories:[], 
                                services:[],
                                service:<?php echo isset($selected_service)&&($selected_service!=false)?json_encode($selected_service):'null';?>, 
                                category:<?php echo isset($selected_category)&&($selected_category!=false)?json_encode($selected_category):'null';?>, 
                                zone:<?php echo isset($sub_zone) && ($sub_zone!=false) ? json_encode($sub_zone) : json_encode($zone);?>
                              };

    venues_listing.totalItems = 0;
    venues_listing.currentPage = 1;
    venues_listing.maxSize = 8;
    venues_listing.numPerPage = 20;
    venues_listing.totalPages = 0;
    venues_listing.initialized  = 0;
    venues_listing.titleText = 'Salons and Spas';
    venues_listing.seoTitle = 'Salons and Spas in <?php echo isset($sub_zone) && ($sub_zone!=false) ? ($sub_zone->name) : ($zone->name);?> - Fabogo';
    venues_listing.session_search_page='';
    venues_listing.session_search_parameters='';

    h = {};
    h['ac']               = 'highlights-ac highlights';
    h['air-conditioned']  = 'highlights-ac highlights';
    h['credit-card']      = 'highlights-credit-card highlights';
    h['deal']             = 'highlights-deal highlights';
    h['economical']       = 'highlights-economical highlights';
    h['home-services']    = 'highlights-home-services highlights';
    h['kids-services']    = 'highlights-kids-services highlights';
    h['4-or-5-star-luxury-hotel'] = 'fa fa-star-o';
    h['luxury']                   = 'highlights-luxury highlights';
    h['mens-only']                = 'highlights-mens-only highlights';
    h['premium']                  = 'highlights-premium highlights';
    h['tv'] = 'highlights-tv highlights';
    h['products-sold'] = 'fa fa-shopping-cart';
    h['unisex'] = 'highlights-unisex highlights';
    h['walk-ins-allowed'] = 'highlights-walk-ins-allowed highlights';
    h['ladies-only'] = 'highlights-ladies-only highlights';
    h['wi-fi'] = 'highlights-wi-fi';
    
    venues_listing.highlights_icons = h;

    prices = {0:''}

    prices[1] = 'economical';

    prices[2] = 'luxury';

    prices[3] = 'premium';
    
    venues_listing.prices_names = prices;


    $scope.$on('$routeChangeSuccess', function() {
      venues_listing.filterVenues();
    });

    this.resetCurrentPage = function(){
      venues_listing.currentPage = 1;
    }

    this.mzk_city_slug = function(params){
      url = '/' + venues_listing.current_city_slug + '/';
      append = 'salons-and-spas';
      if(params.category){
        append = params.category.slug + 's';
      }
      if(params.subzone){
        if(params.subzone.slug!=venues_listing.current_city_slug){
          append = params.subzone.slug + '-' + append;
        }
      }
      if(params.service){
        append = append + '/' + params.service.slug;
      }
      sort = '';
      if(params.sort){
        sort = '?sort='+params.sort.label;
      }

      url = url + append+sort;
      return url;
    };

    this.mzk_single_slug = function(venue){
      url = '/' + venue.citySlug + '/' + venue.slug;
      return url;
    };

    this.mzk_single_slug_photos = function(venue){
      url = venues_listing.mzk_single_slug(venue) + '/photos';
      return url;
    };

    this.mzk_single_slug_rate_cards = function(venue){
      url = venues_listing.mzk_single_slug(venue) + '/rate-card';
      return url;
    };


    var init = function () {

      // $("#fakeloader").fakeLoader({
        // timeToHide:1300,
        // zIndex: '89',
        // bgColor:"#fff",
        // spinner:"spinner2"
      // });

      venues_listing.setupFilters();
      $('.preangular.hidden').removeClass('hidden');
      venues_listing.setTitleText();

      if ($routeParams.segment) { // if we have a city in this
        venues_listing.setupFilters();
        venues_listing.filterVenues();
      }
    };

    var getHighlightCssClass = function(highlight_slug){
      return venues_listing.highlights_icons[highlight_slug];
    }

    var getPriceIconCss = function(price_id){
      str = '';
      switch(price_id){
        case 1:
          str = 'economical';
        break;
        case 2:
          str = 'premium';
        break;
        case 3:
          str = 'luxury';
        break;
      };

      return (venues_listing.highlights_icons[str]);
    };

    var getPriceName = function(price_id){
      str = '';
      switch(price_id){
        case 1:
          str = 'Economical';
        break;
        case 2:
          str = 'Premium';
        break;
        case 3:
          str = 'Luxury';
        break;
      };

      return str;
    };

    this.isAnyCategorySelected = function(){
      if(venues_listing.selects.category == null){
        return true;
      }
      return false;
    }

    this.updateZone = function(zone){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      venues_listing.selects.zone = zone;
      venues_listing.resetCurrentPage();
      venues_listing.setTitleText();
      //venues_listing.filterVenues();
    };

    this.ratingColor = function(rating){
      
    };

    this.updateSort = function(sortr){
      venues_listing.filters.currentSort = sortr;
    }
    this.isSortSelected = function(sortr){
      if(venues_listing.filters.currentSort == null){
        return false;
      }
      if(venues_listing.filters.currentSort.label == sortr.label){
        return true;
      }

      return false;
    }

    this.updateService = function(service){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      venues_listing.selects.service = service;
      venues_listing.resetCurrentPage();
      venues_listing.setTitleText();
      //venues_listing.filterVenues();
    };

    this.isServiceSelected = function(service){
      if(venues_listing.selects.service == null){
        return false;
      }
      if(venues_listing.selects.service.id == service.id){
        return true;
      }

      return false;
    }


    this.updateCategory = function(category){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      venues_listing.selects.category = category;
      venues_listing.resetCurrentPage();
      venues_listing.setTitleText();
      //venues_listing.filterVenues();
    };

    this.isCategorySelected = function(category){
      if(venues_listing.selects.category == null){
        return false;
      }
      if(venues_listing.selects.category.id == category.id){
        return true;
      }

      return false;
    }


    this.setTitleText = function(){
      txt = 'Salons and Spas';
      append = '';
      if(venues_listing.selects.category){
        txt = venues_listing.selects.category.name+'s';
      }else{
        txt = 'Salons and Spas';
      }
      if(venues_listing.selects.service != null){
        txt = venues_listing.selects.service.name;
      }

      if(venues_listing.selects.zone){
        append = ' in ' + venues_listing.selects.zone.name;
      }else{
        append = ' in ' + venues_listing.current_city_name;
      }

      venues_listing.titleText = txt + append;
      venues_listing.seoTitle = venues_listing.titleText + ' - Fabogo';
    }


    this.getTitleText = function(){
      return venues_listing.titleText;
    }

    this.getFilterParameters = function(){

      data = {};
      data['batchNumber'] = venues_listing.currentPage;
      data['zoneID'] = venues_listing.selects.zone.id;
      data['cityID'] = venues_listing.current_city;
      data['queryText'] = '';
      data['costEstimate'] = venues_listing.selects.prices.join(',');
      data['sortBy'] = venues_listing.filters.currentSort.label;
      data['highlightsCSV'] =  venues_listing.selects.highlights.join(',');
      data['categoryIDCSV'] = venues_listing.selects.category != null ? venues_listing.selects.category.id : '';
      data['serviceIDCSV']   =  venues_listing.selects.service?venues_listing.selects.service.id:'';//s.join(',');
      data['session_search_page'] =  venues_listing.session_search_page;
      data['session_search_parameters'] =  venues_listing.session_search_parameters;
      return data;
    }

    this.setupFilters = function(){


        venues_listing.filters.categories = <?php echo json_encode($categories);?>;  //data.data.categoryArray;
        all_highlights = <?php echo json_encode($highlights);?>;  // = data.data.highlightArray;
        genders = [];
        highlights = [];

        for(i in all_highlights){
          if(['4','5','6'].indexOf(all_highlights[i].id)>=0){
            genders.push(all_highlights[i]);
          }else{
            highlights.push(all_highlights[i]);
          }
        }

        //venues_listing.filters.services = data.data.serviceArray;
        venues_listing.filters.genders = genders;
        venues_listing.filters.highlights = highlights;

        venues_listing.setupBreadCrumbs();

      // });

    };

    this.setupBreadCrumbs = function(){
              venues_listing.breadcrumbs = [];
        // add base city

        venues_listing.breadcrumbs.push({label:venues_listing.current_city_name, url:venues_listing.mzk_city_slug({})});
        prms = {};
        if(venues_listing.selects.zone){
          if((venues_listing.selects.zone.name != venues_listing.current_city_name)){
            prms['subzone'] = venues_listing.selects.zone;
            venues_listing.breadcrumbs.push({label:venues_listing.selects.zone.name, 
                                                   url:venues_listing.mzk_city_slug(prms)});
          }
        }

        if(venues_listing.selects.category!=null){
          prms['category'] = venues_listing.selects.category;
          venues_listing.breadcrumbs.push({label:venues_listing.selects.category.name+'s', 
                                                 url:venues_listing.mzk_city_slug(prms)});

        }else{
          venues_listing.breadcrumbs.push({label:'Salons and Spas', 
                                                 url:venues_listing.mzk_city_slug(prms)});
        }



      };



//    this.isServiceSelected = function(service){
//      // return true if added and false if removed
//      //idx = venues_listing.filters.genders.indexOf(gender);
//      idx = $.inArray(price, venues_listing.selects.services);
//      if(idx >=0){
//        return true;
//      }else{
//        return false;
//      }
//    };
//
    this.updateServices = function(service){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(price, venues_listing.selects.services);
      venues_listing.resetCurrentPage();

      if(idx >=0){
        // it exists remove it and return a false;
        venues_listing.selects.services.splice(idx, 1);
        return false;
      }else{
        venues_listing.selects.services.push(service);
        return true;
      }
    };


    this.isZoneSelected = function(zone){
      if(venues_listing.selects.zone == null){
        return false;
      }

      if(venues_listing.selects.zone.id == zone.id){
        return true;
      }else{
        return false;
      }
    };


    this.isPriceSelected = function(price){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(price, venues_listing.selects.prices);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
    };

    this.updatePrices = function(price){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(price, venues_listing.selects.prices);

      if(idx >=0){
        // it exists remove it and return a false;
        venues_listing.selects.prices.splice(idx, 1);
        venues_listing.resetCurrentPage();
        venues_listing.filterVenues();
        
        return false;
      }else{
        venues_listing.selects.prices.push(price);
        venues_listing.resetCurrentPage();
        venues_listing.filterVenues();

        return true;
      }
    };



    this.isGenderSelected = function(gender){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(gender, venues_listing.selects.genders);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
    };

    this.updateGenders = function(gender){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(gender, venues_listing.selects.genders);
      venues_listing.resetCurrentPage();

      if(idx >=0){
        // it exists remove it and return a false;
        venues_listing.selects.genders.splice(idx, 1);
      }else{
        venues_listing.selects.genders.push(gender);
      }

      venues_listing.filterVenues();
    };



    this.isHighlightSelected = function(highlight){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(highlight, venues_listing.selects.highlights);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
    };

    this.updateHighlights = function(highlight){
      // return true if added and false if removed
      //idx = venues_listing.filters.genders.indexOf(gender);
      idx = $.inArray(highlight, venues_listing.selects.highlights);
      venues_listing.resetCurrentPage();

      if(idx >=0){
        // it exists remove it and return a false;
        venues_listing.selects.highlights.splice(idx, 1);
      }else{
        venues_listing.selects.highlights.push(highlight);
      }
        venues_listing.filterVenues();
    };



    this.filterVenues = function(){
      url = 'http://fabogo-dev2.ap-northeast-1.elasticbeanstalk.com/api/searchForWeb';
      
      url = 'http://127.0.0.1:8000/api/searchForWeb';
      url = 'http://fabogosandbox-dev.ap-northeast-1.elasticbeanstalk.com/api/searchForWeb';

      //$("#fakeloader").show();
      $("#venues-listing-holder").LoadingOverlay("show", { imagePosition:'center 120px'});
      //$('#venues-listing-holder').css('opacity', 0.5);

      data = this.getFilterParameters();
      $http.post(url, jQuery.param(data), {
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }
      ).success(function(data){
        if(!('data' in data)){
          venues_listing.filterVenues();
          return;
        }
        venues_listing.venues = data.data.businessArray;
        venues_listing.totalItems = data.data.batchDetails.totalNumberOfBusiness;
        venues_listing.currentPage = data.data.batchDetails.batchNumber;
        venues_listing.ads = data.data.AdsArray;
        venues_listing.initialized = 1;
        $('#msg-no-results').removeClass('hidden');
        venues_listing.session_search_page = data.data.debug.session_search_page;
        venues_listing.session_search_parameters = data.data.debug.session_search_parameters;
        venues_listing.setupBreadCrumbs();
        //$("#fakeloader").hide();
        $("#venues-listing-holder").LoadingOverlay("hide");
        $('#msg-pre-page-loader').remove();

        //$('#venues-listing-holder').css('opacity', 1);

      });
    };

    // fire on controller loaded
    init();    

    this.slugSingle = function(business){
      //return mzk_slug_single(business);
    };

    this.getFilterableServices = function(){
      return venues_listing.filters.services.slice(1,9);
    };

    this.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    this.pageChanged = function() {
      venues_listing.filterVenues();
    };


  }]).config(function($routeProvider, $locationProvider) {

    $routeProvider
     .when('/:zone/salons-and-spas/:services?', { // basic listing
      controller: 'VenuesListingController'
    }).when('/:zone/:subzone/:services?', { // basic listing with subzone
      controller: 'VenuesListingController'
    });



    // use the HTML5 History API
    $locationProvider.html5Mode(true);
  });



}());

$(function(){
  $('.bxslider').bxSlider({ slideMargin:20, pager:false, onSliderLoad:function(){
   $('.show-on-load').slideDown( "slow");
  }});

//  .slick({
//    slidesToShow: 1,
//    variableWidth: false,
//    infinite: true,
//    nextArrow: '<i class="fa gray fa-chevron-right slicker-next absolute" style="cursor:pointer;top:35%;"></i>',
//    prevArrow: '<i class="fa gray fa-chevron-left slicker-prev  absolute" style="cursor:pointer;top:35%;"></i>'
//  });
  
  $('.toggl-services').click(function(){
    h = $(this).find('.toggl-hide').first();

    if($(h).hasClass('hidden')){ // if we are showing all then hide em
      $('#service-list li.hideable').addClass('hidden');
      $(this).find('.toggl-hide').removeClass('hidden');
      $(this).find('.toggl-show').addClass('hidden');
    }else{
      $('#service-list li.hideable').removeClass('hidden');
      $(this).find('.toggl-hide').addClass('hidden');
      $(this).find('.toggl-show').removeClass('hidden');
    }
  });


  $('.mobile-category-filter').slick({
    slidesToShow: 3,
    infinite: false,
    arrows: false
  });
  $('.mobile-category-filter').removeClass('hidden');

  function setupPopoversForMobileAndDesk(){
    $(".popover-mobile").popover({
      container: 'body',
      html: true,
      content: function () {
        p = $(this).data('phone').split('|');
        html = '';
        for(i in p){
          html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
        }
        return html;
      }
    }).click(function(e) {
        e.preventDefault();
    });

    $(".popover-desk").popover({

        container: 'body',
        html: true,
        content: function () {
          p = $(this).data('phone').split('|');
          html = '';
          for(i in p){
            html = html + '<div><a class="btn btn-default dpb text-center" href="javascript:void(0)">'+p[i]+'</a></div>';
          }
          return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });
  }

  setupPopoversForMobileAndDesk();

  $('.call-button').click(function(){
    dta = { business_id: $(this).data('business') };
    $.ajax({
      url:'/businesses/increment/calls/count', method:'GET', data: dta
    });
    ga('send', 'event', 'click on call button', ($(this).hasClass('popover-desk')?'desktop':'mobile'), '{{ Request::url() }}');
  });

  $('.single-native-ctrable').click(function(){
    dta = { ad_id: $(this).data('ad') };
    $.ajax({
      url:'/ads/increment/ctr', method:'GET', data: dta
    });
  });

  $('.package-card-holder').each(function(iv, elem){
    $(elem).magnificPopup({
      delegate: 'a.image', 
      type: 'image',
      gallery: {
        enabled: true
      }
    });
  });

  $('.star-rating-single').rating();//{ readOnly: true, cancel: false, half: true,

  /*$('.filter').focus(function(){
    rel = $(this).attr('rel');
    $('#'+rel).slideDown();
  });

  $('.filter').blur(function(){
    rel = $(this).attr('rel');
    $('#'+rel).slideUp();
  });*/
  $('body').on('focus', '.filter', function(event){
    rel = $(this).attr('rel');
    $('#'+rel).find('.showable:lt(10)').removeClass('hidden');//slideDown();
  });

//  $('.filter').blur(function(){
//    rel = $(this).attr('rel');
//    $('#'+rel).find('li').addClass('hidden');
//  });

  $('#zones-filter-holder').mouseleave(function(){
    f = $('#zones-filter-holder').find('.filter').first();
    rel = $(f).attr('rel');
    $('#'+rel).find('li').addClass('hidden');
    $(f).blur();
  });

  
  
  $('body').on('keyup', '.filter', function(event){
    var ul = $('#' + $(this).attr('rel'));
    if($(this).val() == ''){
      lis = $(ul).find('li');
      $(lis).addClass('hidden');
      lis = lis.slice(0, 15);
      $(lis).removeClass('hidden');
      return;
    }

    var search = $(this).val();
    var rg = new RegExp('('+$.unique(search.split(" ")).join("|")+')',"i");
    $(ul).find('li a').each(function(){
      c = $(ul).find('li.hidden');
      /*if(c.length < 5)*/
      {
        if($.trim($(this).text()).search(rg) == -1) {
          $(this).parent().addClass('hidden');
        } else {
          if(c.length > 15){
            $(this).parent('li').removeClass('hidden');
          }
        }        
      }
    });
  });
})
</script>
@stop
@endsection
