<?php $total_photos = isset($total_photos)?$total_photos:5;?>
@if(count($rateCards) >0)
  <div class=" {{$business->total_packages_count > 0?'pt10':''}}  ">
    <h2 class="item-headers">MENU</h2>
    <div class="rate-card-holder">
      @foreach(mzk_secure_iterable($rateCards) as $ii=>$one_photo)
        @if((count($rateCards)>($total_photos)) && ($ii >=(count($rateCards))))
        @else
          @if($one_photo->hasImage())
            <a  href="{{ mzk_cloudfront_image($one_photo->image->url('base')) }}" 
                class=" ba border-radius-5  {{($ii+1)%(ceil($total_photos/5))==0?'mr5':''}} {{ ($ii>=($total_photos-1))?'hide':''}}    image" style="display:inline-block" ><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" alt="{{ $business->name }} + Rate Card {{ $ii+1 }}" class="venue-thumbnail border-radius-5  op85-on-hover rate-card-image" /></a>
          @endif
        @endif
      @endforeach
      @if(count($rateCards)>($total_photos))
        <a style="background-image:url({{ mzk_cloudfront_image($one_photo->image->url('small')) }});display:inline-block; vertical-align:middle; text-align:center" 
           class="fs150 border-radius-5  trigger-gallery-open ba sq-card   mb0 fw300 btn btn-default p0 bg-lite-gray venue-thumbnail image"
           href="{{ mzk_cloudfront_image($one_photo->image->url('base')) }}">
          <div class="va-container va-container-v va-container-h overlap-transparent">
            <div class="va-middle text-center force-white" style="z-index:999;">
          +{{count($business->rateCards)-($total_photos)}}
          
        </div></div>
        </a>
      @endif
    </div>
  </div>
@endif


