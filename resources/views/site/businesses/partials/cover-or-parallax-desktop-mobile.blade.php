<?php 
if(!isset($phones)):
  $phones = $business->displayablePhone();
endif;
$no_rating = isset($no_rating)?$no_rating:false;
?>
@if((isset($business->cover)&&($business->cover!=null)) || $business->hasAnyCoverImage())
  @if($business->hasCoverYPos())
    <div class="cover-parallax hide-only-mobile container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->getCoverUrl('xlarge'))}}) no-repeat {{ $business->getCoverYPosCss() }} rgba(200, 82, 126, 0); background-size:100%;"></div>
  @else
    <div class="cover-parallax hide-only-mobile container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->getCoverUrl('xlarge'))}}) no-repeat center center rgba(200, 82, 126, 0); background-size:100%;"></div>
  @endif
  <div class="overlap-parallax text-center  container p0 overlap-transparent hide-only-mobile" style=""></div>
  <div class="container relative height-283-mobile" style="z-index:100;height:396px;">
    <div class="w100pc absolute top-0-mobile top-0" style="z-index: 999;">
      @include('elements.searcher')
      <div class="mt0 force-white">
        {{ $breadcrumbs->render() }}
      </div>
    </div>
    <div class="overlap-parallax-content show-only-mobile relative" style="z-index:90">
      <span style="position:absolute;top:7px;right:0px;">
        @if(Auth::check())
          <a href="javascript:void(0)"  title="Favorite {{ $business->name }}" class="fs200 lnk-run-on-app {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart hide-when-unfavorite"></i> 
            <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
          </a>
        @else
          <a href="/users/login" title="Favorite {{ $business->name }}" class="fs200 page-scroll lnk-run-on-app ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart-o " style="color:#fff;"></i> 
          </a>
        @endif
      </span>
      <div class="w100pc absolute " style="bottom:25px;">
        <div class="mt0 ">&nbsp;</div>
        @include('site.businesses.partials.title-and-rating-mobile')
      </div>
    </div>
    <div class="overlap-parallax-content hide-only-mobile" style="">
      <div class="row">
        <div class="col-md-12 p10 mt10 force-white"></div>
      </div>
      <div class="row">
        <div class="col-md-9 center-on-mobile" >
          <div class="force-white" >
            <div class="mt140 pt10 hide-only-mobile"></div>
            @include('site.businesses.partials.title-and-rating-for-parallax', ['business'=>$business, 'phones'=>$phones])
          </div>
          <div class="center-on-mobile btns-ghosted mb10">
            @include('site.businesses.partials.action-bar', ['business'=>$business, 'no_rating'=>$no_rating, 'phones'=>$phones])
          </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>
  <div class="cover-parallax-mobile show-only-mobile container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->getCoverUrl('large'))}}) no-repeat top center rgba(200, 82, 126, 0);"></div>
  <div class="overlap-parallax-mobile text-center container p0 overlap-transparent show-only-mobile" style="z-index:7"></div>
@else
  <div class="cover-parallax-mobile show-only-mobile container p0 text-center bg-burgundy p0"></div>
  <div class="overlap-parallax-mobile text-center container p0 overlap-transparent show-only-mobile" style="z-index:7"></div>
  <div class="cover-parallax text-center container bg-burgundy hide-only-mobile" style=""></div>
  <div class="container relative height-283-mobile" style="z-index:100;height:396px;">
    <div class="w100pc absolute top-0-mobile top-0" style="z-index: 999;">
      @include('elements.searcher')
      <div class="mt0 force-white">
        {{ $breadcrumbs->render() }}
      </div>
    </div>
    <div class="overlap-parallax-content show-only-mobile relative" style="z-index:90">
      <span style="position:absolute;top:7px;right:0px;">
        @if(Auth::check())
          <a href="javascript:void(0)"  title="Favorite {{ $business->name }}" class="fs200 lnk-run-on-app {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart hide-when-unfavorite"></i> 
            <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
          </a>
        @else
          <a href="/users/login" title="Favorite {{ $business->name }}" class="fs200 page-scroll lnk-run-on-app ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart-o " style="color:#fff;"></i> 
          </a>
        @endif
      </span>
      <div class="w100pc absolute " style="bottom:25px;">
        <div class="mt0 ">&nbsp;</div>
        @include('site.businesses.partials.title-and-rating-mobile')
      </div>
    </div>
    <div class="overlap-parallax-content hide-only-mobile" style="">
      <div class="row">
        <div class="col-md-12 p5 force-white">
        </div>
      </div>
    <div class="row">
      <div class="col-md-9 center-on-mobile" >
        <div class=" force-white " >
          <div class="mt140 pt10 hide-only-mobile"></div>
          @include('site.businesses.partials.title-and-rating-for-parallax', ['business'=>$business, 'phones'=>$phones])
        </div>
        <div class="btns-ghosted mb10">
          @include('site.businesses.partials.action-bar', ['business'=>$business, 'no_rating'=>$no_rating, 'phones'=>$phones])
        </div>
      </div>
      <div class="col-md-3">
      </div>
    </div>
  </div>
</div>

@endif
