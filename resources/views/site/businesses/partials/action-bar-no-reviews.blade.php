    <div class="row">
      <div class="col-md-12">
        <div class="mt5 mb5">
          <?php 
          if(!isset($phones)):
            $phones = $business->displayablePhone();
          endif;
          ?>
          @if(count($phones)==1)
            <center>
              <a class="show-only-mobile btn btn-success mb10 btn call-button" data-business="{{$business->id}} " href="tel:{{ $phones }}">Call & Book</a>
            </center>
          @else
            <center>
              <a data-placement="top" style="width:40%; " class="show-only-mobile mb10 btn btn-success btn popover-mobile call-button" data-business="{{$business->id}} " data-toggle="popover" data-phone="{{ join('|', $phones) }}" data-container="body" type="button" data-html="true" href="javascript:void(0)" ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">Call & Book</a>
            </center>
          @endif
          @if(Auth::check())
            <a href="javascript:void(0)" class="btn mr2 {{ Auth::check()? ($business->favourited(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default" rel="{{$business->id}}"  data-route="businesses">
              <i class="fa fa-heart"></i> 
            </a>
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('moderator'))
            <a href="/content/businesses/{{$business->id}}" title="Edit as Admin" class="btn mr2 btn-default">
              <i class="fa fa-shield"></i>
            </a>

            @endif

          @else
            <a href="/users/login" class="btn mr2 ajax-popup-link btn-default  " rel="{{$business->id}}">
              <i class="fa fa-heart"></i> 
            </a>
          @endif
          <a href="javascript:void(0)" class="sharable mr2 btn btn-default btn" data-sharable="Business" data-sharableid="{{$business->id}}"  title="Share on Facebook">
            <span class="fa fa-share-square-o"></span>
            <span class="hide-only-mobile">Share</span>
          </a>
            <span class="btn-group mr2">
              <span class="btn btn-default">
                Your Rating
              </span>
            <span class="btn btn-default hidden-mobile pt0 pb0 pl10 pr10" style="min-height:34px;" >

              <?php $default_rating = Auth::check() ? $business->ratingBy(Auth::user()->id) : $business->ratingByIP(Request::getClientIp()); ?>

              {{ Form::number('rate',  $default_rating, array('class'=>'form-control hidden rating',  'data-score'=>$default_rating, 'data-size'=>'xs', 'rel'=>$business->id)) }}

            </span>
          </span>
        </div>
      </div>
    </div>
