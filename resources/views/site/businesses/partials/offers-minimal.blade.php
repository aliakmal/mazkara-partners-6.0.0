        <div class="  mb10">
          <div style="background-image:url({{ $offer->business->getCoverUrl('medium') }}); " class=" native-collectable border-top-radius-3 bg-image ba bb0 h150 relative" >
            <div class="va-container overlap-transparent-4x va-container-v va-container-h border-top-radius-3 ">
            <div class="absolute force-white " style="bottom:5px; left:5px;">
              <div class="fw700 fs125">{{ $offer->business->name }}</div>
              <div>{{$offer->business->zone_cache}}</div>
            </div>
            
          </div>
          </div>
          <div class=" native-collectable-content-holder">
            <div class=" p10 alert-info">
              <div style="width:100%" class=" dpib fs110">
                <span style="text-decoration:line-through;" class="fs90 medium-gray">
                  {{ mzk_price($offer->original_price, '&nbsp;') }}
                </span>
                <span class="fs120 text-black fw900">
                  {{ mzk_offer_price($offer, '&nbsp;') }}
                </span>
              </div>
            </div>
          </div>
          <div class="p5 ba  bt0 hauto border-bottom-radius-3 ">
            <div class="dpb relative h75">
              <div class="fw700 fs110 pb10 p5">{{ $offer->body }}</div>
              <div class="italic absolute" style="left:5px;bottom:5px;">
                Valid till {{ mzk_f_date($offer->valid_until, 'jS M') }}
              </div>
            </div>

          </div>
        </div>
