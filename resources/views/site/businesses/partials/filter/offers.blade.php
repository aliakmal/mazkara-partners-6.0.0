
<li class="list-group-item pb20 bt0 mt0 bb0">
  <ul class="list-unstyled padded5">
    <li class="hidden">
      @if(isset($params['packages'])&&($params['packages']=='true'))
        <?php $prms = $params; unset($prms['packages']);unset($prms['service']);unset($prms['category']);?>
        <a title="Clear filter" rel="nofollow" class="pull-left" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa fa-check-square selected mr5"></i>
          <b>Packages Available</b>
        </a>
        <span class="pull-right">
          <img src="{{mzk_assets('assets/tag-icon.png')}}" width="16" style="width:16px;" />
        </span>
      @else
        <?php $prms = $params; unset($prms['city']);$prms['packages'] = 'true';unset($prms['service']);unset($prms['category']);?>
        <a class="pull-left" rel="nofollow" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa gray fa-square-o mr5"></i>
          Packages Available
        </a>
        <span class="pull-right">
          <img src="{{mzk_assets('assets/tag-icon.png')}}" width="16" style="width:16px;" />
        </span>
      @endif
      <div class="clearfix"></div>
    </li>
    <li>
    
      @if(isset($params['specials'])&&($params['specials']=='true'))
      
        <?php $prms = $params; unset($prms['specials']);unset($prms['service']);unset($prms['category']);?>
        <a title="Clear filter" rel="nofollow" class="pull-left" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa fa-check-square selected mr5"></i>
          <b>Specials</b>
        </a>
        <span class="pull-right">
          <span class="label label-info">SPECIALS</span>
        </span>
      @else
       <?php $prms = $params; unset($prms['city']);$prms['specials'] = 'true';unset($prms['service']);unset($prms['category']);?>
        <a class="pull-left" rel="nofollow" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa gray fa-square-o mr5"></i>
          Special Offers
        </a>
        <span class="pull-right">
          <span class="label label-info">SPECIALS</span>
        </span>
      @endif
      <div class="clearfix"></div>
    </li>
  </ul>
</li>