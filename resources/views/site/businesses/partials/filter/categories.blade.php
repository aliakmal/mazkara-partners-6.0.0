<li class="list-group-item">
  <b>Looking for?</b>
</li>
<li class="list-group-item">
  <ul class="list-unstyled padded5 ">
    @foreach($categories as $category)
    <?php 
    $prms = $params;
    //if(!isset($params['category'])){ // uncomment for multiple
    $prms['category'] = array();
    //}
    $prms['category'][] = $category['id']; 
    ?>
    <li>
      @if(isset($params['category']) && (in_array($category['id'], $params['category'])))
        <?php unset($prms['category']);?>
        <a title="Clear filter" href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}" class="">
<div>          <i class="fa  fa-circle selected mr5"></i>
          <b>{{ $category['name'] }}</b>
      <span class="pull-right">
        <i class="flaticon {{ ViewHelper::iconCategory($category['slug']) }}"></i>
      </span>
</div>
        </a>
      @else
        <a href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}" class="">
      <div>
          <i class="fa gray fa-circle-o mr5"></i>
          {{ $category['name'] }}
                <span class="pull-right">
        <i class="flaticon {{ ViewHelper::iconCategory($category['slug']) }}"></i>
      </span>
</div>
        </a>
      @endif
      <div class="clearfix"></div>
    </li>
    @endforeach
  </ul>
</li>
