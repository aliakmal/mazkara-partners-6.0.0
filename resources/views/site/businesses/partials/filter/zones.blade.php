<div  class="pt5 mb10 pb5" >
  <div class="  pt5 pl0 pr0 p15  pb5 mb5">  <b>WHERE?</b>
</div>

  <ul class="list-unstyled mb0">


<li class="filter-holder list-group-item p0 b0  pt0">
  <input class="filter form-control input-sm   b0" rel="zone-list"  placeholder="Type Location" />
</li>
<li class="list-group-item p0 pb10 b0">
    @if(isset($sub_zone))
      <?php $afilter = $params;

        ?>
    <ul  class="list-unstyled padded5 ">
        <li>
            <a title="Clear filter" href="{{ MazkaraHelper::slugCity(null, $afilter) }}">
              <div><i class="fa fa-circle selected mr5"></i>
              <b>{{ $sub_zone->name }}</b>
              <span class="pull-right"><i class="fa fa-times"></i></span></div>
            </a>
        </li>
      </ul>
    @endif
  <ul id="zone-list"  class="list-unstyled padded5 " style="display:none">


    @foreach($zones as $i=>$zone)
    <?php 
    $prms = $params;
    unset($prms['city']);
    
    //if(!isset($params['zone'])){
      $prms['zone'] = array();
    //}

    $prms['zone'][] = $zone['id'];

    $prms['subzone']= $zone['slug'];
    ?>
      @if(isset($sub_zone) && ($sub_zone->id == $zone['id']))
      @else
    <li class="{{ $i>15 ? 'hidden':''}}">
        <a href="{{ MazkaraHelper::slugCityZone($zone, $prms)}}">
          <i class="fa gray fa-circle-o mr5"></i>

          {{ $zone['name'] }}
        </a>
    </li>
        @endif
    @endforeach
  </ul>
</li>
</ul></div>