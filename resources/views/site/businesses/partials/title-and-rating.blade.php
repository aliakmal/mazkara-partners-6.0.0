<div class="row mt25">
  <div class="col-md-12">
    <div class="header bb">
      <div class="pull-right">
        <span class="search-item-rating-num-votes"><b>{{$business->accumulatedReviewsCount()}}</b> votes</span>
          {{ ViewHelper::rateBusinessNiblet($business, true)}}<br/>
      </div>
      <h1 class="item-name mt50 fs220 fw300">
        <a href="{{MazkaraHelper::slugSingle($business)}}" title="{{ $business->name }}">
          {{ $business->name }}
        </a>
      </h1>
    </div>
  </div>
</div>