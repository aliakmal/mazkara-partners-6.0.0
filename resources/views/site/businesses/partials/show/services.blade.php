<?php $services = $business_services;
?>
@if(count($services)>0)
  <div class=" service-container">
    <h2 class="item-headers ">SERVICES</h2>
    <?php
      $current_heading = '';
    ?>
    @foreach($services as $ii=>$service)
      @if($service->parent_id != $current_heading)
        <?php $current_heading = $service->parent_id;?>
        <div style="" class="p5 {{ $ii>8 ? 'hidden hideable':'' }} service-object bg-lite-gray">
          <b>{{ strtoupper(MazkaraHelper::getServicesAttribute($service->parent_id, 'name')) }}</b>
        </div>
      @endif
      <div style=" " class="bb {{ $ii>8 ? 'hidden hideable':'' }} service-object p5">
        {{ strtoupper($service->name) }}
        <div class="pull-right">
          {{ $service->getStartingPrice(MazkaraHelper::getCitySlugFromID($business->city_id)) }}
        </div>
      </div>
    @endforeach
    <a id="lnk-to-display-all-services" href="javascript:void(0)" class="dpb bg-lite-gray-3 border-radius-3  mt10   pb10 pt10 text-center">VIEW ALL SERVICES</a>
    <a id="lnk-to-hide-all-services" href="javascript:void(0)" style="display:none" class="dpb bg-lite-gray-3 border-radius-3  mt10   pb10 pt10 text-center">HIDE SERVICES</a>
  </div>
@endif

