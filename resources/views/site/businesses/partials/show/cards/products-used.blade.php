
<?php return '';?>
@if($business->brands()->count()>0)
  <div class="pt10 pb10  text-center p10 bg-lite-gray -3">
    <div class="dpb pb10">
    <!--
      @if($business->isOpenNow())
        <span class="  label label-success mr10 "><small>OPEN NOW</small></span>
      @else
        <span class="  label mr10 "><small>CLOSED NOW</small></span>
      @endif
    -->
    </div>
    <h2 class="item-headers dpib">PRODUCTS USED HERE</h2>
    @foreach($business->brands as $brand)
      <div class="mb10">
        <img src="{{ $brand->logo->image->url() }}" class="img-thumbnail" />
      </div>
    @endforeach

  </div>
@endif