@if($business->categories()->count()>0)
  <div class="pt10 pb10 bbb ">
    <span class="fs125 fw900 pb10 dpb">
      Establishment Type
    </span>
    <?php $cats = [];?>
    @foreach($business->categories as $category)
      @if($category->isActive())
        <?php ob_start();?>
        <a  href="{{ MazkaraHelper::slugCity(null, ['category'=>[$category->id]]) }}">
          {{ ($category->name)}}
        </a>
        <?php $cats[] = trim(ob_get_contents());ob_end_clean();?>
      @endif
    @endforeach
    <p>{{ join(', ', $cats)}}</p>
  </div>
@endif
