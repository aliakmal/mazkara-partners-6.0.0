<?php $highlights = $business->highlights;?>
@if(count($highlights)>0)
  <div class="pt10 pb10 bbb ">
    <span class="fs125 fw900 pb10 dpb">
      Highlights
    </span>
      <ul class="list-unstyled ">
      @foreach($highlights as $highlight)
        @if($highlight->isActive())
          <li >
              {{ ($highlight->name)}}
          </li>
        @endif
      @endforeach
    </ul>
  </div>

@endif
