<script type="text/javascript">
$(function(){

  $('#lnk-to-display-all-services, #lnk-to-display-all-services-mobile').click(function(){
    $('.service-object').removeClass('hidden');
    $('#lnk-to-display-all-services, #lnk-to-display-all-services-mobile').hide();
    $('#lnk-to-hide-all-services, #lnk-to-hide-all-services-mobile').show();
  });

  $('#lnk-to-hide-all-services, #lnk-to-hide-all-services-mobile').click(function(){
    $('.service-object.hideable').addClass('hidden');
    $('#lnk-to-display-all-services, #lnk-to-display-all-services-mobile').show();
    $('#lnk-to-hide-all-services, #lnk-to-hide-all-services-mobile').hide();
  });



  function setupPopoversForMobileAndDesk(){
  $(".popover-mobile").popover({
        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });
  $('.call-button').click(function(){
    data = { business_id: $(this).data('business') };

    $.ajax({
              url:'/businesses/increment/calls/count',
              method:'GET',
              data: data
            });
    ga('send', 'event', 'click on call button', ($(this).hasClass('desktop')?'desktop':'mobile'), '{{ Request::url() }}');
  })

    $('.single-native-ctrable').click(function(){

    dta = { ad_id: $(this).data('ad') };

    $.ajax({
      url:'/ads/increment/ctr', method:'GET', data: dta
    });

    fbq('trackCustom', 'CallToClick', {
      business_id: dta.business_id
    });


  });

  $('.trigger-gallery-open').click(function(){
    $(this).parents('.photos-holder').first().find('a.image').first().trigger('click');
    return false;
  })

  $(".popover-desk").popover({

        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="javascript:void(0)">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });

 
  }

  setupPopoversForMobileAndDesk();
  
  $('.report-link').click(function(){

    bootbox.dialog({
                title: "Report '{{addslashes($business->name)}}'",
                message: '{{addslashes($reportable)}}',
            }
        );


  });

  $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) { 
    event.preventDefault(); 
    $(this).ekkoLightbox();
  }); 

  $('.deal-item').click(function(){
    bootbox.dialog({
      title: $(this).attr('title'),
      message: $('#'+$(this).attr('rel')).html()
    }).on('shown.bs.modal', function (e){
      $('.modal-dialog .carousel').carousel();
    });
  });

  $('.rate-card-holder-main').lightGallery({
    selector: 'a.image:not(.sq-card)',
    download: false,
    getCaptionFromTitleOrAlt: false 
  });

  $('.photos-holder-main').lightGallery({
    selector: 'a.image:not(.sq-card)',
    download: false,
    getCaptionFromTitleOrAlt: false 
  });

  $('.hide-only-mobile .rate-card-holder').lightGallery({
    selector: 'a.image:not(.sq-card)',
    download: false,
    getCaptionFromTitleOrAlt: false 
  });

  $('.rate-card-holder img').click(function(){
    ga('send', 'event', 'ratecards', '{{ Request::url() }}', 'ratecard' + $(this).data('sequence'));
    //_gaq.push(['_trackEvent', 'ratecards',  , 'true']);
  });

  $('.photos-holder img').click(function(){
    ga('send', 'event', 'venuephotos', '{{ Request::url() }}', 'photo' + $(this).data('sequence'));
    //_gaq.push(['_trackEvent', 'venuephotos', '{{ Request::url() }}', $(this).data('sequence') , 'true']);
  });

  $('.package-card-holder').first().next().magnificPopup({
    delegate: 'a.image', 
    type: 'image',
    gallery: {
      enabled: true
    }
  });

  $('.hide-only-mobile .photos-holder').lightGallery({
    selector: 'a.image:not(.sq-card)',
    download: false,
    getCaptionFromTitleOrAlt: false 
  });

  $('.show-only-mobile .rate-card-holder').lightGallery({
    selector: 'a.image:not(.sq-card)',
    download: false,
    getCaptionFromTitleOrAlt: false 
  });


  $('.show-only-mobile .photos-holder').lightGallery({
    selector: 'a.image:not(.sq-card)',
    getCaptionFromTitleOrAlt: false,
    download: false
  });

  $('body').on('click', '.sharable', function(){
    
    @if(Auth::check())
      @if(Auth::user()->hasFacebookAccount())
        var _thisShare = $(this);
        bootbox.prompt({
          title: "Share this on your facebook Account",
          value: "Hey check out {{ $business->name }} on Fabogo",
            size: 'small',
          callback: function(result) {
            $.ajax({
              url:'/share',
              method:'POST',
              data:   { 
                        url:window.location.href, 
                        message: result, 
                        sharable: $(_thisShare).data('sharable'), 
                        sharable_id: $(_thisShare).data('sharableid')
                      },
              success:function(data){
                //$('.sharable').addClass('btn-success');

              }
            });
          }
        });
        @else
          bootbox.alert({
            size: 'small',
            message: "Connect your facebook account on your profile to share this on facebook!"        
          });
        @endif

      @else
        bootbox.alert({
          size: 'small',
          message: "Sign up with your facebook account to share this on facebook!"        
        });
      @endif


  })

  var _MZK_MAP_;

  if($('#item-map-mobile').length > 0){
    var map_mobile = new GMaps({
      div: '#item-map-mobile',
      panControl: false,
      lat:{{ $business->geolocation_latitude }},
      lng:{{ $business->geolocation_longitude }}
    });

    map_mobile.addMarker({
      lat:{{ $business->geolocation_latitude }},
      lng:{{ $business->geolocation_longitude }}
    });
  }

  if($('#item-map').length > 0){
    var map = new GMaps({
      div: '#item-map',
      scrollwheel: false,
      panControl: false,
      lat:{{ $business->geolocation_latitude }},
      lng:{{ $business->geolocation_longitude }}
    });

    map.addMarker({
      lat:{{ $business->geolocation_latitude }},
      lng:{{ $business->geolocation_longitude }}
    });

    var _MZK_MAP_ = map;


  if($('#source-location').length > 0){


      searchBox = document.getElementById('source-location');
      var mapcomplete = new google.maps.places.Autocomplete(searchBox);

      google.maps.event.addListener(mapcomplete, 'place_changed', function() {
        var place = mapcomplete.getPlace();
        if (!place.geometry) {
          return;
        }

        $('#source-x').val(place.geometry.location.lat());
        $('#source-y').val(place.geometry.location.lng());
      });

    //_MZK_MAP_

    var _MZK_LINES_ = [];

      $('#current-locale-button').click(function(){
        GMaps.geolocate({
          success: function(position) {
            $('#source-x').val(position.coords.latitude);
            $('#source-y').val(position.coords.longitude);
            $('#submit-search-button').trigger('click');
          },
          error: function(error) {
            bootbox.alert("Could not determine route from location.");
          },
          not_supported: function() {
            bootbox.alert("Your browser does not support geolocation");
          }
        });
      });

      $('#submit-search-button').click(function(){
        lat = $('#source-x').val();
        lon = $('#source-y').val();
        if((lat+lon)==''){
          return;
        }

        for(i in _MZK_LINES_){
          _MZK_LINES_[i].setMap(null);
        }

        $('#instructions').html('');

        _MZK_MAP_.travelRoute({
          origin: [lat, lon],
          destination: [{{ $business->geolocation_latitude }},{{ $business->geolocation_longitude }}],
          travelMode: 'driving',
          step: function(e) {
            $('#instructions').append('<li class="bb pt10 pb10 fs125">'+e.instructions+'</li>');
            $('#instructions li:eq(' + e.step_number + ')').delay(450 * e.step_number).fadeIn(200, function() {
              _MZK_MAP_.setCenter(e.end_location.lat(), e.end_location.lng());
              line = map.drawPolyline({
                path: e.path,
                strokeColor: '#131540',
                strokeOpacity: 0.6,
                strokeWeight: 6
              });
              _MZK_LINES_.push(line);
            });
          }
        });  
      });




    }
  }

  function resetClearButtonForRatingsPanel(elem, clr){
    if($(elem).val() == 0){
      $(clr).addClass('hidden');
    }else{
      $(clr).removeClass('hidden');
    }
  }



  $('#rating').barrating('show', {
    theme: 'bars-pill',
    showClear: true,
    filledStar: '<i class="fa fa-circle"></i>',
    emptyStar: '<i class="fa fa-circle-o"></i>',

    clearButton:'<i class="fa fa-times"></i>',
    size:'xs',
    starCaptions: function(val) {
      if(val <= 1){
        return 'Poor';
      }
      if(val <= 1.5){
        return 'One and half';
      }
      if(val <= 2.5){
        return 'Average';
      }
      if(val <= 3.5){
        return 'Good';
      }
      if(val <= 4.5){
        return 'Very Good';
      }
      if(val <= 5){
        return 'Fabulous';
      }
    }
  });

  $('.review-clear-rating').click(function(){
    $('#rating').barrating('set', "");
  });

  $('.star-rating-single').barrating({
    showClear: true,
    clearButton:'<i class="fa fa-times"></i>',
    filledStar: '<i class="fa fa-circle"></i>',
    emptyStar: '<i class="fa fa-circle-o"></i>',

    starCaptions: function(val) {
      if(val <= 1){
        return 'Poor';
      }
      if(val <= 1.5){
        return 'One and half';
      }
      if(val <= 2.5){
        return 'Average';
      }
      if(val <= 3.5){
        return 'Good';
      }
      if(val <= 4.5){
        return 'Very Good';
      }
      if(val <= 5){
        return 'Fabulous';
      }

    }
  });//{ readOnly: true, cancel: false, half: true,

  $('#raterupper').barrating({
    showClear: true,
    clearButton:'<i class="fa fa-times"></i>',
    starCaptions: function(val) {
      if(val <= 1){
        return 'Poor';
      }
      if(val <= 1.5){
        return 'One and half';
      }
      if(val <= 2.5){
        return 'Average';
      }
      if(val <= 3.5){
        return 'Good';
      }
      if(val <= 4.5){
        return 'Very Good';
      }
      if(val <= 5){
        return 'Fabulous';
      }

    },
    click:function(value, text) {
      //console.log(value, text);
    },
  <?php 

  if(Auth::check()):
  ?>    
   onSelect: function(value, caption, event){
    var vl = value;
    if(vl == ''){
      return;
    }
      $.ajax({
        url:'/businesses/{{$business->id}}/rate',
        method:'POST',
        data:{rating: value},
        success:function(data){
          $('.review-panel').find('input[name=rating]').barrating('set', vl);
          resetClearButtonForRatingsPanel($('#raterupper'), $('.star-rating').find('.clear-rating').first());
        }
      });
   },
 <?php else:?>
   onSelect: function(value, caption, event){
      $('#ajax-login-link').trigger('click');
      $('#raterupper').barrating('set', 0);
   },
 <?php endif;?>

  });/*

<?php 

  if(Auth::check()):

  ?>.on('rating.clear', function(event) {
      $.ajax({
        url:'/businesses/{{$business->id}}/rating/delete',
        method:'POST',
        data:{rating:$(this).val()},
        success:function(data){
          $('.review-panel').find('input[name=rating]').rating('update', $('#raterupper').val());
          resetClearButtonForRatingsPanel($('#raterupper'), $('.star-rating').find('.clear-rating').first());
        }
      });

  }).on('rating.change', function(event, value, caption) {

      $.ajax({
        url:'/businesses/{{$business->id}}/rate',
        method:'POST',
        data:{rating:$(this).val()},
        success:function(data){
          $('.review-panel').find('input[name=rating]').rating('update', $('#raterupper').val());
          resetClearButtonForRatingsPanel($('#raterupper'), $('.star-rating').find('.clear-rating').first());
        }
      });

})<?php else:?>.on('rating.change', function(event, value, caption) {
    $('#ajax-login-link').trigger('click');
    $('#raterupper').rating('clear');
})<?php endif;?>;
*/
resetClearButtonForRatingsPanel($('#raterupper').first(), $('.star-rating').find('.clear-rating').first());

  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $('.review-clear-rating-single').click(function(){
    $('#raterupper').barrating('set', '');

      $.ajax({
        url:'/businesses/{{$business->id}}/rating/delete',
        method:'POST',
        success:function(data){
          $('.review-panel').find('input[name=rating]').barrating('set',  $('#raterupper').val());
          resetClearButtonForRatingsPanel($('#raterupper'), $('.star-rating').find('.clear-rating').first());
        }
      });

  });



  $('.hoverable').click(function(){
    $(this).find('.hide-on-hover').fadeOut('slow', function(){
      $(this).parents('.hoverable').first().find('.show-on-hover').fadeIn('slow');
    })
  })

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',

      success:function(data){

      }
    })


  });

  $(document).on('click', '.checkedin', function(){
    $(this).removeClass('checkedin');
    $(this).addClass('checkin');

    $.ajax({
      type: 'POST',
      url: '/businesses/{{$business->id}}/checkout',
      success:function(data){

      }
    })
  });

  $(document).on('click', '.checkin', function(){
    $(this).removeClass('checkin');
    $(this).addClass('checkedin');

    $.ajax({
      type: 'POST',
      url: '/businesses/{{$business->id}}/checkin',
      success:function(data){

      }
    })


  });
});

</script>
