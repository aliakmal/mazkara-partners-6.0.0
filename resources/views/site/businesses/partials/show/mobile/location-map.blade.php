  <div class="ba   bg-white  border-radius-3 mb0 pb5 ">
    <div class="p15 pb0"><div class="pt10   pl0 pr0 p15 pt0 bb pb5 mb5"><b>ADDRESS</b></div></div>
    <p class="p15 pt0 pb0 ">
      <b >{{ $business->getZoneName() }}</b> ›
      <span >{{ $business->getAddressAsString() }}</span>
    </p>
  @if($chain_businesses > 0)
    <p class="pl15 p0 pb0 mb10">
      <a href="{{ MazkaraHelper::slugCityChain(null, $business->chain)}}" class="text-success">
        {{ $chain_businesses - 1 }} more venues
      </a>
    </p>
  @endif

@if(!$business->isNonLocationable())

@if($business->isLocationSet())
  <div class="item-map-holder ">
    <a target-"_blank" href="https://www.google.com/maps/dir/Current+Location/{{$business->geolocation_latitude}},{{$business->geolocation_longitude}}" class="show-only-mobile">
      <img src="https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=450x150&maptype=roadmap
&markers=color:red|{{$business->geolocation_latitude}},{{$business->geolocation_longitude}}" class="ba mr10 bbb" style="width:100%" />
    </a>
  </div>
@endif
@endif


</div>
