<?php $total_photos = isset($total_photos)?$total_photos:5;?>
@if(count($rateCards) >0)
  <div class="  show-when-loaded hidden">
    <div class="  pt5 pl0 pr0 p15  pb5 mb5 "><b>MENU</b></div>
    <div class="w100pc slick-out force-gray" style="padding:0px 20px 0px 20px;">
      <ul class="rate-card-holder-mobile rate-card-holder list-unstyled mt5 " >
        @foreach(mzk_secure_iterable($rateCards) as $ii=>$one_photo)
          @if($one_photo->hasImage())
            <li><a href="{{ mzk_cloudfront_image($one_photo->image->url('base')) }}" class=" mr15 bad image" style="display:inline-block" ><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" alt="{{ $business->name }} + Rate Card {{ $ii+1 }}" class="w150 border-radius-3 h150" /></a></li>
          @endif
        @endforeach
      </ul>
    </div>
  </div>
@endif


