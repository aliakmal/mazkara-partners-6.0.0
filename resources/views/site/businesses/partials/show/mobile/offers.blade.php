@if(!isset($offers))
  <?php $offers = $business->offers()->onlyActive()->get();?>
@endif
@if(count($offers) >0)
  <div class=" {{$business->total_packages_count > 0?'':'pt10'}} pb10 ">
    <h2 class="item-headers dpib">{{ strtoupper(mzk_label('specials')) }}</h2>
    <a class="dpib medium-gray fs125 hide-only-mobile mb10" style="vertical-align:middle;" data-toggle="tooltip" data-placement="top" title="Call the venue directly to avail these {{ mzk_label('specials') }}. Don't forget to mention FABOGO!" href="#">
      <i class="fa fa-question-circle"></i>
    </a>
    <div class="package-card-holder ">
      <table class="table mb0 bg-lite-gray">
      @foreach(mzk_secure_iterable($business->offers()->byState('active')->get()) as $ii=>$offer)
        <tr>
          <td class="color-black" style="border-top:3px solid #fff;">{{ $offer->body }}</td>
          <td style="border-top:3px solid #fff;" class="text-right bg-lite-gray fw700 pink pr0">
            <span style="text-decoration:line-through;" class="medium-gray">{{ mzk_price($offer->original_price, '&nbsp;') }}
          </td>
          <td class=" text-right pink" width="15%" style="border-top:3px solid #fff;">{{ mzk_offer_price($offer, '&nbsp;') }}</td>
          @if($offer->isClaimable())
            <td class="color-black text-right" style="width:120px;border-top:3px solid #fff; ">
              <a class="btn btn-pink p5 no-border-radius btn-xs br0 ajax-popup-link" href="{{ route('offers.get.claim.form', [$offer->id]) }}">CLAIM</a>
            </td>
          @endif
        </tr>
      @endforeach
      </table>
      <div class="green dpb hidden text-left p5" style="font-size:12px;">
        <b>
          Call the venue directly to avail these {{ strtolower(mzk_label('specials')) }}. Don't forget to mention FABOGO!
        </b>
      </div>
    </div>
  </div>
@endif
