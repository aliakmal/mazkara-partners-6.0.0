@if(!isset($highlights))
  <?php $highlights = $business->highlights;?>
@endif
@if(count($highlights)>0)
  <div class="p10 va-middle  ">
    <ul class="list-unstyled mt5 mobile-highlight-filter list-inline">
      @foreach($highlights as $highlight)
        @if(MazkaraHelper::isActiveHighlight($highlight->id))
          <li class="mr15 pr15">
            <div class="text-center">
              <div><i class="fs220 {{ mzk_icon_highlights($highlight->slug) }}"></i></div>
              <span class="italic serif">{{ ($highlight->name)}}</span>
            </div>
          </li>
        @endif
      @endforeach
      @if($business->cost_estimate > 0)
        <?php $costOptions = Business::getCostOptions();?>
        <li class="mr15 pr15">
          <div class="text-center">
            <div><i class="fs220 {{mzk_icon_cost_estimate($business->cost_estimate)}}"></i></div>
            <span class="italic serif">{{ ($costOptions[$business->cost_estimate])}}</span>
          </div>
        </li>
      @endif
    </ul>
  </div>
@endif
