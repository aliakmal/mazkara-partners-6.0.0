<?php $services = $business_services;?>
@if(count($services)>0)
  <div class="ba  p15 bg-white pt5 border-radius-3 mb10 pb5 mb10"  >
    <div class="  pt5 pl0 pr0 p15 bb pb5 mb5"><b>SERVICES</b></div>
    <?php
      $current_heading = '';
    ?>
    @foreach($services as $ii=>$service)
      @if($service->parent_id != $current_heading)
        <?php $current_heading = $service->parent_id;?>
        <div style="" class="p5 {{ $ii>8 ? 'hidden':'' }} service-object bg-lite-gray">
          <b>{{ strtoupper(MazkaraHelper::getServicesAttribute($service->parent_id, 'name')) }}</b>
        </div>
      @endif
      <div style=" " class="bb {{ $ii>8 ? 'hidden':'' }} service-object p5">
        {{ strtoupper($service->name) }}
        <div class="pull-right">
          {{ $service->getStartingPrice(MazkaraHelper::getCitySlugFromID($business->city_id)) }}
        </div>
      </div>
    @endforeach
    <a id="lnk-to-display-all-services-mobile" href="javascript:void(0)" class="dpb  border-radius-3  mt10   pb10 pt10 text-center">VIEW ALL SERVICES</a>
  </div>
@endif

