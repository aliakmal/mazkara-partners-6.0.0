@if(is_null($current_user) && (count($reviews)==0))
<?php   return ;?>
@endif

<?php //$services = (mzk_get_active_services()) ;?>

<h2 class="item-headers pt5 hide-only-mobile">
  REVIEWS
  @if(count($reviews)>0)
  <span class="gray">({{count($reviews)}})</span>
  @endif
</h2>
<h2 class="item-headers pt5 show-only-mobile p15 pb0">
  REVIEWS
  @if(count($reviews)>0)
  <span class="gray">({{count($reviews)}})</span>
  @endif
</h2>

<a id="reviews"></a>

@if($current_user) 
  @include('site.reviews.form-base', array('services'=>$services,'business'=>$business,'review'=>$myreview))

@else
<div class="p10 no-border-radius bg-lite-gray dpib mb15 mt10 hide-only-mobile">
  <a href="/users/create" id="ajax-login-link" class="ajax-popup-link pink">SIGN IN</a> TO REVIEW THIS VENUE
</div>
@endif
<div class="show-only-mobile p15 pt0 pb0">
  <a class=" btn mb10 show-only-mobile border-radius-5 lnk-run-on-app btn-turquoise w100pc p5  mt10 " 
    href="javascript:void(0)">
      <i class="fa fa-pencil fs125"></i>
    &nbsp;  
    <span class="fs150 bolder">WRITE REVIEW</span></a>
</div>

@if(count($reviews)==0)
<div class="no-reviews-placeholder hide-only-mobile hidden">
  <div class="well review-placeholder-removeable">
    <a href="javascript:void(0)" class="btn btn-default review-btn">Review</a>
  </div>
  <p class="lead text-center lite-gray" style="font-size:600%;">
    <i class="fa fa-pencil "></i> 
  </p>
  <p class="lead text-center">
    Be the first to review {{$business->name}}
  </p>
</div>
@endif
<div class="reviews-holder">
  @if(count($reviews)>0)
    @foreach($reviews as $ii=>$review)
      <div {{ $ii>2 ? 'class="review-hidden" style="display:none"':'' }} >
        @include('site.reviews.partials.single', ['review'=>$review])
      </div>
    @endforeach
    @if(count($reviews)>3)
      <a href="javascript:void(0)" class="dpb bg-lite-gray-on-hover mb15  ba pb10 pt10 text-center reviews-load-btn ">
        LOAD ALL {{count($reviews)}} REVIEWS
      </a>
    @endif    
  @endif
</div>
@section('js-review')

<script type="text/javascript">
$(function(){
  $('.reviews-load-btn').click(function(){
    $(this).hide();
    $('.review-hidden').show();
  });

//  $.get( "/reviews/business", { business_id: "{{$business->id}}" } )
//    .done(function( data ) {
//    $('.reviews-holder').html(data.html);
//  });
//  $('.reviews-holder').on('click', '.reviews-next-btn', function(){
//    _this = this
//    $(_this).prop('disable', true);
//    $(_this).html('PLEASE WAIT! COMING UP...')
//    $.get( "/reviews/business", { business_id: "7203", page:$(this).data('next') } )
//      .done(function( data ) {
//        $(_this).remove();
//      $('.reviews-holder').append(data.html);
//    });
//  })

  $('#service_ids').selectize({
    maxItems:3
  });

  @if(isset($review))
    //$('#service_ids').selectize('val', [{{ join(',', $review->services()->lists('service_id','service_id')->all()) }}]);
  @endif

  $('#submit-review-button').on('click', function () {
    rev_rating = $('#form-manage-review').find('select[name=rating]').first();
    rev_body = $('#form-manage-review').find('textarea[name=body]').first();
    missing = [];

    if($(rev_rating).val() == 0){
      missing.push('You cannot add a Review without a rating! ');
    }

    if($(rev_body).val().length < 50){
      missing.push('Less than 50 characters? C\'mon babes you can do better than that!');
    }

    if(missing.length>0){
      alert(missing.join("\n"));
      return false;
    }else{
      var $btn = $(this).button('loading')
      $('#form-manage-review').submit();
    }

  });

//  $('body').on('click', '.review-btn', function(){
//    $.ajax({
//      type: "GET",
//      url: '{{ route('businesses.review.form', [$business->id] ) }}',
//      data: [],
//      success: function(result) {
//        $.magnificPopup.open({
//          items: {
//            src: result['html']
//          },
//          type: 'inline'
//        });
//        return false;
//      }
//    });
//  });

//  $('.review-btn').click(function(){
//    $('.review-placeholder-removeable').remove();
//    $('.no-reviews-placeholder').hide();
//    $('.review-panel').show();
//    $('.review-panel').effect("highlight");
//  });


$('#text-review-body').textcounter({
    min: 50,
    max: 2000000,
    countDownText: "Characters Left: ",
    minimumErrorText: "Minimum 50 Characters!",
    counterErrorClass: "text-danger",
    countContainerClass:"pl0",
    counterText: "Review Length: "
});

$('.toggl-review-form').click(function(){
  $(this).hide();
  $('#basic-review-form').show();
});

$('.lnk-close-review-form').click(function(){
  $('.toggl-review-form').show();
  $('#basic-review-form').hide();
});


  $('.review-btn').click(function(e){
    e.preventDefault();
  $('.toggl-review-form').trigger('click');

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });


  });


});

 

</script>
@stop