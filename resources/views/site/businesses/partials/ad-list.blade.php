@if(count($ads)>0)
<div class="hide-only-mobile">
  <small class=" pb10 ">SPONSORED</small>
  <?php $pos = 'rhs';?>
  @foreach($ads as $ad)

    @include('site.businesses.partials.ad-single')
  @endforeach
</div>
@endif
