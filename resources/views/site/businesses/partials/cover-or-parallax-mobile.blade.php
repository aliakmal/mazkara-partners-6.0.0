<?php 
if(!isset($phones)):
  $phones = $business->displayablePhone();
endif;
?>
<div class="show-only-mobile">
  <div class="w100pc absolute" style="z-index: 999;">
    @include('elements.searcher')
  </div>

@if((isset($business->cover)&&($business->cover!=null)) || $business->hasThumbnail() || $business->hasCover() || $business->useStockImage())

  <div class="container" style="z-index:9;">
    <div class="overlap-parallax-content relative" style="z-index:90">

      <span style="position:absolute;top:7px;right:0px;">
        @if(Auth::check())
          <a href="javascript:void(0)"  title="Favorite {{ $business->name }}" class="fs200 lnk-run-on-app {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart hide-when-unfavorite"></i> 
            <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
          </a>
        @else
          <a href="/users/login" title="Favorite {{ $business->name }}" class="fs200 page-scroll lnk-run-on-app ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart-o " style="color:#fff;"></i> 
          </a>
        @endif
      </span>


    <div class="w100pc absolute " style="bottom:25px;">
      <div class="mt0 ">&nbsp;</div>
      @include('site.businesses.partials.title-and-rating-mobile')
    </div>
  </div>
</div>
  <div class="cover-parallax-mobile show-only-mobile container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->cover->image->url('large'))}}) no-repeat center center rgba(200, 82, 126, 0);"></div>
  <div class="overlap-parallax-mobile text-center container p0 overlap-transparent show-only-mobile" style="z-index:7"></div>

@else


  <div class="container" style="z-index:9;">
    <div class="overlap-parallax-content relative" style="z-index:90">

      <span style="position:absolute;top:7px;right:0px;">
        @if(Auth::check())
          <a href="javascript:void(0)"  title="Favorite {{ $business->name }}" class="fs200 lnk-run-on-app {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart hide-when-unfavorite"></i> 
            <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
          </a>
        @else
          <a href="/users/login" title="Favorite {{ $business->name }}" class="fs200 page-scroll lnk-run-on-app ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart-o " style="color:#fff;"></i> 
          </a>
        @endif
      </span>


    <div class="w100pc absolute " style="bottom:25px;">
      <div class="mt0 ">&nbsp;</div>
      @include('site.businesses.partials.title-and-rating-mobile')
    </div>
  </div>
</div>
  <div class="cover-parallax-mobile show-only-mobile container p0 text-center bg-burgundy p0"></div>
  <div class="overlap-parallax-mobile text-center container p0 overlap-transparent show-only-mobile" style="z-index:7"></div>



@endif
</div>