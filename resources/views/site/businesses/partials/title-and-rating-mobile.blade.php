<?php 
        $fcss = 'fw500';
        $cFss  = '';
        if(strlen($business->name)>35){
          $fcss = 'fs90 fw500';
        }elseif(strlen($business->name)>=24){
          $fcss = 'fs90 fw500';
        }

        if(strlen($business->name)<24){
          $fcss = 'fs110 fw500 ';
          $cFss = '';
        }
        if(strlen($business->name)<18){
          $fcss = 'fs125 fw500 ';
          $cFss = '';
        }
      ?>
      <div class="row">
      <div class="col-xs-9">
      <h1 class=" item-name b0 mt5 mb5 pb5 fs220 {{ $cFss }} fw300 bbw no-border" style="z-index:99;">
        <a href="{{MazkaraHelper::slugSingle($business)}}" title="{{ $business->name }}">
            <span class=" {{ $fcss }} force-white dpib mb5"><b>{{ $business->name }}</b></span>
            @if(in_array($business->active, ['temporarily.closed','opening.soon']))
              <div style="font-size: 10px;" class="  label label-warning"><b>{{strtoupper(str_replace('.', ' ', $business->active ))}}</b></div>
            @endif
            <div class="fs75">
            @if($business->isNonLocationable())
            @else
              <div class="force-white pt3 fs75">{{$business->zone_cache}}</div>
            @endif
            </div>
        </a>

      </h1>
      </div>
      <div class="col-xs-3 small-force-white text-center pt15 ">
            {{ ViewHelper::starRating($business->average_rating, ['size' => 's', 
                                                                  'show_num_votes' => true, 
                                                                  'total_votes' => $business->accumulatedReviewsCount()]) }}

</div>
    </div>