<?php $highlights = $business->highlights;?>
@if(count($highlights)>0)
  <div class="bbb pr15 fs125">
    <div class="item-phone pb5">
      Highlights
    </div>
    <ul class="list-unstyled ">
      @foreach($highlights as $highlight)
        <li class="mr10 pr10">
          <a class="lnh pr5 mr5 fw300" href="{{ MazkaraHelper::slugCity(null, ['highlights[]'=>$highlight->id]) }}">
            <i class=" {{mzk_icon_highlights($highlight->slug)}}"></i>
            {{ ($highlight->name)}}
          </a> 
        </li>
      @endforeach
    </ul>
  </div>
@endif
