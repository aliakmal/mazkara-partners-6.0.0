@if(isset($ad->photo_url))
<?php ob_start();?>
@if(strlen($ad->corner_text)>0)
<span style="position:absolute; top:5px; left:5px;">
  <span class="color-{{$ad->corner_text_color}}">{{ ($ad->corner_text) }}</span>
</span>
@endif
<div class=" native-collectable-content-holder" >
  <div class="yellow native-collectable-title" >
    <div class=" dpib" style="width:100%">
      @if($ad->hasHeadline())
        <div class="force-white pull-right   fs110"><b>{{$ad->headline}}</b></div>
      @endif
      {{$ad->caption}}
    </div>
  </div>
</div>

</div>
<div class="p5 border-bottom-radius-3 native-collectable-caption" style="border:1px solid #ddd;border-top:0px;">
<b>{{ strtoupper($ad->title)}} </b>
<?php $banners = ob_get_contents();ob_end_clean();?>
  <a href="{{ $ad->url }}?{{ _apfabtrack([(isset($pos)?$pos:'body'),'ad']) }}" class="no-underline single-native-ctrable" data-ad="{{ $ad->id }}">
    <?php 
      $image = $ad->photo_url;//();
      if($image!=false){
        $image = mzk_cloudfront_image($image);
        $css = "background-size:100%;background-position:center top;position:relative;background-repeat:no-repeat;background-image:url(".$image.");";
      }else{
        $css = "background-color:black;position:relative;";
      }
    ?>
      <div class=" hide-only-mobile mb10">
      <div class=" native-collectable border-top-radius-3 " style="border:1px solid #ddd;border-bottom:0px;height:142px;{{$css}}">
        {{ $banners }}
      </div>
    </div>
    <div class=" show-only-mobile mb10">
      <div class=" native-collectable-mobile " style="height:202px;{{$css}}">
        {{ $banners }}
      </div>
    </div>

  </a>

@endif
