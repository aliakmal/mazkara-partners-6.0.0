<?php $business_url =  MazkaraHelper::slugSingle($business);?>
  <div  class="bg-white relative border-radius-3 ba clearfix">
    <div class="pull-left  mr10 relative">
      <span style="position:absolute;top:7px;left:10px;">
        @if(Auth::check())
          <a href="javascript:void(0)" style="font-size:20px;" title="Favorite {{ $business->name }}" class="lnk-run-on-app {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart hide-when-unfavorite"></i> 
            <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
          </a>
        @else
          <a href="/users/login" style="font-size:20px;" title="Favorite {{ $business->name }}" class="page-scroll lnk-run-on-app ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
            <i class="fa fa-heart-o " style="color:#fff;"></i> 
          </a>
        @endif
      </span>
      <a title="{{{ $business->name }}}" href="{{ $business_url }}?{{_apfabtrack(['main-mobile','venue-image'])}}" class="border-left-radius-3 link-single-venue show-only-mobile" >
        {{ ViewHelper::businessThumbnail($business, [ 'width'=>'145', 
                                                    'height'=>'145', 
                                                    'class'=>' border-left-radius-3  fw300',
                                                    'meta'=>'small',
                              'style'=>'width:145px; height:145px; padding-top:36px;font-size:40px;'])}}
      </a>
    </div>
    <div class="">
      <div class="pos-relative">
        <div class="pull-right  mb10 mr10">
          <div class="search-item-stars text-right show-only-mobile ">
            <span class="">
              {{ ViewHelper::starRating($business->average_rating, 
                                                      ['size' => 's', 
                                                        'show_num_votes' => true, 
                                                        'show_no_votes' =>false,
                                                        'total_votes' => $business->accumulatedReviewsCount()]) }}
              <div class="clear"></div> 
            </span>
          </div>
        </div>
        <h3 class="search-item-name fw500 ovf-hidden mb0">
          <a title="{{{ $business->name }}}" href="{{ $business_url }}?{{_apfabtrack(['main-mobile','venue-name'])}}" class="link-single-venue pr10 ovf-hidden hover-underline result-title">
            <b>{{{ mzk_str_trim($business->name) }}}</b>
          </a>
          <div class="search-item-rating dpib ">
            <div class="clear"></div>
          </div>
        </h3>
        <div class="mt2 mb2 pr10 single-listing-address color-black">
          <b>
            <i class="fa fa-map-marker color-black "></i><span class="color-black"> {{ $business->zone_cache }}</span>
          </b>
          <span title="{{ $business->getAddressAsString() }}" class="color-black search-item-address">› {{ $business->getAddressAsString() }}</span>
        </div>
        <?php
          $highlights =$business->getMeta('highlights');
          $highlights = is_array($highlights)?$highlights:[];
        ?>
        <div class="pull-left dark-gray">
          @foreach((mzk_secure_iterable($highlights)) as $ii=>$highlight)
            <?php $genders = [4,5,6];?>
            @if(in_array($ii, $genders))
              @if(MazkaraHelper::isActiveHighlight($ii))
                <div class="dpib mr5">
                  <a rel="nofollow" title="{{ $highlight }} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['highlights'=>[$ii]]) }}" class="search-item-text text-center medium-gray pr5">
                    <div style="dpib text-center">
                      <i class="dark-gray fs125 {{ mzk_icon_highlights(MazkaraHelper::getHighlightSlug($ii)) }}"></i>
                    </div>
                    <div>
                    <span class="fs90 dark-gray">{{$highlight}}</span>
                  </div>
                  </a>
                </div>
              @endif
            @endif
          @endforeach
        </div>
        <div class="mt15 mr10">
          @if($business->getMobilePhoneText())
          <a data-placement="top" data-business="{{$business->id}}"  
            class="call-button btn border-radius-15 btn-green-line absolute bottom10 right10 mt2 " 
            data-phone="{{ $business->getMobilePhoneText() }}" 
            data-container="body" type="button" data-html="true" href="{{ $business->getMobilePhoneText() }}"
             ref="popover-content-for-desk-{{$business->id}}" 
             id="call-to-book-desk-{{$business->id}}">CALL </a>
            @endif
        </div>
        <div class=""></div>
      </div>
        @if(!isset($no_offers))
        <?php       
          $offers = $business->getMeta('offers');
          $offers_block = '';
        ?>
        @if( (count($offers)>0))
          <a class="btn hidden blue-btn-arrow btn-lite-blue btn-xs mr0 mb5 toggler" rel="offers-for-{{$business->id}}" href="javascript:void(0)">Offers</a>
          <?php ob_start();?>
            <div class=" clearfix pt0 " id="offers-for-{{$business->id}}">
              <table class="mb0 table ">
                @foreach(mzk_secure_iterable($offers) as $ii=>$offer)
                  @if( strtotime($offer->valid_until) >= strtotime(date('Y-m-d')))
                    <?php //$offer = mzk_array_to_object($offer);?>
                      <tr>
                        <td class=" bg-white color-black" style="border-top:3px solid #dddddd;">
                          <div style="overflow:hidden;text-overflow:ellipsis;height:auto;display:inline-block;">{{ mzk_offer_title($offer) }}</div>
                        </td>
                        <td style="border-top:3px solid #dddddd;" class="text-right bg-white fw700 fire pr0"><b>{{ mzk_offer_price($offer) }}</b></td>
                        @if(Offer::areClaimable($offer))
                          <td style="border-top:3px solid #dddddd;" width="10%" class="text-right bg-white pr0">
                            <a class="btn btn-pink no-border-radius p5 btn-xs br0 ajax-popup-link" href="{{ route('offers.get.claim.form', [$offer->id]) }}">CLAIM</a>
                          </td>
                        @endif
                      </tr>
                  @endif
                @endforeach
              </table>
            </div>
          <?php $offers_block = ob_get_contents(); ob_end_clean();?>
        @endif
        @endif
      </div>

      </div>
        @if(!isset($no_offers))

        <div class="  pt0" >{{ $offers_block }}</div>
        @endif

