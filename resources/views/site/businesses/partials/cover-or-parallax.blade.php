<?php 
if(!isset($phones)):
  $phones = $business->displayablePhone();
endif;
?>
<div class="hide-only-mobile">
@if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage())
  
  @if($business->hasCoverYPos())
    <div class="cover-parallax hide-only-mobile container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->getCoverUrl('xlarge'))}}) no-repeat {{ $business->getCoverYPosCss() }} rgba(200, 82, 126, 0); background-size:100%;"></div>
  @else
    <div class="cover-parallax hide-only-mobile container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->getCoverUrl('xlarge'))}}) no-repeat center center rgba(200, 82, 126, 0); background-size:100%;"></div>
  @endif
  <div class="overlap-parallax text-center  container p0 overlap-transparent hide-only-mobile" style=""></div>
  <div class="container" style="z-index:100;">
    @include('elements.searcher-01')
    <div class="overlap-parallax-content hide-only-mobile" style="">
      <div class="row">
        <div class="col-md-12 pt0 force-white">
          {{ $breadcrumbs->render() }}
        </div>
      </div>
      <div class="row">
        <div class="col-md-9 center-on-mobile" >
          <div class=" force-white " >
            <div class="mt140 pt10 hide-only-mobile ">
            </div>
            @include('site.businesses.partials.title-and-rating-for-parallax', ['business'=>$business, 'phones'=>$phones])
          </div>
          <div class="center-on-mobile btns-ghosted mb10">
            @include('site.businesses.partials.action-bar', ['business'=>$business, 'phones'=>$phones])
          </div>
        </div>
        <div class="col-md-2">
        </div>
      </div>
    </div>
  </div>
@else
  <div class="cover-parallax text-center container bg-burgundy" style=""></div>
  <div class="container">
    @include('elements.searcher')

  <div class="overlap-parallax-content" style="">
    <div class="row">
      <div class="col-md-12  force-white">
        {{ $breadcrumbs->render() }}
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 center-on-mobile" >
        <div class=" force-white " >
          <div class="mt140 pt10 hide-only-mobile"></div>
          @include('site.businesses.partials.title-and-rating-for-parallax', ['business'=>$business, 'phones'=>$phones])
        </div>
        <div class="btns-ghosted mb10">
          @include('site.businesses.partials.action-bar', ['business'=>$business, 'phones'=>$phones])
        </div>
      </div>
      <div class="col-md-3">
      </div>
    </div>
  </div>
</div>

@endif
</div>