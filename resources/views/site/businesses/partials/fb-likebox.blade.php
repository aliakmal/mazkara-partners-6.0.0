@if($business->hasFacebookPageActive())
<div class="mb10 pb10"><div  class="fb-page" data-href="{{ $business->facebook_like_box_id }}" 
      data-tabs="timeline" data-small-header="false" 
      data-adapt-container-width="true" data-hide-cover="false" 
      data-show-facepile="true">
  <blockquote cite="{{ $business->facebook_like_box_id }}" class="fb-xfbml-parse-ignore"><a href="{{ $business->facebook_like_box_id }}">{{$business->name}}</a></blockquote></div>
</div>
@endif