<div class="row">
  <div class="col-md-12">
    <div class="header pr0-mobile relative" style="padding-right: 30px;">
      @if($business->isHomeService())
      <div class="hide-only-mobile h80 pt20 mt0">
        <div >
          <span class="label  label-warning"  style="font-size:14px;">
            <span class="fw500" ><i class="fa fa-home"></i> HOME SERVICES</span>
          </span>
        </div>
        </div>
      @else
        <div class="hide-only-mobile h80 mt10"></div>
      @endif
      <span class="hide-only-mobile">
        <div class="pull-right " style="position: absolute;right:30px; bottom:0px">
          <div class="p10 pt25 pr0 ml30 text-center ">
            {{ ViewHelper::starRateBusinessMedal($business, true)}}
          </div>
        </div>
      </span>
      <?php 
        $fcss = 'fw500';
        $cFss  = 'mt5';
        if(strlen($business->name)>45){
          $fcss = 'fs90 fw500';
          $cFss  = 'mt0';
        }elseif(strlen($business->name)>=30){
          $fcss = 'fs110 fw500';
          $cFss  = 'mt0';
        }

        if(strlen($business->name)<30){
          $fcss = 'fs175 fw500 ';
          $cFss = 'mt-30';
        }
        if(strlen($business->name)<23){
          $fcss = 'fs200 fw500 ';
          $cFss = 'mt-30';
        }
      ?>
      <h1 class="item-name  mb5 pb5 fs220 {{ $cFss }} fw300 bbw no-border-mobile">
        <a href="{{MazkaraHelper::slugSingle($business)}}" title="{{ $business->name }}">

          <span class=" {{ $fcss }}">{{ $business->name }}</span>
          @if(in_array($business->active, ['temporarily.closed','opening.soon']))
            <div style="font-size: 10px;" class="  label label-warning"><b>{{strtoupper(str_replace('.', ' ', $business->active ))}}</b></div>
          @endif
          @if($business->isNonLocationable())
          @else
            <div style="font-size:18px;padding-bottom:3px;"  class="pt3">{{$business->zone_cache}}</div>
          @endif
          <div class="show-only-mobile p10 text-center">
            {{ ViewHelper::starRateBasic($business->rating_average, 'l', false, $business->accumulatedReviewsCount() ) }}
          </div>
        </a>

      </h1>

    </div>


  </div>
</div>