@extends('layouts.parallax')
@section('preheader')
  
@stop
@section('content')

@include('site.businesses.partials.cover-or-parallax-desktop-mobile', ['business'=>$business, 'phones'=>$phones])
<div class="hide-only-mobile">
  @include('site.businesses.show-desktop', ['business'=>$business, 'phones'=>$phones])
</div>
<div class="show-only-mobile">
  @include('site.businesses.show-mobile', ['business'=>$business, 'phones'=>$phones])
</div>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))
<script type="text/javascript">
  $(function(){
    <?php if(env('APP_ENV') == 'production'):?>
    $.ajax({
      url:'/businesses/increment/views/count', 
      method:'GET', 
      data: { business_id: <?php echo $business->id;?> }
    });
    <?php endif;?>

    $('.lnk-view-more-desc').click(function(){
      $('.lnk-view-more-desc').hide();
      $('.hide-when-view-more-desc').hide();
      $('.show-when-view-more-desc').show();
    });

    $('.bxslider').bxSlider({ slideMargin:20, pager:false, onSliderLoad:function(){
     $('.show-on-load').slideDown( "slow");
    }});

    $('.mobile-highlight-filter').slick({
      slidesToShow: 3,
      variableWidth: true,
      infinite: false,
      nextArrow: '<i class="fa gray fa-chevron-right slicker-next absolute" style="cursor:pointer;top:35%;"></i>',
      prevArrow: '<i class="fa gray fa-chevron-left slicker-prev  absolute" style="cursor:pointer;top:35%;"></i>'
    });

    $('.show-when-loaded').removeClass('hidden');

    $('.rate-card-holder-mobile').slick({
      slidesToShow: 2,
      variableWidth: true,
      arrows: true,
      nextArrow: '<i class="fa gray fa-chevron-right slicker-next absolute" style="cursor:pointer;right:-15px;top:45%;"></i>',
      prevArrow: '<i class="fa gray fa-chevron-left slicker-prev  absolute" style="cursor:pointer;left:-15px;top:45%;"></i>'
    });


  })
</script>
@stop

@stop
