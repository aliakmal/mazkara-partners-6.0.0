<div class=" hide-only-mobile">
  <div class="container  bg-lite-gray ">
    <div class="row">
      <div class="col-md-3 ">
        @include('site.businesses.partials.show.cards.phone', ['business'=>$business, 'phones'=>$phones])
      </div>
      <div class="col-md-9 ">
        @include('site.businesses.partials.show.cards.highlights', ['business'=>$business, 'highlights'=>$highlights])
      </div>
    </div>
  </div>
</div>

<div class="container " >

<div class="row">  
  <div class="row-height">

  <div class="col-md-3 col-height pt20 bg-lite-gray-3 col-height "  style="vertical-align:top;">
  <div class="hide-only-mobile pt10">
    <div  >

        @if(!$business->isNonLocationable())
          @include('site.businesses.partials.show.cards.location-map', ['business'=>$business])
        @endif
        @include('site.businesses.partials.show.cards.timings', ['business'=>$business])

    </div>
  </div>

  <div class="mt15">
    @include('site.businesses.partials.client-dashboard-link', ['business'=>$business])        
  </div>
  <div class="mt15">
    @include('site.businesses.partials.show.cards.products-used', ['business'=>$business])        
  </div>


  </div>

  <div class="col-md-6 col-height "  style="vertical-align:top;">
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12">
        <!--
        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
        -->
        @if($business->photos()->count() >0)
          <div class=" pl0">
            <h2 class="item-headers ">Photos of {{$business->name}}</h2>
            <div class="photos-holder-main">
              @foreach($business->photos as $ii=>$one_photo)
                @if($one_photo->hasImage())
                  <a  href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}" 
                      class="mr5 ba image border-radius-5 mb5" style="display:inline-block"
                        ><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" 
                              data-sequence="{{($ii+1)}}" class="w150-mobile border-radius-5" /></a>
                @endif
              @endforeach
            </div>  
          </div>
        @endif




      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
<div class="col-md-3 bg-lite-gray-3 col-height pt20" style="vertical-align:top;">

  @if($business->showAdsOnVenue())
    @include('site.businesses.partials.ad-list')
  @else
    @include('site.businesses.partials.fb-likebox')

  @endif



  </div>
</div>
</div>
</div>
</div>
<script src="https://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>
