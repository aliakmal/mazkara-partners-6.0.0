<div class="container bg-white pb15 bb force-gray">
  @include('site.businesses.partials.show.mobile.phone', ['business'=>$business, 'phones'=>$phones])
  @include('site.businesses.partials.show.mobile.highlights', ['business'=>$business, 'highlights'=>$highlights])
</div>
<div class="container pb10  force-gray">
  @include('site.businesses.partials.show.mobile.offers', ['business'=>$business, 'offers'=>$offers])        
  @include('site.businesses.partials.show.mobile.photos', ['business'=>$business, 'photos'=>$photos, 'total_photos'=>5])        
  @include('site.businesses.partials.show.mobile.rate-cards-packages', ['business'=>$business, 'rateCards'=>$rateCards, 'total_photos'=>4])        
</div>
<div class="container pl0-mobile pr0-mobile force-gray">
  @include('site.businesses.partials.show.mobile.services', ['business'=>$business] )
  @include('site.businesses.partials.show.mobile.timings', ['business'=>$business])
  @include('site.businesses.partials.show.mobile.location-map', ['business'=>$business])
</div>

<div class="container pb15 pl0-mobile pr0-mobile force-gray">

  @include('site.businesses.partials.reviews', ['business'=>$business, 'reviews'=>$reviews] )
  <div class="show-on-load p15 pb0 pt5 " style="" >
    <div class="show-only-mobile">
      @if(count($ads)>0)
        <small class=" pb10 ">SPONSORED</small>
        <div class="bxslider">
          @foreach($ads as $ad)
              @include('site.businesses.partials.ad-single')
          @endforeach
        </div>
      @endif
    </div>
  </div>

</div>

