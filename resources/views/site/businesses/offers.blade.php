{{ $breadcrumbs->render() }}
<div class="row" >  
  <div class="col-md-9">
    @include('site.businesses.partials.title-and-rating', ['business'=>$business])
    @include('site.businesses.partials.action-bar-no-reviews', ['business'=>$business])
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12 bg-white pt20">
        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
        @if($business->photos()->count() >0)
          <div class="p10 pl0">
            <h2 class="item-headers">Rate Cards of {{$business->name}}</h2>
            <div class="photos-holder">
              @foreach($business->rate_cards as $one_photo)
                @if($one_photo->hasImage())
                  <a href="{{ $one_photo->image->url('large') }}" class="mr10 image mb15" style="display:inline-block"  ><img src="{{ $one_photo->image->url('small') }}" class="img-rounded" /></a>
                @endif
              @endforeach
            </div>  
          </div>
        @endif

      </div>
    </div>
  </div>
  <div class="col-md-3 pt20">
    <small class=" pb10 mb5">FEATURED</small>
    @foreach($suggested_spas as $native_ad)
      @include('site.businesses.partials.business-niblet', ['business'=>$native_ad])
    @endforeach

  </div>
</div>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))

@stop