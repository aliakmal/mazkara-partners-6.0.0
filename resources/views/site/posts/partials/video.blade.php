<div  class=" clearfix">
  @if($post->hasCover())
    <div class="">
      <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="  " style="position: relative; height: 100%; display: block;" >
        <i class="fa fa-play-circle-o " style="font-size:320%;color:#fff; opacity:0.8; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"></i>
        <img src="{{ $post->cover->image->url('medium') }}" width="100%" class="ba"  />
      </a>
    </div>
  @endif

  <div class=" ">
    <h3 class=" fw500 ovf-hidden mt10 text-center p10 pb5  mb0" style="margin-top:0px;">
      <a title="{{{ $post->title }}} " href="{{ $post->url() }}" class=" dpib fs75">
        {{{ ($post->title) }}}
      </a>
    </h3>
  </div>
</div>
