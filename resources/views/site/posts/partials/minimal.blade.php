    <div  class="hide-only-mobile clearfix ">
      @if($post->hasCover())
      <div class="">
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack('body')}}" class="relative dpb " >
          <div style="background-image:url({{ $post->getCoverUrl('medium') }});background-size:cover" width="100%" class="ba dpb h144"></div>
          <div class="tpos0 absolute overlap-transparent-3x dpb va-container-v va-container-h ">
          </div>
        </a>
      </div>


      @endif

      <div class="ba bg-white pr10 pt5 pb5 pl10 ">
          <div class="ovf-hidden  h85 mb5">
            <h5 class="fw900 media-heading mb0 notransform mt0 " >
            <a title="{{{ $post->title }}} " style="" href="{{ $post->url() }}?{{_apfabtrack('body')}}" class="h24 w100pc nowrap ovf-hidden  dpb dark-gray fs80 ovf-hidden dpib  ">
              <span class="fs110  ">{{{ ($post->title) }}}</span>
            </a>
          </h5>
          <p class="medium-gray  pb0">{{ mzk_str_trim($post->caption, 70) }}</p>
        </div>
        <div class="" >

          <div class="mt10 pt5 bt">
            <div class="pt5 ">
              <div class="fs90">
                <div class="pull-left">
                  <span class="fs125">
                    <i class=" flaticon flaticon-eye110  gray"></i>
                  </span>&nbsp;{{ $post->views }}&nbsp;Views&nbsp;&nbsp;
                </div>
                <div class="pull-right">
                  <span class="fs125">
                    <i class="fa fa-thumbs-up gray"></i>
                  </span>&nbsp;{{ count($post->likes) }}&nbsp;Likes
                </div>
            </div>

              <div class="clearfix"></div>
            </div>
          </div>

        </div>

        
      </div>
    </div>
    <div class="show-only-mobile "> 
    @include('site.posts.partials.single-mobile-2')
    </div>
