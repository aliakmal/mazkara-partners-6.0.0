  <article class=" clearfix  show-only-mobile" style="padding:0px;">
    @include('site.posts.partials.single-mobile-2')
  </article>

  <article class="mb25 clearfix ba hide-only-mobile" style="padding:0px;border:1px solid #f0f0f0">

    <div  class="bg-white clearfix">

      @if($post->hasCover())
      <div class="pull-left pr10 relative">
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body','post-image'])}}" rel="nofollow" class="  " >
          <img src="{{ $post->cover->image->url('medium') }}" width="270" class="br"  />
        </a>
      </div>
      @endif
 
      <div class="">
        <div class="pos-relative p10 pt0 pr0" >
          <div class="mb10 pt5 pl10">
            <div class="text-left   ml0 m10 mb0 pt1 pr10  ">

              <div class="pull-right">
                <span class="">
                  <i class=" flaticon flaticon-eye110 op80 medium-gray"></i>
                  <span class="medium-gray fs90 "><b>{{ $post->views }}&nbsp;Views</b></span>
                </span>
                
                &nbsp;
                <span class="">
                  <i class="fa fa-thumbs-up op80 {{ count($post->likes)>0 ? 'lite-blue':'medium-gray' }} "></i>
                </span>
                <span class="fs90 medium-gray"><b> {{ count($post->likes) }}&nbsp;Likes</b></span>
              </div>
              
              <div class="fs90 medium-gray">
                <?php 
                $services = [];
                foreach($post->services as $service){
                  $services[] = $service->name;
                }
                ?>
                {{ strtoupper(join(', ', $services)) }}

              </div>
            </div>
          </div>
          <h3 class="   ovf-hidden mt10 mb0">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body','post-title'])}}" class="hover-underline fw900 dpib dark-gray">
              {{{ ($post->title) }}}
            </a>
          </h3>
          <div class="mt0 pt5 medium-gray-1 mb0">
            {{{ mzk_str_trim($post->caption, 150) }}}
          </div>
          <div class="mt5 mb10 fs90">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body','read-more'])}}" >Read More <i class="fa fs125 fa-caret-right"></i></a>
          </div>

          <div class="mt5   ">
            <div class="pull-left pr15">
              <a href="{{route('users.profile.show', $post->author->id)}}?{{_apfabtrack(['posts','post-author-avatar'])}}">
                {{ ViewHelper::userAvatar($post->author)}}
              </a>
            </div>

              <a href="{{route('users.profile.show', $post->author->id)}}?{{_apfabtrack(['posts','post-author'])}}">
                {{ $post->authors_full_name }}
              </a>
              <span class="medium-gray">&nbsp;&nbsp;<i>posted {{{ Date::parse($post->published_on)->ago() }}}</i></span>
            <br/>
            <span class="medium-gray">{{ $post->authors_designation }}</span>

              <div class="clear"></div> 

          </div>
          <div class=""></div>

        </div>

        
      </div>
    </div>


  </article>
