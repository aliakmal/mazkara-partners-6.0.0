            <p>
              <b>COMMENTS {{ $post->comments->count()>0 ? '('.$post->comments->count().')' : '' }}</b>
            </p>
            <div class="mt10 pt10 clearfix ">
              @foreach($post->comments as $comment)
                <div class="media pb10 pt10 bb">
                  @if(Auth::check())
                    @if($comment->isDeletableBy(Auth::user()))
                      {{ Form::open(array('style' => 'display: inline-block;', 
                                          'class'=>'pull-right confirmable',  'method' => 'DELETE', 
                                          'route' => array('comments.destroy', $comment->id))) }}
                        <a href="javascript:void(0)" class="submit-parent-form"><i class="fa fa-times"></i></a>
                      {{ Form::close() }}
                    @endif
                  @endif

                  <div class="media-left">
                    <a href="/users/{{$comment->user_id}}/profile">
                      {{ ViewHelper::userAvatar($comment->user, ViewHelper::$avatar35) }}
                    </a>
                  </div>
                  <div class="media-body dark-gray">
                    <b>{{$comment->getDisplayableUsersName()}}</b> 
                    <span class="medium-gray">&nbsp;{{$comment->body}}</span>
                    <p>
                      <small class="gray">
                        {{{ Date::parse($comment->updated_at)->ago() }}}
                      </small>
                    </p>
                  </div>
                </div>
              @endforeach
            </div>
      @if(Auth::check())
        <?php $user = Auth::user();?>
        <div class="media pb10 pt10 ">
          <div class="media-left">
            <a href="/users/{{$user->id}}/profile" >
              {{ViewHelper::userAvatar($user, ViewHelper::$avatar35)}}
            </a>
          </div>
          <div class="media-body" style="width: 100%;">
            {{ Form::open(array('route' => 'comments.store')) }}
              {{ Form::hidden('commentable_type', 'Post')}}
              {{ Form::hidden('commentable_id', $post->id)}}
              {{ Form::hidden('type', 'comment')}}
              {{ Form::text('body', '', ['class'=>'form-control count-limiter', 'placeholder'=>'Write a comment'])}}
            {{ Form::close()}}
          </div>
        </div>
      @else
        <div class="well mt10">
          <a href="/users/create" id="ajax-login-link" class=" ajax-popup-link" title="Sign in to comment"  data-toggle="tooltip" data-placement="left" >
            Sign in
          
           to leave a comment</a>
        </div>
      @endif
