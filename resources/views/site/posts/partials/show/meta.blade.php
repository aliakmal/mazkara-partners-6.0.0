      <div class="pt15 mt15  bb  clearfix ">
        <div class="pull-left mb10 mr10">
          {{ViewHelper::userAvatar($post->author, ViewHelper::$avatar45)}}
        </div>
        <div>
          <b>
            <a href="{{route('users.profile.show', $post->author->id)}}">
              {{$post->author->name}}
            </a>
          </b><br/>
          {{$post->authors_designation }}
          <div class="pull-right medium-gray mt10 mr0">
            POSTED ON {{ strtoupper(mzk_f_date($post->published_on, 'jS M'))}}
          </div>
        </div>
      </div>
      <div class="text-right mt10 mb10">
        <div class="pull-left">
          <span class="medium-gray">
            <i class=" flaticon flaticon-eye110 op80 "></i> {{ $post->views }} Views
          </span> 
          &nbsp;
          <span class="medium-gray">
            @if(Auth::check())
              <a href="javascript:void(0)" class=" medium-gray fav-blue {{ Auth::check()? ($post->liked(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }}  " rel="{{$post->id}}"  data-route="posts">
                <i class="fa fa-thumbs-up "></i> LIKE
              </a>&nbsp;<span id="likes-count-holder">{{ $post->num_likes()}} </span>
              @if($post->isEditableBy(Auth::user()->id))
                @if($post->isEditable())
                  <a href="/posts/{{$post->id}}/edit" class="btn medium-gray btn-xs btn-default " rel="{{$post->id}}">
                    <i class="fa fa-pencil"></i> Edit Post
                  </a>
                @endif
              @endif

            @else
              <a href="/users/login" class="medium-gray ajax-popup-link  " rel="{{$post->id}}">
                <i class="fa fa-thumbs-up"></i> LIKE 
              </a>&nbsp;<span id="likes-count-holder">{{ $post->num_likes()}} </span>
            @endif
          </span>




        </div>

        <div class="pull-right">
          <span class='st_sharethis' displayText='ShareThis'></span>
          <span class='st_facebook' displayText='Facebook'></span>
          <span class='st_twitter' displayText='Tweet'></span>
          <span class='st_googleplus' displayText='Google +'></span>
          <span class='st_pinterest hide-only-mobile' displayText='Pinterest'></span>
          <span class='st_' displayText=''></span>
        </div>

      </div>

      <div class="clearfix"></div>


      @if((Auth::check()) && $post->isEditableBy(Auth::user()->id))
        @include('site.posts.partials.publish-status')

      @endif
