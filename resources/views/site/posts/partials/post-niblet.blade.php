<div class="pt10 pb10  bb bg-white">
  <div class="row">
    <div class="col-md-12">
      <div class="">
      @if($post->hasCover())
        <span class="pull-left mr10">
          <a href="{{ $post->url() }}?{{ _apfabtrack(['rhs']) }}">
            <img src="{{ $post->cover->image->url('thumbnail') }}" width="60"  class="ba"  />
          </a>
        </span>
      @endif

        <div class="fs90">
          <h5 class="ovf-hidden nowrap media-heading  mb0  notransform bolder ">
          <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{ _apfabtrack(['rhs']) }}" class="result-title hover-underline fs80">
            {{{ mzk_str_trim($post->title, 20) }}} 
          </a>
        </h5>
        <span  class="search-item-address fs90">
          › {{ $post->authors_full_name }}
        </span><br/></div>
        <div class="text-right">

          <span class="">
            <i class=" flaticon flaticon-eye110  medium-gray"></i>
            <span class="medium-gray fs90 pl5"><b>{{ $post->views }}</b></span>
          </span>
          
          &nbsp;
          <span class="">
            <i class="fa fa-thumbs-up op80 {{ $post->num_likes()>0 ? 'lite-blue':'medium-gray' }}"></i>
          </span>
          <span class="fs90 medium-gray pl5"><b>{{ $post->num_likes() }}</b></span>



        </div>

      </div>
    </div>
  </div>
</div>