  <article class=" clearfix bg-white show-only-mobile" style="padding:0px;">
    <div  class=" clearfix">
      <div class="mt10 fw300  pl10 pr10 pb10 mb10">
          <div class=" pull-right">
            {{ join(',',$post->services()->lists('name','name')->all()) }}
          </div>

        <div class="pull-left pr15">
          <a href="{{route('users.profile.show', $post->author->id)}}">
            {{ ViewHelper::userAvatar($post->author)}}
          </a>
        </div>

        <b>
          <a href="{{route('users.profile.show', $post->author->id)}}">
            {{ $post->authors_full_name }}
          </a>
        </b><br/>
        {{ $post->authors_designation }}

        <div class="clear"></div> 
      </div>

      @if($post->hasCover())
        <div class="">
          <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="  " >
            <img src="{{ $post->cover->image->url('medium') }}" width="100%" class="ba"  />
          </a>
        </div>
      @endif

      <div class=" ">
          <h3 class=" fw500 ovf-hidden mt10 p10  mb0" style="margin-top:0px;">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class=" dpib ">
              {{{ ($post->title) }}}
            </a>
          </h3>
        <div class="" >

          <div class="p10 pt0 bb">
            <div class="text-left ">
              <div class="pull-left">
                <span class="fs175">
                <i class="fa fa-thumbs-up gray"></i>
              </span>&nbsp;&nbsp;
              <span class="fs175">
                <i class=" fa fa-comment-o  gray"></i>
              </span>
            </div>

              <div class="text-right gray pull-right fs90">
                {{ $post->num_comments() }} Comment(s)<br/>
                {{ $post->num_likes() }} Like(s)
              </div>
              <div class="clearfix"></div>
            </div>
          </div>

        </div>

        
      </div>
    </div>

  </article>
