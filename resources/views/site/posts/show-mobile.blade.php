@extends('layouts.mobile')
@section('preheader')
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "91cce174-96e7-4c79-8290-329bed8eca64", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
@stop
@section('content')
<div class="container ">
  <div class="row">
    <div class="col-md-12">
    <div class="row-height">
      <div class="row bg-lite-gray pb10">
        <div class="col-md-12 " style="vertical-align:top">
          @include('site.posts.partials.show.meta-mobile')
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 " style="vertical-align:top">
          <div class="inside-full-height">
            <h1 class="pt10 mt10  fw500 item-name mt20">{{$post->title}}</h1>
            <div class="mb10 mt10 fs125">
              {{ $post->caption }}
            </div>
            @if($post->isVideo())
              <iframe width="100%" height="450" src="{{ $post->videoUrl() }}" frameborder="0" allowfullscreen>
              </iframe>
              <div class="well fs80">
                {{ $post->caption }}
              </div>
            @else
              @if($post->hasCover())
                <img src="{{ $post->cover->image->url('xlarge') }}" width="100%" class="ba"  />
              @endif
              <div class="pt10 page-font-override">
                {{ $post->body}}
              </div>

            @endif
</div>
    </div></div></div></div>

  </div>
  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$(function(){
  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');
    v = $('#likes-count-holder').html();
    v = Number(v)+1;
    $('#likes-count-holder').html((v).toString());

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');
    v = $('#likes-count-holder').html();
    v = Number(v)-1;
    $('#likes-count-holder').html((v).toString());


    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });


});
</script>

@stop