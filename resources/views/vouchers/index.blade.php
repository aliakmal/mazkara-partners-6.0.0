@extends('layouts.scaffold')

@section('main')

<h1>All Vouchers</h1>

<p>{{ link_to_route('vouchers.create', 'Add New Voucher', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($vouchers->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Offer_id</th>
				<th>User_id</th>
				<th>Code</th>
				<th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($vouchers as $voucher)
				<tr>
					<td>{{{ $voucher->offer_id }}}</td>
					<td>{{{ $voucher->user_id }}}</td>
					<td>{{{ $voucher->code }}}</td>
					<td>{{{ $voucher->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('vouchers.destroy', $voucher->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('vouchers.edit', 'Edit', array($voucher->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no vouchers
@endif

@stop
