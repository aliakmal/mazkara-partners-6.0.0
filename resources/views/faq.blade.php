@extends('layouts.page')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

<h1 class="pt20 pb20 text-center notransform">
   Frequently Asked Questions
</h1>
<div class="well">
<div class=" p15"><ul>
  <li><a href="#q1">What is Fabogo?</a></li>
  <li><a href="#q2">What are offers?</a></li>
  <li><a href="#q3">How can I avail an offer?</a></li>
  <li><a href="#q4">Can I purchase offers online?</a></li>
  <li><a href="#q5">How long until I can avail an offer?</a></li>
  <li><a href="#q6">Do I need to book in advance to avail an offer?</a></li>
  <li><a href="#q7">I run a salon - how can I be listed on Fabogo?</a></li>
  <li><a href="#q8">What are tips?</a></li>
  <li><a href="#q9">I had an unfortunate experience at a salon can I mention it in a tip?</a></li>
</ul></div></div>
<a id="q1"></a>
<h3>What is Fabogo?</h3>
<p>Fabogo is a one stop discovery engine for the best salons and spas in your city with thousands of spas and salons listed.</p>

<a id="q2"></a>
<h3>What are offers?</h3>
<p>Offers on Fabogo are exclusive offers provided by salons and spa's for Fabogo users. They are specially discounted deals that you are unlikely to get anywhere else except on Fabogo.</p>

<a id="q3"></a>
<h3>How can I avail an offer?</h3>
<p>Offer's are listed for free on Fabogo - to avail an offer simply call the participating outlet and inquire or make a booking. Be sure to mention Fabogo when inquiring about the offer.</p>

<a id="q4"></a>
<h3>Can I purchase offers online?</h3>
<p>We're working on online purchase of offers and should be out soon. As of now you cannot purchase offers online at the moment - but that is set to change :)</p>

<a id="q5"></a>
<h3>How long until I can avail an offer?</h3>
<p>All offers have an expiry date which indicates the validity of the offer. Its good practice to avail of offers as soon as possible.</p>

<a id="q6"></a>
<h3>Do I need to book in advance to avail an offer?</h3>
<p>Unless otherwise specified all offers should be booked in advance to avoid any inconveniences.</p>

<a id="q7"></a>
<h3>I run a salon - how can I be listed on Fabogo?</h3>
<p>Hello there - love to hear from you, just give us a shout on our form right <a href="/add-your-business">here</a>.</p>

<a id="q8"></a>
<h3>What are tips?</h3>
<p>Tips are just that, what you'd leave for other visitors to a salon. Think of it as a review only concise like a generous tweet - leave tips to increase your popularity and avail of fantastic deals and rewards that Fabogo would put out soon.</p>

<a id="q9"></a>
<h3>I had an unfortunate experience at a salon can I mention it in a tip?</h3>
<p>We're so sorry to hear about that, you can definitely voice your concerns in a tip for future users - but please remember the golden rules :) be nice be polite.</p>

<hr/>
<h3>Contacting Us</h3>

<p>If there are any questions regarding this TOS you may contact us using the information below.<p>

www.fabogo.com<br/>
Creative City<br/>
Fujairah<br/>
PO.Box: 4422<br/>
United Arab Emirates<br/>
hello@fabogo.com
</div>
    </div>
</div>

@stop
