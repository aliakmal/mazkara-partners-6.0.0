<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fabogo Admin Panel</title>
    <?php echo mzk_stylesheet_link_tag('dashboard', true); ?>

    <?php echo mzk_js_tag('dashboard', true); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        @include('elements.menus.admin-navbar-no-sidebar')
        <div id="page-wrapper" style="margin:0px;padding-top: 30px; border:0px;">
            <div class="container">
            @yield('content')
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<script type="text/javascript">
$(function(){
    $(function(){
        $('input[type="date"]').datepicker({ format: "yyyy-mm-dd", autoclose:true });
    })

})
</script>
</body>

</html>
