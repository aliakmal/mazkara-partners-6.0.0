<!DOCTYPE html>
<html lang="en" ng-app="mazkaraApp">
  <head>
  @include('elements.head')
  <style type="text/css">
  </style>
  </head>
  <body>
    @include('elements.post-body')
    
    <div id="fakeloader"></div>
    @include('elements.nav-alt')
    <div class="container bg-white ">
      @yield('content')
    </div>
    
    @include('elements.footer')
  </body>
</html>