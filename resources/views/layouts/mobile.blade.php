<!DOCTYPE html>
<html lang="en" ng-app="mazkaraApp">
  <head>
  @include('elements.head')
  </head>
  <body>
    @include('elements.post-body')
    
    <div class="container bg-white p0">
      @yield('content')  
    </div>
    
  </body>
</html>