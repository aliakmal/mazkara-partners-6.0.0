<!DOCTYPE html>
<html lang="en" ng-app="mazkaraApp" ng-controller="VenuesListingController as venuesCtrl">
  <head>
  @include('elements.head-angular')
  <style type="text/css">
  </style>
  </head>
  <body>
    @include('elements.post-body')
    
    @include('elements.nav-bottom-search-angular')
    <div class="container bg-lite-gray no-bg-on-mobile ">
      @yield('content')
    </div>
    
    @include('elements.footer')
  </body>
</html>