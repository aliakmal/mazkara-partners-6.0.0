<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fabogo Content Panel</title>

    <?php echo mzk_stylesheet_link_tag('dashboard', true); ?>
    <?php echo mzk_js_tag('dashboard', true); ?>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        @include('elements.menus.admin-navbar-content')
        <div id="page-wrapper">
            @yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<script type="text/javascript">
$(function(){
    $(function(){
        $('input[type="date"]').datepicker({ format: "yyyy-mm-dd", autoclose:true });
    })
    $(function(){
        $('.toggle-side-menu-hide-show').click(function(){
            if($('.navbar-default.sidebar').hasClass('hidden')){
                $('.navbar-default.sidebar').removeClass('hidden');
                $('#page-wrapper').css('marginLeft', '250px');
            }else{
                $('.navbar-default.sidebar').addClass('hidden');
                $('#page-wrapper').css('marginLeft', '0px');
            }
        });
    });

})
</script>
</body>

</html>
