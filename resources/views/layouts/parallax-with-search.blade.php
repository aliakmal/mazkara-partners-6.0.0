<!DOCTYPE html>
<html lang="en" ng-app="mazkaraApp">
  <head>
  @include('elements.head')
  <style type="text/css">
  </style>
  </head>
  <body>
    @include('elements.post-body')
    
    <div id="fakeloader" ></div>
    @include('elements.nav-bottom-search')
    <div class="container bg-white p0">
      @yield('content')
    </div>
    
    @include('elements.footer')
  </body>
</html>