@extends('layouts.page-02')
@section('content')
<img style="width:100%" src="{{mzk_assets('assets/banner-website.png')}}" />

<div class="container">
    <div class="row">
        <div class="col-md-12 bg-white">
            <div class="" style="padding:20px; border:1px dashed #DADADA;margin:20px;">
              <div class="row">
                <div class="col-md-3 text-center">
                  <img width="150" src="{{ mzk_assets('assets/icon_1.png') }}" />
                </div>
                <div class="col-md-9">
                  <h3 class="mt0   notransform">How does it work?</h3>
                  <ul class="ml10 pl5">
                    <li>Registered users need to write a review  for a salon, spa or a fitness centre listed on Fabogo.</li>
                    <li>Every week 1 lucky winner from each city will be announced and will be awarded with a luxurious salon or spa voucher.</li>
                    <li>Winners will be selected on the basis of the review written. The more useful and informative the review, chances to win increase.</li>
                    <li>We will compile all the useful & informative reviews and  put them in the lucky draw to select one lucky winner.</li>
                    <li>All lucky winners will be reached via email every <span style="color:red">Monday</span>.</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="" style="padding:20px; border:1px dashed #DADADA;margin:20px;">
              <div class="row">
                <div class="col-md-3 text-center">
                  <img width="175" src="{{ mzk_assets('assets/icon_2.png') }}" />
                </div>
                <div class="col-md-9">
                  <h3 class="mt0   notransform">Terms and Conditions</h3>
                  <ul class="ml10 pl5">
                    <li>Please Note: Reviews need to be original and unbiased as we will not accept reviews copied from other web-sites.</li>
                    <li>This contest closes on <span style="color:red">3rd July '16</span>.</li>
                    <li>Winners must revert to the email and claim their vouchers within 72 hours.</li>
                    <li>Winners are decided by the judges using unbiased techniques and are considered as final. Once announced, these will not be subject to any changes whatsoever.</li>
                    <li>This contest is open to residents of Dubai and Pune only.</li>
                    <li>Fabogo employees are not eligible to participate in this contest.</li>
                    <li>Fabogo reserves the right to cancel the contest at any time without prior notice.</li>
                  </ul>
                </div>
            </div>
          </div>

            <div class="" style="padding:20px; border:1px dashed #DADADA;margin:20px;">
              <div class="row">
                <div class="col-md-3 text-center">
                  <img width="100" src="{{ mzk_assets('assets/icon_3.png') }}" />
                </div>
                <div class="col-md-9">
                  <h3 class="mt0   notransform">More Details</h3>
                  <ul class="ml10 pl5">
                    <li>An informative review should cover the following in detail:
                      <ul style="margin-left:20px;">
                        <li>Service, stylist, ambiance, equipment, value for money, overall experience<br/>and whether you would recommend it to others.</li>
                      </ul>
                    </li>
                    <li>For any questions, feel free to drop us an email hello@fabogo.com</li>
                  </ul>
                </div>
            </div>
          </div>

        </div>
    </div>
</div>
<div class="bg-lite-gray">
<div class="container " style="padding:30px;">
  <div class="row show-only-mobile">
    <div class="col-md-6 ">
      <div class="bg-white p15 mb15">
        <p class="orange fs175 text-center"><b><img src="https://s3.amazonaws.com/mazkaracdn/assets/badge.png" style="width:45px" /> WINNER IN DUBAI</b></p>
        @include('site.reviews.partials.featured', ['review'=>$dubai_review, 'show_business'=>true,'show_border'=>false, 'show_comments'=>false, 'show_links'=>false, 'business'=>$dubai_review->business])
      </div>





    </div>
    <div class="col-md-6">
      <div class="bg-white p15">
      <p class="orange text-center fs175  "><b><img src="https://s3.amazonaws.com/mazkaracdn/assets/badge.png" style="width:45px" /> WINNER IN PUNE</b></p>
        @include('site.reviews.partials.featured', ['review'=>$pune_review,'show_business'=>true,  'show_border'=>false,  'show_comments'=>false, 'show_links'=>false, 'business'=>$pune_review->business])
        </div>
    </div>
  </div>



  <div class="row hide-only-mobile">
    <div class="col-md-12 ">
      <table class="table" style="border:0px;" cellpadding="0" cellspacing="0" width="100%">
        <tr style="border:0px;">
          <td style="border:0px;" width="49%" class="bg-white"><div class="bg-white p25"><div class="orange fs175 text-center   ">
                <b>
                  <img src="https://s3.amazonaws.com/mazkaracdn/assets/badge.png" style="width:45px" /> WINNER IN DUBAI
                </b>
              </div>
              @include('site.reviews.partials.featured', ['review'=>$dubai_review, 'show_business'=>true, 'show_border'=>false, 'show_comments'=>false, 'show_links'=>false, 'business'=>$dubai_review->business])
            </div>

          </td>
          <td style="border:0px;"></td>
          <td style="border:0px;" class="bg-white" width="49%"><div class="bg-white p25"><div class="orange fs175 text-center   ">
                <b>
                  <img src="https://s3.amazonaws.com/mazkaracdn/assets/badge.png" style="width:45px" /> WINNER IN PUNE
                </b>
              </div>
              @include('site.reviews.partials.featured', ['review'=>$pune_review, 'show_business'=>true, 'show_border'=>false,  'show_comments'=>false, 'show_links'=>false, 'business'=>$pune_review->business])
            </div>
          </td>
        </tr>
      </table>





    </div>
  </div>
</div>
</div>
<script type="text/javascript">
$(function(){

    $(function () {
      $('[data-toggle="popover"]').popover({trigger:'hover', placement:'left'});
    })    
})
</script>
@stop