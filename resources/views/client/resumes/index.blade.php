@extends('layouts.client')
@section('content')
<div class="content">

    <div class="container">



<div class="row">
  <div class="col-md-9">
      <h3 class="portlet-title">
FabHIRE</h3>

<?php $timezone = $business->isUAE()?'uae':'ist';?>
@if ($resumes->count())
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th style="width:15%">Name</th>
        <th style="width:10%">Nationality </th>
        <th style="width:10%">Location</th>
        <th style="width:10%">Phone</th>

        <th>Gender</th>
        <th>D.O.B</th>
        <th>Email</th>
        <th>Preferred&nbsp;Location</th>
        <th>Current&nbsp;Company</th>



        <th>Specialization</th>
        <th >Currency</th>
        <th style="width:10%">Salary</th>
        <th style="width:10%">Experience</th>
        <th>Visa&nbsp;Status</th>
        <th>Passport&nbsp;Status</th>
        <th style="width:5%">Resume</th>
        <th></th>
      </tr>
    </thead>

  @foreach($resumes as $resume)

      <tr>
        <td>{{{ $resume->name }}}</td>
        <td>{{{ mzk_get_country_from_list($resume->nationality) }}}</td>
        <td>{{{ $resume->location }}}</td>
        <td>{{{ $resume->phone }}}</td>

        <td>{{ $resume->gender }}</td>
        <td>{{ $resume->date_of_birth }}</td>
        <td>{{ $resume->email_address }}</td>
        <td>{{ $resume->preferred_location }}</td>
        <td>{{ $resume->current_company }}</td>


        <td>{{{ ucwords(str_replace('-', ' ', $resume->specialization.' '.join(' / ', $resume->specializations->lists('specialization','specialization')->all()))) }}}</td>
        <td>{{ $resume->currency }}</td>
        <td>{{{ $resume->salary }}}</td>
        <td>{{{ ucwords(str_replace('-', ' ', $resume->experience)) }}}</td>
        <td>{{ $resume->visa_status }}</td>
        <td>{{ $resume->passport_status }}</td>
        <td>
          @if(strstr($resume->doc->url(), 'missing'))
            @if($resume->resume_url!='')
              <a target="_blank" href="{{ $resume->resume_url }}"><i class="fa fa-download"></i></a>
            @endif

          @else
            <a target="_blank" href="{{ $resume->doc->url() }}"><i class="fa fa-download"></i></a>
          @endif
        </td>
        <td></td>
      </tr>
  @endforeach
  </table>
</div>


  <div class="row">
    <div class="col-md-12">
  {{ $resumes->appends($params)->render() }}
  <div class="pull-right">
    {{ count($resumes) }} / {{ $resumes->total() }} entries
  </div></div>
</div>

@else

  There are no resumes
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
@endif
</div>
<div class="col-md-3">

  <div class="portlet portlet-boxed">
    <div class="portlet-header">
      <h4>Filter Resumes</h4>
    </div>
    <div class="portlet-body">
      <form >
        <!-- <div class="input-daterange  " id="datepicker">
          <input type="text" placeholder="Select Start Date" value="{{ isset($params['start'])?$params['start']:'' }}" class="input-sm form-control" id="start" name="start" />
          <br/>
          <input type="text" placeholder="Select End Date" value="{{ isset($params['end'])?$params['end']:'' }}" class="input-sm form-control" id="end" name="end" />
        </div>
        <br/> -->
        <input type="text" placeholder="Search" value="{{ isset($params['search'])?$params['search']:''}}" class="input-sm form-control" id="search" name="search" />
          <br/>
        <input type="text" placeholder="Location?" value="{{ isset($params['location'])?$params['location']:''}}" class="input-sm form-control" id="location" name="location" />
          <br/>
        <input type="text" placeholder="Nationality?" value="{{ isset($params['nationality'])?$params['nationality']:''}}" class="input-sm form-control" id="nationality" name="nationality" />
        <!--{{ Form::select('nationality', $nationalities, isset($params['nationality'])?$params['nationality']:'', array('class'=>'form-control', 'placeholder'=>'Nationality?') ) }}&nbsp;-->
          <br/>
        <input type="text" placeholder="Specialization?" value="{{ isset($params['specialization'])?$params['specialization']:''}}" class="input-sm form-control" id="specialization" name="specialization" />
        <!--{{ Form::select('specialization', $specializations, isset($params['specialization'])?$params['specialization']:'', array('class'=>'form-control', 'placeholder'=>'Specialization?') ) }}-->
          <br/>
        <input type="text" placeholder="Experience?" value="{{ isset($params['experience'])?$params['experience']:''}}" class="input-sm form-control" id="experience" name="experience" />
        <!-- {{ Form::select('experience', $experiences, isset($params['experience'])?$params['experience']:'', array('class'=>'form-control', 'placeholder'=>'Experience?') ) }}-->
          <br/>
        <input type="text" placeholder="Phone?" value="{{ isset($params['phone'])?$params['phone']:''}}" class="input-sm form-control" id="phone" name="phone" />
          <br/>
        <input type="text" placeholder="Email?" value="{{ isset($params['email_address'])?$params['email_address']:''}}" class="input-sm form-control" id="email_address" name="email_address" />
          <br/>
        <input type="text" placeholder="Current Company?" value="{{ isset($params['current_company'])?$params['current_company']:''}}" class="input-sm form-control" id="current_company" name="current_company" />
          <br/>
        {{ Form::select('gender', ['male'=>'Male', 'female'=>'Female'], isset($params['gender'])?$params['gender']:'', array('class'=>'form-control', 'placeholder'=>'Gender?') ) }}
          <br/>

        <p>
          <input type="submit" class="btn-block btn btn btn-primary" value="Apply" />
          <a href="resumes"  style="margin-top:5px;" class="btn btn-sm btn-block btn-default">Reset</a>
        </p>
      </form>
    </div>
  </div>


</div>
    </div> <!-- /.container -->

  </div>
</div>

<script type="text/javascript">
$(function(){
    $('.input-daterange').datepicker({
      format: "yyyy-mm-dd", autoclose:true
  });    


});
</script>
@stop