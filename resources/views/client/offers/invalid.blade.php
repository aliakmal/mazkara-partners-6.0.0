<div class="bg-white  col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 p0">                    
  <div class="bg-lite-gray text-center p10 mt0">
    <h4 class="mb0">VALIDATE VOUCHER</h4>
  </div>
  <div class="pt10  pb0 mt10 ml15 mr15 mb10">
    <div class="bg-gray p10 ">
      <div class="dpb text-center mb5">THIS VOUCHER CODE IS <span class="text-danger">INVALID</span>. PLEASE TRY AGAIN.</div>
      {{ Form::text('code', '', ['class'=>'form-control mb10 p10', 'id'=>'validate-voucher-code', 'required'=>true] )}}
      <center>
        <a href="javascript:void(0)" class="btn text-center submit-btn btn-validate-voucher m10 btn-pink no-border-radius">VALIDATE</a>
      </center>

    </div>


  </div>
</div>

<script type="text/javascript">
$(function(){

  $('#validate-voucher-code').focus();

  $('.btn-validate-voucher').click(function(){
    data = { code: $('#validate-voucher-code').val()};
    $.ajax({
      type: "POST",
      url: '{{ route('client.voucher.validate', [mzk_client_bid()] ) }}',
      data: data,
      success: function(result) {
        $.magnificPopup.instance.items[0] = {
          src: result['html'], type: 'inline' };
        $.magnificPopup.instance.updateItemHTML();
        return false;
      }
    });

  });

});
</script>