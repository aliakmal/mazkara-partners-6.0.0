@extends('layouts.client')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Special</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif


@include('client.offers.form')

</div></div>

@stop
