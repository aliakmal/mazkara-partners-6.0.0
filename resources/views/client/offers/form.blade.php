@if(isset($offer))
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->put()->action(route('partner.offers.update',  [mzk_client_bid(), $offer->id]))
                                  ->encodingType('multipart/form-data') }}
  {{ BootForm::bind($offer) }}
@else
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->action(route('partner.offers.store', mzk_client_bid()))
                                  ->encodingType('multipart/form-data') }}
@endif

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
    {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
    {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif

  {{ BootForm::text('Title', 'title') }}
  {{ BootForm::textarea('Description', 'description')->setAttribute('id','description') }}

  <?php 
    $service_items = [];
    $m = Menu_group::optionables(['business_id'=>$business->id]);
    foreach($m as $ix=>$name){
      $sl = $business->serviceItems()->byMenuGroup($ix)->get()->lists('name', 'id')->all();
      if(count($sl)>0){
      $service_items[$name] = $business->serviceItems()->byMenuGroup($ix)->get()->lists('name', 'id')->all();

      }
    }

    $services = [];

    $zone = Zone::find($business->city_id);
    $s = $zone->activeParentServices()->get();
    foreach($s as $service){
      $services[$service->name] = Service::byLocaleActive($zone->id)->where('parent_id', '=', $service->id)->get()->lists('name', 'id')->all();
    }

  ?>

  {{ BootForm::select('Service Items', 'service_items[]')->options($service_items)
                          ->setAttribute('multiple', 'multiple')->setAttribute('id', 'service_items')
                          ->setAttribute('class', 'form-control multiple') }}

  {{ BootForm::select('Service Tag(s)', 'services[]')->options($services)
                          ->setAttribute('multiple', 'multiple')->setAttribute('id', 'service_tags')
                          ->setAttribute('class', 'form-control multiple') }}

  {{ BootForm::select('Tag', 'tag', Offer::tagsList()) }}

  {{ BootForm::select('State', 'state', Offer::activeStates()) }}
  {{ BootForm::select('Cost range', 'cost_range', [''=>'', 'upto'=>'upto', 'exactly'=>'exactly', 'starting-from'=>'starting-from']) }}

  {{ BootForm::text('Original price', 'original_price')->setAttribute('id', 'original_price') }}
  <div class="well">
    <div class="row">
      <div class="col-md-2">
        {{ BootForm::radio('Percentage', 'discount_type', 'percentage')
                          ->setAttribute('id', 'discount_type_percentage')
                          ->setAttribute('ref', 'discount_percentage_off') }}

      </div>
      <div class="col-md-10">
  {{ BootForm::select('Percentage Amount', 'percentage_amount',Offer::discountPercentages())
                ->setAttribute('class','form-control toggable')
                ->setAttribute('id', 'discount_percentage_off') }}

      </div>

    </div>

    <div class="row">
      <div class="col-md-12">
        {{ BootForm::radio('None Price Based Deal', 'discount_type', 'non-price-based-deal')
                          ->setAttribute('id', 'discount_type_percentage')
                          ->setAttribute('ref', 'discount_percentage_off') }}

      </div>
    </div>

  </div>
  <div class="well hidden">
    <div class="row">
      <div class="col-md-2">
  {{ BootForm::radio('Off', 'discount_type', 'off')
                  ->setAttribute('id', 'discount_type_off')
                  ->setAttribute('ref', 'discount_amount_off') }}

      </div>
      <div class="col-md-10">
  {{ BootForm::text('Amount Off', 'amount_off')
                ->setAttribute('class', 'form-control toggable')
                ->setAttribute('id', 'discount_amount_off') }}

      </div>
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('photo', 'Photo:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::file('photo', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
      @if(isset($offer))
        @if($offer->photo)
          <a href="{{ $offer->photo->image->url() }}" class="lightbox"><img src="{{ $offer->photo->image->url('thumbnail') }}" class="img-thumbnail" /></a>
          {{ Form::checkbox("deletablePhotos[]", $offer->photo->id, false ) }}
          Delete?
        @endif
      @endif

    </div>
  </div>

  {{ BootForm::text('Offer price', 'offer_price') }}
  {{ BootForm::text('Offer meta', 'legacy_price_meta') }}
  {{ BootForm::date('Starts from', 'start_date')->placeholder('Valid From?') }}
  {{ BootForm::date('Valid Until', 'valid_until')->placeholder('Valid until when?') }}
  {{ BootForm::text('Maximum vouchers', 'max_num_vouchers') }}

  @foreach(Offer::tocOptions() as $toc)
    {{ BootForm::checkbox($toc, 'toc[]')->value($toc) }}
  @endforeach

  {{ BootForm::token() }}
  {{ BootForm::submit('Save') }}
{{ BootForm::close() }}
<script type="text/javascript">
$(function(){
  __MZK_PRICES__ = {{ json_encode($business->serviceItems()->get()->lists('cost', 'id')->all())}};

  $('form').first().submit(function(){
    dp = $('#discount_type_percentage').find('input').first();
    df = $('#discount_type_off').find('input').first();

    if(($(dp).prop('checked')==false) && ($(df).prop('checked')==false)){
      alert('You must select a discount type!');
      return false;
    }
  })

  $('#service_tags').selectpicker({maxOptions:3}).change(function(){
    var itms = $("#service_tags option:selected").map(function() {

        return $(this).text();
    }).get();
    $('#description').val(itms.join('+'));
    var prcs = $("#service_tags option:selected").map(function() {
        return $(this).val();
    }).get();
  });



  $('#service_items').selectpicker({maxOptions:3}).change(function(){
    var itms = $("#service_items option:selected").map(function() {

        return $(this).text();
    }).get();
    $('#description').val(itms.join('+'));
    var prcs = $("#service_items option:selected").map(function() {
        return $(this).val();
    }).get();
    
    sum = 0;
    for(i in prcs){
      if(__MZK_PRICES__[prcs[i]]){
        sum = sum + Number(__MZK_PRICES__[prcs[i]]);
      }
    }

    $('#original_price').val(sum);
  });

  $('input[type=radio]').each(function(){
    rf = $(this).parents('.radio');
    rf = $(rf).attr('ref');
    if(this.checked){
      $('#'+rf).removeAttr('disabled');
    }else{
      $('#'+rf).prop('disabled', true);
    }

    $(this).click(function(){
      rf = $(this).parents('.radio');
      rf = $(rf).attr('ref');
      $('.toggable').prop('disabled', true);
      if(this.checked){ 
        $('#'+rf).removeAttr('disabled');
      }else{
        $('#'+rf).attr('disabled', true);
      }
      // computeOfferPrice();
  
    });
  });

  $('#original_price').keypress(function(){
    //computeOfferPrice();
  });

  $('select.toggable').change(function(){
    //computeOfferPrice();
  });

  $('input.toggable').keyup(function(){
    //computeOfferPrice();
  });

  function computeOfferPrice(){
    original = $('#original_price');
    offer = $('#offer_price');
    discount_type = $('input:radio:checked').val();
    if(isNaN($(original).val())){
      return;
    }

    if(!($(original).val())>1){
      return;
    }

    if(discount_type == 'percentage'){
      price = $(original).val() - ($(original).val() * ($('#discount_percentage_off').val()/100));
      $(original).attr('disabled', false);
    }

    if(discount_type == 'off'){
      price = $(original).val() - ($('#discount_amount_off').val());
      $(original).attr('disabled', false);
    }

    if(discount_type == 'non-price-based-deal'){
      price = '';
      $(original).attr('disabled', true);
    }

    $(offer).val(price);
  }

});
</script>