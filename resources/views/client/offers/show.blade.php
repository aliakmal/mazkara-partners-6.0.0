@extends('layouts.client')
@section('content')

<h1>Details</h1>

<p>{{ link_to(route('partner.offers.index', mzk_client_bid()), 'Return to All Specials',  array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Body</th>
				<th>Tag</th>
				<th>State</th>
				<th>Price</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $offer->body }}}</td>
			<td>{{{ $offer->tag }}}</td>
			<td>{{{ $offer->state }}}</td>
			<td>{{{ $offer->offer_price }}}</td>
      <td>            
        {{ link_to_route('partner.offers.edit', 'Edit', array(mzk_client_bid(), $offer->id), array('class' => 'btn btn-xs btn-info')) }}
      </td>
      <td>
        <?php $rte = route('partner.offers.destroy', [mzk_client_bid(), $offer->id]);?>
        <form method="post" action="{{$rte}}" style="display: inline-block;" onsubmit="return confirm('Are you sure?')">
          <input type="hidden" value="DELETE" name="_method">
          {{Form::token()}}
          {{ Form::submit('Delete', array('class' => 'btn btn-xs  btn-danger')) }}
        </form>
      </td>
		</tr>
	</tbody>
</table>
@stop
