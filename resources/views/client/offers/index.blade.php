@extends('layouts.client')
@section('content')

<div class="row">
  <div class="col-md-8">


  @if ($offers->count())
  	<table class="table table-striped">
  		<thead>
  			<tr>
  				<th>Body</th>
  				<th>Tag</th>
  				<th>State</th>
  				<th>Price</th>
  				<th>&nbsp;</th>
          <th>&nbsp;</th>
  			</tr>
  		</thead>

  		<tbody>
  			@foreach ($offers as $offer)
  				<tr>
  					<td>{{{ $offer->body }}}</td>
  					<td>{{{ $offer->tag }}}</td>
  					<td>{{ mzk_status_tag($offer->state) }}</td>
  					<td>{{{ $offer->displayable_offer_price }}}</td>
            <td>            
              {{ link_to_route('partner.offers.edit', 'Edit', array(mzk_client_bid(), $offer->id), array('class' => 'btn btn-xs btn-info')) }}

            </td>
            <td>
              <?php $rte = route('partner.offers.destroy', [mzk_client_bid(), $offer->id]);?>
              <form method="post" action="{{$rte}}" style="display: inline-block;" onsubmit="return confirm('Are you sure?')">
                            <input type="hidden" value="DELETE" name="_method">
                  {{Form::token()}}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs  btn-danger')) }}
              </form>

  </td>
  				</tr>
  			@endforeach
  		</tbody>
  	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $offers->render() }}
  <div class="pull-right">
    {{ count($offers) }} / {{ $offers->total() }} entries
  </div></div>
</div>

  @else
  	There are no Specials
  @endif
  </div>
  <div class="col-md-4">
  @if ($business->canAddMoreOffers() )

    <p>
      {{ link_to_route( 'partner.offers.create', 
                        'Add New Special', mzk_client_bid(), 
                      array('class' => 'btn btn-lg btn-jumbo btn-block btn-success')) }}
    </p>
    @endif
    <div class="row">
      <div class="col-md-6 text-center">
        <div class="row-stat">
          <h3>{{$active_offers}}</h3>
          <small class="text-muted">Active Specials</small>
        </div>      
      </div>
      <div class="col-md-6 text-center">
        <div class="row-stat">
          <h3>{{$inactive_offers}}</h3>
          <small class="text-muted">Inactive Specials</small>
        </div>      
      </div>
    </div>
  </div>
</div>

@stop
