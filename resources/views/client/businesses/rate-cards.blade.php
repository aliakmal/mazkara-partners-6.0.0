@extends('layouts.client')
@section('content')

<div class="portlet portlet-default"><div class="portlet-header">
    <h3 class="portlet-title">
      Rate Cards for {{$business->name}}
    </h3>
</div>

{{ Form::model($business, array('class' => 'form-horizontal', 
              'method' => 'POST', 'files'=>true, 
              'action' => array('client\\ProfileController@postRateCards'))) }}
    <div class="panel panel-default">
        <div class="panel-heading">Add Rate Card(s) - Bulk</div>
        <div class="panel-body">

            {{ Form::file('rate_card[]', array( 'accept'=>"image/*", 'multiple'=>'true')) }}
                
        </div>
        <div class="panel-footer">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>
{{ Form::close() }}

{{ Form::model($business, array('class' => 'form-horizontal', 
              'method' => 'POST', 'files'=>true,
               'action' => array('client\\ProfileController@postRateCards'))) }}
<div class="panel panel-default">
    <div class="panel-heading">Edit Rate Card(s)</div>
    <div class="panel-body">

<div class="form-group">
  <div class="row">
    <div class="col-sm-10 ">
      @foreach($business->rateCards as $one_card)
        <div class="content col-sm-3">
          <div class="panel panel-default text-center">
          <div class="panel-body">
          <img src="{{ $one_card->image->url('thumbnail') }}" class="img-thumbnail" />
            <span class="input-group">
                  <span class="input-group-addon">SORT</span>

                {{ Form::text("orderables[$one_card->id]", $one_card->sort, array('class'=>'form-control') ) }}
            </span>
        </div>
        <div class="panel-footer">

          <p style="padding-top:5px;">
            {{ Form::checkbox("deletablePhotos[]", $one_card->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
            <a href="javascript:void(0)" class="delete-existing btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
          </p>
        </div>
            </div>

        </div>
        
      @endforeach
    </div>
  </div>
</div>
        </div>
        <div class="panel-footer">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>


{{ Form::close() }}
<div id="rate_card_holder" style="display:none"><p>{{ Form::file('rate_card[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</p></div>
<script>
$(function(){
    $(document).on('click', '.delete-existing', function(){
        $(this).parents('.content').first().hide();
        $(this).parents('.content').first().find('.deletable').prop('checked', true);
    });

    $('#lnk-add-rate-cards').click(function(){
        $('#rate-cards-here').prepend($('#rate_card_holder').html());
    });


});

</script>

@stop
