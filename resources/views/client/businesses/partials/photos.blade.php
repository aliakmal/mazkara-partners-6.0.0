<div class="portlet portlet-default"><div class="portlet-header">
    <h3 class="portlet-title">
      Photos for {{$business->name}}
    </h3>
</div>


{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 
                                'files'=>true, 
                                'action' => array('client\\ProfileController@postPhotos', 
                                $business->id))) }}
    <div class="panel panel-default">
        <div class="panel-heading">Add Image(s) - Bulk</div>
        <div class="panel-body">

            {{ Form::file('images[]', array( 'accept'=>"image/*", 'multiple'=>'true')) }}
                
        </div>
        <div class="panel-footer">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>
{{ Form::close() }}


{{ Form::model($business, array('class' => 'form-horizontal', 
                                'method' => 'POST', 'files'=>true, 
                                'action' => array('client\\ProfileController@postPhotos', 
                                $business->id))) }}

<div class="panel panel-default">
    <div class="panel-heading">Edit Image(s)</div>
    <div class="panel-body">

<div class="form-group">
    <div class="row">
        <div class="col-sm-10 ">
        @foreach($business->photos as $one_photo)
        <div class="content col-sm-3">
            <div class="panel panel-default text-center">
                <div class="panel-body">
            <a href="{{ $one_photo->image->url('medium') }}" class="lightbox"><img src="{{ $one_photo->image->url('thumbnail') }}" class="img-thumbnail" /></a>
            <span class="input-group">
                  <span class="input-group-addon">SORT</span>

                {{ Form::text("orderables[$one_photo->id]", $one_photo->sort, array('class'=>'form-control') ) }}
            </span>
            <span class="input-group">
                  <span class="input-group-addon">COVER?</span>
                  <span class="form-control">
                    {{ Form::radio("is_cover", $one_photo->id, $one_photo->isCover()) }}
                </span>
            </span>
        </div>
            <div class="panel-footer">

                {{ Form::checkbox("deletablePhotos[]", $one_photo->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
                <a href="javascript:void(0)" class="delete-existing btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                


            </div>
            </div>

        </div>
        @endforeach
        </div>
    </div>

</div>


        </div>
        <div class="panel-footer">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>
{{ Form::close() }}
<div id="image_holder" style="display:none"><p>{{ Form::file('images[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</p></div>
<script>

$(function(){
    $(document).on('click', '.delete-existing', function(){
        $(this).parents('.content').first().hide();
        $(this).parents('.content').first().find('.deletable').prop('checked', true);
    });

    $('#lnk-add-images').click(function(){
        $('#images-here').prepend($('#image_holder').html());
    });


})

</script>