@extends('layouts.client')
@section('content')


<!-- /.row -->
<div class="row">
  <div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <div class="portlet-header">
        <h3>Page views for {{$business->name}}</h3>
      </div>
    </div>
  </div>
          <form id="views-graph-date-range-picker">

  <div class="row">
    <div class="col-md-12">
    <div class="portlet portlet-boxed">
      <div class="portlet-header">
        <div class=" portlet-title">FILTER VIEWS BETWEEN</div>
      </div>
      <div class="portlet-body">
          <div class="input-daterange " id="datepicker">
            <div class=" row">
              <div class="col-md-5"><input type="text" placeholder="Select Start Date" value="{{date('Y-m-d', strtotime($start_date))}}" class="  form-control " id="start-date" name="start" /></div>
              <div class="col-md-5"><input type="text" placeholder="Select End Date" value="{{date('Y-m-d', strtotime($end_date))}}" class=" form-control" id="end-date" name="end" /></div>
              <div class="col-md-2"><a href="javascript:void(0)" id="apply-to-graph" class="btn-block apply-to-graph btn btn btn-primary">Apply</a></div>
            </div>
          </div>
      </div>
    </div>

    </div>
  </div>
    <div class="row">
      <div class="col-md-12 text-right">
        <div class="btn-group" data-toggle="buttons">
          <label class="apply-to-graph btn btn-default active">
            <input type="radio" name="interval" id="interval_daily" value="daily" autocomplete="off" checked> Daily
          </label>
          <label class="apply-to-graph btn btn-default">
            <input type="radio" name="interval" id="interval_weekly" value="weekly" autocomplete="off"> Weekly
          </label>
          <label class="apply-to-graph btn btn-default">
            <input type="radio" name="interval" id="interval_monthly" value="monthly" autocomplete="off"> Monthly
          </label>
        </div>
      </div>
    </div>
    </form>

    <div class="row">
      <div class="col-md-12">
        <div id="reports-line-chart" class="chart-holder-300"></div>
      </div>
    </div>
    <hr/>
    <div class="row">
      <div class="col-md-3 text-center"><div class="row-stat"><h2  id="count-page-views" >{{$count_page_views}}</h2> <small class="text-muted">Total Page Views</small></div></div>
      <div class="col-md-3 text-center"><div class="row-stat"><h2  id="count-avg-page-views" >{{$count_avg_page_views}}</h2> <small class="text-muted">Avg. Daily Page Views</small></div></div>
      <div class="col-md-3 text-center"><div class="row-stat"><h2>{{$count_favorites}}</h2> <small class="text-muted">Total Favorites</small></div></div>
      <div class="col-md-3 text-center">
        <div class="row-stat"><h2  id="count-call-views" >{{$count_call_views}}</h2> <small class="text-muted">Total Click to Calls</small>
        </div>
      </div>

    </div>
</div>
</div><p><br/><br/></p>
<div class="row hidden">
  <div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <div class="portlet-header">
        <h3>Call to Book Impressions {{$business->name}}</h3>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-md-9">
    <div class="portlet portlet-boxed">
      <div class="portlet-header">
        <b>CALL TO BOOK CLICKS BETWEEN</b>
      </div>
      <div class="portlet-body">
        <form id="views-graph-date-range-picker">
          <div class="input-daterange  " id="datepicker">
            <div class="row">
              <div class="col-md-5">
              <input type="text" placeholder="Select Start Date" value="{{date('Y-m-d', strtotime($start_date))}}" class="input-sm form-control" id="call-start-date" name="call-start" />
            </div>
            <div class="col-md-5">
              <input type="text" placeholder="Select End Date" value="{{date('Y-m-d', strtotime($end_date))}}" class="input-sm form-control" id="call-end-date" name="call-end" />
            </div>
            <div class="col-md-2">
            <a href="javascript:void(0)" id="apply-to-calls" class="btn-block btn btn-sm btn btn-primary">Apply</a>
</div>
            </div>
          </div>           
        </form>
      </div>
    </div>

      </div>
    </div>
</div>
</div>

<script type="text/javascript">

$(function() {

  var monthNames = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];

  var chart = new Morris.Area({
    element: 'reports-line-chart',
    xkey: 'dates',
    ykeys: ['views', 'calls'],
    labels: ['Accumulated Views', 'Accumulated Click on Calls'],
    pointSize:3,
    hideHover:true,
    lineWidth:2,
    fillOpacity:0.2,
    gridTextSize: 10,
    lineColors:['#C42B2B', '#FF8800','#0B62A4'],
    xLabelFormat: function (x) { 
      var date = new Date(x);
      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();      

      ivl = $('input[name=interval]:checked').val();
      if(ivl=='monthly'){
        return monthNames[monthIndex] + ' ' + year; 
      }else{
        return day + ' ' + monthNames[monthIndex] + ' ' + year; 
      }
    },
    dateFormat:function (x) { 
      var date = new Date(x);
      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();      

      ivl = $('input[name=interval]:checked').val();
      if(ivl=='monthly'){
        return monthNames[monthIndex] + ' ' + year; 
      }else{
        return day + ' ' + monthNames[monthIndex] + ' ' + year; 
      }
    }
  });

  function callbackViewsAPI(){
    var d1, data, chartOptions;

    $.ajax({
      url: '/partner/{{ mzk_client_bid() }}/api/views',
      minLength: 0,
      data: {
        start: $('#start-date').val(),
        end: $('#end-date').val(),
        business_id: {{ $business->id }},
        interval:$('input[name=interval]:checked').val()
      },
    }).success(function(r){
      chart.setData(r['data']);
      ivl = $('input[name=interval]:checked').val();

      if(ivl=='monthly'){
        chart.options.xLabels = 'month';
        chart.redraw();
      }else{
        chart.options.xLabels = 'default';
        chart.redraw();
      }


      $('#count-page-views').html(r['count_page_views']);
      $('#count-avg-page-views').html(r['count_avg_page_views']);
      $('#count-call-views').html(r['count_call_views']);
    });
  }

  $('#views-graph-date-range-picker .input-daterange').datepicker({
    format: "yyyy-mm-dd", autoclose:true
  });    

  $('#apply-to-graph').click(function(){
    callbackViewsAPI();
  });

  $('input[name=interval]').change(function(){
    callbackViewsAPI();
  });

  function callbackCallsApi(){
    $.ajax({
      url: '/partner/{{ mzk_client_bid() }}/api/calls',
      minLength: 0,
      data: {
        start: $('#call-start-date').val(),
        end: $('#call-end-date').val(),
        business_id: {{ $business->id }}
      },
    }).success(function(r){
      $('#count-call-views').html(r['count_call_views']);
    });
  }

  $('#apply-to-calls').click(function(){
    callbackCallsApi();
  });

  callbackViewsAPI();
  callbackCallsApi();

  function callbackCallLogsAPI(){
    var d1, data, chartOptions;

    $.ajax({
      url: '/partner/{{ mzk_client_bid() }}/api/call_logs',
      minLength: 0,
      data: {
        start: $('#calls-start-date').val(),
        end: $('#calls-end-date').val(),
        business_id: {{ $business->id }}
      },
    }).success(function(r){
      data = [{ 
        label: "Calls Made", data: r.data
      }];

      ivl = $('input[name=interval]:checked').val();
      tickView = 'day';
      if(ivl=='monthly'){
        tickView = 'month';
      }

      chartOptions = {
        xaxis: {
          min: r.start, max: r.end, mode: "time", tickSize: [1, tickView]
        },
        series: {
          lines: {
            show: true, fill: false, lineWidth: 3
          },
          points: {
            show: true, radius: 3, fill: true, fillColor: "#ffffff", lineWidth: 2
          }
        },
        grid: { 
          hoverable: true, clickable: false, borderWidth: 0 
        },
        legend: {
          show: true
        },
        tooltip: true,
        tooltipOpts: {
          content: '%s: %y'
        },
        colors: mvpready_core.layoutColors
      };    

      var holder = $('#calls-line-chart');

      if (holder.length) {
        $.plot(holder, data, chartOptions );
      }

    });
  }

  $('#views-calls-date-range-picker .input-daterange').datepicker({format: "yyyy-mm-dd", autoclose:true});    

  $('#apply-to-calls-graph').click(function(){
    callbackCallLogsAPI();
  });
});

</script>
@stop

