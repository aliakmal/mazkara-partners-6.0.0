{{ Form::open(array('url' => route('partner.call_logs.resolves.store', mzk_client_bid()), 
                    'class' => 'form-horizontal')) }}
  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
    {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
    {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif


  <div class="form-group">
    <div class="col-sm-12">
      {{ Form::select('action', [ 0=>'This is NOT a lead', 1 => 'This IS a lead'], Input::get('action'), array('class'=>'form-control ')) }}
    </div>
  </div>
  <div class="form-group">
      <div class="col-sm-12">
        {{ Form::textarea('description', Input::old('description'), 
                      array('class'=>'dpb form-control', 'placeholder'=>'Comment for Resolution?')) }}
      </div>
  </div>
  {{ Form::hidden('call_log_id', $id)}}
  <div class="form-group">
      <div class="col-sm-12">
        {{ Form::submit('Create', array('class' => 'btn text-center btn-lg btn-primary dpb')) }}
      </div>
  </div>

{{ Form::close() }}



