{{ Form::open(array('url' => route('partner.call_logs.disputes.store', mzk_client_bid()), 
                    'class' => 'form-horizontal')) }}
  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
    {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
    {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif

  <div class="form-group">
      <div class="col-sm-12">
        {{ Form::textarea('description', Input::old('description'), 
                      array('class'=>'dpb form-control', 'placeholder'=>'Why do you think this call should not be billed?')) }}
      </div>
  </div>
  {{ Form::hidden('call_log_id', $id)}}
  <div class="form-group">
      <div class="col-sm-12">
        {{ Form::submit('Create', array('class' => 'btn text-center btn-lg btn-primary dpb')) }}
      </div>
  </div>

{{ Form::close() }}



