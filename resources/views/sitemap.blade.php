@extends('layouts.master-open')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
<h1 class=" notransform pt20 pb20">Sitemap for {{ ucwords($city) }}</h1>
<div class="row">
  @foreach($totals as $letter=>$counts)
    <div class="col-md-4">
      <h4><b>{{ $letter }}</b></h4>
      <div>
        @if($counts['venues']>0)
          <p class="mb0">
            Venues: 
            <?php $pages = ceil($counts['venues']/80);?>
            @for($i=1;$i<=$pages;$i++)
              <a href="{{ route('sitemap.venues.for.'.strtolower($city), ['letter'=>$letter, 'page'=>$i]) }}" class="medium-gray dpib ml5">{{ $i }}</a> 
            @endfor
          </p>
        @endif
        @if($counts['locations']>0)
          <p>
            Locations:
            <?php $pages = ceil($counts['locations']/80);?>
            @for($i=1;$i<=$pages;$i++)
              <a href="{{ route('sitemap.locations.for.'.strtolower($city), ['letter'=>$letter, 'page'=>$i]) }}" class="medium-gray dpib ml5">{{ $i }}</a> 
            @endfor
          </p>
        @endif
      </div>
    </div>
  @endforeach
</div>
    </div>
</div>
<p>&nbsp;</p>
</div>
@stop
