@extends('layouts.master')
@section('content')
<div class="container">    
  <div id="" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">                    
    <h1 class="text-center notransform">Reset Password?</h1>
    <hr/>
    <p class="lead text-center">Awesome, set a brand new password for your account! Something easy to remember but hard to guess!</p>
@include('elements.messages')

    <form method="POST" action="{{{ URL::to('/users/reset_password') }}}" accept-charset="UTF-8">
    <input type="hidden" name="token" value="{{{ $token }}}">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

    <div class="form-group">
        <input class="form-control" placeholder="EMAIL ADDRESS" type="text" name="email" id="email" value="{{ old('email') }}">
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="PASSWORD" type="password" name="password" id="password">
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="CONFIRM PASSWORD" type="password" name="password_confirmation" id="password_confirmation">
    </div>


    <div class="form-actions form-group">
        <button type="submit" class="btn form-control btn-primary">RESET YOUR PASSWORD</button>
    </div>
</form>
</div></div>
<br/><br/><br/><br/><br/>
@endsection