@extends('layouts.scaffold')

@section('main')

<h1>Show Favorite</h1>

<p>{{ link_to_route('favorites.index', 'Return to All favorites', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>User_id</th>
				<th>Favorable_id</th>
				<th>Favorable_type</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $favorite->user_id }}}</td>
					<td>{{{ $favorite->favorable_id }}}</td>
					<td>{{{ $favorite->favorable_type }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('favorites.destroy', $favorite->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('favorites.edit', 'Edit', array($favorite->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
