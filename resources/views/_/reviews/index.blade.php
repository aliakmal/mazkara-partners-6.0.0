@extends('layouts.scaffold')

@section('main')

<h1>All Reviews</h1>

<p>{{ link_to_route('reviews.create', 'Add New Review', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($reviews->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Rating</th>
				<th>Body</th>
				<th>User_id</th>
				<th>Business_id</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($reviews as $review)
				<tr>
					<td>{{{ $review->rating }}}</td>
					<td>{{{ $review->body }}}</td>
					<td>{{{ $review->user_id }}}</td>
					<td>{{{ $review->business_id }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('reviews.destroy', $review->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('reviews.edit', 'Edit', array($review->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no reviews
@endif

@stop
