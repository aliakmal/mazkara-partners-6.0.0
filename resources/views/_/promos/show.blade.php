@extends('layouts.scaffold')

@section('main')

<h1>Show Promo</h1>

<p>{{ link_to_route('promos.index', 'Return to All promos', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Caption</th>
				<th>Description</th>
				<th>Fine_print</th>
				<th>Offer_amount</th>
				<th>Starts</th>
				<th>Ends</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $promo->title }}}</td>
					<td>{{{ $promo->caption }}}</td>
					<td>{{{ $promo->description }}}</td>
					<td>{{{ $promo->fine_print }}}</td>
					<td>{{{ $promo->offer_amount }}}</td>
					<td>{{{ $promo->starts }}}</td>
					<td>{{{ $promo->ends }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('promos.destroy', $promo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('promos.edit', 'Edit', array($promo->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
