@extends('layouts.scaffold')

@section('main')

<h1>All Packages</h1>

<p>{{ link_to_route('packages.create', 'Add New Package', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($packages->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Caption</th>
				<th>Description</th>
				<th>Fine_print</th>
				<th>Offer_amount</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($packages as $package)
				<tr>
					<td>{{{ $package->title }}}</td>
					<td>{{{ $package->caption }}}</td>
					<td>{{{ $package->description }}}</td>
					<td>{{{ $package->fine_print }}}</td>
					<td>{{{ $package->offer_amount }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('packages.destroy', $package->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('packages.edit', 'Edit', array($package->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no packages
@endif

@stop
