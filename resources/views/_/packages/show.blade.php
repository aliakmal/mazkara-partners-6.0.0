@extends('layouts.scaffold')

@section('main')

<h1>Show Package</h1>

<p>{{ link_to_route('packages.index', 'Return to All packages', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Caption</th>
				<th>Description</th>
				<th>Fine_print</th>
				<th>Offer_amount</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $package->title }}}</td>
					<td>{{{ $package->caption }}}</td>
					<td>{{{ $package->description }}}</td>
					<td>{{{ $package->fine_print }}}</td>
					<td>{{{ $package->offer_amount }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('packages.destroy', $package->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('packages.edit', 'Edit', array($package->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
