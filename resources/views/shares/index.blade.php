@extends('layouts.scaffold')

@section('main')

<h1>All Shares</h1>

<p>{{ link_to_route('shares.create', 'Add New Share', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($shares->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Type</th>
				<th>User_id</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($shares as $share)
				<tr>
					<td>{{{ $share->type }}}</td>
					<td>{{{ $share->user_id }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('shares.destroy', $share->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('shares.edit', 'Edit', array($share->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no shares
@endif

@stop
