@extends('layouts.scaffold')

@section('main')

<h1>All Offers</h1>

<p>{{ link_to_route('offers.create', 'Add New Offer', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($offers->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Body</th>
				<th>Tag</th>
				<th>State</th>
				<th>Price</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($offers as $offer)
				<tr>
					<td>{{{ $offer->body }}}</td>
					<td>{{{ $offer->tag }}}</td>
					<td>{{{ $offer->state }}}</td>
					<td>{{{ $offer->price }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('offers.destroy', $offer->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('offers.edit', 'Edit', array($offer->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no offers
@endif

@stop
