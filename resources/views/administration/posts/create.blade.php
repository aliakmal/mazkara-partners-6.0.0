@extends('layouts.admin-editor')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Post</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

@include('administration.posts.form')

@stop
