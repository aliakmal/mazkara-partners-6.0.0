@extends('layouts.admin-editor')
@section('content')

<h1>Archived Review Posts</h1>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:250px;', 'placeholder'=>'Search')) }}
    {{ Form::select('author_id', ([''=>'Author?'] + User::onlyAuthors()->orderby('name', 'asc')->lists('name', 'id')->all()), Input::get('author_id'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}

</div>
@if ($posts->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Author</th>
				<th>State</th>
        <th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($posts as $post)
				<tr>
					<td>
            @if($post->type == Post::ARTICLE)
              <i class="fa fa-file-text-o" title="Article"></i>
            @elseif($post->type == Post::VIDEO)
              <i class="fa fa-video-camera" title="Video"></i>
            @elseif($post->type == Post::SELFIE)
              <i class="fa fa-photo" title="Selfie"></i>
            @endif

            {{{ $post->title }}}</td>
					<td>{{{ mzk_str_trim($post->caption, 50) }}}</td>
					<td>{{{ $post->author->name }}}</td>
					<td>{{ mzk_status_tag($post->state) }}</td>
          <td>
            <a href="{{ $post->url() }}" class="btn btn-xs btn-success" title="Preview"><i class="fa fa-eye"></i></a>
          </td>
          <td>
          	{{ link_to_route('admin.posts.unarchive', 'Unarchive', array($post->id), array('class' => 'btn btn-xs btn-warning')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>


  <div class="row">
    <div class="col-md-12">
  {{ $posts->appends($params)->render() }}
  <div class="pull-right">
    {{ count($posts) }} / {{ $posts->total() }} entries
  </div></div>
</div>
  
@else
	There are no posts
@endif

@stop
