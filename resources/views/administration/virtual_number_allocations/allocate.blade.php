@extends('layouts.admin-crm')
@section('content')

<div class="row">
  <div class="col-md-10 col-md-offset-2">
    <h1>Allocate a virtual number to {{ $business->name }}</h1>

    @if ($errors->any())
    	<div class="alert alert-danger">
  	    <ul>
          {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    	</div>
    @endif
  </div>
</div>


{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.virtual_number_allocations.store'))
                                ->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif

  {{ BootForm::select('Virtual number', 'virtual_number_id', $virtual_numbers->byLocale()->onlyActive()->lists('body','id')->all()) }}
  {{ BootForm::text('Primary Phone number',   'phone_1')->placeholder('Primary phone number to redirect to') }}
  {{ BootForm::text('Secondary Phone number', 'phone_2')->placeholder('Secondary phone number to redirect to') }}
  {{ BootForm::text('Tertiary Phone number',  'phone_3')->placeholder('Tertiary phone number to redirect to') }}
  {{ BootForm::hidden('business_id', $business->id)->value($business->id) }}
  {{ BootForm::textarea('Notes', 'desc')->placeholder('Notes') }}
  {{ BootForm::text('Valid Until',  'valid_until')->placeholder('Date Valid till')->addClass('dateinput') }}
  {{ BootForm::select('Is IVR enabled', 'is_ivr_enabled', ['0'=>'No', '1'=>'Yes']) }}


  {{ BootForm::radio('Yes! Activate PPL', 'is_ppl', 1) }}
  {{ BootForm::radio('NO PPL', 'is_ppl', 0) }}

  {{ BootForm::hidden('state', 'active')->value('active') }}
  {{ BootForm::token() }}
  {{ BootForm::submit('Allocate Number') }}

{{ BootForm::close() }}

<script type="text/javascript">
$(function(){
  $('.dateinput input').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yy-mm-dd", autoclose:true });
})
</script>
@stop

