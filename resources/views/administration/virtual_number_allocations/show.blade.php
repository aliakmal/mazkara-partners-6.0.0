@extends('layouts.admin-crm')
@section('content')

<h1>Show Virtual_number_allocation</h1>

<p>{{ link_to_route('admin.virtual_number_allocations.index', 'Return to All virtual_number_allocations', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Desc</th>
				<th>Business_id</th>
				<th>Virtual_number_id</th>
				<th>Start</th>
				<th>End</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $virtual_number_allocation->desc }}}</td>
			<td>{{{ $virtual_number_allocation->business_id }}}</td>
			<td>{{{ $virtual_number_allocation->virtual_number_id }}}</td>
			<td>{{{ $virtual_number_allocation->start }}}</td>
			<td>{{{ $virtual_number_allocation->end }}}</td>
			<td>{{{ $virtual_number_allocation->state }}}</td>
      <td>
          {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.virtual_number_allocations.destroy', $virtual_number_allocation->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
          {{ Form::close() }}
          {{ link_to_route('admin.virtual_number_allocations.edit', 'Edit', array($virtual_number_allocation->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
