@extends('layouts.admin-users')
@section('content')

<div class="row">
    <div class="col-md-10 ">
        <h1>Edit Account</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
        {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->put()->action(route('admin.accounts.update', $account->id))->encodingType('multipart/form-data') }}
            {{ BootForm::bind($account) }}

         @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
              {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
            </div>
          @endif
          @if (Session::get('notice'))
            <div class="alert alert-success">
              {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
            </div>
          @endif

          <fieldset><legend>Basic Details</legend>

            {{ BootForm::text('Full name', 'name')->placeholder('Full Name') }}
            {{ BootForm::textarea('About', 'about')->placeholder('A bit about you') }}
            {{ BootForm::select('Gender', 'gender', array('male'=>'Male', 'female'=>'Female', ''=>'Rather not say')) }}
            {{ BootForm::text('Email', 'email')->placeholder('Email address') }}
            {{ BootForm::password('Password', 'password')->placeholder('Password') }}
            {{ BootForm::password('Confirm Password', 'password_confirmation')->placeholder('password confirmation') }}
          </fieldset>

<?php
  $u = Auth::user();
  $u = User::find($u->id);
  if($u->canAccessRoles()):
?>

          <fieldset><legend>Roles</legend>
            @foreach(Role::all() as $role)
              @if(!in_array($role->id, User::$non_selectable_roles))
                @if($account->roles()->where('roles.id', '=', $role->id)->count() > 0)
                  {{ BootForm::checkbox($role->name, 'roles[]')->value($role->id)->check() }}
                @else
                  {{ BootForm::checkbox($role->name, 'roles[]')->value($role->id) }}
                @endif
              @endif
            @endforeach
          </fieldset>
          <fieldset><legend>Zones</legend>
            @foreach(MazkaraHelper::getCitiesList() as $city)

              @if($account->zones()->where('zones.id', '=', $city['id'])->count() > 0)
                {{ BootForm::checkbox($city['name'], 'zones[]')->value($city['id'])->check() }}
              @else
                {{ BootForm::checkbox($city['name'], 'zones[]')->value($city['id']) }}
              @endif
              
            @endforeach
          </fieldset>
<?php endif;?>

          {{ BootForm::token() }}
          {{ BootForm::submit('Save Details') }}
          {{ BootForm::close() }}


    </div>
</div>
@stop