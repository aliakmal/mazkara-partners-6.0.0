@extends('layouts.admin-users')
@section('content')

<div class="row">
  <div class="col-md-12">
<h1>{{ $account->full_name }}</h1>
<p>{{ link_to_route('admin.accounts.index', 'All accounts', null, array('class'=>'btn btn-sm pull-right btn-primary')) }}</p>
<dl class="dl-horizontal">
  <dt>Username</dt><dd>{{ $account->username }}</dd>
  <dt>Email</dt><dd>{{ $account->email }}</dd>
  <dt>Account(s)</dt>
  <dd>
    @if(trim($account->username)!='')
      <span class="label label-default" title="Signed up with email and password"><i class="fa fa-lock"></i></span>&nbsp;
    @endif
    @foreach($account->accounts as $acc)
      @if($acc->provider=='facebook')
        <span class="label label-primary"  title="Signed up with facebook"><i class="fa fa-facebook"></i></span>&nbsp;
      @endif
      @if($acc->provider=='google')
        <span class="label label-danger"  title="Signed up with google"><i class="fa fa-google"></i></span>&nbsp;
      @endif
    @endforeach
  </dd>
  <dt>Roles</dt><dd>{{ join(',', $account->roles()->lists('roles.name','roles.name')->all()) }}</dd>
  <?php $fields = [ 'gender','twitter','instagram','favorites_count','ratings_count',
                    'reviews_count','check_ins_count', 'followers_count', 'follows_count', 
                    'phone', 'designation', 'designated_at_text', 'designated_at_url', 
                    'contact_email_address', 'location'];?>
  @foreach($fields as $field)
    <dt>{{ ucwords(str_replace('_', ' ', $field)) }}</dt>
    <dd>{{ $account->$field }}</dd>
  @endforeach
</dl>
<!-- {{ Form::open(array('style' => 'display: inline-block;margin-right:10px;',   'method' => 'DELETE', 'route' => array('admin.accounts.destroy', $account->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-sm btn-danger')) }}
{{ Form::close() }} -->
{{ link_to_route('admin.accounts.edit', 'Edit', array($account->id), array('class' => 'btn btn-sm btn-info')) }}
&nbsp;<a href="{{ route('users.profile.show', array($account->id)) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>

  </div>
</div>
@stop