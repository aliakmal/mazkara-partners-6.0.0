@extends('layouts.admin-crm')
@section('content')

<h1>Show Business_zone</h1>

<p>{{ link_to_route('admin.business_zones.index', 'Return to All business_zones', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<h2>{{{ $business_zone->name }}}</h2>
<p>
  @foreach ($business_zone->zones()->get() as $zone)
    <span class="label label-info">
      {{ $zone->name }}
    </span> 
  @endforeach
</p>
{{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.business_zones.destroy', $business_zone->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
{{ Form::close() }}
{{ link_to_route('admin.business_zones.edit', 'Edit', array($business_zone->id), array('class' => 'btn btn-info')) }}
@stop