@extends('layouts.admin-crm')
@section('content')

<div class="row">
  <div class="col-md-12">
  <div class="pull-right">
    <p></p>
    <p>{{ link_to_route('admin.business_zones.create', 'Add New Business zone', null, array('class' => 'btn btn-sm btn-success')) }}</p>

  </div>
  <h1>All Business zones</h1>
</p>



@if ($business_zones->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Name</th>
        <th>Zones</th>
				<th>&nbsp;</th>
        <th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($business_zones as $business_zone)
				<tr>
          <td>{{{ $business_zone->id }}}</td>
					<td>{{{ $business_zone->name }}}</td>
          <td><small>{{{ join(', ',$business_zone->zones()->lists('name')->all()) }}}</small></td>
          <td>
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?')", 'method' => 'DELETE', 'route' => array('admin.business_zones.destroy', $business_zone->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
            </td>
        <td>
              {{ link_to_route('admin.business_zones.edit', 'Edit', array($business_zone->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $business_zones->render() }}
  <div class="pull-right">
    {{ count($business_zones) }} / {{ $business_zones->total() }} entries
  </div></div>
</div>

@else
	There are no business_zones
@endif
</div></div>
@stop