@extends('layouts.admin-crm')
@section('content')

<h1>Facebook Page Listings</h1>

<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text(  'byID', Input::get('byID'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;',
                    'placeholder'=>'By Business ID')) }}

    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}
    
    {{ Form::select('facebook', ([ 
                              'all'=>'All Listings', 
                              'facebook'=>'Active Facebook', 
                              'nonfacebook'=>'Inactive Facebook', 
                              ]),
                           Input::get('facebook'), array('class'=>'form-control', 'style'=>'width:250px;', 'placeholder'=>'Active/Inactive')) }}
    {{ Form::select('sort', ([ ''=>'Sort by?', 
                              'idAsc'=>'ID Ascending', 
                              'idDesc'=>'ID Descending', 
                              'nameAsc'=>'Name Ascending', 
                              'nameDesc'=>'Name Descending', 
                              'lastUpdateAsc'=>'Last Update Ascending',
                              'lastUpdateDesc'=>'Last Update Descending',
                              ]),
                           Input::get('sort'), array('class'=>'form-control', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>


@if ($businesses->count())


  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Facebook Page ID</th>
        <th>Active?</th>
        <th></th>
      </tr>
    </thead>

    <tbody>
      @foreach ($businesses as $business)
        <tr>
          <td>{{{ $business->id }}}</td>
          <td>{{{ $business->name }}}</td>
          <td>{{{ $business->facebook_like_box_id }}}</td>
          <td> 
            @if($business->hasFacebookPageActive())
              <span class="label label-primary"><i class="fa fa-facebook"></i></span>
            @endif
          </td>
          <td>
            <a href="{{ route('admin.businesses.facebook.edit', [$business->id])}}" class="btn btn-xs btn-warning">Edit</a>&nbsp;
            <a href="{{ MazkaraHelper::slugSingle($business) }}"  class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
          
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>


  
  <div class="row">
    <div class="col-md-12">
  {{ $businesses->appends($params)->render() }}
  <div class="pull-right">
    {{ count($businesses) }} / {{ $businesses->total() }} entries
  </div></div>
</div>
@else
  There are no businesses
@endif

@stop

