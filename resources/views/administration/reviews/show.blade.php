@extends('layouts.admin-users')
@section('content')


<h1>Show Review</h1>

<p>{{ link_to_route('admin.reviews.index', 'Return to All reviews', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Rating</th>
				<th>Body</th>
				<th>User_id</th>
				<th>Business_id</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $review->rating }}}</td>
					<td>{{{ $review->body }}}</td>
					<td>{{{ $review->user_id }}}</td>
					<td>{{{ $review->business_id }}}</td>
          <td>
            @if(Auth::user()->can("manage_highlights"))

              {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.reviews.destroy', $review->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.reviews.edit', 'Edit', array($review->id), array('class' => 'btn btn-info')) }}
            @endif
          </td>
		</tr>
	</tbody>
</table>

@stop
