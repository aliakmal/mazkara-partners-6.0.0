@extends('layouts.admin-users')
@section('content')
<?php
    $selected_services = $review->services()->lists('service_id', 'service_id')->all()
?>

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Review</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($review, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.reviews.update', $review->id))) }}

        <div class="form-group">
            {{ Form::label('rating', 'Rating:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::input('number', 'rating', Input::old('rating'), array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('services', 'Services:', array('class'=>'col-md-2 control-label ' )) }}
          <div class="col-sm-10 ">

          <select name="services[]" class="form-control" style="height:350px;" multiple="multiple" title="Tag up to 3 services"   data-max-option="3" data-live-search="true" id="service_ids" class="form-control">
            
            @foreach ($services as $parent=>$children)
              <optgroup label="{{$parent}}">
                @foreach ($children as $service)
                  
                    <option value="{{$service['id']}}" {{ in_array($service['id'], $selected_services)?'selected':''}}  >{{$service['name']}}</option>
                  
                @endforeach
              </optgroup>
            @endforeach
          </select>
        </div>
        </div>

        <div class="form-group">
            {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'Body')) }}
            </div>
        </div>



<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}&nbsp;
      {{ link_to_route('reviews.show', 'Cancel', $review->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop
