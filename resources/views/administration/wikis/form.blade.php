@if(isset($wiki))
  {{ Form::model($wiki, array('class' => 'form-horizontal', 'id'=>'wiki-form', 'onsubmit'=>"return validateForm();", 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.wikis.update', $wiki->id))) }}
@else
  {{ Form::open(array('route' => 'admin.wikis.store',  'id'=>'wiki-form', 'onsubmit'=>"return validateForm();", 'files'=>true, 'class' => 'form-horizontal')) }}
@endif

  <div class="form-group">
    {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('title', isset($wiki)?$wiki->title:Input::old('title'), array('title'=>'Title', 'class'=>'input-lg required form-control', 'placeholder'=>'Title')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('cover_image', 'Cover Image:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Select file...</span>
        <!-- The file input field used as target for the file upload widget -->
        {{ Form::file('cover_image', '', array('class'=>'input-lg  form-control', 'id'=>'cover_image')) }}
    </span>
    <span id="files">
@if(isset($wiki))
@if($wiki->cover)

<img width="100" src="{{ $wiki->cover->image_thumbnail_url}}" />
@endif
@endif
    </span>
    </div>
  </div>
    {{ Form::hidden('cover_image_id', isset($wiki)?$wiki->cover_image_id:Input::old('cover_image_id'), array('title'=>'Cover Image','id'=>'cover_image_id', 'class'=>'input-lg required form-control')) }}
  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('description', isset($wiki)?$wiki->description:Input::old('description'), array('class'=>'form-control description required', 'title'=>'Description', 'placeholder'=>'Description')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('service_details', 'Service details:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('service_details', isset($wiki)?$wiki->service_details:Input::old('service_details'), array('class'=>'form-control required', 'id'=>'service_details', 'title'=>'Service Details', 'placeholder'=>'Service details')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('service_id', 'Service:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('service_id', $services, isset($wiki)?$wiki->service_id:Input::old('service_id'), array('class'=>'form-control required', 'title'=>'Service', 'placeholder'=>'Service?')) }}
    </div>
  </div>
  {{ Form::hidden('active', 1, array('title'=>'Active State','class'=>'form-control ')) }}
  <div class="well">
    <a href="javascript:void(0)" id="lnk-add-section" class="btn btn-default">Add Section</a>
  </div>
  <div id="section-holder" class="well">
    @if(isset($wiki))
      @foreach($wiki->sections as $id=>$section)
        <?php $suffix = 'pre-'.$id.'-'.time();?>
        <div class="holder" id="{{ $suffix }}">
          <div class="form-group">
             {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('display_json[data_array]['.$suffix.'][title]', $section->title, array('class'=>'input-lg required form-control', 'title'=>'Section Title', 'placeholder'=>'Title')) }}
            </div>
          </div>
          <div class="form-group">
            {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('display_json[data_array]['.$suffix.'][description]', $section->description, array('class'=>'form-control description required', 'title'=>'Section Description', 'placeholder'=>'Description')) }}
            </div>
          </div>
          <div class="form-group">
            {{ Form::label('image_alt', 'Image alt:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('display_json[data_array]['.$suffix.'][image_alt]', $section->image_alt, array('class'=>'input-lg  form-control', 'title'=>'Section Img Alt', 'placeholder'=>'Image Alt')) }}
            </div>
          </div>
            <div>
              <span class="btn btn-success span-imageable fileinput-button">
                  <i class="glyphicon glyphicon-plus"></i>
                  <span>Select file...</span>
                  <!-- The file input field used as target for the file upload widget -->
                {{ Form::file('image', '', array('class'=>'input-lg imageable form-control ')) }}
              </span>
              <div>
                <img width="100" src="{{ $section->image_url}}" />
                <a href="javascript:void(0)" class="btn btn-sm btn-danger deletable">Delete</a>

              </div>
            </div>

          <div class="form-group">
            <div class="col-sm-10">
              {{ Form::hidden('display_json[data_array]['.$suffix.'][image_url]', $section->image_url, array('class'=>'input-lg image_url  form-control','title'=>'Section Image', 'placeholder'=>'Image URL')) }}
            </div>
          </div>
          <div class="form-group">
            <a href="javascript:void(0)" style="display:block;width:100%;" class="btn btn-danger lnk-remove-section" id="lnk-{{ $suffix }}">DELETE</a>
          </div>
        </div>
      @endforeach
    @endif
  </div>
  <div class="form-group">
     {{ Form::label('seo_keywords', 'seo keywords:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('display_json[seo_keywords]', isset($wiki)?$wiki->seo_keywords:Input::old('seo_keywords'), array('class'=>'input-lg required form-control', 'title'=>'SEO Keyords', 'placeholder'=>'SEO keywords')) }}
    </div>
  </div>
<div class="form-group">
     {{ Form::label('meta_keywords', 'meta keywords:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('display_json[meta_keywords]', isset($wiki)?$wiki->meta_keywords:Input::old('meta_keywords'), array('class'=>'input-lg required form-control','title'=>'META Keywords', 'placeholder'=>'Meta keywords')) }}
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($wiki))
        {{ link_to_route('admin.wikis.show', 'Cancel', $wiki->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>
  </div>
{{ Form::close() }}
<div class="hidden" id="addable-section">

<?php $m = '___REPLACE___';?>
<div class="holder" id="{{ $m }}">
  <div class="form-group">
     {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('display_json[data_array]['.$m.'][title]', '', array('class'=>'input-lg required form-control','title'=>'Section Title', 'placeholder'=>'Title')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('display_json[data_array]['.$m.'][description]', '', array('class'=>'form-control description required', 'title'=>'Section Description', 'placeholder'=>'Description')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('image_alt', 'Image alt:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('display_json[data_array]['.$m.'][image_alt]', '', array('class'=>'input-lg  form-control', 'title'=>'Section Image Alt', 'id'=>'text-'.$m, 'placeholder'=>'Image Alt')) }}
    </div>
  </div>
  <div>
  <span class="btn btn-success span-imageable fileinput-button">
      <i class="glyphicon glyphicon-plus"></i>
      <span>Select file...</span>
      <!-- The file input field used as target for the file upload widget -->
    {{ Form::file('image', '', array('class'=>'input-lg imageable form-control')) }}
  </span>
  <div></div>
</div>
  <span id="files"></span>

  <div class="form-group">
     {{ Form::label('image_url', 'Image Url:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::hidden('display_json[data_array]['.$m.'][image_url]', '', array('class'=>'input-lg  image_url form-control','title'=>'Section Image ', 'placeholder'=>'Image URL')) }}
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10">

      <a href="javascript:void(0)" style="display:block;" class="btn btn-danger lnk-remove-section" id="lnk-{{ $m }}">DELETE</a>
    </div>
  </div>
</div>


</div>

<script type="text/javascript">

  function validateForm(){
    errors = [];

    $('#wiki-form .required').each(function(i){
      if($(this).hasClass('description')){
        if($(this).summernote('code') == ''){
          errors.push( $(this).attr('title') + ' is required');
        }
      }else{
        if($(this).val() == ''){
          errors.push( $(this).attr('title') + ' is required');
        }
      }
    });

    if(errors.length > 0){
      alert('There were errors: \n'+errors.join("\n"));
      return false;
    }else{
      return true;
    }
  }



  // var $summernote = $('#service_details');
  // $summernote.summernote({
  //   height: 500,
  //   fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36'],
  //   toolbar: [
  //     // [groupName, [list of button]]
  //     ['style', ['bold', 'italic', 'underline', 'clear']],
  //     ['font', ['strikethrough', 'superscript', 'subscript']],
  //     ['para', ['ul', 'ol', 'paragraph']]
  //   ]
  // });

  $('textarea.description').summernote({
    height: 500,
    fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36'],
    toolbar: [
      // [groupName, [list of button]]
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['strikethrough', 'superscript', 'subscript']],
      ['para', ['ul', 'ol', 'paragraph']]
    ]
  });



  $(function(){
    $('body').on('click', '.deletable', function(){
      $(this).parents('.holder').first().find('input.image_url').val('');
      $(this).parents('div').first().html('');
    });

    var editors = [];

    $('#lnk-add-section').click(function(){
      html = $('#addable-section').html();
      nm = Math.floor(Date.now());
      html = html.replace(/___REPLACE___/g, nm);

      $('#section-holder').append(html);
      bm = $('#'+nm+' textarea.description').summernote({
        height: 500,
        fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36'],
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['para', ['ul', 'ol', 'paragraph']]
        ]
      });

      editors.push(bm);



      $('#'+nm+' .span-imageable input').fileupload({
        url: "/crm/upload-file",
        dataType: 'json',
        done: function (e, data) {
            _this = this;
            $.each(data.result, function (index, file) {
                $(_this).parents('div').first().find('div').html('<img src="'+file.thumb+'" /><a href="javascript:void(0)" class="btn btn-sm btn-danger deletable">Delete</a>');
                $(_this).parents('.holder').first().find('input.image_url').val(file.url);
            });



        }
      });
    });

    $('#section-holder').on('click', '.lnk-remove-section', function(){
      $(this).parents('.holder').first().remove();
    });

    $('#section-holder .span-imageable input').fileupload({
      url: "/crm/upload-file",
      dataType: 'json',
      done: function (e, data) {
          _this = this;
          $.each(data.result, function (index, file) {
              $(_this).parents('div').first().find('div').html('<img src="'+file.thumb+'" />');
              $(_this).parents('.holder').first().find('input.image_url').val(file.url);
          });
      }
    });



    $('#cover_image').fileupload({
        url: "/crm/upload-file",
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result, function (index, _file) {

              $('#files').html('<img src="'+_file.thumb+'" />');
              $('#cover_image_id').val(_file.id);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });



  });


</script>
