@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
<div class="pull-right">
  {{ link_to_route('admin.leads.edit', 'Edit', array($lead->id), array('class' => 'btn btn-xs btn-info')) }}
  {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?');", 'method' => 'DELETE', 'route' => array('admin.leads.destroy', $lead->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
  {{ Form::close() }}
</div>
<h1>{{$lead->id}} - {{ $lead->name }}</h1>
<hr/>

    <dl class="dl-horizontal">
      @foreach(['name', 'phone', 'email', 'interested_in'] as $fd)
        @if(isset($lead->$fd))
          <dt>{{ucwords($fd)}}</dt>
          <dd>{{ ucwords($lead->$fd) }}</dd>
        @endif
      @endforeach
    </dl>    

  </div>

</div>
<p><a href="{{ route('admin.leads.index') }}" class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Back</a></p>
@stop