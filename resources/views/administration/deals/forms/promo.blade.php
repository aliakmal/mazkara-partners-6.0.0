<?php 
if(!isset($deal)):
  $action = route('admin.businesses.create.deal', array($business->id, 'promo'));
else:
  $action = route('admin.businesses.update.deal', array($business->id, 'promo', $deal->id));
endif;?>

{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
          ->post()
          ->action($action)->encodingType('multipart/form-data') }}
@if(isset($deal))
  {{ BootForm::bind($deal) }}
@endif
   @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Title', 'title')->placeholder('Promotion Title') }}
  {{ BootForm::text('Caption', 'caption')->placeholder('Caption')->setAttribute('maxlength', 140) }}
  {{ BootForm::textarea('Description', 'description')->placeholder('Description') }}
  {{ BootForm::textarea('Fine Print', 'fine_print')->placeholder('Fine Print') }}
  {{ BootForm::file('Photo(s)', 'artworks[]')->setAttribute('multiple', true) }}
      @if(isset($deal))
        @foreach($deal->artworks as $artwork)
          <a href="{{ $artwork->image->url() }}" class="lightbox"><img src="{{ $artwork->image->url('thumbnail') }}" class="img-thumbnail" /></a>
          {{ Form::checkbox("deletablePhotos[]", $artwork->id, false ) }}
          Delete?

        @endforeach

      @endif

  {{ BootForm::date('Starts', 'starts')->placeholder('starts on')->addClass('dateinput') }}
  {{ BootForm::date('Ends', 'ends')->placeholder('ends on')->addClass('dateinput') }}
  {{ BootForm::select('Status', 'status', Deal::$statuses) }}
  {{ BootForm::text('Offer Amount', 'offer_amount')->placeholder('Offer Amount') }}

  {{ BootForm::token() }}
  {{ BootForm::submit('Save') }}

{{ BootForm::close() }}


<script type="text/javascript">
$(function(){
  $('.dateinput input').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yyyy-mm-dd", autoclose:true });

})
</script>
