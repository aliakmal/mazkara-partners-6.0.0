@extends('layouts.admin-content')
@section('content')

<h1>Show Comment</h1>

<p>{{ link_to_route('comments.index', 'Return to All comments', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Body</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $comment->body }}}</td>
      <td>
          {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('comments.destroy', $comment->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
          {{ Form::close() }}
          {{ link_to_route('comments.edit', 'Edit', array($comment->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop

