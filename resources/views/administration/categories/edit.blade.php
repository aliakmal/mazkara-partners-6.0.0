@extends('layouts.admin-content')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Category</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($category, array('class' => 'form-horizontal', 'method' => 'PATCH','files'=>true, 'route' => array('admin.categories.update', $category->id))) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('slug', 'Slug:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('slug', Input::old('Slug'), array('class'=>'form-control', 'placeholder'=>'Slug')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('images', 'Images(s):', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                {{ Form::file('images[]', array('multiple'=>true, 'accept'=>"image/*", 'capture'=>'camera')) }}
            </div>

            @foreach($category->photos as $one_photo)
                <img src="{{ $one_photo->image->url('thumbnail') }}" class="img-thumbnail" />
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('css', 'Css Class:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('css', Input::old('css'), array('class'=>'form-control', 'placeholder'=>'Css')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('parent_id', 'Parent_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('parent_id', Category::Selector('Select Parent Category'), Input::old('parent_id'), array('class'=>'form-control')) }}
            </div>
        </div>
  <div class="form-group">
    {{ Form::label('state', 'Active:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('state', ['active'=>'Active', 'inactive'=>'Inactive'], 
                    ($category->isActive()?'active':'inactive'), 
                               array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('displayable', 'Displayable:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('displayable', [0=>'Do Not Display on Front', '1'=>'Display on Front'], 
                                ($category->isDisplayable()?1:0), 
                               array('class'=>'form-control')) }}
    </div>
  </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.categories.show', 'Cancel', $category->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop
