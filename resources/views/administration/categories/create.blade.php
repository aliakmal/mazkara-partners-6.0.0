@extends('layouts.admin-content')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Category</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'admin.categories.store', 'files'=>true, 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
            </div>
        </div>
    <div class="form-group">
        {{ Form::label('images', 'Images(s):', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::file('images[]', array('multiple'=>true, 'accept'=>"image/*", 'capture'=>'camera')) }}
        </div>
    </div>
        <div class="form-group">
            {{ Form::label('css', 'Css Class:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('css', Input::old('css'), array('class'=>'form-control', 'placeholder'=>'Css')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('parent_id', 'Parent_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('parent_id', Category::Selector('Select Parent Category'), Input::old('parent_id'), array('class'=>'form-control')) }}
            </div>
        </div>

  <div class="form-group">
    {{ Form::label('state', 'Active:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('state', ['active'=>'Active', 'inactive'=>'Inactive'], Input::old('state'), 
                               array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('displayable', 'Displayable:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('displayable', [0=>'Do Not Display on Front', '1'=>'Display on Front'], Input::old('displayable'), 
                               array('class'=>'form-control')) }}
    </div>
  </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
@stop
