@extends('layouts.admin-content')
@section('content')

<h1>All Categories</h1>

<p>{{ link_to_route('admin.categories.create', 'Add New Category', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($categories->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Name</th>
				<th>Description</th>
				<th> </th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($categories as $category)
				<tr>
          <td>{{{ $category->id }}}</td>
					<td>{{{ $category->name }}}</td>
					<td>{{{ $category->description }}}</td>
					<td>
            {{$category->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>'}} 
            {{$category->isDisplayable()?'<span class="label label-warning" title="Visible on Front"><i class="fa fa-eye"></i></span>':'<span title="Hidden on front" class="label label-default"><i class="fa fa-eye"></i></span>'}} 
          </td>
          <td>
            <!--
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.categories.destroy', $category->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }} -->
            {{ link_to_route('admin.categories.edit', 'Edit', array($category->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no categories
@endif

@stop
