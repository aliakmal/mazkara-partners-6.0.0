@extends('layouts.admin-content')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>All Brands</h1>

    <p>{{ link_to_route('admin.brands.create', 'Add New Brand', null, array('class' => 'btn btn-lg btn-success')) }}</p>

    @if ($brands->count())
    	<table class="table table-striped">
    		<thead>
    			<tr>
    				<th>Name</th>
    				<th>&nbsp;</th>
    			</tr>
    		</thead>

    		<tbody>
    			@foreach ($brands as $brand)
    				<tr>
    					<td>{{{ $brand->name }}}</td>
              <td>
                  {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.brands.destroy', $brand->id))) }}
                      {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                  {{ Form::close() }}
                  {{ link_to_route('admin.brands.edit', 'Edit', array($brand->id), array('class' => 'btn btn-info')) }}
              </td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	There are no brands
    @endif
  </div>
</div>

@stop