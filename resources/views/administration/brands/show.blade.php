@extends('layouts.admin-content')
@section('content')
<h1>Show Brand</h1>

<p>{{ link_to_route('admin.brands.index', 'Return to All brands', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $brand->name }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.brands.destroy', $brand->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.brands.edit', 'Edit', array($brand->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>
@stop