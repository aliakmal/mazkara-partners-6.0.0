@extends('layouts.admin-crm')
@section('content')


<h1>Featured Listings</h1>

<div class="">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'route'=>'admin.post.featured', 'method' => 'POST')) }}
  <ol >
    @for($i=1; $i<=10; $i++)
      <li>
        {{ Form::text('name_'.$i, (isset($businesses[$i])?$businesses[$i]->name:''), array('class'=>' listing-suggestor ', 'style'=>'width:500px;')) }}
        {{ Form::hidden('business_id['.$i.']', (isset($businesses[$i])?$businesses[$i]->id:''), array('class'=>'form-control', 'style'=>'width:500px;')) }}
        <a href="javascript:void(0)" class="btn btn-xs btn-default clear-order"><i class="fa fa-trash"></i></a>
      </li>
    @endfor
  </ol>
  <button type="submit" class="btn btn-default">Save Order</button>
  {{ Form::close() }}
</div>

<script type="text/javascript">
$(function(){
  $('.clear-order').click(function(){
    $(this).parent('li').find('input').val('');
  });

  $('.listing-suggestor').autocomplete({
    source: "{{ route('crm.businesses.search') }}",
    select: function( event, ui ) {
      $(this).next('input').val(ui.item.id);
      this.value = ui.item.label;
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $("<li>").append("<a>" + item.label + "</a>").appendTo( ul );
  };

});
</script>


@stop

