@extends('layouts.admin-crm')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h1>Create Campaign</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>



{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.campaigns.store'))->encodingType('multipart/form-data') }}

 @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Title', 'title')->placeholder('Title') }}
  {{ BootForm::text('Caption', 'caption')->placeholder('Caption') }}
  {{ BootForm::text('Starts', 'starts')->placeholder('starts on')->addClass('dateinput') }}
  {{ BootForm::text('Ends', 'ends')->placeholder('ends on')->addClass('dateinput') }}
  {{ BootForm::select('Merchant', 'merchant_id', Merchant::all()->lists('name', 'id')->all()) }}

  {{ BootForm::token() }}
<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>
{{ BootForm::close() }}

<script type="text/javascript">
$(function(){
  $('input.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });
})
</script>

@stop
