  <script id="service-item-template" type="text/x-handlebars-template">
<form class="form-horizontal" method="post" action="/crm/ads/{{id}}" enctype="multipart/form-data">
<fieldset>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="title">Title</label>
  <div class="controls">
    <input id="title" name="title" placeholder="Title of Ad" value="{{title}}" class="input-xlarge" type="text">
    
  </div>
</div>

<!-- File Button --> 
<div class="control-group">
  <label class="control-label" for="image">Picture</label>
  <div class="controls">
    <input id="image" name="image" class="input-file" type="file">
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="caption">Caption</label>
  <div class="controls">
    <input id="caption" name="caption" placeholder="Caption of Ad" class="input-xlarge" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="url">Url</label>
  <div class="controls">
    <input id="url" name="url" placeholder="Url for native ad" class="input-xlarge" type="text">
    
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label" for="singlebutton"></label>
  <div class="controls">
    <input id="singlebutton" name="singlebutton" class="btn btn-primary">Save</button>
  </div>
</div>

</fieldset>
</form>



  <article><div class="row" id="single-service-item-{{ id }}">
    <div class="col-md-6">
      <a href="javascript:void(0)"  class="inline-editable" id="name" data-type="text" data-pk="{{ id }}" data-url="/admin/special_items/updatable" data-title="Enter name">
      {{ name }}
      </a></div>
    <div class="col-md-3">
      <a href="javascript:void(0)" class="inline-editable" id="cost" data-type="text" data-pk="{{ id }}" data-url="/admin/special_items/updatable" data-title="Enter name">
        {{ cost }}
      </a>
    </div>
    <div class="col-md-1">
      <form method="POST" action="/admin/service_items/{{ id }}" accept-charset="UTF-8" style="display: inline-block;">
        <input name="_method" value="DELETE" type="hidden">
        <input class="btn btn-danger btn-xs" value="Delete" type="submit">
      </form>

    </div>    
  </div></article>
  </script>
