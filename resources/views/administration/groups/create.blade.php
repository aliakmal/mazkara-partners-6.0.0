@extends('layouts.admin-content')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Group</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'admin.groups.store', 'files'=>true, 'class' => 'form-horizontal')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('type', 'Type:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::select('type', ['group'=>'Group'],  Input::old('type'), array('class'=>'form-control')) }}
        </div>
    </div>
  <div class="form-group">
      {{ Form::label('businesses', 'Assigned Venues:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('businesses-search', '', array('class'=>'form-control', 'id'=>'business-search', 'placeholder'=>'Type to search for users to allocate')) }}
        <p></p>
        <ul class="list-group" id="businesses-holder">
          @if(isset($group))
            @foreach($group->grouped_businesses as $business)
              @include('administration.groups.partials.business', ['business'=>$business])
            @endforeach
          @endif
        </ul>
      </div>
  </div>
<div class="form-group">
  {{ Form::label('cover', 'Cover Photo', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::file('cover', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
    @if(isset($group))
      @if($group->cover)
        <a href="{{ $group->cover->image->url() }}" class="lightbox"><img src="{{ $group->cover->image->url('thumbnail') }}" class="img-thumbnail" /></a>
        {{ Form::checkbox("deletablePhotos[]", $group->cover->id, false ) }}
        Delete?
      @endif
    @endif

  </div>
</div>
<div class="form-group">
  {{ Form::label('banner', 'Banner Photo', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::file('banner', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
    @if(isset($group))
      @if($group->banner)
        <a href="{{ $group->banner->image->url() }}" class="lightbox"><img src="{{ $group->banner->image->url('thumbnail') }}" class="img-thumbnail" /></a>
        {{ Form::checkbox("deletablePhotos[]", $group->banner->id, false ) }}
        Delete?
      @endif
    @endif

  </div>
</div>
<div class="form-group">
  {{ Form::label('is_custom_active', 'Activate Custom Page:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::select('is_custom_active',  [0=>'Inactive', '1'=>'Activated'],   Input::old('is_custom_active'),array('class'=>'form-control', 'placeholder'=>'State')) }}
  </div>
</div>
    <div class="form-group">
        {{ Form::label('pre_side_html', 'Pre side html:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::textarea('pre_side_html', Input::old('pre_side_html'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
        </div>
    </div>
  <div class="form-group">
    {{ Form::label('valid_until', 'Valid Until:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('valid_until', Input::old('valid_until'), array('class'=>' form-control datepicker', 'placeholder'=>'Dated')) }}
    </div>
  </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}


<script type="text/javascript">
$(function(){
  $('#business-search').autocomplete({
    source:'/content/groups/get-outlets',
    select: function(event, ui){
      $('#businesses-holder').append(ui.item.html);
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( '<div >' + item.id + ' - ' +item.name + '<small>('+item.zone_cache+')</small></div>' )
      .appendTo( ul );
  };  
  $('.datepicker').datepicker({ format: "yyyy-mm-dd", autoclose:true });

  $(document).on('click', '.deletable-link', function(){
    $(this).prevAll('.deletable').first().attr('checked', true);
    $(this).parents('li').first().css('backgroundColor', 'red').fadeOut( "slow", function() {
      $(this).remove();
    });
  });


});
</script>
@stop

