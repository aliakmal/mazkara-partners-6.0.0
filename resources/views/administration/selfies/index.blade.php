@extends('layouts.admin-users')
@section('content')

<h1>All User Posts</h1>

<p>{{ link_to_route('admin.selfies.create', 'Add New User Post', null, array('class' => 'btn btn-sm btn-success')) }}</p>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:250px;', 'placeholder'=>'Search')) }}
    {{ Form::select('state', ([''=>'State?'] + Post::getStates()), Input::get('state'), array('class'=>'form-control ', 'placeholder'=>'Category')) }}
    {{ Form::select('author_id', ([''=>'Author?'] + User::onlyAuthors()->orderby('name', 'asc')->lists('name', 'id')->all()), Input::get('author_id'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}

</div>
@if ($posts->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
        <th>&nbsp;</th>
				<th>Author</th>
				<th>State</th>
        <th>Created At</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($posts as $post)
				<tr>
					<td>
            @if($post->type == Post::ARTICLE)
              <i class="fa fa-file-text-o" title="Article"></i>
            @elseif($post->type == Post::VIDEO)
              <i class="fa fa-video-camera" title="Video"></i>
            @elseif($post->type == Post::SELFIE)
              <i class="fa fa-photo" title="Selfie"></i>
            @elseif($post->type == Post::URL)
              <i class="fa fa-paperclip" title="url"></i>
            @endif

            {{{ $post->title }}}
          </td><td>
<i class=" fa fa-eye op80 "></i>&nbsp;{{ $post->views }}
&nbsp;&nbsp;&nbsp;<i class="fa fa-thumbs-up "></i>&nbsp;{{ $post->num_likes()}}
          </td>
					<td>{{{ $post->author->name }}}</td>
					<td>
            {{ mzk_status_tag($post->state) }}
          </td>
          <td>
            {{ ($post->created_at) }}
          </td>
          <td>

            <a href="{{ $post->url() }}" class="btn btn-xs btn-success" title="Preview"><i class="fa fa-eye"></i></a>
          	{{ link_to_route('admin.selfies.edit', 'Edit', array($post->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm(\'Delete Post? Are you sure?\');', 'method' => 'DELETE', 'route' => array('admin.selfies.destroy', $post->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}
          </td>

				</tr>
        @foreach($post->comments as $comment)
          <tr class="info">
            <td colspan="5">
              <div  style="padding-left:30px;border-left:3px solid #CCC;">
                <small>
                  <a href="javascript:void(0)" class="item-inline-editable"
                     id="body" data-type="textarea" data-pk="{{$comment->id}}" 
                     data-url="{{ route('admin.comments.updatable') }}" 
                     data-title="Edit Comment">{{ $comment->body}}</a> </small> 
                     <p><small> <b>Posted:</b>
                                    on {{ \Carbon\Carbon::parse($comment->created_at)->toDayDateTimeString()}}
                                </small> By 
              {{ link_to_route('users.profile.show', $comment->getDisplayableUsersName(), array($comment->user_id)) }}
</p>
              </div>
            </td>
            <td ></td>
            <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm(\'Delete Comment? Are you sure?\');', 'method' => 'DELETE', 'route' => array('admin.comments.destroy', $comment->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}

            </td>
          </tr>
        @endforeach
			@endforeach
		</tbody>
	</table>


  <div class="row">
    <div class="col-md-12">
  {{ $posts->appends($params)->render() }}
  <div class="pull-right">
    {{ count($posts) }} / {{ $posts->total() }} entries
  </div></div>
</div>
  
@else
	There are no user posts
@endif

@stop
