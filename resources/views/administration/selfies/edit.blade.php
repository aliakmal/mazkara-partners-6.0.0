@extends('layouts.admin-users')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit User Post</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>
@if($post->type == 'url')
    @include('administration.selfies.form-url')
@else
    @include('administration.selfies.form')
@endif
@stop
