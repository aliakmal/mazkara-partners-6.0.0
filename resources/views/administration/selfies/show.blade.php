@extends('layouts.admin-users')
@section('content')

<h1>Show Post</h1>

<p>{{ link_to_route('posts.index', 'Return to All posts', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Slug</th>
				<th>Caption</th>
				<th>Body</th>
				<th>Author_id</th>
				<th>State</th>
				<th>Published_on</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $post->title }}}</td>
			<td>{{{ $post->slug }}}</td>
			<td>{{{ $post->caption }}}</td>
			<td>{{{ $post->body }}}</td>
			<td>{{{ $post->author_id }}}</td>
			<td>{{{ $post->state }}}</td>
			<td>{{{ $post->published_on }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 
                            'route' => array('admin.posts.destroy', $post->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.posts.edit', 'Edit', array($post->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
