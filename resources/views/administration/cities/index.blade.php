@extends('layouts.admin-content')
@section('content')
<h1>All Cities</h1>

<p>{{ link_to_route('admin.cities.create', 'Add New City', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($zones->count())


	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($zones as $zone)
				<tr>
					<td>{{{ $zone->stringPath() }}}</td>
					<td>{{{ $zone->description }}}</td>
          <td>
              {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.cities.destroy', $zone->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.cities.edit', 'Edit', array($zone->id), array('class' => 'btn btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no zones
@endif

@stop
