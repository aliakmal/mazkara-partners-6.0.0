@extends('layouts.admin-content')
@section('content')

<h1>Show Zone</h1>

<p>{{ link_to_route('admin.cities.index', 'Return to All cities', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Description</th>
				<th>Parent_id</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{{ $zone->name }}}</td>
			<td>{{{ $zone->description }}}</td>
			<td>{{{ $zone->parent_id }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 
            'route' => array('admin.cities.destroy', $zone->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.cities.edit', 'Edit', array($zone->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
