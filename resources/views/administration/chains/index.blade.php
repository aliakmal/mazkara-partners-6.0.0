@extends('layouts.admin-content')
@section('content')

<h1>All Chain Groups</h1>

<p>{{ link_to_route('admin.chains.create', 'Add New Group', null, array('class' => 'btn btn-lg btn-success')) }}</p>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:550px;', 'placeholder'=>'Search')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>

@if ($groups->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Type</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($groups as $group)
				<tr>
					<td>{{{ $group->name }}}</td>
					<td>{{{ $group->type }}}</td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.chains.destroy', $group->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.chains.edit', 'Edit', array($group->id), array('class' => 'btn btn-info')) }}
            <a href="{{ MazkaraHelper::slugCityChain(null, $group)}}" class="btn btn-default">View On Front</a>
            <a href="/content/businesses?chain={{ $group->id}}" class="btn btn-default">
              {{$group->businesses()->count()}} Chain(s)</a>



          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $groups->render() }}
  <div class="pull-right">
    {{ count($groups) }} / {{ $groups->total() }} entries
  </div></div>
</div>


@else
	There are no groups
@endif

@stop
