@extends('layouts.admin-content')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Chain Group</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'admin.chains.store', 'files'=>true, 'class' => 'form-horizontal')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('type', 'Type:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::select('type', ['chain'=>'Chain'],  Input::old('type'), array('class'=>'form-control', 'placeholder'=>'Type')) }}
        </div>
    </div>
<div class="form-group">
  {{ Form::label('cover', 'Cover Photo', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::file('cover', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
    @if(isset($group))
      @if($group->cover)
        <a href="{{ $group->cover->image->url() }}" class="lightbox"><img src="{{ $group->cover->image->url('thumbnail') }}" class="img-thumbnail" /></a>
        {{ Form::checkbox("deletablePhotos[]", $group->cover->id, false ) }}
        Delete?
      @endif
    @endif

  </div>
</div>
<div class="form-group">
  {{ Form::label('banner', 'Banner Photo', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::file('banner', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
    @if(isset($group))
      @if($group->banner)
        <a href="{{ $group->banner->image->url() }}" class="lightbox"><img src="{{ $group->banner->image->url('thumbnail') }}" class="img-thumbnail" /></a>
        {{ Form::checkbox("deletablePhotos[]", $group->banner->id, false ) }}
        Delete?
      @endif
    @endif

  </div>
</div>
<div class="form-group">
  {{ Form::label('is_custom_active', 'Activate Custom Page:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::select('is_custom_active',  [0=>'Inactive', '1'=>'Activated'],   Input::old('is_custom_active'),array('class'=>'form-control', 'placeholder'=>'State')) }}
  </div>
</div>
    <div class="form-group">
        {{ Form::label('pre_side_html', 'Pre side html:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::textarea('pre_side_html', Input::old('pre_side_html'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
        </div>
    </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

@stop

