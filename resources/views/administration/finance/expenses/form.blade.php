@if(isset($expense))
  {{ Form::model($expense, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'POST', 'route' => array('finance.expenses.update', $expense->id))) }}
@else
  {{ Form::open(array('route' => 'finance.expenses.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif
  <div class="form-group">
    {{ Form::label('desc', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('desc', Input::old('desc'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('currency', 'Currency:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('currency', Payment::getCurrencies(), Input::old('currency'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('amount', 'Amount:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('amount', Input::old('amount'), array('class'=>'form-control', 'placeholder'=>'Amount')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('dated', 'Dated:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('dated', Input::old('dated'), array('class'=>'form-control dateinput', 'placeholder'=>'Dated')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('city_id', 'City:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('city_id', Zone::query()->cities()->get()->lists('name', 'id')->all(), Input::old('city_id'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}
<script type="text/javascript">
$(function(){
  $('.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });

});
</script>
