
        <table class="table table-striped">
          <tr>
            <th>&nbsp;</th>
            <?php $currencies = ['aed', 'usd', 'inr'];?>
            @foreach($result as $period)
              <?php $m = mzk_month_array();?>
              @foreach($currencies as $currency)
                @if($$currency==1)
                  <td>
                    <b>
                      <?php echo $m[$period['month']];?>&nbsp;{{$period['year']}}
                      ({{$currency}})
                    </b>
                  </td>
                @endif
              @endforeach
            @endforeach
          </tr>
          @foreach($all_users as $user)
            <tr>
              <td>{{ str_replace(' ', '&nbsp;', $user->name) }}</td>
            @foreach($result as $period)

              @if(isset($period['users'][$user->id]))
                @if($aed==1)
                  <td>{{ number_format($period['users'][$user->id]['aed'], 2) }}</td>
                @endif
                @if($inr==1)
                  <td>{{ number_format($period['users'][$user->id]['inr'], 2) }}</td>
                @endif
                @if($usd==1)
                  <td>{{ number_format($period['users'][$user->id]['usd'], 2) }}</td>
                @endif
              @else
                @if($aed==1)
                  <td>0</td>
                @endif
                @if($inr==1)
                  <td>0</td>
                @endif
                @if($usd==1)
                  <td>0</td>
                @endif
              @endif
          @endforeach
            </tr>
          @endforeach
          <tr>
            <th>TOTAL</th>
            @foreach($result as $period)
              @if($aed==1)
                <th>{{ number_format($period['total_aed'], 2) }}</th>
              @endif
              @if($inr==1)
                <th>{{ number_format($period['total_inr'], 2) }}</th>
              @endif
              @if($usd==1)
                <th>{{ number_format($period['total_usd'], 2) }}</th>
              @endif

            @endforeach

          </tr>
          <tr class="hidden danger">
            <th>CREDIT</th>
            @foreach($result as $period)
              @if($aed==1)
                <th>{{ number_format($period['credit_total_aed'], 2) }}</th>
              @endif
              @if($inr==1)
                <th>{{ number_format($period['credit_total_inr'], 2) }}</th>
              @endif
              @if($usd==1)
                <th>{{ number_format($period['credit_total_usd'], 2) }}</th>
              @endif
            @endforeach

          </tr>
          <tr class="warning">
            <th>EXPENSES</th>
            @foreach($result as $period)
              @if($aed==1)
                <th>{{ number_format($period['expenses_aed'], 2) }}</th>
              @endif
              @if($inr==1)
                <th>{{ number_format($period['expenses_inr'], 2) }}</th>
              @endif
              @if($usd==1)
                <th>{{ number_format($period['expenses_usd'], 2) }}</th>
              @endif
            @endforeach
          </tr>
          <tr>
            <th>FINAL&nbsp;TOTAL</th>
            @foreach($result as $period)

              @if($aed==1)
                <th>{{ number_format(($period['total_aed']- ($period['credit_total_aed'])), 2) }}</th>
              @endif
              @if($inr==1)
                <th>{{ number_format(($period['total_inr']- ($period['credit_total_inr'])), 2) }}</th>
              @endif
              @if($usd==1)
                <th>{{ number_format(($period['total_usd']- ($period['credit_total_usd'])), 2) }}</th>
              @endif
            @endforeach

          </tr>
          <tr>
            <th>EBR</th>
            @foreach($result as $period)
              <th>
                @if($period['expenses_aed']==0)
                  {{ number_format( (($period['total_aed'] - $period['credit_total_aed'])/1)*100, 2) }}
                @else
                  {{ number_format( (($period['total_aed'] - $period['credit_total_aed'])/$period['expenses_aed'])*100, 2) }}
                @endif
              </th>
              @if(($aed+$inr+$usd)==2)
                <th></th>
              @endif
              @if(($aed+$inr+$usd)==3)
                <th></th>
              @endif
            @endforeach

          </tr>

        </table>
