@extends('layouts.admin-finance')
@section('content')
<div class="row">
  <div class="col-md-12">
    <br/><br/>
    <?php
      $start_month = $result['start_month'];
      $start_year = $result['start_year'];
      $has_end_month = $result['has_end_month'];
      $end_month = $result['end_month'];
      $end_year = $result['end_year'];
      $current_city = $result['current_city'];
    ?>
    <div class="well">
      {{ Form::open(array('style' => 'display: ;', 
                          'class'=>'frm-filter form-inline', 
                          'method' => 'GET')) }}
      <div class="row">
        <div class="col-md-6">
          <p>Start Month</p>

          <div class="row">
            <div class="col-md-6">
              {{ Form::select('month', mzk_month_array(), $start_month, array('class'=>'form-control ', 'style'=>'width:100%;' )) }}
            </div>
            <div class="col-md-6">
              {{ Form::select('year', mzk_year_array(), $start_year, array('class'=>'form-control ', 'style'=>'width:100%' )) }}
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <p style="margin-bottom:0px;">
            End Month?
            {{ Form::checkbox('has_end_month', 1, ($has_end_month == 1?true:false), array('id'=>'chk_has_end_month')) }}
          </p>
          <div class="row">
            <div class="col-md-6">
              {{ Form::select('end_month', mzk_month_array(), $end_month, array('class'=>'form-control end-dates', 'style'=>'width:100%;' )) }}
            </div>
            <div class="col-md-6">
              {{ Form::select('end_year', mzk_year_array(), $end_year, array('class'=>'form-control end-dates', 'style'=>'width:100%;' )) }}
            </div>
          </div>

        </div>
      </div>
      &nbsp;
      <div class="row">
        <div class="col-md-2">
          {{ Form::select('city', [ '0'=>'All Cities', '1'=>'Dubai', '88'=>'Pune', '89'=>'Mumbai'], $current_city, array('class'=>'form-control ', 'style'=>'width:100%;' )) }}
        </div>
        <div class="col-md-3">
          {{ Form::select('user_id', ['0'=>'All POCs']+User::select()->byPocs()->get()->lists('selectable_full_name', 'id')->all(), Input::old('user_id'), array('class'=>'form-control', 'style'=>'width:100%;')) }}
        </div>
        <div class="col-md-3">
        {{ Form::select('merchant_id', ['0'=>'All Merchants']+Merchant::select()->byLocale()->get()->lists('name', 'id')->all(), Input::old('merchant_id'), array('class'=>'form-control', 'style'=>'width:100%;')) }}

        {{ Form::hidden('csv', 0, array('class'=>'csv-flag ' )) }}
        </div>
        <div class="col-md-4">
          <a href="javascript:void(0)" class="btn btn-warning btn-csv">DOWNLOAD CSV</a>&nbsp;
          
          <button type="submit" class="btn btn-default">GENERATE REPORT</button>
        </div>

        </div>
      </div>



      {{ Form::close() }}
    </div>
  </div>
  <h2>Statement of Income for 
      <?php $m = mzk_month_array(); echo $m[$start_month];?> {{$start_year}}
      @if($start_month != $end_month)
      to <?php echo $m[$end_month];?> {{$end_year}}
      @endif
  </h2>
  <hr/>
  <?php $month_data = [];?>
  <table class="table table-striped" style="overflow:scroll;display:block;">
    <thead>
      <tr>
        <th colspan="7" style="background-color:#CCCCCC;"></th>
        @foreach($result['months'] as $ii=>$vv)
          <th colspan="9" style="{{ $ii%2 ==0?'':'background-color:#CCCCCC;'}}">
            <?php $ii = explode('-', $vv); ?>
            <?php echo $m[(int)$ii[1]] ;?> - {{ $ii[0] }}
          </th>
        @endforeach
      </tr>
      <tr>
        <th>INVOICE DESC</th>
        <th>MERCHANT&nbsp;ID</th>
        <th>MERCHANT&nbsp;NAME</th>
        <th>CITY</th>
        <th>SALES&nbsp;POC</th>
        <th>START&nbsp;DATE</th>
        <th>END&nbsp;DATE</th>
        @foreach($result['months'] as $ii=>$vv)
          <th>MONTHLY&nbsp;NO&nbsp;TAX&nbsp;(AED)</th>
          <th>MONTHLY&nbsp;NO&nbsp;TAX&nbsp;(INR)</th>
          <th>MONTHLY&nbsp;NO&nbsp;TAX&nbsp;(USD)</th>
          <th>MONTHLY&nbsp;TAX&nbsp;(AED)</th>
          <th>MONTHLY&nbsp;TAX&nbsp;(INR)</th>
          <th>MONTHLY&nbsp;TAX&nbsp;(USD)</th>
          <th>MONTHLY&nbsp;(AED)</th>
          <th>MONTHLY&nbsp;(INR)</th>
          <th>MONTHLY&nbsp;(USD)</th>
        @endforeach
      </tr>
    </thead>
    <tbody>
      <?php 

        $invoices = $result['invoices'];
        $credit_notes = $result['credit_notes'];
        $start_month_expenses = $result['start_month_expenses'];
      ?>
    @foreach($invoices as $invoice)
      <tr>
        <td>{{ link_to_route('admin.invoices.show', $invoice->title, [$invoice->id]) }}</td>
        <td>
          <a href="{{route('admin.merchants.show', [$invoice->merchant_id])}}">
            {{ ($invoice->merchant_id) }}
          </a>
        </td>
        <td>
          <a href="{{route('admin.merchants.show', [$invoice->merchant_id])}}">
            {{ ($invoice->merchant->name) }}
          </a>
        </td>
        <td>{{ $invoice->merchant->city_name() }}</td>
        <td>{{ $invoice->poc_name() }}</td>
        <td>{{ mzk_f_date($invoice->start_date) }}</td>
        <td>{{ mzk_f_date($invoice->end_date) }}</td>
        @foreach($result['months'] as $vv)
          <?php
            $month_data[$vv] = [];
            $vx = explode('-', $vv);
            $current_month = $vx[1];
            $current_year = $vx[0];

            $month_data[$vv]['income_aed'] = isset($month_data[$vv]['income_aed'])?$month_data[$vv]['income_aed']: 0;
            $month_data[$vv]['income_inr'] = isset($month_data[$vv]['income_inr'])?$month_data[$vv]['income_inr']: 0;
            $month_data[$vv]['income_usd'] = isset($month_data[$vv]['income_usd'])?$month_data[$vv]['income_usd']: 0;

            $month_data[$vv]['credit_aed'] = isset($month_data[$vv]['credit_aed'])?$month_data[$vv]['credit_aed']: 0;
            $month_data[$vv]['credit_inr'] = isset($month_data[$vv]['credit_inr'])?$month_data[$vv]['credit_inr']: 0;
            $month_data[$vv]['credit_usd'] = isset($month_data[$vv]['credit_usd'])?$month_data[$vv]['credit_usd']: 0;

            $month_data[$vv]['total_tax_aed'] = isset($month_data[$vv]['total_tax_aed'])?$month_data[$vv]['total_tax_aed']: 0;
            $month_data[$vv]['total_tax_inr'] = isset($month_data[$vv]['total_tax_inr'])?$month_data[$vv]['total_tax_inr']: 0;
            $month_data[$vv]['total_tax_usd'] = isset($month_data[$vv]['total_tax_usd'])?$month_data[$vv]['total_tax_usd']: 0;

            $month_data[$vv]['income_no_tax_aed'] = isset($month_data[$vv]['income_no_tax_aed'])?$month_data[$vv]['income_no_tax_aed']: 0;
            $month_data[$vv]['income_no_tax_inr'] = isset($month_data[$vv]['income_no_tax_inr'])?$month_data[$vv]['income_no_tax_inr']: 0;
            $month_data[$vv]['income_no_tax_usd'] = isset($month_data[$vv]['income_no_tax_usd'])?$month_data[$vv]['income_no_tax_usd']: 0;


            $month_data[$vv]['contrib_income_aed'] = $invoice->currentMonthContributionAED($current_month, $current_year);
            $month_data[$vv]['contrib_income_inr'] = $invoice->currentMonthContributionINR($current_month, $current_year);
            $month_data[$vv]['contrib_income_usd'] = $invoice->currentMonthContributionUSD($current_month, $current_year);

            $month_data[$vv]['contrib_tax_aed'] = $invoice->currentMonthTaxAED($current_month, $current_year);
            $month_data[$vv]['contrib_tax_inr'] = $invoice->currentMonthTaxINR($current_month, $current_year);
            $month_data[$vv]['contrib_tax_usd'] = $invoice->currentMonthTaxUSD($current_month, $current_year);
          ?>    
          <td>AED {{{ number_format(($month_data[$vv]['contrib_income_aed'] - $month_data[$vv]['contrib_tax_aed']), 2) }}}</td>
          <td>INR {{{ number_format(($month_data[$vv]['contrib_income_inr'] - $month_data[$vv]['contrib_tax_inr']), 2) }}}</td>
          <td>USD {{{ number_format(($month_data[$vv]['contrib_income_usd'] - $month_data[$vv]['contrib_tax_usd']), 2) }}}</td>

          <td>AED {{{ number_format($month_data[$vv]['contrib_tax_aed'], 2) }}}</td>
          <td>INR {{{ number_format($month_data[$vv]['contrib_tax_inr'], 2) }}}</td>
          <td>USD {{{ number_format($month_data[$vv]['contrib_tax_usd'], 2) }}}</td>

          <td>AED {{{ number_format($month_data[$vv]['contrib_income_aed'], 2) }}}</td>
          <td>INR {{{ number_format($month_data[$vv]['contrib_income_inr'], 2) }}}</td>
          <td>USD {{{ number_format($month_data[$vv]['contrib_income_usd'], 2) }}}</td>
          <?php 
          $month_data[$vv]['income_aed'] += $month_data[$vv]['contrib_income_aed'];
          $month_data[$vv]['income_inr'] += $month_data[$vv]['contrib_income_inr'];
          $month_data[$vv]['income_usd'] += $month_data[$vv]['contrib_income_usd'];

          $month_data[$vv]['income_no_tax_aed'] += $month_data[$vv]['contrib_income_aed'];
          $month_data[$vv]['income_no_tax_inr'] += $month_data[$vv]['contrib_income_inr'];
          $month_data[$vv]['income_no_tax_usd'] += $month_data[$vv]['contrib_income_usd'];

          $month_data[$vv]['income_no_tax_aed'] -= $month_data[$vv]['contrib_tax_aed'];
          $month_data[$vv]['income_no_tax_inr'] -= $month_data[$vv]['contrib_tax_inr'];
          $month_data[$vv]['income_no_tax_usd'] -= $month_data[$vv]['contrib_tax_usd'];
          
          $month_data[$vv]['total_tax_aed'] += $month_data[$vv]['contrib_tax_aed'];
          $month_data[$vv]['total_tax_inr'] += $month_data[$vv]['contrib_tax_inr'];
          $month_data[$vv]['total_tax_usd'] += $month_data[$vv]['contrib_tax_usd'];
          ?>        
        @endforeach
      </tr>
    @endforeach
    @foreach($credit_notes as $credit_note)
      <tr class="danger">

        <td>{{ $credit_note->id }}</td>
        <td>
          <a href="{{ route('admin.credit_notes.pdf', array($credit_note->id)) }}" class=" ">
            {{ $credit_note->title }}
          </a>
        </td>
        <td><a href="{{route('admin.merchants.show', [$invoice->merchant_id])}}">{{ ($credit_note->invoice->merchant->name)}}</a></td>
        <td>{{ $credit_note->invoice->merchant->city_name() }}</td>
        <td></td>
        <td></td>
        <td></td>
        @foreach($result['months'] as $vv)
          <?php
            $vx = explode('-', $vv);
            $current_month = $vx[1];
            $current_year = $vx[0];
            $month_data[$vv]['credit_current_aed'] = $credit_note->currentMonthContributionAED($current_month, $current_year);
            $month_data[$vv]['credit_current_inr'] = $credit_note->currentMonthContributionINR($current_month, $current_year);
            $month_data[$vv]['credit_current_usd'] = $credit_note->currentMonthContributionUSD($current_month, $current_year);
          ?>
        <td>AED {{{ number_format($month_data[$vv]['credit_current_aed'], 2) }}}</td>
        <td>INR {{{ number_format($month_data[$vv]['credit_current_inr'], 2) }}}</td>
        <td>USD {{{ number_format($month_data[$vv]['credit_current_usd'], 2) }}}</td>
        <td></td><td></td><td></td>
        <td></td><td></td><td></td>
        <?php 
        $month_data[$vv]['credit_aed'] += $month_data[$vv]['credit_current_aed'];
        $month_data[$vv]['credit_inr'] += $month_data[$vv]['credit_current_inr'];
        $month_data[$vv]['credit_usd'] += $month_data[$vv]['credit_current_usd'];

        ?>
        @endforeach
      </tr>
    @endforeach
      <tr>
        <td colspan="7" >
          <b>GROSS INCOME</b>
        </td>
        @foreach($result['months'] as $vv)
          <td>
            <b>AED {{ number_format($month_data[$vv]['income_no_tax_aed'], 2) }}</b>
          </td>
          <td>
            <b>INR {{ number_format($month_data[$vv]['income_no_tax_inr'], 2) }}</b>
          </td>
          <td>
            <b>USD {{ number_format($month_data[$vv]['income_no_tax_usd'], 2) }}</b>
          </td>
          <td>
            <b>AED {{ number_format($month_data[$vv]['total_tax_aed'], 2) }}</b>
          </td>
          <td>
            <b>INR {{ number_format($month_data[$vv]['total_tax_inr'], 2) }}</b>
          </td>
          <td>
            <b>USD {{ number_format($month_data[$vv]['total_tax_usd'], 2) }}</b>
          </td>
          <td>
            <b>AED {{ number_format($month_data[$vv]['income_aed'], 2) }}</b>
          </td>
          <td>
            <b>INR {{ number_format($month_data[$vv]['income_inr'], 2) }}</b>
          </td>
          <td>
            <b>USD {{ number_format($month_data[$vv]['income_usd'], 2) }}</b>
          </td>
        @endforeach
      </tr>
      <tr class="danger">
        <td  colspan="7" >
          <b>CREDIT RAISED</b>
        </td>
        @foreach($result['months'] as $vv)
        <td>
          <b>AED {{ number_format($month_data[$vv]['credit_aed'], 2) }}</b>
        </td>
        <td>
          <b>INR {{ number_format($month_data[$vv]['credit_inr'], 2) }}</b>
        </td>
        <td>
          <b>USD {{ number_format($month_data[$vv]['credit_usd'], 2) }}</b>
        </td>
        <td></td><td></td><td></td>
        <td></td><td></td><td></td>
        @endforeach
      </tr>
<?php 



foreach($result['start_month_expenses'] as $my=>$expenses){
  $result['start_month_expenses'][$my]['expenses_usd'] = 0;
  $result['start_month_expenses'][$my]['expenses_inr'] = 0;
  $result['start_month_expenses'][$my]['expenses_aed'] = 0;

  foreach($expenses as $expense){
    if(is_object($expense)){
      $result['start_month_expenses'][$my]['expenses_usd']  += $expense->amount_usd;
      $result['start_month_expenses'][$my]['expenses_inr']  += $expense->amount_inr;
      $result['start_month_expenses'][$my]['expenses_aed']  += $expense->amount_aed;
    }
  }
}

?>

      <tr>
        <td colspan="7" >
          <b>NET INCOME</b>
        </td>
        <td colspan="6"></td>
        @foreach($month_data as $vv=>$da)
          <td>
            <b>AED {{ number_format($month_data[$vv]['income_aed'] - ($month_data[$vv]['credit_aed']), 2) }}</b>
          </td>
          <td>
            <b>INR {{ number_format($month_data[$vv]['income_inr'] - ($month_data[$vv]['credit_inr']), 2) }}</b>
          </td>
          <td>
            <b>USD {{ number_format($month_data[$vv]['income_usd'] - ($month_data[$vv]['credit_usd']), 2) }}</b>
          </td>
          <td></td><td></td><td></td>
        <td></td><td></td><td></td>
        @endforeach
      </tr>
      <tr class="warning">
        <td colspan="7" >
          <b>EXPENSES</b>
        </td>
        @foreach($result['start_month_expenses'] as $my=>$expenses)
          <td>
            <b>AED {{ number_format($result['start_month_expenses'][$my]['expenses_aed'], 2) }}</b>
          </td>
          <td>
            <b>INR {{ number_format($result['start_month_expenses'][$my]['expenses_inr'], 2) }}</b>
          </td>
          <td>
            <b>USD {{ number_format($result['start_month_expenses'][$my]['expenses_usd'], 2) }}</b>
          </td>
          <td></td><td></td><td></td>
          <td></td><td></td><td></td>
        @endforeach
      </tr>
      <tr>
        <td colspan="9">
          <b>EBR</b>
        </td>
        @foreach($month_data as $vv=>$data)

        <td>
          @if($result['start_month_expenses'][$vv]['expenses_aed']==0)
            <b>AED {{ number_format(($month_data[$vv]['income_aed'] - ($month_data[$vv]['credit_aed'])/1)*100, 2) }}</b>
          @else
            <b>AED {{ number_format(($month_data[$vv]['income_aed'] - ($month_data[$vv]['credit_aed'])/$result['start_month_expenses'][$vv]['expenses_aed'])*100, 2) }}</b>
          @endif
        </td>
        <td colspan="6"></td>

      @endforeach
      </tr>
    </tbody>
  </table>
  
</div>
<script type="text/javascript">
$(function(){

  if($('#chk_has_end_month').prop('checked') == true){
    $('.end-dates').attr('disabled', 'disabled');
  }else{
    $('.end-dates').removeAttr('disabled');
  }

  $('#chk_has_end_month').click(function(){
    console.log($('.end-dates'));
    if($(this).prop('checked') == true){
      console.log(123);
      $('.end-dates').attr('disabled', 'disabled');
    }else{
      $('.end-dates').removeAttr('disabled');
    }
  });
})
$('.btn-csv').click(function(){
  $('.csv-flag').val('1');
  $('.frm-filter').submit();
  $('.csv-flag').val('0');
});
</script>

@stop
