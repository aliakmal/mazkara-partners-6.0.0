<div class="row">
  <div class="col-md-12">
<div style="overflow-y:scroll; padding:15px; margin:15px 0px 15px 0px;border:1px solid #ccc; height:300px;">
  <table class="table">
  <thead>
    <tr>
      <th>Dated</th>
      <th>Called</th>
      <th>Caller</th>
      <th>Duration</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($call_logs as $call_log)
      <tr>
        <td>{{{ $call_log->dated }}} {{{ $call_log->timed }}}</td>
        <td>{{{ $call_log->called_number }}}</td>
        <td>{{{ $call_log->caller_number }}}</td>
        <td>{{{ $call_log->caller_duration }}}</td>
      </tr>
    @endforeach
  </tbody>

</table>
</div></div></div>
  <div class="row">
  <div class="col-md-12 ">
  <dl class="dl-horizontal">
    <dt>TOTAL CALLS</dt>
    <dd>{{ count($call_logs) }}</dd>
    <dt>UNIT COST</dt>
    <dd>{{ $data['currency'] }} {{ $data['unit_price'] }}</dd>
    <dt>TOTAL COST</dt>
    <dd>{{ $data['currency'] }} {{ count($call_logs) * $data['unit_price'] }}</dd>
  </dl>

</div></div>

{{ Form::hidden('amount', (count($call_logs) * $data['unit_price']) ) }}
{{ Form::hidden('items[0][desc]', count($call_logs).' calls') }}
{{ Form::hidden('items[0][qty]', count($call_logs)) }}
{{ Form::hidden('items[0][price]', $data['unit_price']) }}
{{ Form::hidden('items[0][total]', (count($call_logs) * $data['unit_price']) )}}

