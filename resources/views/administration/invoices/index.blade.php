@extends('layouts.admin-finance')
@section('content')

<div class="row">
	<div class="col-md-12">

<h1>All Invoices
<p class="pull-right">
  {{ link_to_route('admin.invoices.create', 'Add New Invoice', null, array('class' => 'btn btn-sm btn-success')) }}
  {{ link_to_route('admin.invoices.ppl.create', 'Add New PPL Invoice', null, array('class' => 'btn btn-sm btn-info')) }}
</p>
</h1>
<div class="row">
  <div class="col-md-12">
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:250px;', 'placeholder'=>'Search')) }}
    {{ Form::select('state', ([''=>'State?'] + Invoice::getStates()), Input::get('state'), array('class'=>'form-control ')) }}
    {{ Form::select('sales_poc', ([''=>'Sales POC?'] + User::select()->byPocs()->get()->lists('selectable_full_name', 'id')->all()), Input::get('sales_poc'), array('class'=>'form-control ', 'style'=>'width:250px;')) }}
    {{ Form::select('merchant_id', ([''=>'Merchant?'] + Merchant::orderby('name', 'asc')->lists('name', 'id')->all()), Input::get('merchant_id'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>

  </div>
</div>
@if ($invoices->count())
<div class="row">
	<div class="col-md-12">


			@foreach ($invoices as $invoice)
      <div class="list-group-item" style="position:relative;">

        <div class="row">
          <div class="col-md-3">

          <h4 class="list-group-item-heading fs150">
            {{ link_to_route('admin.invoices.show', $invoice->title, [$invoice->id]) }}
          </h4>
        </div>
        <div class="col-md-4">
            <small>SALES POC:</small>
            <br/><span class="fw700 fs110">{{{ $invoice->poc_name() }}}</span>
        </div>
        <div class="col-md-5">
            <small>MERCHANT:</small>
            <br/><span class="fw700 fs110">{{ $invoice->merchant->name }}</span>
          <div class="pull-right" style="position: absolute;top:0px;right:10px">
            @if(Auth::user()->hasRole('admin'))
            <!--
              {{ Form::open(array('style' => 'display: inline-block;margin-right:3px;', 'onsubmit'=>"return confirm('Are you sure?')", 'method' => 'DELETE', 
                                  'route' => array('admin.invoices.destroy', $invoice->id))) }}
                  <button type="submit" class="btn btn-xs btn-danger">
                    <i class="fa fa-trash fa-bin"></i>
                  </button>
              {{ Form::close() }}
            -->
            @endif
            <a href="{{ route('admin.invoices.edit', array($invoice->id))}}" class="btn btn-xs btn-info" style="margin-right:3px;"><i class="fa fa-pencil"></i></a>

            <span  class="label fs90 label-{{mzk_invoice_state_label_css($invoice->state)}}" >{{ strtoupper($invoice->state) }}</span>
          </div>

        </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <small>PERIOD:</small><br/>
            <span class="fw700 fs110">{{{ mzk_f_date($invoice->start_date) }}} - {{{ mzk_f_date($invoice->end_date) }}}</span>
        </div>
          <div class="col-md-2"><small>MONTH CONTRIB</small><br/><span class="fw700 fs110">{{{ $invoice->currency }}} {{{ $invoice->currentMonthContribution() }}}</span></div>
          <div class="col-md-2"><small>MONTH AVERAGE</small><br/><span class="fw700 fs110">{{{ $invoice->currency }}} {{{ $invoice->monthlyAverage() }}}</span></div>
          <div class="col-md-2"><small>CONTRACT AMOUNT</small><br/><span class="fw700 fs110">{{{ $invoice->currency }}} {{{ $invoice->amount }}}</span></div>
          <div class="col-md-2"><small>AMOUNT DUE</small><br/><span class="fw700 fs110">{{{ $invoice->currency }}} {{{ $invoice->amount_due }}}</span></div>
        </div>
      </div>
			@endforeach
	</div>
</div>
<p>&nbsp;</p>
  <div class="row">
    <div class="col-md-12">
  {{ $invoices->appends($params)->render() }}
  <div class="pull-right">
    {{ count($invoices) }} / {{ $invoices->total() }} entries
  </div></div>
</div>


@else
	There are no invoices
@endif

	</div>
</div>

@stop

