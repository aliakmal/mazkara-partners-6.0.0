@extends('layouts.blank')
@section('content')

<?php $css = 'style="padding-bottom:0px; margin-bottom:0px;margin-top:0px;"';?>
<?php $csstop = 'style="padding-bottom:0px; margin-bottom:0px;margin-top:30px;"';?>
<table width="100%">
  <tr>
    <td width="50%" align="left" style="vertical-align:top;padding-left:0px;margin-left:0px;">
      <img src="{{mzk_assets('assets/fab-logo.png')}}" align="left" width="175px" style="text-align:left;"  />
    </td>
    <td align="right" width="50%">
      <?php $entity = mzk_get_entities($credit_note->invoice->entity_id);?>
      <div >{{ $entity['name'] }}</div>
      @foreach($entity['address'] as $vv)
        <div>{{ $vv }}</div>
      @endforeach
      <div>www.fabogo.com</div>
    </td>
  </tr>
</table>

    <table class="table" {{$csstop}}  width="50%">
      <tr>
        <th width="30%">ISSUED FOR INVOICE #</th>
        <td width="70%">{{ $credit_note->invoice->title }}</td>
      </tr>
    </table>
    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">{{ strtoupper(mzk_slug_to_words($credit_note->type) )}} #</th>
        <td width="70%">{{ $credit_note->title }}</td>
      </tr>
    </table>

    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">{{ strtoupper(mzk_slug_to_words($credit_note->type) )}} DATE</th>
        <td width="70%">{{ mzk_f_date($credit_note->created_at) }}</td>
      </tr>
    </table>
    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">MERCHANT</th>
        <td width="70%">{{ $credit_note->invoice->merchant->name }}</td>
      </tr>
    </table>
    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">CITY</th>
        <td width="70%">{{ $credit_note->invoice->city_name() }}</td>
      </tr>
    </table>

      <table class="table table-striped" {{$csstop}} width="100%">
        <tr>
          <td><b>REASON/DETAILS</b></td>
        </tr>
      </table>
      <table class="table" {{$css}}  width="100%">
        <tr>
          <td width="70%" >{{ $credit_note->desc }}</td>
          <td width="30%" align="right">{{ $credit_note->invoice->currency }} {{ $credit_note->amount }}</td>
        </tr>
      </table>
      <table class="table" {{$css}}  width="100%">
        <tr>
          <td width="70%" align="right"><span style="font-size:110%">Total</span></td>
          <td width="30%" align="right" id="total-invoice"><span style="font-size:110%">{{ $credit_note->invoice->currency }} {{ $credit_note->amount }}</span></td>
        </tr>
      </table>
        <table class="table" {{$css}}  width="100%">
          <tr>
            <td  align="left">
              <span >
                {{ ucwords(mzk_convert_number_to_words((int)$credit_note->amount))  }} 
                {{ mzk_currency_name_from_sym($credit_note->invoice->currency) }} 
                Only
              </span>
            </td>
          </tr>
        </table>

<p style="margin-top:150px;"><br/></p>
<div class="alert alert-warning">Note: This {{mzk_slug_to_words($credit_note->type)}} is computer generated and doesn't require a signature.</div>
@stop
