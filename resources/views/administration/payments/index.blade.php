@extends('layouts.admin-finance')
@section('content')

<div class="row">
	<div class="col-md-12">


<h1>All Payments
	<p class="pull-right">
		{{ link_to_route('admin.payments.create', 'Add New Payment', null, 
											array('class' => 'btn btn-sm btn-success')) }}
	</p>
</h1>
<div class="row">
  <div class="col-md-12">
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search-invoice', Input::get('search-invoice'), array('class'=>'form-control col-md-2', 'style'=>'width:250px;', 'placeholder'=>'Search by Invoice')) }}
    {{ Form::select('state', ([''=>'State?'] + Payment::getStates()), Input::get('state'), array('class'=>'form-control ')) }}
    {{ Form::select('merchant_id', ([''=>'Merchant?'] + $all_merchants), Input::get('merchant_id'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>

  </div>
</div>


@if ($payments->count())
	<table  class="table table-striped">
		<thead>
			<tr>
				<th>Type</th>
        <th>Merchant</th>
        <th>Amount</th>
        <th>Amount Payable</th>
				<th>State</th>
				<th>Created On</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($payments as $payment)
				<tr>
					<td>
            {{{ ucwords($payment->type) }}} 
            @if($payment->isCheque())
              (#{{$payment->chq_number}}, {{$payment->chq_bank}}, {{mzk_f_date($payment->chq_date)}})
            @endif
          </td>
          <td style="max-width:150px;">{{{ isset($all_merchants[$payment->merchant_id]) ? $all_merchants[$payment->merchant_id] : 'undefined' }}}</td>
          <td>{{{ $payment->currency }}} {{{ $payment->amount }}}</td>
          <td>{{{ $payment->currency }}} {{{ $payment->amount_applicable }}}</td>
          <td>
            <span class="label label-{{mzk_invoice_state_label_css($payment->state)}}">{{$payment->state}}</span>
          </td>
          <td>{{ mzk_f_date($payment->created_at) }}</td>
          <td>
            <a href="{{ route('admin.payments.show', array($payment->id)) }}" class="btn btn-xs btn-success">
            	<i class="fa fa-eye"></i>
            </a>
            @if(Auth::user()->hasRole('admin'))
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?')", 'method' => 'DELETE', 
      														'route' => array('admin.payments.destroy', $payment->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
            @endif
            {{ link_to_route('admin.payments.edit', 'Edit', array($payment->id), 
            								array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $payments->render() }}
  <div class="pull-right">
    {{ count($payments) }} / {{ $payments->total() }} entries
  </div></div>
</div>

@else
	There are no payments
@endif

	</div>
</div>
@stop
