@extends('layouts.admin-content')
@section('content')

<h1>Show Zone</h1>

<p>{{ link_to_route('admin.zones.index', 'Return to All zones', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Description</th>
				<th>Parent_id</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{{ $zone->name }}}</td>
			<td>{{{ $zone->description }}}</td>
			<td>{{{ $zone->parent_id }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.zones.destroy', $zone->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.zones.edit', 'Edit', array($zone->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

<h4>Counts of services against the zone</h4>
<table class="table">
  @foreach($zone->zone_counters()->get() as $zone_count)
  <tr>
    <td>
      <a href="{{MazkaraHelper::slugCityZone($zone, ['service'=>[$zone_count->service_id]]) }}"> 
        {{$zone_count->service->name}}
      </a>
    </td>
    <td>{{$zone_count->business_count}}</td>
  </tr>
  @endforeach
</table>

@stop
