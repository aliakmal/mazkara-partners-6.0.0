@extends('layouts.admin-content')
@section('content')


<h1>All Zones</h1>

<p>{{ link_to_route('admin.zones.create', 'Add New Zone', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($zones->count())


	<table class="table table-striped">
		<thead>
			<tr>
        <th>{{'#'}}</th>
				<th>Name</th>
				<th>Description</th>
        <th>Business Zone</th>
        <th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($zones as $zone)
				<tr>
          <td>{{ $zone->id }}</td>
					<td>{{ $zone->stringPath() }}</td>
					<td>{{ $zone->description }}</td>
          <td>{{ $zone->business_zone?$zone->business_zone->name:'' }}</td>
          <td>
            {{ $zone->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>' }} 
          </td>
          <td>
            <a href="{{ route('admin.zones.show', array($zone->id)) }}" class="btn btn-xs btn-success ">
              <i class="fa fa-eye"></i>
            </a>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.zones.destroy', $zone->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.zones.edit', 'Edit', array($zone->id), array('class' => 'btn btn-info  btn-xs')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no zones
@endif

@stop
