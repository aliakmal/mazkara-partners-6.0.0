@extends('layouts.admin-content')
@section('content')

<h1>Show Highlight</h1>

<p>{{ link_to_route('admin.highlights.index', 'Return to All highlights', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Slug</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{{ $highlight->name }}}</td>
			<td>{{{ $highlight->slug }}}</td>
			<td>{{{ $highlight->description }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.highlights.destroy', $highlight->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.highlights.edit', 'Edit', array($highlight->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
