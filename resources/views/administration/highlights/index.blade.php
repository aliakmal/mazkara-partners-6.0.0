@extends('layouts.admin-content')
@section('content')

<h1>All Highlights</h1>

<p>{{ link_to_route('admin.highlights.create', 'Add New Highlight', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($highlights->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Description</th>
        <th> </th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($highlights as $highlight)
				<tr>
          <td>{{ $highlight->id }}</td>
					<td>{{ $highlight->name }}</td>
					<td>{{ $highlight->slug }}</td>
					<td>{{ $highlight->description }}</td>
          <td>{{ $highlight->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>'}} </td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.highlights.destroy', $highlight->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.highlights.edit', 'Edit', array($highlight->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no highlights
@endif

@stop
