@extends('layouts.admin-content')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Job</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($job, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.jobs.update', $job->id))) }}
    <div class="form-group">
        {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::textarea('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'Body')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('location', 'Location:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
        {{ Form::select('location', Zone::getCitiesArray(), Input::old('location'), 
                           array('class'=>'form-control')) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('state', 'State:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
        {{ Form::select('state', ['active'=>'Active', 'inactive'=>'Inactive'], Input::old('state'), 
                           array('class'=>'form-control')) }}
        </div>
    </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.jobs.show', 'Cancel', $job->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
<script type="text/javascript">
$(function(){
    $('textarea').wysihtml5();
});
</script>

@stop
