@extends('layouts.admin-content')
@section('content')

<h1>Show Job</h1>

<p>{{ link_to_route('admin.jobs.index', 'Return to All jobs', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Location</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $job->title }}}</td>
			<td>{{{ $job->body }}}</td>
			<td>{{{ $job->state }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.jobs.destroy', $job->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.jobs.edit', 'Edit', array($job->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>
{{ $job->location }}

@stop
