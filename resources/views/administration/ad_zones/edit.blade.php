@extends('layouts.admin-crm')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Ad_zone</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($ad_zone, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.ad_zones.update', $ad_zone->id))) }}

    <div class="form-group">
        {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('caption', 'Caption:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('caption', Input::old('caption'), array('class'=>'form-control', 'placeholder'=>'Caption')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('zone_id', 'Zone_id:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::input('number', 'zone_id', Input::old('zone_id'), array('class'=>'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('category_id', 'Category_id:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::input('number', 'category_id', Input::old('category_id'), array('class'=>'form-control')) }}
        </div>
    </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.ad_zones.show', 'Cancel', $ad_zone->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
@stop
