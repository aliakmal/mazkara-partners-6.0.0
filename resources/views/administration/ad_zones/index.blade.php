@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
  <div class="pull-right">
    <p></p>
    <p>{{ link_to_route('admin.ad_zones.create', 'Add New Ad zone', null, array('class' => 'btn btn-sm btn-success')) }}</p>


  </div>
  <h1>All Ad Zones</h1>
</p>



@if ($ad_zones->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Title</th>
				<th>Caption</th>
				<th>Zone</th>
				<th>Category</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($ad_zones as $ad_zone)
				<tr>
          <td>{{{ $ad_zone->id }}}</td>
					<td>{{{ $ad_zone->title }}}</td>
					<td>{{{ $ad_zone->caption }}}</td>
					<td>{{{ $ad_zone->zone->name }}}</td>
					<td>{{{ $ad_zone->category->name }}}</td>
          <td>
              {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit'=>'return confirm(\'Are you sure\')',  'route' => array('admin.ad_zones.destroy', $ad_zone->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.ad_zones.edit', 'Edit', array($ad_zone->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no ad_zones
@endif
</div></div>
@stop
