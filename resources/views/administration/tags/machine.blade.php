<?php 
$taggds = [];

foreach($obj->tags as $tag){
  $taggds[$tag->name] = $tag->name;
}
$taggables = [];

switch($taggable_type){
  case 'Invoice':
    $taggables = ['facebook'=>'Facebook', 'ppl'=>'PPL', 'banner'=>'Banner', 'upsell'=>'Upsell',
                  'downsell'=>'Downsell', 'renewal'=>'Renewal', 'winback'=>'Winback', 
                  'new sale'=>'New Sale'];
  break;
  case 'Photo':
    $taggables = ['interior'=>'Interior', 'facade'=>'Facade', 'service'=>'Service'];
  break;
  case 'RateCard':
    $taggables = ['client copy with rates'=>'client copy with rates',
      'client copy without rates'=>'client copy without rates',
      'fab menu'=>'fab menu'];
    $taggable_type = 'Photo';
  break;
}

?>
<style type="text/css">
.selectize-input{
  min-width:141px;
}
</style>
<div style="border:1px solid #ccc;margin-top:5px; margin-bottom:5px;">
  <div style="background-color:#000;color:#fff;padding:5px;">TAG MACHINE</div>
<div style="padding:5px;" id="tag_machine_for_{{$taggable_type}}_{{$taggable_id}}" class="tag-machine">
  {{ Form::hidden('taggable_type', $taggable_type, ['class'=>'selectable']) }}
  {{ Form::hidden('taggable_id', $taggable_id, ['class'=>'selectable']) }}
  {{ Form::select('tags[]', $taggables, $taggds, ['id' => 'tags_for_'.$taggable_type.'_'.$taggable_id, 
                                             'class'=>'selectize selectable', 'style'=>'min-width:100px', 
                                             'multiple' => 'multiple']) }}
  <a href="javascript:void(0)" class="btn btn-xs btn-primary">save tags</a>
</div>
</div>