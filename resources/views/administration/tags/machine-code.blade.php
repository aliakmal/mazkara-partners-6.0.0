<script language="javascript">
$(function(){
  $('.selectize').each(function(i){
    $(this).selectize();
  });
  $('.tag-machine a').click(function(){
    frm = $(this).parents('.tag-machine').first();
    var tthis = this;
    names = ['taggable_type','taggable_id','tags'];
    inputs = $(frm).find('.selectable:input');
    $(tthis).html('saving...');
    data = {
      taggable_type:$(frm).find('input[name=taggable_type]').first().val(),
      taggable_id:$(frm).find('input[name=taggable_id]').first().val(),
      tags:$(frm).find('select').first().val(),
    };
    $.ajax({
      url: '/admin/tag',
      method: 'POST',
      data:data,
      success:function(r){
        $(tthis).html('save tags');
      }
    });
  });
});

</script>
