@extends('layouts.admin-crm')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Call_log</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($call_log, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.call_logs.update', $call_log->id))) }}

        <div class="form-group">
            {{ Form::label('dated', 'Dated:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('dated', Input::old('dated'), array('class'=>'form-control', 'placeholder'=>'Dated')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('timed', 'Timed:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('timed', Input::old('timed'), array('class'=>'form-control', 'placeholder'=>'Timed')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('called_number', 'Called_number:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('called_number', Input::old('called_number'), array('class'=>'form-control', 'placeholder'=>'Called_number')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('caller_number', 'Caller_number:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('caller_number', Input::old('caller_number'), array('class'=>'form-control', 'placeholder'=>'Caller_number')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('caller_duration', 'Caller_duration:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::input('number', 'caller_duration', Input::old('caller_duration'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('agent_list', 'Agent_list:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('agent_list', Input::old('agent_list'), array('class'=>'form-control', 'placeholder'=>'Agent_list')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('call_connected_to', 'Call_connected_to:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('call_connected_to', Input::old('call_connected_to'), array('class'=>'form-control', 'placeholder'=>'Call_connected_to')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('call_transfer_status', 'Call_transfer_status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('call_transfer_status', Input::old('call_transfer_status'), array('class'=>'form-control', 'placeholder'=>'Call_transfer_status')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('call_transfer_duration', 'Call_transfer_duration:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::input('number', 'call_transfer_duration', Input::old('call_transfer_duration'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('call_recording_url', 'Call_recording_url:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('call_recording_url', Input::old('call_recording_url'), array('class'=>'form-control', 'placeholder'=>'Call_recording_url')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('call_start_time', 'Call_start_time:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('call_start_time', Input::old('call_start_time'), array('class'=>'form-control', 'placeholder'=>'Call_start_time')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('call_pickup_time', 'Call_pickup_time:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('call_pickup_time', Input::old('call_pickup_time'), array('class'=>'form-control', 'placeholder'=>'Call_pickup_time')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('caller_circle', 'Caller_circle:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('caller_circle', Input::old('caller_circle'), array('class'=>'form-control', 'placeholder'=>'Caller_circle')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('business_id', 'Business_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::input('number', 'business_id', Input::old('business_id'), array('class'=>'form-control')) }}
            </div>
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.call_logs.show', 'Cancel', $call_log->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop