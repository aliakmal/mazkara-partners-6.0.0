@extends('layouts.admin-finance')
@section('content')
<div class="row">
  <div class="col-md-12">
        <h1>{{ $merchant->name }}

                <div class="pull-right">
                {{ link_to_route('admin.merchants.edit', 'Edit', array($merchant->id), array('class' => 'btn btn-info btn-xs')) }}
      @if(Auth::user()->hasRole('admin'))
        {{ Form::open(array('style' => 'display: inline-block;margin:0px 3px 0px 3px;', 'method' => 'DELETE', 
                            'onsubmit'=>"return confirm('Deleting this merchant will delete all associated virtual number allocations and its invoices - are you sure you want to delete this merchant? You have been warned...');",
                            'route' => array('admin.merchants.destroy', $merchant->id))) }}
          {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
        {{ Form::close() }}
      @endif
    </div>
</h1>


    <hr style="margin:3px 0px 3px 0px;"/>
    <div class="row">
      <div class="col-md-12">
        <h5>
          <i class="fa fa-envelope" ></i>&nbsp;{{ $merchant->email }}&nbsp;&nbsp;
          <i class="fa fa-map-marker"></i>&nbsp;{{ $merchant->city_name() }}&nbsp;&nbsp;
          <b>Sales POC:</b>&nbsp;{{ $merchant->poc_name() }}&nbsp;&nbsp;
          @if(!empty($merchant->tan))
            <b>TAN:</b>
            {{ $merchant->tan }}
          @endif
        </h5>
        <br/><br/>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h5>BUSINESSES ALLOCATED</h5>
        @foreach($merchant->businesses()->withPivot(['preferred_email'])->get() as $business)
          <div class="panel panel-default">
            <div class="panel-body">
            <div class="media">
            <div class="">
              <div class="pull-right">
                <div class="btn-group"> 
                  <button class="btn btn-default btn-sm dropdown-toggle" 
                          type="button" data-toggle="dropdown" 
                          aria-haspopup="true" aria-expanded="false"> Actions
                      <span class="caret"></span> 
                  </button> 
                  <ul class="dropdown-menu"> 
                    <li>
                      @if($business->canAccessLeadsReport())
                        <a href="{{ route('admin.merchants.toggle_leads_access.businesses', $business) }}" title="Can access leads - click to toggle">
                          <i class="fa fa-book"></i> Disallow Leads
                        </a>
                      @else
                        <a href="{{ route('admin.merchants.toggle_leads_access.businesses', $business) }}" title="Cannot access leads - click to toggle"><i class="fa fa-book"></i> Allow Leads</a>
                      @endif
                    </li> 
                    <li>
                      @if($business->canAccessResumesReport())
                        <a href="{{ route('admin.merchants.toggle_resumes_access.businesses', $business) }}" title="Can access Resumes - click to toggle">
                          <i class="fa fa-file"></i> Disallow Fabhire
                        </a>
                      @else
                        <a href="{{ route('admin.merchants.toggle_resumes_access.businesses', $business) }}" title="Cannot access Resumes - click to toggle">
                          <i class="fa fa-file"></i> Allow Fabhire
                        </a>
                      @endif
                    </li> 
                    <li>
                    @if(!$business->isCurrentlyAllocatedAVirtualNumber() == true)
                      <a href="{{route('admin.virtual_numbers.for.business.allocate', $business->id)}}">
                        <i class="fa fa-phone"></i>
                      Allocate V.Number</a>
                    @endif

                    </li> 
                    <li>
                      <a title="History of virtual numbers allocated" href="{{ route('admin.businesses.virtual_numbers.show', array($business->id) ) }}">
                        <span><i class="glyphicon glyphicon-calendar"></i> History V. Numbers </span>
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('admin.businesses.call_logs', array($business->id) ) }}">
                        <span><i class="glyphicon glyphicon-phone"></i> Call Logs </span>
                      </a>
                    </li>

                    <li>
                      @if($business->isCallDebuggable())
                        <a href="{{ route('admin.merchants.toggle_debug.businesses', $business) }}" title="Can access leads - click to toggle">
                          <i class="fa fa-wrench"></i> Deactivate Debug Mode
                        </a>
                      @else
                        <a href="{{ route('admin.merchants.toggle_debug.businesses', $business) }}" title="Can access leads - click to toggle">
                          <i class="fa fa-wrench"></i> Activate Debug Mode
                        </a>
                      @endif
                    </li> 

                  </ul> 
                </div>

              </div>
              </div>
              <h4 class="media-heading">
                <a href="{{route('admin.businesses.show', ['id'=>$business->id])}}">
                ({{$business->id}}) {{$business->name}}
                </a>
              </h4>
              <i class="glyphicon glyphicon-map-marker"></i> {{ $business->zone_cache }}<br/>
              @if($business->pivot->preferred_email!='')
                <p>
                  <a title="Preferred Email - alerts sent on this email" href="mailto:{{ $business->pivot->preferred_email }}"> 
                    <i class="glyphicon glyphicon-envelope"></i> {{ $business->pivot->preferred_email }}<br/>
                  </a>
                </p>
              @endif
              @if($business->isCurrentlyAllocatedAVirtualNumber() == true)
                <p title="Allocated Number">
                  <i class="fa fa-phone"></i> {{ $business->current_virtual_number_allocation_number() }}
                  <?php $cn = $business->current_virtual_number_allocation();?>
                  <input {{ ( $cn->state !='active' ? ' checked="checked" ' : '') }} data-business="{{$business->id}}" type="checkbox" name="my-checkbox" data-number="{{ $cn->id }}" class=" toggle-button for-v-nums"  />
                </p>
              @else
                <p>
                  <a  title="No Number allocated - click to allocate" 
                      href="{{route('admin.virtual_numbers.for.business.allocate', $business->id)}}" 
                      class="text-danger">
                    <i class="fa fa-phone"></i>  No Virtual Number (Click to allocate)
                  </a> 
                </p>
              @endif
            <hr style="margin:5px 0px 5px 0px"/>
            <div class="row">
              <div class="col-md-12">
                <h5>
              @if($business->canAccessLeadsReport())
                <span class="label label-success" title="Can access leads module"><i class="fa fa-book"></i> LEADS</span>
              @else
                <span class="label label-default"  title="Cannot access leads module"><i class="fa fa-book"></i> <s>LEADS</s></span>
              @endif
              &nbsp;
              @if($business->canAccessResumesReport())
                <span class="label label-success" title="Can access resumes module"><i class="fa fa-file"></i> FABHIRE</span>
              @else
                <span class="label label-default"  title="Cannot access resumes module"><i class="fa fa-file"></i> <s>FABHIRE</s></span>
              @endif
              &nbsp;
                @if($business->isCurrentlyAllocatedAVirtualNumber() == true)
                  <span class="label label-success" title="Has virtual number allocated currently">
                    <i class="fa fa-phone"></i> VIRTUAL NUM.
                  </span>&nbsp;
                    @if($cn->isIVREnabled())
                      <span class="label label-info">IVR</span>
                    @else
                      <span class="label label-default"><s>IVR</s></span>
                    @endif
                    &nbsp;
                  
                @else
                  <span class="label label-default" title="Has no virtual number allocated currently">
                    <i class="fa fa-phone"></i> <s>VIRTUAL NUM.</s>
                  </span>&nbsp;
                @endif
                @if($business->isCallDebuggable())
                  <span class="label label-success" title="Debug Mode is On">
                    <i class="fa fa-circle text-success"></i> DEBUG 
                  </span>
                @else
                  <span class="label label-default" title="Debug Mode Off">
                    <i class="fa fa-circle text-danger"></i> <s>DEBUG </s>
                  </span>
                @endif

              </h5>
              </div>
            </div>
            </div>
          </div>
        </div>
          <hr/>
        @endforeach
      </div>
      <div class="col-md-6">
        <h5>USERS ALLOCATED</h5>
        @foreach($merchant->users as $user)
          <div class="media">
            <div class="">
              <h4 class="media-heading">
                {{$user->full_name}}
              </h4> 
              <small>{{$user->email}}</small>
              <span class="pull-right">
                <a href="javascript:void(0)" data-user="{{$user->id}}" class="btn btn-sm lnk-edit-module-access btn-default">Module Access</a>
              </span>
            </div>
          </div>
          <hr/>
        @endforeach
      </div>
    </div>
    <p><a href="{{ route('admin.merchants.index') }}" class='btn btn-default'><i class="fa fa-chevron-left"></i> Back</a></p>
  </div>
</div>
<script type="text/javascript">
  $(function(){
    $('.lnk-edit-module-access').click(function(){
      $.ajax({
        url: '/crm/merchants/access/{{$merchant->id}}/' + $(this).data('user'),
        method: 'GET',
      }).done(function(result){
        bootbox.dialog({
          message: result['html'],
        });
      });
      
    });


    $('.toggle-button')
              .bootstrapToggle({size:'mini', on: 'Enabled', off: 'Disabled'})
                .change(function(){
                  url = '{{ route('admin.businesses.virtual_numbers.toggle', '__replace__')}}'.replace('__replace__', $(this).data('business'));
                  $.ajax({ url:url })
                });
    $('.toggle-button.disable').bootstrapToggle('disable');

  });
</script>
@stop