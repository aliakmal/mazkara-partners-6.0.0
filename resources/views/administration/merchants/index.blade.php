@extends('layouts.admin-finance')
@section('content')

<div class="row"><div class="col-md-12">


  <div class="pull-right">
    
    <p> {{ link_to_route('admin.merchants.create', 'Add New Merchant', null, 
                      array('class' => 'btn btn-sm btn-success')) }}
        {{ link_to_route('admin.merchants.export', 'Export', null, array('class'=>'btn btn-sm btn-warning'))}}
    </p>


  </div>

<h1>All Merchants</h1>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}
    {{ Form::text('phone', Input::get('phone'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Virtual Number?')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>
@if ($merchants->count())
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Name</th>
        <th>Phone</th>
        <th>Email</th>
				<th>Location</th>
				<th>Sales POC</th>
        <th>Virtual#/Venues</th>
        <th>Call Tracking</th>
        <th>Registration Date</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($merchants as $merchant)
				<tr>
          <td>{{ $merchant->getDisplayableMerchantID() }}</td>
					<td>{{ link_to_route('admin.merchants.show', $merchant->name, array($merchant->id)) }}</td>
          <td>{{{ $merchant->phone }}}</td>
          <td>{{{ $merchant->email }}}</td>
					<td>{{{ $merchant->city_name() }}}</td>
					<td>{{{ $merchant->poc_name() }}}</td>
          <td>
            {{ $merchant->getCountOfVenuesWithVirtualNumbers() }} /
            {{ $merchant->businesses()->count() }}
          </td>
          <td>
            <input   type="checkbox" name="my-checkbox" data-merchant="{{ $merchant->id }}" class="{{ $merchant->canActivateDeactivateVirtualNumbers()?'':'disable' }} toggle-button for-v-nums" {{ $merchant->isAnyVirtualNumberActive() ? 'checked':'' }} />
          </td>
          <td>{{{ mzk_f_date($merchant->created_at) }}}</td>
          <td>
            {{ link_to_route('admin.merchants.edit', 'Edit', array($merchant->id), array('class' => 'btn btn-xs btn-info')) }}
            @if(Auth::user()->hasRole('admin'))
            <!--
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?');",  'method' => 'DELETE', 
                                  'route' => array('admin.merchants.destroy', $merchant->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
            -->
            @endif
            <a href="{{ route('admin.merchants.show', array($merchant->id) )}}" class="btn btn-xs btn-success">
              <i class="fa fa-eye"></i>
            </a>
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
  {{ $merchants->appends($params)->render() }}
@else
	There are no merchants
@endif
</div></div>
<script type="text/javascript">
$(function(){
  $('.toggle-button').bootstrapToggle( 
                                  { 
                                    size:'mini',
                                    on: 'Enabled',
                                    off: 'Disabled'                                    
                                    } );
  $('.toggle-button.disable').bootstrapToggle('disable');

});
</script>
@stop