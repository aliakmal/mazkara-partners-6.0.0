<li class="list-group-item">
  <div class="row">
    <div class="col-md-5">
      {{ $business['name'] }} {{ Form::hidden('businesses[]', $business['id']) }}
    </div>
    <div class="col-md-6">
      {{ Form::text('business_emails[]', $business['preferred_email'], 
                              [ 'class'=>'form-control', 
                                'placeholder'=>'Preferred Emails?']) }}
    </div>
    <div class="col-md-1">
      {{Form::checkbox('deletableBusinesses[]', $business['id'], '', ['class'=>'deletable','style'=>'display:none'] )}}<a href="javascript:void(0)" class="deletable-link"><i class="fa fa-times"></i></a>
    </div>
  </div>
</li>