{{ Form::open(array('route' => 'admin.merchants.access', 'class' => 'form-horizontal')) }}
<h4>Mobile Module Access</h4>
<div class="form-group">
  {{ Form::label('user_name', 'User:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-4">
    {{ $user->full_name }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('merchant_name', 'Merchant:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-4">
    {{ $merchant->name }}
  </div>
</div>
{{ Form::hidden('merchant_user_id', $merchant_user->id) }}

<strong>Modules</strong>
<hr/>
@foreach($modules as $module)
<div class="form-group">
  {{ Form::label('module_name', $module->name, array('class'=>'col-md-2 control-label')) }}

  {{ Form::hidden('modules['.$module->id.'][id]', is_object($modules_access[$module->id])?$modules_access[$module->id]->id:0) }}
  {{ Form::hidden('modules['.$module->id.'][module_id]', $module->id) }}
  {{ Form::hidden('modules['.$module->id.'][merchantUser_id]', $merchant_user->id) }}
  <div class="col-sm-4">
    {{ Form::select('modules['.$module->id.'][access]', 
                    ['1'=>'No Access', '2'=>'Read Only', '3'=>'Full Access'], 
                    (is_object($modules_access[$module->id])?$modules_access[$module->id]->access:1), 
                    array('class'=>'form-control', 'id'=>'business-search')) }}
  </div>
</div>
@endforeach
<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}