@extends('layouts.admin-crm')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Import resumes</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
  @if (Session::get('message'))
    <div class="alert alert-success">
      {{ is_array(Session::get('message'))?join(',', Session::get('message')):Session::get('message') }}
    </div>
  @endif

    </div>
</div>

{{ Form::open(array('route' => 'crm.resumes.post.import', 'files'=>true, 'class' => 'form-horizontal')) }}
  <div class="form-group">
    {{ Form::label('csv', 'CSV resumes:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::file('csv',  array('class'=>'form-control', 'placeholder'=>'Name')) }}
      <small>Make sure the csv has the fields Name, Phone, Email, interested_in</small>
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('businesses', 'Click to allocate resumes to businesses:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('businesses[]', $businesses,'',  array('class'=>'form-control', 'multiple'=>true)) }}
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}

@stop

