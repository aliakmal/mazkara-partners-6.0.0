@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="pull-right btn-group">{{ link_to_route('admin.resumes.create', 'Add New resume', null, array('class' => 'btn  btn-success')) }} <!-- {{ link_to_route('crm.resumes.get.import', 'Import resumes', null, array('class' => 'btn  btn-default')) }} --></div>
    <h1>All Resumes</h1>
    @include('elements.messages')
    <div class="well">
    <form method="GET" class="form-inline">
      {{ Form::text('search', isset($params['search'])?$params['search']:'', array('class'=>'form-control', 'placeholder'=>'Name') ) }}&nbsp;
      {{ Form::text('location', isset($params['location'])?$params['location']:'', array('class'=>'form-control', 'placeholder'=>'Location') ) }}&nbsp;
      {{ Form::select('nationality', $nationalities, isset($params['nationality'])?$params['nationality']:'', array('class'=>'form-control', 'placeholder'=>'Nationality?') ) }}&nbsp;
      {{ Form::select('specialization', $specializations, isset($params['specialization'])?$params['specialization']:'', array('class'=>'form-control', 'placeholder'=>'Specialization?') ) }}&nbsp;
      <button class="btn btn-sm btn-primary">Filter</button>
      <a href="?" class="btn btn-sm btn-default">Reset</a>
    </form>
  </div>

    @if ($resumes->count())
    	<table class="table table-striped">
    		<thead>
    			<tr>
            <th>ID</th>
    				<th>Name</th>
            <th>Phone</th>
            <th>Location</th>
            <th>Nationality</th>
            <th>Specialization</th>
            <th>Resume</th>
            <th>Created at</th>
    				<th>&nbsp;</th>
    			</tr>
    		</thead>

    		<tbody>
    			@foreach ($resumes as $resume)
    				<tr>
              <td>{{ $resume->id }}</td>
              <td>{{{ $resume->name }}}</td>
              <td>{{{ $resume->phone }}}</td>
              <td>{{{ $resume->location }}}</td>
              <td>{{{ mzk_get_country_from_list($resume->nationality) }}}</td>
              <td>{{{ join(', ', $resume->specializations->lists('specialization', 'specialization')->all()) }}}</td>
              <td><a href="{{{ $resume->doc->url() }}}" download type="application/octet-stream"><i class="fa fa-download"></i></a></td>
              <td>{{{ $resume->created_at }}}</td>
              <td>
                <a href="{{ route('admin.resumes.show', array($resume->id)) }}" class='btn btn-success  btn-xs'>
                  <i class="fa fa-eye"></i>
                </a>&nbsp;
                {{ link_to_route('admin.resumes.edit', 'Edit', array($resume->id), array('class' => 'btn btn-info  btn-xs')) }}
                {{ Form::open(array('style' => 'display: inline-block;', 
                                    'method' => 'DELETE', 'onsubmit'=>"return confirm('Are you sure you want to delete this resume?')",
                                    'route' => array('admin.resumes.destroy', $resume->id))) }}
                    &nbsp;{{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
                {{ Form::close() }}
              </td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
      <!--
      {{ Form::open(array('route' => 'crm.resumes.attach', 'method'=>'POST', 'class' => 'form-horizontal')) }}
        <div class="well">
          <div class="row">
            <div class="col-md-4">
              {{ Form::select('business', $businesses,'',  array('class'=>'form-control')) }}
              {{ Form::hidden('resume_ids', '',  array('class'=>'form-control', 'id'=>'resume_ids_list')) }}
            </div>
            <div class="col-md-3">
              <a class="btn btn-sm btn-default" id="lnk-attach">ATTACH</a>
            </div>
          </div>
        </div>
      {{ Form::close() }}
    -->

  {{ $resumes->appends($params)->render() }}

    @else
    	There are no resumes
    @endif
  </div>
</div>
<!--<script type="text/javascript">
  $(function(){
    $('#lnk-attach').click(function(){
      resume_ids = $('input.chk_resume_id:checked').map(function() {
          return this.value;
        }).get();
      $('#resume_ids_list').val(resume_ids.join(','));
      $(this).parents('form').first().submit();

    });
  });
</script>-->

@stop