
@if(isset($resume))
  {{ Form::model($resume, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.resumes.update', $resume->id))) }}
@else
  {{ Form::open(array('route' => 'admin.resumes.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif
  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('phone', 'Phone:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('phone', Input::old('phone'), array('class'=>'form-control', 'placeholder'=>'Phone')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('location', 'Location:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('location', Input::old('location'), array('class'=>'form-control', 'placeholder'=>'Location')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('salary', 'Salary:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('salary', Input::old('salary'), array('class'=>'form-control', 'placeholder'=>'Salary')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('nationality', 'Nationality:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('nationality', $nationalities, Input::old('nationality'), array('class'=>'form-control', 'placeholder'=>'Nationality')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('experience', 'Experience:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('experience', $experiences, Input::old('experience'), array('class'=>'form-control', 'placeholder'=>'Select Experience')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('specializations', 'Specialization:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('specializations[]', $specializations, (isset($resume)?$resume->specializations->lists('specialization','specialization')->all():Input::old('specializations')), array('class'=>'form-control', 'id'=>'specializations', 'multiple'=>true)) }}
    </div>
  </div>


  <div class="form-group">
    {{ Form::label('doc', 'Upload Resume:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::file('doc', array('class'=>'form-control', 'placeholder'=>'Upload Resume')) }}
      @if(isset($resume) && $resume->hasCVAttached())
      <br/>
        <a href="{{ $resume->doc->url()}}">Download Current Resume</a>
      @endif
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($resume))
        {{ link_to_route('admin.resumes.show', 'Cancel', $resume->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>
  </div>
{{ Form::close() }}
<script type="text/javascript">
$(function(){
  $('#specializations').selectpicker({
    style: 'btn-default',
    size: 8
  });

});

</script>