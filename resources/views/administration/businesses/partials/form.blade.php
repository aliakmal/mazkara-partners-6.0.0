@if(isset($business))
    {{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'PATCH', 'files'=>true, 'route' => array('admin.businesses.update', $business->id))) }}
@else
{{ Form::open(array('route' => 'admin.businesses.store','files'=>true, 'class' => 'form-horizontal')) }}

@endif
        <div class="form-group">
            {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('images', 'Images(s):', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-default btn-xs btn-danger" id="lnk-add-images">Add Image(s)</a>
                <div id="images-here">{{ Form::file('images[]', array( 'accept'=>"image/*", 'capture'=>'camera')) }}</div>
               
            </div>
            @if(isset($business))

            <div class="row">
                <div class="col-sm-10 col-sm-offset-2">
                @foreach($business->photos as $one_photo)
                <div class="content col-sm-2">
                    <img src="{{ $one_photo->image->url('thumbnail') }}" class="img-thumbnail" />
                    <p>
                        {{ Form::checkbox("deletablePhotos[]", $one_photo->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
                        <a href="javascript:void(0)" class="delete-existing btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                    </p>

                </div>
                @endforeach
                </div>
            </div>
            @endif

        </div>

        <div class="form-group">
            {{ Form::label('categories', 'Categories:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <ul class="list-unstyled" >
                    @foreach($categories as $category)
                        <li>
                            {{ Form::checkbox('categories[]', $category->id, in_array($category->id, $selected_categories) ) }}
                            {{ $category->name }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('highlights', 'Highlights:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <ul class="list-unstyled" >
                    @foreach($highlights as $highlight)
                        <li>
                            {{ Form::checkbox('highlights[]', $highlight->id, in_array($highlight->id, $selected_highlights) ) }}
                            {{ $highlight->name }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('services', 'Services:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <ul id="services-list">
                    @foreach($services as $service)
                        <li>
                            {{ Form::checkbox('services[]', $service->id, in_array($service->id, $selected_services) ) }}
                            {{ $service->name }}
                            @if(count($service->children) >0)
                                <ul>
                                    @foreach($service->children as $child)
                                    <li>
                                        {{ Form::checkbox('services[]', $child->id ) }}
                                        {{ $child->name }}
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>


        <div class="form-group">
            {{ Form::label('phone', 'Phone:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('phone', Input::old('phone'), array('class'=>'form-control', 'placeholder'=>'Phone')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('website', 'Website:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('website', Input::old('website'), array('class'=>'form-control', 'placeholder'=>'Website')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('facebook', 'Facebook:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('facebook', Input::old('facebook'), array('class'=>'form-control', 'placeholder'=>'Website')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('twitter', 'Twitter:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('twitter', Input::old('twitter'), array('class'=>'form-control', 'placeholder'=>'Website')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('google', 'Google Plus:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('google', Input::old('google'), array('class'=>'form-control', 'placeholder'=>'Website')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('chain_id', 'Chain Of:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('chain_id', ( array(0=>'None') + Business::lists('name', 'id')->all()), Input::old('chain_id'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('rate_card', 'Rate Card:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-default btn-xs btn-danger" id="lnk-add-rate-cards">Add Rate Card(s)</a>
                <div id="rate-cards-here">{{ Form::file('rate_card[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</div>
                @if(isset($business))
                    <div class="row">
                        @foreach($business->rateCards as $one_card)
                            <div class="content col-sm-2">
                            <img src="{{ $one_card->image->url('thumbnail') }}" class="img-thumbnail" />
                                <p>
                                    {{ Form::checkbox("deletablePhotos[]", $one_card->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
                                    <a href="javascript:void(0)" class="delete-existing btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                </p>
                            </div>
                        @endforeach
                    </div>
                @endif

                <!-- {{ Form::file('rate_card[]', array('multiple'=>true,  'accept'=>"image/*", 'capture'=>'camera')) }} -->
            </div>
        </div>


      {{ Form::input('hidden', 'geolocated', Input::old('geolocated'), array('class'=>'form-control')) }}

        <div class="form-group">
            {{ Form::label('zone_id', 'Zone_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('zone_id', Zone::lists('name', 'id')->all(), Input::old('zone_id'), array('class'=>'form-control')) }}
            </div>
        </div>

        <fieldset>
            <legend>Timings</legend>
            <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-default btn-xs btn-danger" id="lnk-add-timings">Add Timing(s)</a>
            <div id="timings-here"></div>
            @if(isset($business))
                @foreach($business->timings as $timing)
                    @include('administration.businesses.partials.timings')
                    <?php unset($timing);?>
                @endforeach
            @endif
        </fieldset>


        <fieldset>
            <legend>Location</legend>
            <div class="row">
                <div class="col-md-6">
                    <div id="map" style="height:400px">
                    </div> 
                </div>
                <div class="col-md-6 text-left" >
                    <div class="form-group">
                        <a href="javascript:void(0)" id="link-get-users-location" class=" btn btn-info">
                            Get Current Location
                        </a>
                    </div>
                    <div class="form-group">
                        {{ Form::label('landmark', 'Landmark:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::textarea('landmark', Input::old('landmark'), array('class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('geolocation_latitude', 'Latitude:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::text('geolocation_latitude', Input::old('geolocation_latitude'), array('class'=>'form-control', 'placeholder'=>'Geolocation_latitude')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('geolocation_longitude', 'Longitude:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::text('geolocation_longitude', Input::old('geolocation_longitude'), array('class'=>'form-control', 'placeholder'=>'Geolocation_longitude')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('geolocation_address', 'Street:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::text('geolocation_address', Input::old('geolocation_address'), array('class'=>'form-control', 'placeholder'=>'Geolocation_address')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('geolocation_city', 'City:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::text('geolocation_city', Input::old('geolocation_city'), array('class'=>'form-control', 'placeholder'=>'Geolocation_city')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('geolocation_state', 'State:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::text('geolocation_state', Input::old('geolocation_state'), array('class'=>'form-control', 'placeholder'=>'Geolocation_state')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('geolocation_country', 'Country:', array('class'=>'col-md-3 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::text('geolocation_country', Input::old('geolocation_country'), array('class'=>'form-control', 'placeholder'=>'Geolocation_country')) }}
                        </div>
                    </div>


                </div>
            </div>            
        </fieldset>
        <hr />
        <div class="form-group">
            {{ Form::label('active', 'Active:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('active', Business::activeStates(), Input::old('active'), array('class'=>'form-control')) }}

            </div>
        </div>
<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
<div id="rate_card_holder" style="display:none"><p>{{ Form::file('rate_card[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</p></div>
<div id="image_holder" style="display:none"><p>{{ Form::file('images[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</p></div>
<div id="timing_holder"  style="display:none" >

@include('administration.businesses.partials.timings')
</div>
<script>
$(function(){

    $(document).on('click', '.toggles .btn', function(){
        inp = $(this).find('input');
        hd = $(this).parents('.toggles').first().find('input:hidden');
        if(!$(this).hasClass('active')){
            $(hd).val( S($(hd).val() + $(inp).val()).trim() );
        }else{
            $(hd).val( S(S((hd).val()).replaceAll($(inp).val(), '')).trim() );
        }

    });

    
    $(document).on('click', '.delete-existing', function(){
        $(this).parents('.content').first().hide();
        $(this).parents('.content').first().find('.deletable').prop('checked', true);
    });

    $(document).on('click', '#timings-here .delete', function(){
        $(this).parents('.content').first().remove();

    });

    $(document).on('focus', '.timepicker', function(){
        $(this).timepicker({ timeFormat: 'H:i' });
    })


    $('#lnk-add-images').click(function(){
        $('#images-here').prepend($('#image_holder').html());

    });


    $('#lnk-add-timings').click(function(){
         html = $('#timing_holder').html();
         html = S(html).replaceAll('_REPLACE_', new Date().getTime());
        $('#timings-here').prepend(html.s);
       
    });


    $('#lnk-add-rate-cards').click(function(){
        $('#rate-cards-here').prepend($('#rate_card_holder').html())
    })

    
    $('#services-list').bonsai({
      expandAll: true,
      checkboxes: true, 
      handleDuplicateCheckboxes: true 
    });


    geocoder = new google.maps.Geocoder()
    var setBusinessPosition = function(position){
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        $('#geolocation_latitude').val(lat).change();
        $('#geolocation_longitude').val(lng).change();
        lc = new google.maps.LatLng(lat, lng);

        var mapContext = business_map.locationpicker('map');
        mapContext.marker.setPosition(lc);
        mapContext.map.panTo(lc);

        geocoder.geocode({
                    latLng: lc
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        mapContext.locationName = results[0].formatted_address;
                        mapContext.addressComponents = get_address_component_from_google_geocode(results[0].address_components);
                        updateControls(mapContext.addressComponents);
                    }
                })

    };
    var noLocation = function(){

    };

    var updateControls = function(addressComponents) {
        $('#geolocation_address').val(addressComponents.addressLine1);
        $('#geolocation_city').val(addressComponents.city);
        $('#geolocation_state').val(addressComponents.stateOrProvince);
        /*/$('#us5-zip').val(addressComponents.postalCode);*/
        $('#geolocation_country').val(addressComponents.country);

    };

    var business_map = $('#map').locationpicker({
        radius: 0,
        @if(isset($business))
            location: { latitude:<?php echo $business->geolocation_latitude;?>, longitude:<?php echo $business->geolocation_longitude;?> },
        @endif
        enableAutocomplete: false,
        inputBinding: {
            latitudeInput: $('#geolocation_latitude'),
            longitudeInput: $('#geolocation_longitude'),
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            updateControls(addressComponents);
            $('#geolocated').val(1);            
        },
    });
    $('#link-get-users-location').click(function(){
        $.geolocation.get({win: setBusinessPosition, fail: noLocation});
    })
})

</script>