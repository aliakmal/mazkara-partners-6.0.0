{{ Form::open(['url'=>route('admin.business.bulk.cheat.rate', $business->id)])}}
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Rating</h3>
  </div>
  <div class="panel-body">
    <table class="table" style="width:30%" >
      <thead>
      </thead>
      <tbody>
      @for($i=5; $i>0.5; $i-=0.5)
      <tr>
        <td>{{(string)$i}}</td>
        <td class="" style="width:50%">
          <input name="ratings[{{$i}}]" class="cheat-rates-for-{{$i}} input-cheat-rate form-control" rel="{{$i}}" type="text" value="{{ Review::query()->isCheatRate()->byRating($i)->byBusiness($business->id)->get()->count() }}" />
        </td>
      </tr>
      @endfor
      </tbody>
    </table>

    <dl class="dl-horizontal">
      <dt>Total Cheat Votes</dt>
      <dd id="total-votes-holder">{{ $business->accumulatedReviewsCount() }}</dd>
      <dt>Average Rating</dt>
      <dd id="average-rating-holder">{{ $business->rating_average }}</dd>
    </dl>
  </div>
  <div class="panel-footer">
    <input class="btn btn-primary" type="submit" value="SAVE" />
    <a id="reset-cheat-rates" href="javascript:void(0)" class="btn btn-default">RESET</a>
  </div>

</div>
{{ Form::close() }}


<script type="text/javascript">

$(function(){

  resetAverageCheatRates = function(){
    inputs = $('.input-cheat-rate');
    avg = 0;
    cnt = 0;
    for(i=0;i<inputs.length;i++){
      rt = Number($(inputs[i]).attr('rel'));
      if($(inputs[i]).val() > 0){
        avg = avg + (Number($(inputs[i]).val()) * rt);
        cnt = Number($(inputs[i]).val()) + cnt;
      }
    }

    avg = avg/cnt;

    $('#total-votes-holder').html(cnt);
    $('#average-rating-holder').html(avg);
  };

  resetAverageCheatRates();

  $('#reset-cheat-rates').click(function(){
    $('.input-cheat-rate').val('0');
    $(this).parents('form').first().submit();
  });

  $('.input-cheat-rate').blur(function(){
    resetAverageCheatRates();
  });

   $('.lnk-add-cheat-rate').click(function(){
    dt = { 
            business_id:{{$business->id}}, 
            rating:$(this).attr('rel') 
          };
    $.ajax({
      url:'/content/businesses/rate/cheat', 
      type:'GET',
      data:dt
    }).done(function(data){
      $('.cheat-rates-for-' + data['rating']).html(data['num-rating']);
      console.log($('.cheat-rates-for-' + data['rating']));
      $('#total-votes-holder').html(data['count']);
      $('#average-rating-holder').html(data['average']);
    });
 });

   $('.lnk-del-cheat-rate').click(function(){
    dt = { 
            business_id:{{$business->id}}, 
            rating:$(this).attr('rel') 
          };
    $.ajax({
      url:'/content/businesses/rate/uncheat', 
      type:'GET',
      data:dt
    }).done(function(data){
      $('.cheat-rates-for-' + data['rating']).html(data['num-rating']);
      $('#total-votes-holder').html(data['count']);
      $('#average-rating-holder').html(data['average']);
    });
 });

});
</script>