
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-warning">
      <div class="panel-heading">
        <h3 class="panel-title">Offers</h3>
      </div>
      <div class="panel-body">
      {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()
                    ->action(route('admin.offers.store'))
                    ->encodingType('multipart/form-data') }}

        @if (Session::get('error'))
          <div class="alert alert-error alert-danger">
          {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
          </div>
        @endif
        @if (Session::get('notice'))
          <div class="alert alert-success">
          {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
          </div>
        @endif
        {{ BootForm::textarea('Body', 'title') }}
        {{ BootForm::select('Tag', 'tag', Offer::tagsList()) }}
        {{ BootForm::text('Prepend Price', 'price_meta')->placeholder('Eg. 40%, Before '.mzk_currency_symbol().' 100') }}
        {{ BootForm::text('Price', 'offer_price')->placeholder('Price here') }}
        {{ BootForm::date('Valid Until', 'valid_until')->placeholder('Valid until when?') }}
        {{ BootForm::select('State', 'state', Offer::activeStates()) }}

        {{ BootForm::hidden('business_id', '')->value($business->id) }}

        {{ BootForm::token() }}
        {{ BootForm::submit('Save') }}
      {{ BootForm::close() }}
      </div>
    </div>
  </div>
</div>
<div class="row">

  <div class="col-md-12 " style="font-size:12px;">
  <table class="table table-striped">
    @foreach($business->offers as $offer)
      <tr>
        <td>
          <a href="javascript:void(0)"  class="inline-editable" id="body" data-type="textarea" data-pk="{{$offer->id}}" data-url="{{ route('admin.offers.items.update') }}" data-title="Text">{{ $offer->body }}</a>

        </td>
        <td>
          <a href="javascript:void(0)"  class="tag-editable" id="tag" data-type="select" data-pk="{{$offer->id}}" data-url="{{ route('admin.offers.items.update') }}" data-title="select tag">
            {{ $offer->tag }}
          </a>
        </td>
        <td>
          <a href="javascript:void(0)"  class="state-editable" id="state" data-type="select" data-pk="{{$offer->id}}" data-url="{{ route('admin.offers.items.update') }}" data-title="select tag">
            {{ $offer->state }}
          </a>
        </td>
        <td>
          <a href="javascript:void(0)"  class="inline-editable" id="price" data-type="text" data-pk="{{$offer->id}}" data-url="{{ route('admin.offers.items.update') }}" data-title="Price">
            {{{ $offer->price }}}
          </a>
        </td>
        <td>
          <a href="javascript:void(0)"  class="inline-editable" id="price_meta" data-type="text" data-pk="{{$offer->id}}" data-url="{{ route('admin.offers.items.update') }}" data-title="Price meta">
            {{ $offer->price_meta }}
          </a>
        </td>
        <td>
          <a href="javascript:void(0)"  class="inline-editable" id="valid_until" data-type="date" data-pk="{{$offer->id}}" data-url="{{ route('admin.offers.items.update') }}" data-title="Valid until">
            {{{ $offer->valid_until }}}
          </a>
        </td>
        <td>
          {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.offers.destroy', $offer->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
          {{ Form::close() }}
        </td>
      </tr>
    @endforeach
  </table>
  </div>
</div>
<script type="text/javascript">

$(function(){
  $(document).ready(function() {
    $('.inline-editable').editable();

    $('.tag-editable').editable({
        value: 'hot',
        source: {{ json_encode(Offer::tagsForEditable()) }}
    });

    $('.state-editable').editable({
        value: 'active',
        source: {{ json_encode(Offer::activeStatesForEditable()) }}
    });

    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    return $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
  });

})
</script>