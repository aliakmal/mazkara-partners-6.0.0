<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Comments</h3>
  </div>
  <div class="panel-body">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.comments.store'))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
    {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
    {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::textarea('Comment', 'body')->placeholder('Talk stuff?') }}
  {{ BootForm::select('Type of Comment', 'type', Comment::getTypes()) }}

  {{ BootForm::hidden('commentable_type', '')->value($commentable_type) }}
  {{ BootForm::hidden('commentable_id', '')->value( $commentable_id) }}

  {{ BootForm::token() }}
  {{ BootForm::submit('Comment') }}
{{ BootForm::close() }}
</div>
</div>
@foreach($comments as $comment)
<div class="well bg-white bb">
  <div class="p5">
    {{ $comment->body }}
  </div>
  <div class="pull-right">
    {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.comments.destroy', $comment->id))) }}
      {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
  {{ Form::close() }}
  </div>
  <div class="p5 pt0">
    @if($comment->type == 'bug')
        <span class="label label-danger"><i class="fa fa-bug"></i> BUG</span>
    @elseif($comment->type == 'callback')
        <span class="label label-default"><i class="fa fa-phone"></i> CALL BACK</span>
    @elseif($comment->type == 'followup')
        <span class="label label-info"><i class="fa fa-hand-o-left"></i> FOLLOW UP</span>
    @elseif($comment->type == 'inaccurate')
        <span class="label label-warning"><i class="fa fa-warning"></i> INACCURATE</span>
    @endif
    - <small>
    <b>{{ $comment->user->full_name }}</b> posted {{{ Date::parse($comment->updated_at)->ago() }}}</small>
  </div>
</div>
@endforeach
