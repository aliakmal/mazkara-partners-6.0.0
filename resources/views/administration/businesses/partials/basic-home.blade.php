@if(isset($business))

{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@postBasic', $business->id))) }}

@else
{{ Form::open(array('route' => 'admin.businesses.store','files'=>true, 'class' => 'form-horizontal', 'id'=>'form-add-business')) }}

@endif



  <div class="form-group">
    {{ Form::label('ref', 'Slip ID:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('ref', Input::old('ref'), array('class'=>'form-control', 'placeholder'=>'Reference - slip number(optional)')) }}

    </div>
  </div>
  <div class="form-group">
    {{ Form::label('lot_id', 'Lot ID:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('lot_id', Input::old('lot_id'), array('class'=>'form-control', 'placeholder'=>'Lot number(optional)')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'required'=>true,  'placeholder'=>'Name', 'maxlength'=>60)) }}
      <small>(maximum length 60 characters)</small>
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('cover', 'Cover:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::file('cover', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
      @if(isset($business))
        @if($business->hasCover())
          <a href="{{ $business->getCoverUrl() }}" class="lightbox"><img src="{{ $business->getCoverUrl('thumbnail') }}" class="img-thumbnail" /></a>

        @endif

      @endif

    </div>
  </div>
    {{ Form::hidden('type', 'home-service') }}

  <!-- <div class="form-group">
    {{ Form::label('type', 'Business Type:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('type', Business::getTypes(), Input::old('type'), array('class'=>'form-control', 'placeholder'=>'Cost Estimate?')) }}
    </div>
  </div> -->


  <div class="form-group">
    {{ Form::label('phone', 'Phone:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10 input_fields_wrap">
      <div class="input-group">
      {{ Form::text('phone[]', '', array('class'=>'form-control', 'placeholder'=>'Phone')) }}
      <span class="input-group-btn">
        <a href="javascript:void(0)" class="btn btn-default add-btn" rel="phone"><i class="fa fa-plus"></i></a>
      </span>
    </div>
      @if(isset($business))
        @foreach($business->phone as $phone)
          <div class=" pb5 input-group">
          {{ Form::text('phone[]', $phone, array('class'=>'form-control', 'placeholder'=>'Phone')) }}
          <span class="input-group-btn">

          <a href="#" class="remove_field  btn btn-danger"><i class="fa fa-times"></i></a>
        </span>
        </div>
        @endforeach
      @endif
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10 input_fields_wrap">
      <div class="input-group pb5">
      {{ Form::text('email[]', '', array('class'=>'form-control', 'placeholder'=>'Email')) }}
      <span class="input-group-btn">
        <a href="javascript:void(0)" class="btn btn-default add-btn" rel="email"><i class="fa fa-plus"></i></a>
      </span>
    </div>
      @if(isset($business))
        @foreach($business->email as $email)
          <div class=" pb5 input-group">
          {{ Form::text('email[]', $email, array('class'=>'form-control', 'placeholder'=>'Email')) }}
          <span class="input-group-btn">

          <a href="#" class="remove_field  btn btn-danger"><i class="fa fa-times"></i></a>
        </span>
        </div>
        @endforeach
      @endif
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('website', 'Website:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('website', Input::old('website'), array('class'=>'form-control', 'placeholder'=>'Website')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('facebook', 'Facebook:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('facebook', Input::old('facebook'), array('class'=>'form-control', 'placeholder'=>'facebook')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('twitter', 'Twitter:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('twitter', Input::old('twitter'), array('class'=>'form-control', 'placeholder'=>'twitter')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('instagram', 'Instagram:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('instagram', Input::old('instagram'), array('class'=>'form-control', 'placeholder'=>'instagram')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('google', 'Google Plus:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('google', Input::old('google'), array('class'=>'form-control', 'placeholder'=>'google')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('cost_estimate', 'Cost Estimate:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('cost_estimate', Business::getCostOptions(), Input::old('cost_estimate'), array('class'=>'form-control', 'placeholder'=>'Cost Estimate?')) }}
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">Set if this is a Chain</div>
    <div class="panel-body">
      <div class="form-group">
        {{ Form::label('chain_id', 'Chain Of:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-7">
          {{ Form::select('chain_id',  (array(0=>'None') + Group::byLocale()->get()->lists('label', 'id')->all()), Input::old('chain_id'), array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-3">
          {{ link_to_route('admin.chains.create', 'Add New Chain/Group', null, array('target'=>'_blank', 'class' => 'btn  btn-default')) }}

        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
      {{ Form::label('zone_id', 'Sub Zone:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        <select id="zone_id" name="zone_id" multiple="multiple" class="form-control" required='true'>
          $html.='<option value="" selected disabled>Select a Sub zone</option>';
          <?php $html = '';?>
          @foreach ($zones as $zone)
          <?php 
            if($zone->depth <= 1){
              $html.= (($html=='')?'':'</optgroup>').'<optgroup label="'.$zone->stringPath().'">';
            }else{
              //if($zone->isActive())
              {
                $html.='<option value="'.$zone->id.'" '.($zone->id == (isset($business) ? $business->zone_id : Input::old('zone_id'))? ' selected="selected" ':'' ).'>'.$zone->name.'</option>';
              }
            }
          ?>

          @endforeach
          {{ $html.'</optgroup>'}}

        </select>

      </div>
  </div>


  <div class="form-group">
    {{ Form::label('categories', 'Categories:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      <ul class="list-unstyled categories-list" >
        @foreach($categories as $category)
          <li>
            {{ Form::checkbox('categories[]', $category->id, in_array($category->id, $selected_categories) ) }}
            {{ $category->name }}
          </li>
        @endforeach
      </ul>
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('active', 'Active:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('active', Business::activeStates(), Input::old('active'), array('class'=>'form-control')) }}

    </div>
  </div>

  <div class="form-group">
      <label class="col-sm-2 control-label">&nbsp;</label>
      <div class="col-sm-10">
        {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      </div>
  </div>

{{ Form::close() }}
<script>
$(function(){

@if(!isset($business))
  $('#form-add-business').submit(function(){
    if($('.categories-list input:checked').length == 0){
      alert('You must select atleast one category!');
      return false;
    }

    if($('input#ref').val() == ''){
      return confirm('Are you sure you want to add without a Slip ID?');
    }else{
      return true;
    }



  });

@endif

    $(document).on('click', '.toggles .btn', function(){
        inp = $(this).find('input');
        hd = $(this).parents('.toggles').first().find('input:hidden');
        if(!$(this).hasClass('active')){
            $(hd).val( S($(hd).val() + $(inp).val()).trim() );
        }else{
            $(hd).val( S(S((hd).val()).replaceAll($(inp).val(), '')).trim() );
        }

    });
    
    $('#services-list').bonsai({
      expandAll: true,
      checkboxes: true, // depends on jquery.qubit plugin
      handleDuplicateCheckboxes: true // optional
    });

    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add-btn"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(this).parents(".input_fields_wrap").first().append('<div class="pb5 input-group"><input type="text" class="form-control" name="'+$(this).attr('rel')+'[]"/><span class="input-group-btn"><a href="#" class="remove_field btn btn-danger"><i class="fa fa-times"></i></a></span></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('.input-group').first().remove(); x--;
    })


})

</script>