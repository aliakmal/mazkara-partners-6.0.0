<div class="well">
  <span class="btn-group">
    @if($business->has_sample_menu == 0)
      <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}?return=back" class="btn btn-xs btn-success">ACTUAL MENU</a>
      <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}?return=back" class="btn btn-xs btn-default">CLICK TO CHANGE</a>
    @else
      <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}?return=back" class="btn btn-xs btn-danger">SAMPLE MENU</a>
      <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}?return=back" class="btn btn-xs btn-default">CLICK TO CHANGE</a>
    @endif
  </span>
</div>

{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'onsubmit'=>'return false;', 'action' => array('Administration\\BusinessesController@postServices', $business->id))) }}
<div class="form-group">
  <div class="col-sm-4">

    <!-- Nav tabs -->
    <ul class="nav nav-pills nav-stacked" role="tablist">
      <?php $allowed_services = $services->lists('id', 'id')->all();?>
      @foreach(Service::query()->showParents()->get() as $ii=>$service)
        <li role="presentation" class="{{ $ii >0 ? '' : 'active' }}">
          <a href="#{{ $service->slug }}" aria-controls="{{ $service->slug }}" role="tab" data-toggle="tab">{{ $service->name }}</a>
        </li>
      @endforeach
    </ul>
    <p>&nbsp;</p>
    <button class="btn btn-lg btn-primary">Save</button>
  </div>
  <div class="col-sm-8">
    <div class="tab-content">
      @foreach(Service::query()->showParents()->get() as $ii=>$service)
        <div role="tabpanel" class="tab-pane {{ $ii >0 ? '' : 'active' }}" id="{{ $service->slug }}">
          <table class="table">
            @foreach($service->children as $child)
              @if(in_array($child->id, $allowed_services))
                <tr>
                  <td>
                    {{ Form::checkbox('services['.$child->id.']', $child->id, in_array($child->id, $selected_services), ['class'=>'main-selector', 'ref'=>'sp-'.$child->id] ) }}
                    {{ $child->name }}
                  </td>
                  <td>
                    {{ Form::text('starting_prices['.$child->id.']', (isset($selected_service_prices[$child->id])?$selected_service_prices[$child->id]:''), ['class'=>'form-control '.'sp-'.$child->id, 'id'=>'sp-'.$child->id] ) }}
                  </td>
                  <td>
                    {{ Form::checkbox('known_for['.$child->id.']', 1, (isset($selected_known_for[$child->id])?$selected_known_for[$child->id]:false), ['class'=>'form-control sp-'.$child->id] ) }} Known for
                  </td>
                </tr>
              @endif
            @endforeach
          </table>
        </div>
      @endforeach
    </div>
  </div>
</div>


{{ Form::close() }}
<script>
$(function(){
  $('form button').click(function(){

    chks = $('input.main-selector[type=checkbox]:checked');
    services = [];
    prices = [];
    known_for = [];

    test_empty = [];

    $(chks).each(function(){
      nm = $(this).attr('ref');
      vl = $(this).val();
      if($('#'+nm).val() == ''){
        test_empty.push(1);
      }
      prices.push($('#'+nm).val());
      kf_chk = $('input.'+nm+'[type=checkbox]').first();

      known_for.push(kf_chk[0].checked?1:0);

      services.push(vl);
    });
    if(test_empty.length>0){
      @if($business->has_sample_menu == 0)
        alert('You left some starting prices as null - this is not a sample menu?');
        return false;
      @else
        if(confirm('You left some starting prices as null, are you sure?')==false){
          return false;
        }
      @endif
    }

    _btn = $(this);
    _btn.html('Saving...');

    $.ajax({
      url: '/content/businesses/{{$business->id}}/ajax/services',
      method: 'POST',
      data: {
        services:services,
        starting_prices:prices,
        known_for:known_for
      },
    }).success(function(r){
      _btn.html('Save');

    }).error(function(){
      _btn.html('Save');
      alert('Something went wrong. Try again in a bit.');
    });

  });
  $('input.main-selector[type=checkbox]').click(function(){
    if(this.checked){
      $('.'+$(this).attr('ref')).removeAttr('disabled');
    }else{
      $('.'+$(this).attr('ref')).prop('disabled', 'true');
    }
  });

  $('input.main-selector[type=checkbox]').each(function(){
    if(this.checked){
      $('.'+$(this).attr('ref')).removeAttr('disabled');
    }else{
      $('.'+$(this).attr('ref')).prop('disabled', 'true');
    }
  });


});

</script>