{{ Form::model($business, array('class' => 'form-horizontal', 
              'method' => 'POST', 'files'=>true, 
              'action' => array('Administration\\BusinessesController@postRateCards', 
                                $business->id))) }}
  <div class="panel panel-default">
    <div class="panel-heading">Add Rate Card(s) - one at a time, camera friendly</div>
    <div class="panel-body">
      <div class="col-sm-10">
        <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-default btn-sm btn-info" id="lnk-add-rate-cards">Add Rate Card(s)</a>
        <div class="well" id="rate-cards-here">{{ Form::file('rate_card[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</div>
      </div>
    </div>
    <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}
{{ Form::model($business, array('class' => 'form-horizontal', 
              'method' => 'POST', 'files'=>true, 
              'action' => array('Administration\\BusinessesController@postRateCards', 
                                $business->id))) }}
    <div class="panel panel-default">
      <div class="panel-heading">Add Rate Card(s) - Bulk</div>
      <div class="panel-body">
        {{ Form::file('rate_card[]', array( 'accept'=>"image/*", 'multiple'=>'true')) }}
      </div>
      <div class="panel-footer">
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
      </div>
    </div>
{{ Form::close() }}

{{ Form::model($business, array('class' => 'form-horizontal', 
              'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@postRateCards', $business->id))) }}
<div class="panel panel-default">
    <div class="panel-heading">Edit Rate Card(s)</div>
    <div class="panel-body">

<div class="form-group">
  <div class="row">
    <div class="col-sm-10 ">
      @foreach($business->rateCards as $one_card)
        <div class="content col-sm-3">
          <div class="panel panel-default text-center">
          <div class="panel-body">
          <img src="{{ $one_card->image->url('thumbnail') }}" class="img-thumbnail" />
            <span class="input-group">
              <span class="input-group-addon">SORT</span>
              {{ Form::text("orderables[$one_card->id]", $one_card->sort, array('class'=>'form-control') ) }}
            </span>
            <div class="input-group">
               @include('administration.tags.machine', [ 'obj'=>$one_card,
                                                        'taggable_type'=>'RateCard',
                                                        'taggable_id'=>$one_card->id]) 

            </div>

            
        </div>
        <div class="panel-footer">

          <p style="padding-top:5px;">
            {{ Form::checkbox("deletablePhotos[]", $one_card->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
            <a href="javascript:void(0)" class="delete-existing btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
            <a href="{{ route('admin.businesses.show.photo', [$business->id, $one_card->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-crop"></i></a>
            <a href="{{ route('admin.businesses.rotate.photo', array($business->id, $one_card->id, 90) ) }}" class="btn btn-info btn-sm">
              <i class="fa fa-rotate-left"></i>
            </a>
            <a href="{{ route('admin.businesses.rotate.photo', array($business->id, $one_card->id, -90) ) }}" class="btn btn-info btn-sm">
              <i class="fa fa-rotate-right"></i>
            </a>
          </p>
        </div>
            </div>

        </div>
        
      @endforeach
    </div>
  </div>
</div>
        </div>
        <div class="panel-footer">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>


{{ Form::close() }}
<div id="rate_card_holder" style="display:none"><p>{{ Form::file('rate_card[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</p></div>
<div class="well">{{ Form::model($business, array('class' => 'form-horizontal', 'onsubmit'=>"return confirm('Are you sure?');", 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@fixRateCards', $business->id))) }}
  {{ Form::submit('Fix all Rate Cards', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}
</div>
        @include('administration.tags.machine-code')

<script>
$(function(){
  $(document).on('click', '.delete-existing', function(){
    $(this).parents('.content').first().hide();
    $(this).parents('.content').first().find('.deletable').prop('checked', true);
  });

  $('#lnk-add-rate-cards').click(function(){
    $('#rate-cards-here').prepend($('#rate_card_holder').html())
  });
});

</script>