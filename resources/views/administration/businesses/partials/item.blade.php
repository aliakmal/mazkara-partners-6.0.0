<tr   data-item="{{$item->id}}">
    <td >
      <input type="checkbox" name="items[]" value="{{$item->id}}" />
      {{ join(',', $item->services()->get()->lists('name', 'name')->all()) }}
    </td>
    <td >
      <a href="javascript:void(0)"  class="item-inline-editable" id="name" data-type="text" data-pk="{{$item->id}}" data-url="{{ route('admin.special.items.update') }}" data-title="Enter name">
        {{ $item->name }}
      </a>({{$item->state}})
    </td>
    <td >
      <a href="javascript:void(0)" class="item-inline-editable" id="description" data-type="text" data-pk="{{$item->id}}" data-url="{{ route('admin.special.items.update') }}" data-title="Description">
        {{ $item->description }}
      </a>
    </td>
    <td >
      <a href="javascript:void(0)" class="item-inline-editable" id="cost" data-type="text" data-pk="{{$item->id}}" data-url="{{ route('admin.special.items.update') }}" data-title="Price">
        {{ $item->cost }}
      </a>
    </td>
    <td >
      <a href="javascript:void(0)" class="item-inline-editable" id="cost_type" data-type="select" data-pk="{{$item->id}}" data-url="{{ route('admin.special.items.update') }}" data-title="Cost Type">
        {{ $item->cost_type }}
      </a>
    </td>
    <td >
      <a href="javascript:void(0)" class="item-inline-editable" id="duration" data-type="text" data-pk="{{$item->id}}" data-url="{{ route('admin.special.items.update') }}" data-title="Duration">
        {{ $item->duration }} 
      </a>
    </td>
    <td>
      <a href="javascript:void(0)" class="item-inline-editable" id="duration_type" data-type="select" data-pk="{{$item->id}}" data-url="{{ route('admin.special.items.update') }}" data-title="Duration Type">
        {{ $item->duration_type }}
      </a>
    </td>
    <td>
      {{ Form::open(array('style' => 'display: inline-block;', 
                          'method' => 'DELETE', 'class'=>'deletable-form', 
                          'route' => array('admin.service_items.destroy', $item->id))) }}
        {{ Form::submit('Delete', array('class' => 'deletable-form-btn btn btn-danger btn-xs', 'data-item'=>$item->id)) }}
    {{ Form::close() }}
    </td>    
</tr>
