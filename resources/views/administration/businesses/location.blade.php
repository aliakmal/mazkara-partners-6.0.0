@extends('layouts.admin')
@section('content')

<script src="https://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>
<div class="row">
    <div class="col-md-10 ">
        <h1>{{$business->name}} Location</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>


@include('administration.businesses.partials.locations')
{{ link_to_route('admin.businesses.show', 'Return', array($business->id), array('class' => 'btn btn-info btn-xs')) }}

@stop
