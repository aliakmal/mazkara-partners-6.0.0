@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-10 ">
        <h1>Crop {{$business->name}} Photo</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($business, 
                    array(  'class' => 'form-horizontal', 
                            'method' => 'POST', 'files'=>true, 
                            'action' => array('Administration\\BusinessesController@postPhoto',
                                            $business->id, $photo->id)
                            )) }}
<div class="form-group">
    <div class="row">
        <div class="col-md-10">
            <img id="crop-target" class="col-sm-12" src="{{ $photo->image->url()}}"  />
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
      {{ Form::input('hidden', 'x' , '',  array('id'=>'x' )) }}
      {{ Form::input('hidden', 'x2', '',  array('id'=>'x2')) }}
      {{ Form::input('hidden', 'y' , '',  array('id'=>'y' )) }}
      {{ Form::input('hidden', 'y2', '',  array('id'=>'y2')) }}
      {{ Form::input('hidden', 'w' , '',  array('id'=>'w' )) }}
      {{ Form::input('hidden', 'h' , '',  array('id'=>'h' )) }}

      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

<a href="{{ route('admin.businesses.rotate.photo', array($business->id, $photo->id, 90) ) }}" class="btn btn-info btn-xs">
<i class="fa fa-rotate-left"></i>
</a>

<a href="{{ route('admin.businesses.rotate.photo', array($business->id, $photo->id, -90) ) }}" class="btn btn-info btn-xs">
<i class="fa fa-rotate-right"></i>
</a>


{{ link_to_route('admin.businesses.show', 'Return', array($business->id), array('class' => 'btn btn-info btn-xs')) }}

<script type="text/javascript">
$(function(){
    $('#crop-target').Jcrop({
        onChange: showCoords,
        onSelect: showCoords,
        trueSize: ['<?php echo $image->width();?>', '<?php echo $image->height();?>']
    });

    function showCoords(c)
    {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#x2').val(c.x2);
        $('#y2').val(c.y2);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };    
});
</script>

@stop
