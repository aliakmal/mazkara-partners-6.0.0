@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-10 ">
        <h1>Crop {{$business->name}} Photo</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($business, 
                    array(  'class' => 'form-horizontal', 
                            'method' => 'POST', 'files'=>true, 
                            'action' => array('Administration\\BusinessesController@postCrop',
                                            $business->id, $photo->id)
                            )) }}
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            <div id="crop-target" style="width:1170px; height:450px;background:rgb(0,0,0) url({{ $photo->image->url('xlarge')}}); background-size:100%; background-repeat:no-repeat;" >
            </div>
</div>


        </div>

    </div>
  <div class="form-group">
<div class="row">
    <div class="col-sm-12">
      <div class="p10">
      <p><b>X:</b><span id="x-holder">{{$business->cover_x_pos}}</span>{{ Form::hidden('x' , $business->cover_x_pos,  array('id'=>'x' )) }}</p>
      <p><b>Y:</b><span id="y-holder">{{$business->cover_y_pos}}</span>{{ Form::hidden('y' , $business->cover_y_pos,  array('id'=>'y' )) }}</p>
    </div>

      <div class="well">
        {{ Form::submit('Save', array('class' => 'btn btn-sm btn-primary')) }}
        {{ link_to_route('admin.businesses.show', 'Return', array($business->id), array('class' => 'btn btn-info pull-right btn-sm')) }}
      </div>

    </div>
  </div>

</div>

{{ Form::close() }}




<script type="text/javascript">


$(function(){

$('#crop-target').backgroundDraggable(
  {axis: 'y',
  done: function() {
    backgroundPosition = $('#crop-target').css('background-position');
    pos = backgroundPosition.split(' ');
    $('#x-holder').html(pos[0]);
    $('#y-holder').html(pos[1]);
    $('#x').val(pos[0].replace('px', ''));
    $('#y').val(pos[1].replace('px', ''));
  }
});
});
</script>

@stop
