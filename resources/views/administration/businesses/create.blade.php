@extends('layouts.admin-content')
@section('content')
<script src="http://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Business</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>


@include('administration.businesses.partials.basic')
@stop
