@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Basic Details</h1>
        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>


@include('administration.businesses.partials.basic')

{{ link_to_route('admin.businesses.show', 'Return', array($business->id), array('class' => 'btn btn-info btn-xs')) }}

@stop
