@extends('layouts.admin')
@section('content')
  @if (Session::get('message'))
    <div class="alert alert-success">
      @if(is_array(Session::get('message')))
        @foreach(Session::get('message') as $ii=>$vv)
          <b>{{$ii}}</b> {{$vv}}<br/>
        @endforeach

      @else
        {{ (Session::get('message')) }}
      @endif
    </div>
  @endif
@include('administration.businesses.base-niblet', ['business'=>$business])

@if($business->isCurrentlyAllocatedAVirtualNumber() == true)
  <?php $cn = $business->current_virtual_number_allocation();?>
  <h2>Currently Allocated Number: {{$cn->virtual_number_text()}}</h2>
  @if($cn->state == 'active')
    <span class="label label-success">ACTIVE</span>
  @elseif($cn->state == 'hold')
    <span class="label label-warning">ON HOLD</span>
  @endif
@else
  <a href="{{route('admin.virtual_numbers.for.business.allocate', $business->id)}}" class="btn btn-warning">Allocate a Virtual Number</a>
@endif

<div class="row">
  <div class="col-md-12">
    <h2>Number(s) allocated</h2>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Virtual number</th>
          <th>Allocated To</th>
          <th>Notes</th>
          <th>Allocated on</th>
          <th>Deallocated on</th>
          <th>Current State</th>
          <th>Valid Until</th>
          <th>Is IVR Enabled</th>
         <th></th><th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($business->virtual_number_allocations()->orderby('id', 'DESC')->get() as $virtual_number_allocation)
          <tr>
            <td>{{{ $virtual_number_allocation->virtual_number_text() }}}</td>
            <td>{{{ join(', ', array_filter([$virtual_number_allocation->phone_1, $virtual_number_allocation->phone_2, $virtual_number_allocation->phone_3])) }}}</td>
            <td>{{{ $virtual_number_allocation->desc }}}</td>
            <td>{{{ $virtual_number_allocation->starts }}}</td>
            <td>{{{ $virtual_number_allocation->ends }}}</td>
            <td>
              {{ mzk_status_tag($virtual_number_allocation->state) }}
            </td>
            <td>                  
              <a href="javascript:void(0)" class="item-inline-editable" id="valid_until" data-type="date" data-pk="{{$virtual_number_allocation->id}}" data-url="{{ route('admin.virtual.allocations.item.update') }}" data-title="Edit Valid Until Date">{{ $virtual_number_allocation->valid_until}}</a>
            </td>
            <td>
              @if($virtual_number_allocation->isPPL())
                <span class="label label-danger">PPL</span>
              @endif
            </td>
            <td>
              @if($virtual_number_allocation->isIVREnabled())
                <span class="label label-success">IVR ENABLED</span>
              @else
                <span class="label label-danger">IVR DISABLED</span>
              @endif

            </td>
            <td>
              @if($virtual_number_allocation->isEditable())
              @endif
              @if($virtual_number_allocation->isDeallocatable())
                {{ link_to_route('admin.virtual_numbers.for.business.deallocate', 'Deallocate', array($virtual_number_allocation->id), array('class' => 'btn btn-xs btn-warning')) }}
              @endif

              {{ link_to_route('admin.call_logs.for.virtual_number_allocation', 'View Call Logs', array($virtual_number_allocation->id), array('class' => 'hidden btn btn-xs btn-info')) }}
            </td>
          </tr>

        @endforeach
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
$(function(){
  $('.item-inline-editable').editable();
});
</script>
@stop

