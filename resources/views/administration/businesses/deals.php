    <div role="tabpanel" class="tab-pane pt10" id="dpps">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Discounts / Packages / Promos</h3>
  </div>
  <div class="panel-body">
    <table class="table">
      @foreach($business->deals as $deal)
        <tr>
          <td>{{ $deal->artworks()->count() > 0  ? '<img src="'.$deal->artworks->first()->image->url('thumbnail').'" class="img-thumbnail" />' : '' }}</td>
          <td>{{ $deal->title }}</td>
          <td>
            @if($deal->type == 'Discount')
              <span class="label label-success">DISCOUNT</span>
            @elseif($deal->type == 'Promo')
              <span class="label label-warning">PROMO</span>
            @elseif($deal->type == 'Package')
              <span class="label label-info">PACKAGE</span>
            @endif
          </td>
          <td>
            @if(($deal->type == 'Discount') || ($deal->type == 'Promo'))
              {{ $deal->starts }} to
              {{ $deal->ends }}
            @endif
          </td>
          <td>
            {{ $deal->offer_amount }}
            @if($deal->type == 'Discount')
              (was {{ $deal->original_amount }})
            @endif
          </td>
          <td>
            @if($deal->status == 'active')
              <span class="label label-success">ACTIVE</span>
            @else
              <span class="label label-default">INACTIVE</span>
            @endif
          </td>
          <td>
            <a title="View Front End" href="{{ MazkaraHelper::slugDeal($deal->business, $deal) }}" class="btn btn-success btn-sm">
              <i class="fa fa-eye"></i>
            </a>
          </td>
          <td>
            {{ link_to_route('admin.businesses.edit.deal', 'Edit', array($business->id, strtolower($deal->type), $deal->id), array('class'=>'btn btn-default btn-sm'))  }}</td>
          <td>
            {{ Form::model($deal, array('class' => 'form-horizontal', 'onsubmit'=>"return confirm('Are you sure?');", 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@fixDealPhotos', $deal->id))) }}
              {{ Form::submit('Fix Photos', array('class' => 'btn btn-sm btn-primary')) }}
            {{ Form::close() }}
          </td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.businesses.delete.deal', $deal->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-sm')) }}
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </table>
  </div>
  <div class="panel-footer">
    {{ link_to_route('admin.businesses.new.deal', 'Add Discount', array($business->id, 'discount'), array('class'=>'btn btn-success')) }}
    {{ link_to_route('admin.businesses.new.deal', 'Add Promo', array($business->id, 'promo'), array('class'=>'btn btn-warning')) }}
    {{ link_to_route('admin.businesses.new.deal', 'Add Package', array($business->id, 'package'), array('class'=>'btn btn-info')) }}
  </div>  
</div>

    </div>
