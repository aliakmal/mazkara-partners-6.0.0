@extends('layouts.admin-content')
@section('content')

<h1>All Services</h1>

<p>{{ link_to_route('admin.services.create', 'Add New Service', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($services->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Name</th>
        <th>Slug</th>
				<th>Categories</th>
        <th>State </th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($services as $service)
				<tr>
          <td>{{{ $service->id }}}</td>
          <td>{{{ $service->stringPath() }}}</td>
          <td>{{{ $service->slug }}}</td>
					<td>{{{ join(',', $service->categories->lists('name','id')->all()) }}}</td>
          <td>{{$service->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>'}} </td>
          <td>
            <!--
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit'=>'return confirm(\'Delete Post? Are you sure?\');', 'route' => array('admin.services.destroy', $service->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}-->
            {{ link_to_route('admin.services.edit', 'Edit', array($service->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no services
@endif

@stop
