@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    
    <div class="pull-right">
      <br/>
      <a class="btn btn-success" href="{{ route('admin.adverts.create', array('campaign_id'=>$advert_campaign->id)) }}">Add Ad to Campaign</a>
      {{ link_to_route('admin.advert_campaigns.edit', 'Edit', array($advert_campaign->id), array('class' => 'btn btn-info')) }}
      {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?');", 'method' => 'DELETE', 'route' => array('admin.advert_campaigns.destroy', $advert_campaign->id))) }}
        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
      {{ Form::close() }}
    </div>
    <h1>Campaign {{'#'.$advert_campaign->id}}</h1>
    <hr/>
    {{ $advert_campaign->start_date }} to {{ $advert_campaign->end_date }}
  </div>
</div>
<div class="row">
<div class="col-md-12">

  <table class="table table-striped">
    <thead>
      <tr>
        <!--<th>ID</th>-->
        <th>Venues</th>
        <th>Zones</th>
        <th>Services</th>
        <th>Slots</th>
        <th>Starts</th>
        <th>Ends</th>
        <th>Remarks</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    @foreach($adverts as $advert)
      <tr>
        <!-- <td>{{ $advert->id }}</td> -->
        <td>{{ $venues[$advert->business_id] }}</td>
        <td>
          <?php 
          $zone_ids = explode(',', $advert->zone_ids);
          $zone_ids = array_unique($zone_ids);
          ?>
          @foreach($zone_ids as $zone_id)
            {{ $zones[$zone_id] }}, 
          @endforeach
        </td>
        <td>
          <?php $service_ids = explode(',', $advert->service_ids);
          $service_ids = array_unique($service_ids);
          ?>
          @foreach($service_ids as $service_id)
            {{ $services[$service_id] }},
          @endforeach
        </td>
        <td>{{ $advert->slot }}</td>
        <td>
          <a href="javascript:void(0)" class="item-inline-editable" 
            id="start_date" data-type="date" 
            data-pk="{{$advert->id}}" data-url="{{ route('admin.adverts.updatable') }}" 
            data-title="Edit Start Date">{{ $advert->start_date}}</a>
        </td>
        <td><a href="javascript:void(0)" class="item-inline-editable" 
            id="end_date" data-type="date" 
            data-pk="{{$advert->id}}" data-url="{{ route('admin.adverts.updatable') }}" 
            data-title="Edit End Date">{{ $advert->end_date}}</a></td>
        <td>{{ $advert->description }}</td>
        <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm("Are you sure you want to delete this advert?")', 'method' => 'DELETE', 'route' => array('admin.adverts.destroy', $advert->ids))) }}
          <!--<input type="hidden" name="ids" value="{{ $advert->ids }}" />-->
          <button class="btn btn-danger" style="border:0px;"> Delete </button>
        {{ Form::close() }}

        </td>
      </tr>
    @endforeach
    </tbody>

  </table>
</div>
</div>
<p><a href="{{ route('admin.advert_campaigns.index') }}" class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Back</a></p>
<script type="text/javascript">
$(function(){
  $('.item-inline-editable').editable({placement:'bottom'});
});
</script>
@stop