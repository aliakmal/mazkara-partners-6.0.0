@if(isset($advert))
  {{ Form::model($advert, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.adverts.update', $advert->id))) }}
@else
  {{ Form::open(array('route' => 'admin.adverts.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif




  {{ Form::hidden('campaign_id', $advert_campaign->id) }}

  <div class="form-group">
    {{ Form::label('start_date', 'Start date:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('start_date', Input::old('start_date'), array('class'=>'dateinput form-control', 'placeholder'=>'Start date')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('end_date', 'End date:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('end_date', Input::old('end_date'), array('class'=>'dateinput form-control', 'placeholder'=>'End date')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('business_id', 'Venue:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('business_id', $businesses, Input::old('business_id'), array('class'=>'form-control', 'placeholder'=>'Venues?')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('service_ids', 'Services:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('service_ids[]', $services, Input::old('service_id'), array('multiple'=>'multiple','class'=>'form-control', 'id'=>'services-selector', 'data-live-search'=>"true")) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('zone_ids', 'Zone(s):', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('zone_ids[]', $zones, Input::old('zone_id'), array('multiple'=>'multiple', 'id'=>'zones-selector', 'class'=>'form-control', 'data-live-search'=>"true")) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('slot', 'Slot:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('slot', [], array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($advert))
        {{ link_to_route('admin.adverts.show', 'Cancel', $advert->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>


  </div>
{{ Form::close() }}
<script type="text/javascript">
  var populateSlots = function(){
    service_ids = $('#services-selector').val();
    zone_ids = $('#zones-selector').val();
    start_date = $('#start_date').val();
    end_date = $('#end_date').val();
    exclude = 0;

    if((service_ids == '') || (zone_ids == '') || (start_date == '') || (end_date == '')){
      var options = $('#slot');
      options.html('');
      return;
    }

    $.ajax(
            {
              url:'/crm/adverts/get/avalable/slots', 
              method: 'GET',
              data:{ 
                service_ids:service_ids, exclude:exclude,
                zone_ids:zone_ids,start_date:start_date,
                end_date:end_date                     }
            })
      .done(function(data){
        var options = $('#slot');
        options.html('');

        for(i in data){
          options.append($("<option />").val(i).text(data[i]));
        }
      });

  };

  $('#services-selector').selectpicker({
    style: 'btn-default',
    size: 8
  }).on('changed.bs.select', function(){
    populateSlots();
  });    


  $('#zones-selector').selectpicker({
    style: 'btn-default',
    size: 8
  }).on('changed.bs.select', function(){
    populateSlots();
  });
</script>
<script type="text/javascript">
$(function(){
    $('input.dateinput').datepicker({ format: "yyyy-mm-dd", 
                                      autoclose:true, 
                                      startDate: '{{ $advert_campaign->start_date }}', 
                                      endDate: '{{ $advert_campaign->end_date }}' });
})
</script>