@extends('layouts.scaffold')

@section('main')

<h1>Show Invoice_item</h1>

<p>{{ link_to_route('invoice_items.index', 'Return to All invoice_items', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Desc</th>
				<th>Price</th>
				<th>Qty</th>
				<th>Total</th>
				<th>Invoice_id</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $invoice_item->desc }}}</td>
					<td>{{{ $invoice_item->price }}}</td>
					<td>{{{ $invoice_item->qty }}}</td>
					<td>{{{ $invoice_item->total }}}</td>
					<td>{{{ $invoice_item->invoice_id }}}</td>
					<td>{{{ $invoice_item->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('invoice_items.destroy', $invoice_item->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('invoice_items.edit', 'Edit', array($invoice_item->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
