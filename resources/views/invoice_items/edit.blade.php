@extends('layouts.scaffold')

@section('main')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Invoice_item</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($invoice_item, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('invoice_items.update', $invoice_item->id))) }}

        <div class="form-group">
            {{ Form::label('desc', 'Desc:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('desc', Input::old('desc'), array('class'=>'form-control', 'placeholder'=>'Desc')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('price', 'Price:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('price', Input::old('price'), array('class'=>'form-control', 'placeholder'=>'Price')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('qty', 'Qty:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::input('number', 'qty', Input::old('qty'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('total', 'Total:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('total', Input::old('total'), array('class'=>'form-control', 'placeholder'=>'Total')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('invoice_id', 'Invoice_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::input('number', 'invoice_id', Input::old('invoice_id'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('state', 'State:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('state', Input::old('state'), array('class'=>'form-control', 'placeholder'=>'State')) }}
            </div>
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('invoice_items.show', 'Cancel', $invoice_item->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop