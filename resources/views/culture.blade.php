@extends('layouts.page')
@section('content')
<h1 class="text-center notransform">Life @ Fabogo</h1>
<p>&nbsp;</p>
<section class="hide-only-mobile" style="background-size:35%;background-image:url('{{mzk_assets('assets/culture/bg.png')}}'); background-repeat:no-repeat; background-position:center center;">
  <div class="container">
    <div class="row">
      <div class="col-md-10  col-md-offset-1">
        <div class="row">
          <div class="col-md-4">
            <div class="text-center " style="margin-left:50px; padding-top:160px; padding-bottom:35px;">
              <p><img width="100" src="{{mzk_assets('assets/culture/no-text/stocked-pantry.png')}}" /></p>
              <h3>Stocked Pantry</h3>
              <p>Free snacks, so you’re high on energy and an endless supply of chai & coffee!</p>
            </div>
            <div class="text-center">
              <p><img width="100" src="{{mzk_assets('assets/culture/no-text/learn-hustle.png')}}" /></p>
              <h3>Learn to Hustle</h3>
              <p>You'll take your work to the next level, become a game-changer and get shit done</p>
            </div>
            <div class="text-center " style="margin-left:50px;padding-top:40px;">
              <p><img width="115" src="{{mzk_assets('assets/culture/no-text/exponential-growth.png')}}" /></p>
              <h3>Exponential Growth</h3>
              <p>Create something super exciting, and be a part of our supersonic growth story</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="text-center">
              <p><img width="115" src="{{mzk_assets('assets/culture/no-text/fun-times.png')}}" /></p>
              <h3>Fun Times</h3>
              <p>Post work chilling and office parties, cuz we encourage a healthy work-life balance</p>
            </div>
            <div class="text-center " style="padding-top:180px; font-weight:bold; font-size:200%;">
                Why<br/>Work With<br/>Us
            </div>
          </div>
          <div class="col-md-4">
            <div class="text-center " style="margin-right:50px;padding-bottom:30px; margin-top:100px;">
              <p><img width="132" src="{{mzk_assets('assets/culture/no-text/salary-perks.png')}}" /></p>
              <h3>Competitive Salary & Killer Incentives</h3>
              <p>Your perks and pockets are as important to us too, you know</p>
            </div>
            <div class="text-center">
              <p><img width="100" src="{{mzk_assets('assets/culture/no-text/free-grooming.png')}}" /></p>
              <h3>Free Grooming</h3>
              <p>Complimentary Salon & Spa services for everyone, cuz you gotta look, feel, and be fab</p>
            </div>
            <div class="text-center "  style="margin-right:50px;padding-top:60px;">
              <p><img width="120" src="{{mzk_assets('assets/culture/no-text/change-world.png')}}" /></p>
              <h3>Change the World</h3>
              <p>One bad haircut at a time</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<p>&nbsp;</p>
<p>&nbsp;</p>
<section class="show-only-mobile">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="text-center " style="font-weight:bold; font-size:200%;">
            Why Work With Us
        </div>
        <div class="text-center ">
          <p><img width="100" src="{{mzk_assets('assets/culture/no-text/stocked-pantry.png')}}" /></p>
          <h3>Stocked Pantry</h3>
          <p>Free snacks, so you’re high on energy and an endless supply of chai & coffee!</p>
        </div>
          <div class="text-center">
            <p><img width="100" src="{{mzk_assets('assets/culture/no-text/learn-hustle.png')}}" /></p>
            <h3>Learn to Hustle</h3>
            <p>You'll take your work to the next level, become a game-changer and get shit done</p>
          </div>
          <div class="text-center ">
            <p><img width="115" src="{{mzk_assets('assets/culture/no-text/exponential-growth.png')}}" /></p>
            <h3>Exponential Growth</h3>
            <p>Create something super exciting, and be a part of our supersonic growth story</p>
          </div>
          <div class="text-center">
            <p><img width="115" src="{{mzk_assets('assets/culture/no-text/fun-times.png')}}" /></p>
            <h3>Fun Times</h3>
            <p>Post work chilling and office parties, cuz we encourage a healthy work-life balance</p>
          </div>
          <div class="text-center">
            <p><img width="132" src="{{mzk_assets('assets/culture/no-text/salary-perks.png')}}" /></p>
            <h3>Competitive Salary & Killer Incentives</h3>
            <p>Your perks and pockets are as important to us too, you know</p>
          </div>
          <div class="text-center">
            <p><img width="100" src="{{mzk_assets('assets/culture/no-text/free-grooming.png')}}" /></p>
            <h3>Free Grooming</h3>
            <p>Complimentary Salon & Spa services for everyone, cuz you gotta look, feel, and be fab</p>
          </div>
          <div class="text-center">
            <p><img width="120" src="{{mzk_assets('assets/culture/no-text/change-world.png')}}" /></p>
            <h3>Change the World</h3>
            <p>One bad haircut at a time</p>
          </div>
        </div>
      </div>
  </div>
</section>


<script type="text/javascript">
$(function(){

    $(function () {
      $('[data-toggle="popover"]').popover({trigger:'hover', placement:'left'});
    })    
})
</script>

@stop
