@extends('layouts.master-open')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
<h1 class=" notransform pt20 pb20">{{ ucwords($city) }} Locations({{ $letter }})</h1>
<div class="row " style="padding-right:25px;">
  @foreach($locations as $location)
    @foreach($categories as $category)
    <div class="col-md-4">
      <div class="row mb10">
        <a href="{{ MazkaraHelper::slugCityZone($location, ['category'=>[$category->id]])}}">{{ str_plural($category->name) }} in {{ $location->name }}</a>
      </div>
    </div>
    @endforeach
  @endforeach
</div>
<div class="row">
  <div class="col-md-12 pt10 pb10 text-right mt10 mb10">
    {{ $locations->appends(['letter'=>$letter])->render()}}

  </div>
</div>
    </div>
</div>
</div>
@stop
