@extends('layouts.master-open')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
<h1 class=" notransform pt20 pb20">{{ ucwords($city) }} Venues({{ $letter }})</h1>
<div class="row " style="padding-right:25px;">
  @foreach($venues as $venue)
    <div class="col-md-4">
      <div class="row mb10">
        <div class="col-md-9 col-xs-9">
          <a href="{{ MazkaraHelper::slugSingle($venue)}}">{{ $venue->name }}</a>
          <p class="mb0">{{ $venue->zone_cache }}&nbsp;</p>
        </div>
        <div class="col-md-3 col-xs-3 text-right fs90">
          <a href="{{ MazkaraHelper::slugSinglePhotos($venue)}}" >Photos</a><br/>
          <a href="{{ MazkaraHelper::slugSingleRateCards($venue)}}">Rate Cards</a>
        </div>
      </div>
    </div>
  @endforeach
</div>
<div class="row">
  <div class="col-md-12 pt10 pb10 text-right mt10 mb10">
    {{ $venues->appends(['letter'=>$letter])->render()}}

  </div>
</div>
    </div>
</div>
</div>
@stop
