<!DOCTYPE html>
<html lang="en">
  <head>
  @include('elements.head')
  </head>
  <body>
    @include('elements.post-body')
    @include('elements.nav-alt')


<div class="container">
    <div class="row">
        <div class="col-md-3 col-md-offset-2">
            <img src="{{mzk_assets('assets/cry.png')}}" />
        </div>
        <div class="col-md-5 ">
            <h1 class="text-center  notransform">OMG! OMG! OMG!</h1>

<p class="lead">We're so sorry you have to see this. Its possible this is a broken link or you clicked on a link thats not really working - so you won't find anything here.</p>

<p class="lead">But don't worry - you're still fabulous and we know it. There's loads of amazing places on Fabogo to make you look and feel super stunning.</p>

<p class="lead">Just click the big button below to go back to our home page - xoxo</p>            
<p class="text-center"><a href="/" class="btn btn-lite-blue btn-lg">Back Home</a></p>
        </div>
    </div>
</div>
    <p class="p15"></p>
    @include('elements.footer')
  </body>
</html>
