
@if (Auth::check())
  <li>
    <a target="_self" href="{{{ URL::to('users/'.Auth::user()->id.'/profile') }}}">
    {{ViewHelper::userAvatar(Auth::user(), ViewHelper::$avatar25)}}
    </a>
  </li>

  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" >
      <i class="fa fa-bars fw300 fs175"></i>
    </a>
    <ul class="dropdown-menu pt0 pb0" role="menu">
        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('finance') || Auth::user()->hasRole('sales-admin') || Auth::user()->hasRole('moderator') )
          <li class="bb "><a class="p5" target="_self" href="{{{ URL::to('/admin') }}}"><i class="fa w20 fa-cog"></i> Admin Panel</a></li>
        @endif
      <li class="bb ">
        <a target="_self" href="{{{ URL::to('users/'.Auth::user()->id.'/profile') }}}"  class="p5">
          <i class="fa fa-user w20"></i> Profile
        </a>
      </li>
<!--  <li>
        <a href="{{{ URL::to('users/vouchers') }}}">
          <i class="fa fa-money"></i> My Vouchers
        </a>
      </li>
    -->
      <li class="bb ">
        <a target="_self" href="{{{ URL::to('users/'.Auth::user()->id.'/profile#reviews') }}}"  class="p5">
          <i class="fa fa-check-square-o w20"></i> Reviews
        </a>
      </li>
      <li class="bb ">
        <a target="_self" href="{{{ URL::to('users/edit') }}}"  class="p5">
          <i class="fa fa-pencil w20"></i> Edit Profile
        </a>
      </li>
      <li>
        <a target="_self" href="{{{ URL::to('users/logout') }}}"  class="p5">
          <i class="fa fa-lock w20"></i> Logout</a>
      </li>
    </ul>
  </li>
@else
  <li class="mt15">
    <a target="_self" style="font-size:13px;" href="/users/facebook/login" class="page-scroll fw900 uppercase dark-gray facebook-login-link">Login with Facebook</a></li>
  <li class="mt15">
    <span style="display:none;"><img src="https://s3.amazonaws.com/mazkaracdn/assets/bg-login-screen.jpg"/></span>
    <a target="_self" href="/users/create" id="ajax-login-link" style="font-size:13px;padding-right:0px;" 
        class="page-scroll   uppercase fw900  ajax-popup-link ml10 mr15">
        Login</a></li>
@endif
