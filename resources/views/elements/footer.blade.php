<?php
$all_zones = ViewHelper::zonesComboAsSelectableArray();
//dump(ViewHelper::zonesComboAsArray());
?>
@if(in_array(env('APP_ENV'), ['production']))
  <script src="https://www.gstatic.com/firebasejs/3.3.2/firebase.js"></script>
  <script>
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyAKPbRA7kQnMPa23qWZHuL7Sg4W8e_5IYs",
      authDomain: "mazkara-web-app-90214.firebaseapp.com",
      databaseURL: "https://mazkara-web-app-90214.firebaseio.com",
      storageBucket: "mazkara-web-app-90214.appspot.com",
      messagingSenderId: "209861490613"
    };
    firebase.initializeApp(config);
  </script>



<!--<script src="https://www.gstatic.com/firebasejs/3.1.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAKPbRA7kQnMPa23qWZHuL7Sg4W8e_5IYs",
    authDomain: "mazkara-web-app-90214.firebaseapp.com",
    databaseURL: "https://mazkara-web-app-90214.firebaseio.com",
    storageBucket: "mazkara-web-app-90214.appspot.com",
  };
  firebase.initializeApp(config);
</script>-->

  <script type="text/javascript">
   var __uzdbm_a = "<?php //global $shieldsquare_response;echo $shieldsquare_response->pid?>";
  </script>
  <div id="ss_098786_234239_238479_190541"></div>
  <script async = "true" type="text/javascript" src="https://cdn.perfdrive.com/static/jscall_min.js"></script>
@endif
<div id="full-screen-overlay"></div>
<nav id="footer" class="container mt0">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-4">
        <div class="row">
          <div class="col-md-3">
            <div class="text-left  fnav">
              <nav id="footer  " style="padding-top:0px">
                <a target="_self" href="/?{{ _apfabtrack('footer') }}" class="dpb show-ib-only-mobile hover-color-turquoise p5 pt0 pb10 page-scroll">HOME</a>
                <a target="_self" href="/about?{{ _apfabtrack('footer') }}" class="dpb show-ib-only-mobile hover-color-turquoise p5 pt0 pb10 page-scroll">ABOUT US</a>
                <!-- <a target="_self" href="https://blog.fabogo.com?{{ _apfabtrack('footer') }}" class="dpb show-ib-only-mobile hidden pb10 hover-color-turquoise p5 pt0 page-scroll ">BLOG</a>-->
                <a target="_self" href="/jobs?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile hover-color-turquoise pb10 p5 pt0 dpb">WE'RE HIRING!</a>
                <a target="_self" href="/add-your-business?{{ _apfabtrack('footer') }}" class="dpb show-ib-only-mobile hover-color-turquoise pb10 p5 pt0 page-scroll ">ADD YOUR BUSINESS</a>
                <a target="_self" href="/contact?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile hover-color-turquoise p5 pb10 pt0 dpb">CONTACT US</a>
              </nav>
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="btn-group p5 pt0  ">
              <a class="btn btn-default no-border-radius   dropdown-toggle city" data-toggle="dropdown" href="#" style="background-color:transparent;color:white;">
                {{ strtoupper(MazkaraHelper::getLocale()) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span>
              </a>
              <ul class="dropdown-menu dropdown-user no-border-radius" style="background-color:#000;">
                @foreach(['dubai'=>'Dubai, U.A.E', 'sharjah'=>'Sharjah, U.A.E','abu-dhabi'=>'Abu Dhabi, U.A.E', 'pune'=>'Pune, India', 'mumbai'=>'Mumbai, India'] as $c=>$city)
                <li><a class="hover-color-turquoise" target="_self" href="{{ route('home.change.locale', [MazkaraHelper::getLocale(), $c]) }}?{{ _apfabtrack('footer') }}">
                  {{ strtoupper($city) }}
                </a></li>
                @endforeach
              </ul>
            </div>
            <a target="_self" href="/tos?{{ _apfabtrack('footer') }}" class="page-scroll dpb show-ib-only-mobile hover-color-turquoise p5 pb10 pt0">TERMS OF SERVICE</a>
            <a target="_self" href="/privacy-policy?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile hover-color-turquoise pb10 p5 pt0 dpb">PRIVACY POLICY</a>
            <a target="_self" href="/cookie-policy?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile hover-color-turquoise pb10 p5 pt0 dpb">COOKIE POLICY</a>
            <a target="_self" href="/faq?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile hover-color-turquoise p5 pt0 pb10 dpb">FAQs</a>
            <a target="_self" href="/abu-dhabi/directory?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile dpb hover-color-turquoise p5 pb10 pt0 uppercase">Abu Dhabi Directory</a>
            <a target="_self" href="/dubai/directory?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile dpb hover-color-turquoise p5 pb10 pt0 uppercase">Dubai Directory</a>
            <a target="_self" href="/sharjah/directory?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile dpb hover-color-turquoise p5 pb10 pt0 uppercase">Sharjah Directory</a>
            <a target="_self" href="/pune/directory?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile dpb hover-color-turquoise p5 pb10 pt0 uppercase">Pune Directory</a>
            <a target="_self" href="/mumbai/directory?{{ _apfabtrack('footer') }}" class="page-scroll show-ib-only-mobile dpb hover-color-turquoise p5 pb10 pt0 uppercase">Mumbai Directory</a>
          </div>
          <div class="col-md-3 ">
            <p>
              <a target="_blank" href="https://itunes.apple.com/us/app/id1086643190">
                <img width="100%" src="{{mzk_assets('assets/home/apple-store.png')}}" />
              </a>
            </p><br/>
            <p>  
              <a target="_blank" href="https://play.google.com/store/apps/details?id=com.mazkara.user">
                <img width="100%" src="{{mzk_assets('assets/home/google-store.png')}}" />
              </a>
            </p>
            <div class="p10 m10">
              <small>&copy; MAZKARA FZ. LLC. </small>
            </div>

          </div>
          <div class="col-md-3 ">
          <p class="dpb show-ib-only-mobile pb5"><a target="_blank" href="https://www.facebook.com/gofabogo"><i class="fa fa-facebook"></i></a>&nbsp;</p>
          <p class="dpb show-ib-only-mobile pb5"><a target="_blank" href="https://twitter.com/gofabogo"><i class="fa fa-twitter"></i></a>&nbsp;</p>
          <p class="dpb show-ib-only-mobile pb5"><a target="_blank" href="https://instagram.com/gofabogo"><i class="fa fa-instagram"></i></a>&nbsp;</p>
          <p class="dpb show-ib-only-mobile pb5"><a target="_blank" href="https://www.linkedin.com/company/fabogo?"><i class="fa fa-linkedin"></i></a>&nbsp;</p>
          <p class="dpb show-ib-only-mobile "><a target="_blank" rel="publisher" href="https://plus.google.com/110768350761848887136"><i class="fa fa-google-plus"></i></a>&nbsp;</p>
          


          </div>

        </div>
      </div>
    </div>
  </div>
</nav>
@include('elements.popup-download-app')
<script type="text/javascript">
  var _MAZKARA_CITIES_ = {{ json_encode($all_zones) }};
  var _MAZKARA_CATEGORIES_ = {{ json_encode(MazkaraHelper::getCategoriesList())}};
  var __MZK_CITIES__ = {{ json_encode(ViewHelper::getCitiesArray()); }};
  function getSearchURL(){
    return 'http://fabogosandbox-dev.ap-northeast-1.elasticbeanstalk.com/api/quickFormSearch';

    return 'http://127.0.0.1:8000/api/quickFormSearch';
  }

  function getSelectedZone(){
    zone_id = $('#location-selector-name').val();
    if(zone_id == ''){
      zone_id = <?php echo MazkaraHelper::getLocaleID();?>;
    }

    return zone_id
  }


</script>
@if(isset($javascript_include))
  {{ mzk_js_tag($javascript_include, true) }}
@else
  {{ mzk_js_tag('application', true) }}
@endif

<script type="text/javascript">
// var mazkara_application = angular.module('mazkaraApp', [], function($interpolateProvider) {
//     $interpolateProvider.startSymbol('<{');
//     $interpolateProvider.endSymbol('}>');
//   }).controller('NavigationController', ['$scope', '$location', function($scope, $location){
//     $scope.goToUrl = function(url){
//       $location.path(url);
//     };
//   }]);

</script>
<script type="text/javascript" src="/js/base.js"></script>
<script type="text/javascript" src="https://s3.amazonaws.com/mazkaracdn/js/form-validator/jquery.form-validator.min.js"></script>

@section('js')
@show
@section('js-review')
@show

@include('elements.fb')
{{ mzk_js_tag('application.footer', true) }}
