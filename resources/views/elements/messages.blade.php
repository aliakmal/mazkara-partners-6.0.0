@if (Session::get('messages'))
  <div class="alert alert-error alert-danger">
    {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
  </div>
@endif
@if (Session::get('notice'))
  <div class="alert alert-success">
    {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
  </div>
@endif
@if (Session::get('warning'))
  <div class="alert alert-danger">
    {{{ is_array(Session::get('warning'))?join(',', Session::get('warning')):Session::get('warning') }}}
  </div>
@endif

    @if (Session::get('status'))
        <div class="alert alert-success">{{{ Session::get('status') }}}</div>
    @endif

@if (isset($errors) && is_array($errors))
  @if (count($errors) > 0)
      <div class="alert alert-error alert-danger">
        <ul>
          @foreach ($errors as $error)
              <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
  @endif
@endif