<script language="JavaScript">

window.fbAsyncInit = function() {
  //SDK loaded, initialize it
  FB.init({
    appId      : '{{ Config::get("services.facebook.client_id", "FBID") }}',
    xfbml      : true,
    cookie     : true,
    version    : 'v2.5'
  });

  //check user session and refresh it
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      //user is authorized
    } else {
      //user is not authorized
    }
  });
};

//load the JavaScript SDK
(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//add event listener to login button



$(function(){
  $(document).on('click','.facebook-login-link', function(){

    //do the login
    FB.login(function(response) {
      if (response.authResponse) {
        //user just authorized your app
        if(response.status == 'connected'){
          $.ajax({
            url: '/users/facebook/auth',
            method:'POST',
            data: { fbObject:response.authResponse }
          }).success(function(r){
            //bootbox.alert("Facebook login successful - the page will reload. Please wait.");
            window.location.href = window.location.href;
            window.location.reload();
          })
        }else{
          bootbox.alert({
            size: 'small',
            message: "Facebook login failed - something went wrong :(. Please try again a bit later!"        
          });
        }
      }
    }, {scope: '{{ join(",", Config::get("services.facebook.scope", "FB")) }}', return_scopes: true});
    return false;
  });

});
</script>