<?php $services = MazkaraHelper::getServicesHierarchyList();?>

@foreach($services as $ii=>$service)
  <li class="dropdown yamm-fw parent" style="">
    <a class="dropdown-toggle main-menu-btn fw900 {{ $ii > 1 ? '':'m---l-15' }} text-center" data-animations="fadeInDown" data-toggle="dropdown"  href="#">
    {{ strtoupper($service['name']) }}
    </a>
    @if(count($service['children']) >0)
    <ul class="dropdown-menu bg-lite-gray" role="menu" style="min-height: 308px">
      <li class="container">
        <div class="yamm-content">
          <div class=""> 
            <div class="row"> 
              <div class="col-sm-2 hide-only-mobile">
                <img class="ba" style="position: absolute;top:-20px;height:296px;left:-25px;" alt="{{ mzk_slug_to_words($service['slug']) }}" src="{{mzk_assets('assets/menu-'.$service['slug'].'.jpg')}}"  />
              </div>
              <div class="col-sm-10">
                <div class="row">
                  @foreach(array_chunk($service['children'], 8) as $childs)
                    <div class="col-md-4">
                      <ul class="list-unstyled">
                    @foreach($childs as $ix=>$child)
                      <li>
                        <?php $url = MazkaraHelper::slugCity(null, ['service'=>[$child['id']]]);?>
                        <a ng-click="venuesCtrl.updateService({id:<?php echo $child['id'];?>, name:'<?php echo $child['name'];?>', slug:'<?php echo $child['slug'];?>'})" class="fw900  m5 p5  btn no-border-radius fs80" ng-href="{{ $url }}">
                          {{ $child['name'] }}
                        </a>
                      </li>
                    @endforeach
                    </ul></div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </li>

      </ul>
    @endif
  </li>
@endforeach

<li class="yamm-fw dropdown no-border parent">
    <a class="  fire main-menu-btn fw900 "  href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}">
      FAB FINDS
    </a>

</li>
<li class="yamm-fw no-border dropdown parent">
    <a class="   main-menu-btn fw900  turquoise-on-hover"  href="{{ route('posts.index.all')}}">
      STORIES
    </a>

</li>

