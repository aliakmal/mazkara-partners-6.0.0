<div class="hidden">
  <div  id="download-app" class="mfp-hide m10 p25 text-center">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="bg-lite-gray">
          <div id="tf-menu" class="pb10 pr0 pl10" style="padding-right:0px;" >
            <a class="navbar-brand" style="background-position:left;" href="/"></a>
          </div>
        </div>
        <div class="bg-popup  text-center p25">
          <p class="italic serif force-white">
            To know more about other beauty and wellness tips and venues. 
          </p>
          <p class="fs175 force-white fw600"><b>Get the Fabogo app</b></p>
          <img src="{{ mzk_assets('assets/phone-pop-up.png')}}" />
          <div class="p10">
            <a target="_blank" href="https://itunes.apple.com/us/app/id1086643190">
              <img height="50" src="{{mzk_assets('assets/home/apple-store.png')}}" />
            </a>
            &nbsp;&nbsp;
            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.mazkara.user">
              <img height="50" src="{{mzk_assets('assets/home/google-store.png')}}" />
            </a>
          </div>
          <div class="p10">
            <a href="javascript:void(0)" onclick="$.magnificPopup.close();" class="serif force-white italic p10">No thanks.</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
