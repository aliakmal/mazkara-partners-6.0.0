<?php 
  $owner = $activity->meta['owner'];
  $business = $activity->meta['business'];
?>
<div class="text">
  <i class="glyphicon glyphicon-picture"></i>
  {{ $owner->name }} 
  uploaded {{ $business->photos_count }} photos and deleted {{ $business->deleted_photos }} photos
  to
  <a href="{{ route('admin.businesses.show', [$business->id]) }}">{{ $business->name }}, {{ $business->zone_cache }}</a> 
</div>

