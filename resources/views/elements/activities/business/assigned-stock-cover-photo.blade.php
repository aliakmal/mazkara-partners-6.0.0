<?php 
  $owner = $activity->meta['owner'];
  $business = $activity->meta['business'];
?>
<div class="text">
  <i class="glyphicon glyphicon-picture"></i>
  {{ $owner->name }} allocated a stock photo to <a href="{{ route('admin.businesses.show', [$business->id]) }}">{{ $business->name }}, {{ $business->zone_cache }}</a> <br/>
  <img src="{{ mzk_assets('assets/stock/'.$business->stock_photo.'.jpg') }}" class="img-thumbnail" style="height:60px;" />

</div>

