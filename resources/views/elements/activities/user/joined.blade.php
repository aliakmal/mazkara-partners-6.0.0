<?php 
  $user = $activity->user;
  $business = $activity->itemable;
?>
<div class="bbb mb10 pb5">
<div class="media pb5  ">
    <div class="pull-right gray">
      <small>
        <i class="fa fa-user"></i> JOINED
      </small>
    </div>
  <div class="media-left">

    <a href="/users/{{$user->id}}/profile" >
      {{ViewHelper::userAvatar($user)}}
    </a>
  </div>
  <div class="media-body">
    <b class="media-heading   pt5">
      <a title="{{{ $user->full_name }}}" href="/users/{{$user->id}}/profile" class=" result-title">
        {{{ ViewHelper::formatUsername($user) }}} 
      </a>
    </b>
      joined Fabogo! <b>Welcome aboard!</b>
    <br/>
    <small class="gray">{{{ Date::parse($activity->updated_at)->ago() }}}</small>

  </div>
</div>

</div>
