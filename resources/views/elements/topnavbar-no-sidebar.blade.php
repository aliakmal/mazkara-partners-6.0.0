<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle b0 fs125" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/admin">Fabogo</a>
  </div>
  <!-- /.navbar-header -->


  <ul class="nav navbar-top-links navbar-right">
    @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('sales-admin') || Auth::user()->hasRole('sales'))
    <li>
      <a href="/admin/dashboard">
        White Board
      </a>
    </li>
    @endif

    <!-- /.dropdown -->
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user">
        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
        </li>
        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
        </li>
        <li class="divider"></li>
        <li><a href="/admin/users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
      </ul>
      <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
  </ul>
    <!-- /.navbar-top-links -->
  <ul class="nav navbar-top-links navbar-right">
    <!-- /.dropdown -->
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        {{ MazkaraHelper::getAdminDefaultLocaleLabel() }} <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user">
        @if(Auth::user()->hasRole('admin'))
          @foreach(MazkaraHelper::getCitiesList() as $city)
            <li>
              <a href="/admin/set-locale/{{ $city['slug'] }}">
                {{ $city['name'] }}
              </a>
            </li>
          @endforeach
        @else
          @foreach(Auth::user()->zones()->get() as $city)
            <li>
              <a href="/admin/set-locale/{{ $city->slug }}">
                {{ $city->name }}
              </a>
            </li>
          @endforeach
        @endif
      </ul>
      <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
  </ul>


</nav>