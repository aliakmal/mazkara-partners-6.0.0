<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')


  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        @if(Auth::user()->can("manage_listings"))

          <li>
            <a href="{{ route('admin.businesses.index') }}">Business</a>
          </li>
          <li>
            <a href="{{ route('admin.chains.index') }}">Chains</a>
          </li>

          <li>
            <a href="{{ route('admin.groups.index') }}">Groups</a>
          </li>
          <li>
            <a href="{{ route('admin.brands.index') }}">Product Brands</a>
          </li>
          
        @endif

        @if(Auth::user()->can("manage_highlights"))
          <li>
            <a href="{{ route('admin.highlights.index') }}">Highlights</a>
          </li>
        @endif
        @if(Auth::user()->can("manage_zones"))
          <li>
            <a href="{{ route('admin.zones.index') }}">Zones</a>
          </li>
        @endif
        @if(Auth::user()->can("manage_categories"))
          <li>
            <a href="{{ route('admin.categories.index') }}">Categories</a>
          </li>
          <li>
            <a href="{{ route('admin.services.index') }}">Services</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.index') }}">Community Posts</a>
          </li>
        @endif
        @if(Auth::user()->can("manage_users"))
          <li>
            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Administration<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="{{ route('admin.jobs.index') }}">Jobs</a>
              </li>

            </ul>
            <!-- /.nav-second-level -->
          </li>
        @endif
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>