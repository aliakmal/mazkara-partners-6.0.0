<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')


  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        @if(Auth::user()->can('manage_merchants'))
          <li>
            <a href="{{ route('admin.merchants.index') }}">Merchants</a>
          </li>
          <li>
            <a href="{{ route('admin.merchants.global') }}">Global Merchants</a>
          </li>
        @endif
        
        <li>
          <a href="{{ route('admin.invoices.index') }}">Invoices</a>
        </li>
        <li>
          <a href="{{ route('admin.payments.index') }}">Payments</a>
        </li>
        @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('finance'))
        <li>
          <a href="{{ route('finance.expenses') }}">Expenses</a>
        </li>
        <li>
          <a href="{{ route('finance.reports') }}">Income Reports</a>
        </li>
        <li>
          <a href="{{ route('finance.billings.users') }}">Billing Reports(POC)</a>
        </li>
        <li>
          <a href="{{ route('finance.billings.cities') }}">Billing Reports(City)</a>
        </li>

        @endif
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>