
<header role="banner" class="navbar">

    <div class="container">
      <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
        </button>
        <a class="navbar-brand navbar-brand-img" style="padding-top:5px;" href="/partner">
          <img src="{{mzk_assets('assets/clients-logo-fabogo.png')}}" />
        </a>
      </div> <!-- /.navbar-header -->

      <nav role="navigation" class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-left">
          <!-- /.dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              Manage - {{ mzk_client_get_default_business_name() }} <i class="fa fa-caret-down"></i> 
            </a>
            @if(Auth::user()->hasRole('client'))
            <ul class="dropdown-menu dropdown-user">
              @foreach(Auth::user()->businesses() as $business)
                <li>
                  <a href="/partner/{{$business->id}}">{{$business->name}}, {{$business->zone_cache}}</a>
                </li>
              @endforeach
            </ul>
            @endif
            <!-- /.dropdown-user -->
          </li>
          <li>
            <a class="" style="padding-left:0px;margin-left:0px;color:#fff" target="_blank" href="{{MazkaraHelper::slugSingle(Business::find(mzk_client_bid()))}}"><small>Jump <i class="fa "></i></small></a>
          </li>
          <!-- /.dropdown -->
        </ul>


  <ul class="nav navbar-nav navbar-right">
    <!-- <li >
      <a href="{{ route('client.voucher.validate.form', [mzk_client_bid()] ) }}" style="padding:5px 10px; margin-top:10px;" class="ajax-popup-link btn btn-default btn-sm">VALIDATE VOUCHER</a>
    </li> -->
    <!-- /.dropdown -->
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user">
        <li><a href="/"><i class="fa fa-user fa-fw"></i> Back to Fabogo</a>
        </li>
        <li class="divider"></li>
        <li><a href="/partner/users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
      </ul>
      <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
  </ul>


      </nav>

    </div> <!-- /.container -->

  </header>

<div class="mainnav ">

    <div class="container">

      <a data-target=".mainnav-collapse" data-toggle="collapse" class="mainnav-toggle">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-bars"></i>
      </a>

      <nav role="navigation" class="collapse mainnav-collapse">


        <ul class="mainnav-menu">
          <li class="dropdown @if(Route::is('client.home') ) active @endif ">
            <a href="/partner/{{ mzk_client_bid() }}" > 
              <i class="fa fa-dashboard fa-fw"></i> Analytics 
            </a>
          </li>
          @if(!Auth::user()->hasRole('client'))
          <li class="dropdown  {{ strstr(Request::url(), '/offers') ? 'active' : '' }} ">
            <a href="/partner/{{ mzk_client_bid() }}/offers">
              <i class="fa fa-star fa-fw"></i> Specials & Deals
            </a>
          </li>
          @endif
          <li class="dropdown  {{ strstr(Request::url(), '/reviews') ? 'active' : '' }}">
            <a href="/partner/{{ mzk_client_bid() }}/reviews" ><i class="fa fa-comments fa-fw"></i> Manage Review(s)</a>
          </li>
          <li class="dropdown   {{ strstr(Request::url(), '/call_logs') ? 'active' : '' }}">
            <a href="/partner/{{ mzk_client_bid() }}/call_logs" ><i class="fa fa-phone fa-fw"></i> Call Logs</a>
          </li>
          <?php 
          $show_leads = false;
          $business = mzk_client_get_default_business_object();

          //if(Auth::user()->hasRole('admin')){
          //  $show_leads = true;
          //}else
          {
            // if(!Auth::user()->hasRole('client')){
            //  $show_leads = false;
            // }else{
              if($business->canAccessLeadsReport()){
                $show_leads = true;
              }else{
                $show_leads = false;
              }
            //}
          }
          ?>

          @if($show_leads == true)
          <li class="dropdown   {{ strstr(Request::url(), '/leads') ? 'active' : '' }}">
            <a href="/partner/{{ mzk_client_bid() }}/leads" ><i class="fa fa-group fa-fw"></i> FabREACH</a>
          </li>

          @endif

                    <?php 
          $show_resumes = false;
          $business = mzk_client_get_default_business_object();

          //if(Auth::user()->hasRole('admin')){
          //  $show_resumes = true;
          //}else
          {
            // if(!Auth::user()->hasRole('client')){
            //  $show_resumes = false;
            // }else{
              if($business->canAccessResumesReport()){
                $show_resumes = true;
              }else{
                $show_resumes = false;
              }
            //}
          }
          ?>

          @if($show_resumes == true)
            <li class="dropdown   {{ strstr(Request::url(), '/resumes') ? 'active' : '' }}">
              <a href="/partner/{{ mzk_client_bid() }}/resumes" ><i class="fa fa-file fa-fw"></i> FabHIRE</a>
            </li>
          @endif

            @if(!Auth::user()->hasRole('client'))
            <!--
            <li class="dropdown">
              <a data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                <i class="fa fa-pencil fa-fw"></i> Edit Salon Profile <i class="mainnav-caret"></i>
              </a>
              <ul role="menu" class="dropdown-menu">
              @if(!Auth::user()->hasRole('client'))
                <li>
                  <a href="/partner/photos">
                  <i class="fa fa-image dropdown-icon "></i> 
                  Manage Photos
                  </a>
                </li>
                <li>
                  <a href="/partner/rate-cards">
                  <i class="fa fa-money dropdown-icon "></i> 
                  Manage Rate Cards
                  </a>
                </li>
                @endif
                <li>
                  <a href="/partner/change-request">
                  <i class="fa fa-cogs dropdown-icon "></i> 
                  Change Request
                  </a>
                </li>
              </ul>
            </li> -->
            @endif



        </ul>

      </nav>

    </div> <!-- /.container -->

  </div>




<script type="text/javascript">
  $(function(){
    $('.coming-soon').click(function(){
      alert('Coming soon');return false;
    });
  })
</script>