<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')


  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
          <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        @if(Auth::user()->can("manage_categories"))
          <li>
            <a href="{{ route('admin.posts.index') }}">All Contributor Posts</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.drafts') }}">Drafts({{Post::ofStates('draft')->count()}})</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.pending') }}">Pending({{Post::ofStates('publish-for-review')->count()}})</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.published') }}">Published({{Post::ofStates('published')->count()}})</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.rejects') }}">Rejected({{Post::ofStates('reject')->count()}})</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.archives') }}">Archives({{Post::ofStates('archive')->count()}})</a>
          </li>
          <li>
            <a href="{{ route('admin.posts.all') }}">ALL</a>
          </li>

        @endif
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>