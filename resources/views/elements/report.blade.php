{{ BootForm::openHorizontal([
  'sm' => [4, 8],
  'lg' => [2, 10]
])->post()->action('/report')->encodingType('multipart/form-data') }}{{ 
  BootForm::text('Full name', 'name')->placeholder('Full Name')->required() }}{{ 
  BootForm::email('Email', 'email')->placeholder('Email address')->required() }}{{ 
  BootForm::textarea('Message', 'message')->placeholder('What is wrong')->required() }}{{ 
  BootForm::hidden('url', '')->value( Request::url() ) }}{{ 
  BootForm::submit('Send Report') }}{{ 
  BootForm::close() }}