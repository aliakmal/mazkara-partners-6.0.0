<div class="navbar-header">
  @if (User::loggedIn())
    <a class=" mr5 navbar-toggle hide-only-mobile collapsed" style="border:0px;vertical-align:middle; padding:5px 0px" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        {{ViewHelper::userAvatar(Auth::user(), ViewHelper::$avatar25)}}
    </a>
  @else
    <button type="button" class="pt6 pb7 mr5 hide-only-mobile border-radius-5  navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="fa fa-user "></span>
    </button>
  @endif
  <button type="button" class="navbar-toggle hide-only-mobile border-radius-5  collapsed" data-toggle="collapse" data-target="#bs-main-navbar-collapse-1">
    <span class="sr-only">Toggle main menu</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <span class="show-only-mobile">
    <a href="javascript:void(0)" style="margin-top:15px;" class="btn hidden show-only-mobile lnk-run-on-app  btn-turquoise btn-xs pull-right mr5  btn dpib show-only-mobile btn-default ">
      <b>Download App</b>
    </a>
  </span>

</div>
