<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

  'facebook' => [
    // 'client_id' => '1610477315910881',
    // 'client_secret' => '460e017d8f2e9f3c52dca412c4a71f22',
    // 'scope' => array( 'email'),

    'client_id' => '1572896792985537',
    'client_secret' => '97de785cefdcfb130cfd1e7d46ca06e2',
    'scope' => array( 'user_friends', 'email', 'user_about_me', 'publish_actions'),
    'redirect' => env('APP_URL').'/users/facebook/auth'
  ],

  'google' => [
    'client_id'     => '209861490613-6db4ls8d5nv5uoc1g0p8212v7gfrhord.apps.googleusercontent.com',
    'client_secret' => 'Un5GLgsvLC0agv9_s7s3CMJ1',
    'scope'         => array('userinfo_email', 'userinfo_profile'),
    'redirect' => env('APP_URL').'/users/google/auth'
  ],


];
