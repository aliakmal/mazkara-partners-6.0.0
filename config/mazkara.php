<?php

return array(
  // unique slugs which are generated using an alternative to the generic slug format
  // 'city-slug'=>'function to generate slug'
  'city-region-slugs'=>['ncr'=>'slugWithCityRegion'],
);