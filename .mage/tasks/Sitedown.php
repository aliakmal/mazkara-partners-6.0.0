<?php
namespace Task;

use Mage\Task\AbstractTask;

class Sitedown extends AbstractTask
{
  public function getName(){
    return 'Put the site into maintenance mode';
  }

  public function run(){
    $env = $this->getParameter('env', 'dev'); 
    if($env == 'dev'){
      $command = "ln -s /var/www/demo.mazkara.com/current /var/www/site.down";
    }elseif($env == 'production'){
      $command = "ln -s /var/www/mazkara.com/current /var/www/site.down";
    }

    $result = $this->runCommandRemote($command);

    return $result;
  }
}