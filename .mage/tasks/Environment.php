<?php
namespace Task;

use Mage\Task\AbstractTask;

class Environment extends AbstractTask
{
  public function getName(){
    return 'Fixing file permissions';
  }

  public function run(){
    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'demo'=>'demo.fabogo.com',
                      'production'=>'fabogo.com',
                      'cron'=>'cron.mazkara.com',
                      'test'=>'test.mazkara.com'
                    );
    $folder = $folders[$env];
    if(in_array($env, ['cron', 'test', 'demo', 'production'])){
      echo "Copying the environment variables from .env.".$env.' to .env ... ';
      $command = 'rm /var/www/'.$folder.'/current/.env; cp /var/www/'.$folder.'/current/.env.'.$env.' /var/www/'.$folder.'/current/.env;';
    }else{
      echo "Copying the environment variables from .env.".$env.' to .env ... ';
      $command = 'rm /data/sites/www/'.$folder.'/current/.env; cp /data/sites/www/'.$folder.'/current/.env.'.$env.' /data/sites/www/'.$folder.'/current/.env;';
    }    
    $result = $this->runCommandRemote($command);

    return $result;
  }
}