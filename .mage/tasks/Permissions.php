<?php
namespace Task;

use Mage\Task\AbstractTask;

class Permissions extends AbstractTask
{
  public function getName(){
    return 'Fixing file permissions';
  }

  public function run(){
    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'demo'=>'demo.fabogo.com',
                      'production'=>'fabogo.com',
                      'cron'=>'cron.mazkara.com',
                      'test'=>'test.mazkara.com'
                    );
    $folder = $folders[$env];
    if(in_array($env, ['cron', 'test', 'demo', 'production'])){
      echo "Fixing the file permissions on /var/www/".$folder."/current/app ...";
      $command = 'cd /var/www/'.$folder.'/current; sudo chmod 777 storage/ -R; sudo chmod 777 bootstrap/cache/ -R;';

    }else{
      echo "Fixing the file permissions on /data/sites/www/".$folder."/current/app ...";
      $command = 'cd /data/sites/www/'.$folder.'/current; sudo chmod 777 storage/ -R; sudo chmod 777 bootstrap/cache/ -R;';
    }
    $result = $this->runCommandRemote($command);

    return $result;
  }
}