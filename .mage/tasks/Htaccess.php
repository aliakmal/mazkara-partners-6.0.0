<?php
namespace Task;

use Mage\Task\AbstractTask;

class Htaccess extends AbstractTask
{
  public function getName(){
    return 'Setting HTACCESS';
  }

  public function run(){
    $env = $this->getParameter('env', 'production');
    $folders = array(
                     'demo'       =>  'demo.fabogo.com',
                     'production' =>  'fabogo.com'
                    );
    $command = '';

    if(isset($folders[$env])){
      $folder = $folders[$env];
      echo "Copying the environment variables from .htaccess.".$env.' to .htaccess ... ';
      $folder = '/var/www/'.$folder.'/current';
      $command = 'rm '.$folder.'/public/.htaccess; cp '.$folder.'/.htaccess.'.$env.' '.$folder.'/public/.htaccess;';
      echo "Copying the environment variables from .htaccess.".$env.' to .htaccess ... ';

    }

    $result = $this->runCommandRemote($command);

    return $result;
  }
}