<?php
namespace Task;

use Mage\Task\AbstractTask;

class Clearcache extends AbstractTask
{
  public function getName(){
    return 'Clearing Views and Cache';
  }

  public function run(){
    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'demo'=>'demo.fabogo.com',
                      'production'=>'fabogo.com',
                      'cron'=>'cron.mazkara.com',
                      'test'=>'test.mazkara.com'
                    );
    $folder = $folders[$env];

    if(in_array($env, ['cron', 'test', 'demo', 'production'])){
      echo 'Clearing cache on '.$folder.' in '.$env.' environment ... ';
      $command = 'cd /var/www/'.$folder.'/current; sudo php artisan cache:clear; php artisan mazkara:generate.json;';

    }else{
      echo 'Clearing cache on '.$folder.' in '.$env.' environment ... ';
      $command = 'cd /data/sites/www/'.$folder.'/current; sudo php artisan cache:clear; php artisan mazkara:generate.json;';
    }

    $result = $this->runCommandRemote($command);
    
    //\Session::forget('mazkara.city');

    return $result;
  }
}