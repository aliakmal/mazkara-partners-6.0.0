<?php
use Service;
class BusinessTest extends TestCase {
  public function setUp(){
    parent::setUp();
  }

  public function test_business_has_multiple_services(){
    $business = $this->getBusinessObject();
    $this->pp($this->getServiceObject());

    $business->services()->sync();
    
    $this->assertEquals(count($business->services), 3);
  }

  public function test_when_updating_services_the_service_count_attr_should_reflect_number_of_services(){

  }

  public function getBusinessObject(){
    $business = Fakefactory::overrideAttributes(['phone' => ['1111111'], 'email'=>['a11@d11.co']])->create('Business');
    return $business;
  }

  public function getServiceObject(){
    $service = new Service();
    return $service;
  }
}

